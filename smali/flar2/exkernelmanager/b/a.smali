.class public Lflar2/exkernelmanager/b/a;
.super Landroid/support/v7/widget/cc;


# instance fields
.field a:Lflar2/exkernelmanager/b/c;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/cc;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cz;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lflar2/exkernelmanager/b/a;->c(Landroid/view/ViewGroup;I)Lflar2/exkernelmanager/b/d;

    move-result-object v0

    return-object v0
.end method

.method public a(ILflar2/exkernelmanager/b/f;)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/b/a;->d(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lflar2/exkernelmanager/b/a;->c()V

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/cz;I)V
    .locals 0

    check-cast p1, Lflar2/exkernelmanager/b/d;

    invoke-virtual {p0, p1, p2}, Lflar2/exkernelmanager/b/a;->a(Lflar2/exkernelmanager/b/d;I)V

    return-void
.end method

.method public a(Lflar2/exkernelmanager/b/c;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/b/a;->a:Lflar2/exkernelmanager/b/c;

    return-void
.end method

.method public a(Lflar2/exkernelmanager/b/d;I)V
    .locals 2

    iget-object v1, p1, Lflar2/exkernelmanager/b/d;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/b/f;

    invoke-virtual {v0}, Lflar2/exkernelmanager/b/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lflar2/exkernelmanager/b/d;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/b/f;

    invoke-virtual {v0}, Lflar2/exkernelmanager/b/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lflar2/exkernelmanager/b/b;

    invoke-direct {v0, p0, p1}, Lflar2/exkernelmanager/b/b;-><init>(Lflar2/exkernelmanager/b/a;Lflar2/exkernelmanager/b/d;)V

    invoke-virtual {p1, v0}, Lflar2/exkernelmanager/b/d;->a(Lflar2/exkernelmanager/b/e;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-virtual {p0}, Lflar2/exkernelmanager/b/a;->c()V

    return-void
.end method

.method public c(Landroid/view/ViewGroup;I)Lflar2/exkernelmanager/b/d;
    .locals 3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/b/d;

    invoke-direct {v1, v0}, Lflar2/exkernelmanager/b/d;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public f(I)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/b/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/b/a;->e(I)V

    return-void
.end method
