.class public final Lflar2/exkernelmanager/r;
.super Ljava/lang/Object;


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field public static final g:[Ljava/lang/String;

.field public static final h:[Ljava/lang/String;

.field public static final i:[Ljava/lang/String;

.field public static final j:[Ljava/lang/String;

.field public static final k:[Ljava/lang/String;

.field public static final l:[Ljava/lang/String;

.field public static final m:[Ljava/lang/String;

.field public static final n:[Ljava/lang/String;

.field public static final o:[Ljava/lang/String;

.field public static final p:[Ljava/lang/String;

.field public static final q:[Ljava/lang/String;

.field public static final r:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpufreq/ondemand/gboost"

    aput-object v1, v0, v3

    const-string v1, "/sys/devices/system/cpu/cpufreq/elementalx/gboost"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/dev/block/platform/msm_sdcc.1/by-name/boot"

    aput-object v1, v0, v3

    const-string v1, "/dev/block/platform/sdhci-tegra.3/by-name/LNX"

    aput-object v1, v0, v4

    const-string v1, "/dev/block/platform/f9824900.sdhci/by-name/boot"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->b:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "/sys/class/thermal/thermal_zone0/temp"

    aput-object v1, v0, v3

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/cpu_temp"

    aput-object v1, v0, v4

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/FakeShmoo_cpu_temp"

    aput-object v1, v0, v5

    const-string v1, "/sys/class/thermal/thermal_zone1/temp"

    aput-object v1, v0, v6

    const-string v1, "/sys/class/i2c-adapter/i2c-4/4-004c/temperature"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/temperature"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "/sys/devices/platform/tegra_tmon/temp1_input"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "/sys/kernel/debug/tegra_thermal/temp_tj"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "/sys/devices/platform/s5p-tmu/temperature"

    aput-object v2, v0, v1

    sput-object v0, Lflar2/exkernelmanager/r;->c:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/gpuclk"

    aput-object v1, v0, v3

    const-string v1, "/sys/class/kgsl/kgsl-3d0/gpuclk"

    aput-object v1, v0, v4

    const-string v1, "/sys/kernel/tegra_gpu/gpu_rate"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->d:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/max_gpuclk"

    aput-object v1, v0, v3

    const-string v1, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    aput-object v1, v0, v4

    const-string v1, "/sys/kernel/tegra_gpu/gpu_cap_rate"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/min_pwrlevel"

    aput-object v1, v0, v3

    const-string v1, "/sys/class/kgsl/kgsl-3d0/min_pwrlevel"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/gpu_available_frequencies"

    aput-object v1, v0, v3

    const-string v1, "/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies"

    aput-object v1, v0, v4

    const-string v1, "/sys/kernel/tegra_gpu/gpu_available_rates"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/pwrscale/trustzone/governor"

    aput-object v1, v0, v3

    const-string v1, "/sys/class/kgsl/kgsl-3d0/pwrscale/trustzone/governor"

    aput-object v1, v0, v4

    const-string v1, "/sys/class/kgsl/kgsl-3d0/devfreq/governor"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "/sys/module/lm3630_bl/parameters/backlight_dimmer"

    aput-object v1, v0, v3

    const-string v1, "/sys/module/msm_fb/parameters/backlight_dimmer"

    aput-object v1, v0, v4

    const-string v1, "/sys/module/mdss_dsi/parameters/backlight_dimmer"

    aput-object v1, v0, v5

    const-string v1, "/sys/backlight_dimmer/backlight_dimmer"

    aput-object v1, v0, v6

    sput-object v0, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    aput-object v1, v0, v3

    const-string v1, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    aput-object v1, v0, v4

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/vdd_levels"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "/sys/class/timed_output/vibrator/amp"

    aput-object v1, v0, v3

    const-string v1, "/sys/class/timed_output/vibrator/voltage_level"

    aput-object v1, v0, v4

    const-string v1, "/sys/drv2605/rtp_strength"

    aput-object v1, v0, v5

    sput-object v0, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/module/hall_sensor/parameters/disable_cover"

    aput-object v1, v0, v3

    const-string v1, "/sys/module/lid/parameters/enable_lid"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/android_touch/sweep2wake"

    aput-object v1, v0, v3

    const-string v1, "/sys/android_key/sweep2wake"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "/sys/android_touch/doubletap2wake"

    aput-object v1, v0, v3

    const-string v1, "/sys/android_key/doubletap2wake"

    aput-object v1, v0, v4

    const-string v1, "/sys/devices/platform/spi-tegra114.2/spi_master/spi2/spi2.0/input/input0/wake_gesture"

    aput-object v1, v0, v5

    const-string v1, "/sys/devices/f9966000.i2c/i2c-1/1-004a/tsp"

    aput-object v1, v0, v6

    const-string v1, "/sys/devices/virtual/input/lge_touch/dt_wake_enabled"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "/sys/module/lge_touch_core/parameters/doubletap_to_wake"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "/sys/devices/virtual/input/lge_touch/touch_gesture"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "/proc/touchpanel/double_tap_enable"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "/sys/devices/virtual/input/input1/wakeup_gesture"

    aput-object v2, v0, v1

    sput-object v0, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/android_touch/pocket_detect"

    aput-object v1, v0, v3

    const-string v1, "/sys/android_key/pocket_detect"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->o:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kcal_ctrl.0/kcal_ctrl"

    aput-object v1, v0, v3

    const-string v1, "/sys/devices/platform/DIAG0.0/power_rail_ctrl"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->p:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/devices/platform/kcal_ctrl.0/kcal"

    aput-object v1, v0, v3

    const-string v1, "/sys/devices/platform/DIAG0.0/power_rail"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/sys/module/msm_thermal/parameters/limit_temp_degC"

    aput-object v1, v0, v3

    const-string v1, "/sys/module/msm_thermal/parameters/temp_threshold"

    aput-object v1, v0, v4

    sput-object v0, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    return-void
.end method
