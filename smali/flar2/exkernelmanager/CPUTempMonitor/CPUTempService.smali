.class public Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;
.super Landroid/app/Service;


# instance fields
.field private a:Landroid/app/NotificationManager;

.field private b:Lflar2/exkernelmanager/utilities/m;

.field private c:Landroid/os/Handler;

.field private d:Landroid/content/BroadcastReceiver;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private final g:I

.field private h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/16 v0, 0x1194

    iput v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->g:I

    new-instance v0, Lflar2/exkernelmanager/CPUTempMonitor/a;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/CPUTempMonitor/a;-><init>(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)V

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->h:Ljava/lang/Runnable;

    return-void
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const v0, 0x7f0e00e5

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->f:Ljava/lang/String;

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide v2, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x20

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b0F"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->f:Ljava/lang/String;

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b0C"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p0}, Landroid/support/v4/app/az;->a(Landroid/content/Context;)Landroid/support/v4/app/az;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/az;->a(Ljava/lang/Class;)Landroid/support/v4/app/az;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/az;->a(Landroid/content/Intent;)Landroid/support/v4/app/az;

    invoke-virtual {p0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0e0112

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f020088

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a:Landroid/app/NotificationManager;

    const/16 v2, 0x2a

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method private b()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/CPUTempMonitor/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/CPUTempMonitor/b;-><init>(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;Lflar2/exkernelmanager/CPUTempMonitor/a;)V

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->d:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->c:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Nexus 6"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "/sys/class/thermal/thermal_zone6/temp"

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->e:Ljava/lang/String;

    :goto_0
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a:Landroid/app/NotificationManager;

    invoke-direct {p0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->b()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->c:Landroid/os/Handler;

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->c:Landroid/os/Handler;

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    sget-object v1, Lflar2/exkernelmanager/r;->c:[Ljava/lang/String;

    aget-object v0, v1, v0

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->d:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->c:Landroid/os/Handler;

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->a:Landroid/app/NotificationManager;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "tempUnit"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->f:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "prefTempUnit"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->f:Ljava/lang/String;

    goto :goto_0
.end method
