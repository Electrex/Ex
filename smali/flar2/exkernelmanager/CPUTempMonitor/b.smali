.class Lflar2/exkernelmanager/CPUTempMonitor/b;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/CPUTempMonitor/b;->a:Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;Lflar2/exkernelmanager/CPUTempMonitor/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/CPUTempMonitor/b;-><init>(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/b;->a:Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-static {v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->c(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/b;->a:Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-static {v1}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->b(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/CPUTempMonitor/b;->a:Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-static {v0}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->c(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/CPUTempMonitor/b;->a:Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-static {v1}, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;->b(Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
