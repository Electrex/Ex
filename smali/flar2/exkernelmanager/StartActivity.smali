.class public Lflar2/exkernelmanager/StartActivity;
.super Landroid/app/Activity;

# interfaces
.implements Lflar2/exkernelmanager/x;


# static fields
.field public static b:Z

.field private static final c:[B


# instance fields
.field a:Landroid/content/Context;

.field private d:Lcom/b/a/a/a/a;

.field private e:Lcom/b/a/a/a/e;

.field private f:Lcom/b/a/a/a/n;

.field private g:Landroid/app/ProgressDialog;

.field private h:Landroid/widget/TextView;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lflar2/exkernelmanager/StartActivity;->c:[B

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/StartActivity;->b:Z

    return-void

    nop

    :array_0
    .array-data 1
        -0x54t
        0x5dt
        0x37t
        -0x10t
        -0x27t
        0x63t
        -0x43t
        -0x41t
        -0x22t
        0x1bt
        0x2et
        0x3ft
        -0x63t
        0x29t
        0x2ct
        -0xdt
        0x4dt
        -0x33t
        0x1bt
        -0x4bt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/StartActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lflar2/exkernelmanager/StartActivity;->i:Z

    return v0
.end method

.method static synthetic b(Lflar2/exkernelmanager/StartActivity;)Lcom/b/a/a/a/n;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->f:Lcom/b/a/a/a/n;

    return-object v0
.end method

.method static synthetic c(Lflar2/exkernelmanager/StartActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->l()V

    return-void
.end method

.method private d()V
    .locals 3

    new-instance v0, Lflar2/exkernelmanager/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/ac;-><init>(Lflar2/exkernelmanager/StartActivity;Lflar2/exkernelmanager/y;)V

    iput-object v0, p0, Lflar2/exkernelmanager/StartActivity;->e:Lcom/b/a/a/a/e;

    new-instance v0, Lcom/b/a/a/a/a;

    iget-object v1, p0, Lflar2/exkernelmanager/StartActivity;->f:Lcom/b/a/a/a/n;

    const-string v2, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm5DGkWq4RC8dQJwEUZTOlWoCPaztDE759fAQmHLFLq8JTXWO4V+lEVdjGdMe8YtOIpEZHJ133SuEiSvCt9YQtJU06sbpjkzSUKFnvLYyv1dPV/HmEn7NOzlYGgD7QjcAuSE4bW5FFI8pa4/KYruDtcZA1UFKiNM2wEYz0b3TSg3cTr1BAivY6jLld9pP1lDRZw0RYJj378oVo2IYuBvVzI+IF0FlrAxylYKqz0c6AQol874y20y21X3J7zg5YoZm2ec+uU2sMntbGczorwLiHYvnL8lFAeTNWixw8KGeTh6YW8+q3PXw3nsx8jcLSvgfuxSp5djp+g9Bf9cx6pE0hQIDAQAB"

    invoke-direct {v0, p0, v1, v2}, Lcom/b/a/a/a/a;-><init>(Landroid/content/Context;Lcom/b/a/a/a/t;Ljava/lang/String;)V

    iput-object v0, p0, Lflar2/exkernelmanager/StartActivity;->d:Lcom/b/a/a/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->h:Landroid/widget/TextView;

    const v1, 0x7f0e0043

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->e()V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/StartActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->f()V

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->d:Lcom/b/a/a/a/a;

    iget-object v1, p0, Lflar2/exkernelmanager/StartActivity;->e:Lcom/b/a/a/a/e;

    invoke-virtual {v0, v1}, Lcom/b/a/a/a/a;->a(Lcom/b/a/a/a/e;)V

    return-void
.end method

.method static synthetic e(Lflar2/exkernelmanager/StartActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->e()V

    return-void
.end method

.method private f()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e00c0

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f020092

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(I)Landroid/support/v7/app/ac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00c1

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003c

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/aa;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/aa;-><init>(Lflar2/exkernelmanager/StartActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0101

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/z;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/z;-><init>(Lflar2/exkernelmanager/StartActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0045

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/y;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/y;-><init>(Lflar2/exkernelmanager/StartActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private g()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->h:Landroid/widget/TextView;

    const v1, 0x7f0e0132

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lflar2/exkernelmanager/utilities/d;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/d;-><init>()V

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/d;->a()Ljava/lang/String;

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v1, "prefFirstRun"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflar2/exkernelmanager/StartActivity;->i:Z

    iget-boolean v0, p0, Lflar2/exkernelmanager/StartActivity;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "prefFirstRun"

    invoke-static {v0, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefTutorial"

    invoke-static {v0, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefFirstRunMonitor"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_0
    new-instance v0, Lflar2/exkernelmanager/DailyListener;

    invoke-direct {v0}, Lflar2/exkernelmanager/DailyListener;-><init>()V

    invoke-static {v0, p0, v4}, Lcom/commonsware/cwac/wakeful/a;->a(Lcom/commonsware/cwac/wakeful/b;Landroid/content/Context;Z)V

    const-string v0, "prefSubVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Sense"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "flar2.exkernelmanager.powersave.PowersaveWidgetProvider"

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->k()V

    const-string v0, "prefTempUnit"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "prefTempUnit"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v0, "prefCheckForUpdates"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "prefCheckForUpdates"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_3
    const-string v0, "prefAutoReboot"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "prefAutoReboot"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_4
    const-string v0, "prefInstaller"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "prefInstaller"

    const-string v1, "express"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v0, "prefHotplug"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    const-string v1, "busybox pidof mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "prefHotplug"

    const-string v1, "mpdecision"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_0
    const-string v0, "prefBMSpinnerSelection"

    invoke-static {v0, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefDimmerPS"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "prefDimmerPS"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_7
    const-string v0, "prefVibOptPS"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "prefVibOptPS"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_8
    const-string v0, "prefWakePS"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "prefWakePS"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_9
    const-string v0, "prefCPUMaxPrefPS"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Nexus6"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "prefCPUMaxPrefPS"

    const-string v1, "1497600"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_1
    const-string v0, "prefGPUMaxPrefPS"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "prefGPUMaxPrefPS"

    const-string v1, "320000000"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const-string v0, "prefCPUGovPF"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "prefCPUGovPF"

    const-string v1, "ondemand"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const-string v0, "prefAppVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "prefAppVersion"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    :cond_d
    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->i()V

    sget-boolean v0, Lflar2/exkernelmanager/MainApp;->b:Z

    if-eqz v0, :cond_e

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->j()V

    :cond_e
    invoke-static {}, Lflar2/exkernelmanager/utilities/k;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->l()V

    :goto_2
    return-void

    :cond_f
    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    const-string v1, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "prefHotplug"

    const-string v1, "custom"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const-string v0, "prefHotplug"

    const-string v1, "disabled"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const-string v0, "prefCPUMaxPrefPS"

    const-string v1, "1300000"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_12
    iget-boolean v0, p0, Lflar2/exkernelmanager/StartActivity;->i:Z

    if-nez v0, :cond_13

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->l()V

    goto :goto_2

    :cond_13
    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->d()V

    goto :goto_2
.end method

.method private h()Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->f:Lcom/b/a/a/a/n;

    invoke-virtual {v0}, Lcom/b/a/a/a/n;->b()Z

    move-result v0

    return v0
.end method

.method private i()V
    .locals 4

    const/4 v1, 0x4

    const-string v0, "prefAppVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v1, :cond_0

    const-string v0, "prefAppVersion"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->j()V

    const-string v0, "prefBMStartMarker"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 8

    const/4 v4, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->a:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflar2/exkernelmanager/StartActivity;->a:Landroid/content/Context;

    const-class v3, Lflar2/exkernelmanager/PowerService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lflar2/exkernelmanager/StartActivity;->a:Landroid/content/Context;

    invoke-static {v2, v4, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/32 v6, 0x895440

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private k()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "prefPowersaver"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefPowersaver"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefPowersaverNotify"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_0
    const-string v0, "prefPerformance"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefPerformance"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefPerformanceNotify"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method private l()V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lflar2/exkernelmanager/StartActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->showDialog(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/StartActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflar2/exkernelmanager/StartActivity;->a:Landroid/content/Context;

    const-class v2, Lflar2/exkernelmanager/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/StartActivity;->startActivity(Landroid/content/Intent;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_2

    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/StartActivity;->overridePendingTransition(II)V

    :cond_2
    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->o()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/StartActivity;->finish()V

    goto :goto_1
.end method

.method private m()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0076

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f020092

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(I)Landroid/support/v7/app/ac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0077

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/ab;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/ab;-><init>(Lflar2/exkernelmanager/StartActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private n()V
    .locals 1

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/StartActivity;->setRequestedOrientation(I)V

    return-void
.end method

.method private o()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/StartActivity;->setRequestedOrientation(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->h:Landroid/widget/TextView;

    const v1, 0x7f0e0018

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->n()V

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    sget-boolean v0, Lflar2/exkernelmanager/StartActivity;->b:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->m()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/StartActivity;->g()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/StartActivity;->setContentView(I)V

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/StartActivity;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lflar2/exkernelmanager/StartActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/b/a/a/a/n;

    new-instance v2, Lcom/b/a/a/a/p;

    sget-object v3, Lflar2/exkernelmanager/StartActivity;->c:[B

    invoke-virtual {p0}, Lflar2/exkernelmanager/StartActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/b/a/a/a/p;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, p0, v2}, Lcom/b/a/a/a/n;-><init>(Landroid/content/Context;Lcom/b/a/a/a/s;)V

    iput-object v1, p0, Lflar2/exkernelmanager/StartActivity;->f:Lcom/b/a/a/a/n;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    const v1, 0x7f0e0103

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    const v0, 0x7f0c00bf

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/StartActivity;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->h:Landroid/widget/TextView;

    const v1, 0x7f0e0132

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v0, "root_fragment"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/u;

    if-nez v0, :cond_0

    new-instance v0, Lflar2/exkernelmanager/u;

    invoke-direct {v0}, Lflar2/exkernelmanager/u;-><init>()V

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "root_fragment"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->d:Lcom/b/a/a/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->d:Lcom/b/a/a/a/a;

    invoke-virtual {v0}, Lcom/b/a/a/a/a;->a()V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/StartActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    return-void
.end method
