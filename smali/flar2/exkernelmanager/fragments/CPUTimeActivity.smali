.class public Lflar2/exkernelmanager/fragments/CPUTimeActivity;
.super Landroid/support/v7/app/ad;


# instance fields
.field n:Lflar2/exkernelmanager/utilities/h;

.field private o:Lflar2/exkernelmanager/utilities/m;

.field private p:Lflar2/exkernelmanager/utilities/e;

.field private q:Landroid/widget/ListView;

.field private r:Lflar2/exkernelmanager/a/a;

.field private s:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private t:Ljava/util/ArrayList;

.field private u:Landroid/support/v7/widget/Toolbar;

.field private v:Landroid/view/View;

.field private w:Landroid/view/View;

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    return-void
.end method

.method private a(FFF)F
    .locals 1

    invoke-static {p1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/CPUTimeActivity;FFF)F
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->a(FFF)F

    move-result v0

    return v0
.end method

.method private a(J)Ljava/lang/String;
    .locals 11

    const-wide/16 v0, 0xa

    mul-long/2addr v0, p1

    const-string v2, "%02d:%02d:%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    sget-object v5, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0, v1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sub-long v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->l()V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->s:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->o:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->y:I

    return v0
.end method

.method static synthetic f(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->v:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->w:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/support/v7/widget/Toolbar;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->u:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method static synthetic i(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->n()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->r:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private l()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/ar;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/ar;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Lflar2/exkernelmanager/fragments/al;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/ar;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private m()V
    .locals 25

    move-object/from16 v0, p0

    iget-object v2, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v16

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, v16

    array-length v9, v0

    const/4 v4, 0x0

    move/from16 v24, v4

    move-wide v4, v6

    move/from16 v6, v24

    :goto_0
    if-ge v6, v9, :cond_2

    aget-object v7, v16, v6

    const-string v10, " "

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v7, v7, v10

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-double v10, v10

    add-double/2addr v10, v4

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move-wide v4, v10

    goto :goto_0

    :cond_2
    const-string v6, "prefCPUTimeDeepSleep"

    invoke-static {v6}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v2, v6

    const-wide/16 v6, 0xa

    div-long/2addr v2, v6

    long-to-double v6, v2

    add-double/2addr v4, v6

    move-wide v6, v4

    move-wide v4, v2

    :goto_1
    const-string v2, "prefCPUTimeSaveOffsets"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "prefCPUTimeOffsets"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v8, Ljava/util/ArrayList;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v3, v2

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    add-long/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v3, v2

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    sub-double/2addr v6, v2

    const-string v2, "prefCPUTimeDeepSleep"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :try_start_0
    const-string v3, "prefCPUTimeDeepSleepOffset"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-double v10, v10

    sub-double/2addr v6, v10

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v4, v2

    move-wide v12, v6

    move-object v11, v8

    move-wide v6, v4

    :goto_4
    const-string v2, "prefCPUTimeDeepSleep"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    long-to-double v2, v6

    div-double/2addr v2, v12

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-double v14, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    new-instance v3, Lflar2/exkernelmanager/fragments/as;

    const v4, 0x7f0e0058

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->a(J)Ljava/lang/String;

    move-result-object v8

    const-wide/high16 v18, 0x4024000000000000L    # 10.0

    div-double v18, v14, v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v9

    const-wide/high16 v18, 0x4024000000000000L    # 10.0

    div-double v14, v14, v18

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    long-to-int v10, v14

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v10}, Lflar2/exkernelmanager/fragments/as;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v2, 0x0

    move v14, v2

    move v15, v3

    :goto_5
    move/from16 v0, v17

    if-ge v14, v0, :cond_0

    aget-object v2, v16, v14

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v3, v4

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "prefCPUTimeSaveOffsets"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v11, :cond_5

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_5
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-double v6, v6

    div-double/2addr v6, v12

    const-wide v8, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-double v0, v6

    move-wide/from16 v18, v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    new-instance v3, Lflar2/exkernelmanager/fragments/as;

    move-object/from16 v0, p0

    iget-object v5, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->p:Lflar2/exkernelmanager/utilities/e;

    invoke-virtual {v5, v4}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->a(J)Ljava/lang/String;

    move-result-object v8

    const-wide/high16 v22, 0x4024000000000000L    # 10.0

    div-double v22, v18, v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v9

    const-wide/high16 v22, 0x4024000000000000L    # 10.0

    div-double v18, v18, v22

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->round(D)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v10, v0

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v10}, Lflar2/exkernelmanager/fragments/as;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    const-string v2, "prefCPUTimeSort"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :cond_7
    add-int/lit8 v3, v15, 0x1

    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v15, v3

    goto/16 :goto_5

    :catch_0
    move-exception v3

    const-string v3, "prefCPUTimeSaveOffsets"

    const/4 v9, 0x0

    invoke-static {v3, v9}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_8
    move-wide v12, v6

    move-object v11, v8

    move-wide v6, v4

    goto/16 :goto_4

    :cond_9
    move-wide v12, v6

    move-object v11, v8

    move-wide v6, v4

    goto/16 :goto_4

    :cond_a
    move-wide v6, v4

    move-wide v4, v2

    goto/16 :goto_1
.end method

.method private n()Ljava/util/List;
    .locals 7

    const/4 v6, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->m()V

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v1, 0x7f0e005a

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/fragments/as;

    new-instance v3, Lflar2/exkernelmanager/a/o;

    invoke-direct {v3}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v4, v0, Lflar2/exkernelmanager/fragments/as;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, v0, Lflar2/exkernelmanager/fragments/as;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lflar2/exkernelmanager/fragments/as;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/a/o;->c(Ljava/lang/String;)V

    iget v0, v0, Lflar2/exkernelmanager/fragments/as;->e:I

    invoke-virtual {v3, v0}, Lflar2/exkernelmanager/a/o;->d(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v1, 0x7f0e00e5

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public k()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    const/4 v4, 0x1

    if-lt v2, v4, :cond_1

    iget v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->x:I

    :cond_1
    neg-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const v7, 0x7f090008

    const v6, 0x7f090007

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->setContentView(I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->u:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->u:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->overridePendingTransition(II)V

    const v0, 0x7f0e0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/fragments/al;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/al;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->n:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c00a7

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->r:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->r:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030020

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    const v0, 0x7f0c00aa

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->v:Landroid/view/View;

    const v0, 0x7f0c00ae

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->w:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->x:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->x:I

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x64

    iput v0, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->y:I

    const v0, 0x7f0c00ac

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0c00ad

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/16 v2, 0x1c2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/16 v2, 0x140

    move v3, v2

    :goto_0
    const v2, 0x7f0c00a8

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->s:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->s:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v5, v5, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(ZII)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->s:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0025

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeColor(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->s:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v3, 0x3

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->s:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v3, Lflar2/exkernelmanager/fragments/am;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/am;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/bu;)V

    new-instance v2, Lflar2/exkernelmanager/fragments/ao;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ao;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lflar2/exkernelmanager/fragments/ap;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ap;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->q:Landroid/widget/ListView;

    new-instance v3, Lflar2/exkernelmanager/fragments/aq;

    invoke-direct {v3, p0, v0, v1}, Lflar2/exkernelmanager/fragments/aq;-><init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->l()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v2, 0x15e

    move v3, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v2, 0x212

    move v3, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v2, 0x1a4

    move v3, v2

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x7f0a000f
        0x7f0a0006
        0x7f0a000f
    .end array-data
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    const v5, 0x7f0c017e

    const v4, 0x7f0c017d

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const-string v0, "prefCPUTimeDeepSleep"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    :goto_0
    const-string v0, "prefCPUTimeSort"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    :goto_1
    return v2

    :cond_0
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    goto :goto_0

    :sswitch_1
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const-string v1, "prefCPUTimeDeepSleep"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :goto_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->l()V

    goto :goto_0

    :cond_0
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const-string v1, "prefCPUTimeDeepSleep"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto :goto_1

    :sswitch_2
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const-string v1, "prefCPUTimeSort"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :goto_2
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->l()V

    goto :goto_0

    :cond_1
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const-string v1, "prefCPUTimeSort"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto :goto_2

    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/UserSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0173 -> :sswitch_3
        0x7f0c0174 -> :sswitch_4
        0x7f0c017d -> :sswitch_1
        0x7f0c017e -> :sswitch_2
    .end sparse-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->l()V

    return-void
.end method
