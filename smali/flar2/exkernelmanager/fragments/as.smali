.class Lflar2/exkernelmanager/fragments/as;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:J

.field e:I

.field final synthetic f:Lflar2/exkernelmanager/fragments/CPUTimeActivity;


# direct methods
.method public constructor <init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/as;->f:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/as;->a:Ljava/lang/String;

    iput-wide p3, p0, Lflar2/exkernelmanager/fragments/as;->d:J

    iput-object p5, p0, Lflar2/exkernelmanager/fragments/as;->b:Ljava/lang/String;

    iput-object p6, p0, Lflar2/exkernelmanager/fragments/as;->c:Ljava/lang/String;

    iput p7, p0, Lflar2/exkernelmanager/fragments/as;->e:I

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 4

    iget-wide v2, p0, Lflar2/exkernelmanager/fragments/as;->d:J

    move-object v0, p1

    check-cast v0, Lflar2/exkernelmanager/fragments/as;

    iget-wide v0, v0, Lflar2/exkernelmanager/fragments/as;->d:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lflar2/exkernelmanager/fragments/as;->d:J

    check-cast p1, Lflar2/exkernelmanager/fragments/as;

    iget-wide v2, p1, Lflar2/exkernelmanager/fragments/as;->d:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
