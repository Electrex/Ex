.class public Lflar2/exkernelmanager/fragments/BatteryActivity;
.super Landroid/support/v7/app/ad;

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field A:Landroid/widget/TextView;

.field B:Landroid/widget/TextView;

.field C:Landroid/widget/Spinner;

.field D:Ljava/util/List;

.field E:I

.field F:J

.field G:J

.field H:I

.field I:I

.field J:J

.field K:J

.field L:Landroid/view/MenuItem;

.field M:Landroid/view/MenuItem;

.field N:Landroid/view/MenuItem;

.field O:Landroid/view/MenuItem;

.field P:Landroid/view/MenuItem;

.field Q:Landroid/view/MenuItem;

.field R:Lflar2/exkernelmanager/utilities/h;

.field S:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private T:Landroid/content/BroadcastReceiver;

.field n:Landroid/view/View;

.field o:Landroid/widget/TextView;

.field p:Landroid/widget/TextView;

.field q:Landroid/widget/TextView;

.field r:Landroid/widget/TextView;

.field s:Landroid/widget/TextView;

.field t:Landroid/widget/TextView;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/widget/TextView;

.field x:Landroid/widget/TextView;

.field y:Landroid/widget/TextView;

.field z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/fragments/n;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/fragments/n;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->T:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 9

    const-string v0, "%02d:%02d:%02d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(III)V
    .locals 9

    const/16 v8, 0x28

    const/16 v5, 0x19

    const/16 v4, 0xe

    const/16 v7, -0x100

    const/high16 v6, -0x10000

    const-string v0, "prefTempUnit"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->z:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-le p2, v5, :cond_3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->z:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    :goto_0
    div-int/lit8 v1, p1, 0xa

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    int-to-double v2, v1

    const-wide v4, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4040000000000000L    # 32.0

    add-double/2addr v2, v4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->A:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    double-to-int v2, v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0F"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    if-ge v1, v8, :cond_6

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->A:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    :goto_2
    const/4 v0, 0x2

    if-ne p3, v0, :cond_8

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->B:Landroid/widget/TextView;

    const-string v1, "Charging"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    return-void

    :cond_3
    if-gt p2, v5, :cond_4

    if-lt p2, v4, :cond_4

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_4
    if-ge p2, v4, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_5
    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->A:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0C"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    if-lt v1, v8, :cond_7

    const/16 v0, 0x34

    if-gt v1, v0, :cond_7

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    :cond_7
    const/16 v0, 0x34

    if-le v1, v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    :cond_8
    const/4 v0, 0x5

    if-ne p3, v0, :cond_9

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->B:Landroid/widget/TextView;

    const-string v1, "Full"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->B:Landroid/widget/TextView;

    const-string v1, "Discharging"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/BatteryActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->l()V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/BatteryActivity;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(III)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 13

    const v12, 0x7f0e0133

    const-wide v10, 0x3fe3333333333333L    # 0.6

    const/4 v2, 0x1

    const/4 v0, 0x0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    const-string v4, "EX Battery Monitor"

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "os.version"

    invoke-static {v5}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "image/jpeg"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/Pictures/Screenshots/battery.jpg"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v1, 0x7f0c0068

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v5}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v10

    double-to-int v6, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-double v8, v7

    mul-double/2addr v8, v10

    double-to-int v7, v8

    const/4 v8, 0x1

    invoke-static {v1, v6, v7, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x55

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v5, v0}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    const-string v1, "android.intent.extra.STREAM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file:///"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "see_all"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v12}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const-string v1, "twitter"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EX battery stats: Screen "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "https://twitter.com/intent/tweet?text=%s"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "%"

    const-string v5, " percent"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.twitter"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BatteryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_5
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move v0, v2

    :goto_4
    move v1, v0

    goto :goto_3

    :cond_6
    move v1, v0

    :cond_7
    if-eqz v1, :cond_0

    invoke-virtual {p0, v12}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method private k()V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->M:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->M:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_2
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hangout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hangout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->P:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->P:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_4
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "apps.plus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "apps.plus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->O:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->O:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_6
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mail"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mail"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->Q:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->Q:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_8
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_9
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->N:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->N:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_a
    return-void
.end method

.method private l()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    const-string v0, "prefDisableBatteryMon"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p0}, Lcom/c/a/n;->a(Landroid/content/Context;)Lcom/c/a/n;

    move-result-object v0

    sget-object v1, Lcom/c/a/x;->c:Lcom/c/a/x;

    invoke-virtual {v0, v1}, Lcom/c/a/n;->a(Lcom/c/a/x;)Lcom/c/a/n;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/c/a/n;->a(I)Lcom/c/a/n;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/c/a/n;->b(I)Lcom/c/a/n;

    move-result-object v0

    const-string v1, "ENABLE"

    invoke-virtual {v0, v1}, Lcom/c/a/n;->b(Ljava/lang/CharSequence;)Lcom/c/a/n;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/fragments/q;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/q;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;)V

    invoke-virtual {v0, v1}, Lcom/c/a/n;->a(Lcom/c/a/c/a;)Lcom/c/a/n;

    move-result-object v0

    const-string v1, "Service is disabled"

    invoke-virtual {v0, v1}, Lcom/c/a/n;->a(Ljava/lang/CharSequence;)Lcom/c/a/n;

    move-result-object v0

    invoke-static {v0}, Lcom/c/a/aa;->a(Lcom/c/a/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->p()V

    goto :goto_0
.end method

.method private m()Z
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()I
    .locals 5

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lflar2/exkernelmanager/fragments/BatteryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v0, "level"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v0, "scale"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    return v0

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method private o()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private p()V
    .locals 12

    const v10, 0x4a5bba00    # 3600000.0f

    const/4 v1, 0x0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->m()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefBMScrOnMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    const-string v4, "prefBMScrOffTime"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    const-string v6, "prefBMScrOffUsage"

    invoke-static {v6}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v6

    iget-wide v8, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->F:J

    sub-long v8, v2, v8

    iget-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->G:J

    sub-long/2addr v4, v2

    iget v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->H:I

    sub-int v3, v0, v2

    iget v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->I:I

    sub-int/2addr v6, v0

    if-lez v6, :cond_2

    int-to-float v0, v6

    mul-float/2addr v0, v10

    long-to-float v2, v4

    div-float/2addr v0, v2

    move v2, v0

    :goto_1
    if-lez v3, :cond_3

    int-to-float v0, v3

    mul-float/2addr v0, v10

    long-to-float v1, v8

    div-float/2addr v0, v1

    :goto_2
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v7, "###.##"

    invoke-direct {v1, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->r:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Time:  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0, v8, v9}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->t:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Usage:  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, "%"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->p:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Idle drain:  "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    float-to-double v10, v2

    invoke-virtual {v1, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "% per hour"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->s:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Time:  "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v4, v5}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->u:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Usage:  "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "%"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->q:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Active drain:  "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    float-to-double v6, v0

    invoke-virtual {v1, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "% per hour"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "prefBMChargeTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->o()J

    move-result-wide v2

    const-string v6, "prefBMPlugMarker"

    invoke-static {v6}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v6

    sub-long/2addr v2, v6

    add-long/2addr v0, v2

    :goto_3
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->v:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Time charging:  "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v6, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->K:J

    sub-long/2addr v0, v6

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->w:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deep sleep:  "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v6, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    sub-long v6, v0, v6

    invoke-direct {p0, v6, v7}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-long v2, v8, v4

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->y:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Time on Battery:  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {p0, v2, v3}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    sub-long/2addr v0, v2

    sub-long v0, v4, v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->x:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Held awake:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const-string v0, "prefBMScrOnTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->o()J

    move-result-wide v4

    const-string v0, "prefBMScrOnMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    const-string v0, "prefBMScrOnUsage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    const-string v4, "prefBMScrOnLevel"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v4

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->n()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/2addr v0, v4

    goto/16 :goto_0

    :cond_1
    const-string v0, "prefBMScrOnTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "prefBMScrOnUsage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :cond_2
    move v2, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :cond_4
    const-string v0, "prefBMChargeTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_3
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->R:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->setContentView(I)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->overridePendingTransition(II)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v7/app/a;->a(Z)V

    const-string v0, "Battery Monitor"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c0066

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->n:Landroid/view/View;

    const v0, 0x7f0c0064

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->o:Landroid/widget/TextView;

    new-instance v0, Lflar2/exkernelmanager/fragments/j;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/j;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->R:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c005e

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->R:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->S:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->S:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeColor(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->S:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->S:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, Lflar2/exkernelmanager/fragments/k;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/k;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/bu;)V

    const v0, 0x7f0c007a

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lflar2/exkernelmanager/fragments/m;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/m;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;)V

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0060

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->z:Landroid/widget/TextView;

    const v0, 0x7f0c0063

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->B:Landroid/widget/TextView;

    const v0, 0x7f0c0061

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->A:Landroid/widget/TextView;

    const v0, 0x7f0c0067

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    const-string v1, "Since start"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "prefBMFullMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    :cond_0
    iput v3, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->E:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    const-string v1, "Since now"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "prefBMCustomMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    new-instance v0, Landroid/text/format/DateFormat;

    invoke-direct {v0}, Landroid/text/format/DateFormat;-><init>()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Since "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MMM dd hh:mm a"

    const-string v3, "prefBMCustomMarker"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const-string v0, "prefBMSpinnerSelection"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "prefBMSpinnerSelection"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iget v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->E:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    iget v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->E:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    :goto_0
    const v0, 0x7f0c0077

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->p:Landroid/widget/TextView;

    const v0, 0x7f0c0070

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->q:Landroid/widget/TextView;

    const v0, 0x7f0c006e

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->r:Landroid/widget/TextView;

    const v0, 0x7f0c0073

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->s:Landroid/widget/TextView;

    const v0, 0x7f0c006f

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->t:Landroid/widget/TextView;

    const v0, 0x7f0c0074

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->u:Landroid/widget/TextView;

    const v0, 0x7f0c0079

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->v:Landroid/widget/TextView;

    const v0, 0x7f0c0075

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->w:Landroid/widget/TextView;

    const v0, 0x7f0c006b

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->y:Landroid/widget/TextView;

    const v0, 0x7f0c0076

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->x:Landroid/widget/TextView;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->l()V

    return-void

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    const-string v1, "prefBMSpinnerSelection"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    :array_0
    .array-data 4
        0x7f0a000f
        0x7f0a0006
        0x7f0a000f
    .end array-data
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    const v4, 0x7f0c0171

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f100000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const-string v0, "prefDisableBatteryMon"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    :goto_0
    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->L:Landroid/view/MenuItem;

    if-eqz p1, :cond_0

    const v0, 0x7f0c0177

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->M:Landroid/view/MenuItem;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->M:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c0176

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->O:Landroid/view/MenuItem;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->O:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c0178

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->N:Landroid/view/MenuItem;

    const v0, 0x7f0c0179

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->P:Landroid/view/MenuItem;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->P:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c017a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->Q:Landroid/view/MenuItem;

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->k()V

    return v3

    :cond_1
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v0, p3}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "prefBMSpinnerSelection"

    invoke-static {v4, p3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v4, "Since start"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iput-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->G:J

    iput-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->F:J

    iput v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->H:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->I:I

    iput-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->K:J

    const-string v0, "prefBMDeepSleepOffset"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->l()V

    return-void

    :cond_1
    const-string v4, "Since last full charge"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v0, "prefBMFullMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const-string v0, "prefBMFullTimeOn"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->F:J

    const-string v0, "prefBMFullTimeOff"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->G:J

    const-string v0, "prefBMFullUsageOn"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->H:I

    const-string v0, "prefBMFullUsageOff"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->I:I

    const-string v0, "prefBMFullTimeCharge"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->K:J

    const-string v0, "prefBMDeepSleepFullOffset"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    goto :goto_0

    :cond_2
    const-string v4, "Since now"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "prefBMCustomMarker"

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->o()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->m()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "prefBMScrOnMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const-string v1, "prefBMCustomTimeOn"

    invoke-static {v1, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v1, "prefBMCustomTimeOff"

    const-string v2, "prefBMScrOffTime"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v1, "prefBMCustomTimeOn"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->F:J

    const-string v1, "prefBMCustomTimeOff"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->G:J

    const-string v1, "prefBMCustomUsageOn"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMCustomUsageOff"

    const-string v1, "prefBMScrOffUsage"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMCustomUsageOn"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->H:I

    const-string v0, "prefBMCustomUsageOff"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->I:I

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "prefBMChargeTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->o()J

    move-result-wide v2

    const-string v4, "prefBMPlugMarker"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    :goto_2
    const-string v2, "prefBMCustomTimeCharge"

    invoke-static {v2, v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->K:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    const-string v0, "prefBMDeepSleepCustomOffset"

    iget-wide v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    iget v1, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->E:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->D:Ljava/util/List;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Since "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MMM dd hh:mm a"

    const-string v3, "prefBMCustomMarker"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    const-string v0, "prefBMScrOnTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->o()J

    move-result-wide v2

    const-string v4, "prefBMScrOnMarker"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v2, v0

    const-string v0, "prefBMScrOnUsage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    const-string v1, "prefBMScrOnLevel"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->n()I

    move-result v4

    sub-int/2addr v1, v4

    add-int/2addr v0, v1

    goto/16 :goto_1

    :cond_5
    const-string v0, "prefBMScrOnTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "prefBMScrOnUsage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    :cond_6
    const-string v0, "prefBMChargeTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_2

    :cond_7
    const-string v0, "prefBMCustomTimeOn"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->F:J

    const-string v0, "prefBMCustomTimeOff"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->G:J

    const-string v0, "prefBMCustomUsageOn"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->H:I

    const-string v0, "prefBMCustomUsageOff"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->I:I

    const-string v0, "prefBMDeepSleepCustomOffset"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->J:J

    const-string v0, "prefBMCustomTimeCharge"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->K:J

    goto/16 :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x1

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    goto :goto_0

    :sswitch_1
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v6}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const-string v2, "prefDisableBatteryMon"

    invoke-static {v2, v6}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    invoke-static {p0}, Lcom/c/a/n;->a(Landroid/content/Context;)Lcom/c/a/n;

    move-result-object v2

    const-string v3, "Service enabled"

    invoke-virtual {v2, v3}, Lcom/c/a/n;->a(Ljava/lang/CharSequence;)Lcom/c/a/n;

    move-result-object v2

    sget-object v3, Lcom/c/a/x;->a:Lcom/c/a/x;

    invoke-virtual {v2, v3}, Lcom/c/a/n;->a(Lcom/c/a/x;)Lcom/c/a/n;

    move-result-object v2

    invoke-static {v2}, Lcom/c/a/aa;->a(Lcom/c/a/n;)V

    :goto_1
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v2, v6}, Landroid/widget/Spinner;->setSelection(I)V

    new-instance v2, Lflar2/exkernelmanager/fragments/o;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/o;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const-string v2, "prefDisableBatteryMon"

    invoke-static {v2, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto :goto_1

    :sswitch_2
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BatteryActivity;->stopService(Landroid/content/Intent;)Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-string v4, "prefBMDeepSleepOffset"

    invoke-static {v4, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v2, "prefDisableBatteryMon"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BatteryActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v2, v6}, Landroid/widget/Spinner;->setSelection(I)V

    new-instance v2, Lflar2/exkernelmanager/fragments/p;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/p;-><init>(Lflar2/exkernelmanager/fragments/BatteryActivity;)V

    const-wide/16 v4, 0x5dc

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :sswitch_3
    const-string v1, "facebook"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4
    const-string v1, "apps.plus"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    const-string v1, "twitter"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    const-string v1, "hangout"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "mail"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    const-string v1, "see_all"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/UserSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0171 -> :sswitch_1
        0x7f0c0172 -> :sswitch_2
        0x7f0c0173 -> :sswitch_9
        0x7f0c0174 -> :sswitch_a
        0x7f0c0176 -> :sswitch_4
        0x7f0c0177 -> :sswitch_3
        0x7f0c0178 -> :sswitch_5
        0x7f0c0179 -> :sswitch_6
        0x7f0c017a -> :sswitch_7
        0x7f0c017b -> :sswitch_8
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/support/v7/app/ad;->onPause()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->T:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->setRequestedOrientation(I)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BatteryActivity;->T:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BatteryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BatteryActivity;->l()V

    return-void
.end method
