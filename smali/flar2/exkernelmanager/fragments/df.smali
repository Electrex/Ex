.class Lflar2/exkernelmanager/fragments/df;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Lflar2/exkernelmanager/fragments/cz;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/cz;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/df;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/cz;->f(Lflar2/exkernelmanager/fragments/cz;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/cz;->e(Lflar2/exkernelmanager/fragments/cz;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e00af

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->a:[Ljava/lang/String;

    aget-object v1, v1, v4

    const-string v2, "585"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x8

    if-ge p2, v0, :cond_1

    const-string v0, "prefGPUMinBoot"

    invoke-static {v0, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefGPUMin"

    const-string v1, "0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/cz;->a(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "0"

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/cz;->c(Lflar2/exkernelmanager/fragments/cz;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/cz;->b(Lflar2/exkernelmanager/fragments/cz;)V

    return-void

    :cond_1
    add-int/lit8 v0, p2, -0x7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefGPUMinBoot"

    invoke-static {v1, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefGPUMin"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/cz;->a(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/cz;->c(Lflar2/exkernelmanager/fragments/cz;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/cz;->f(Lflar2/exkernelmanager/fragments/cz;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/cz;->e(Lflar2/exkernelmanager/fragments/cz;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e00dd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->a:[Ljava/lang/String;

    aget-object v1, v1, v4

    const-string v2, "585"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x7

    if-ge p2, v0, :cond_3

    const-string v0, "prefGPUMinBoot"

    invoke-static {v0, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefGPUMin"

    const-string v1, "0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/cz;->a(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "0"

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/cz;->c(Lflar2/exkernelmanager/fragments/cz;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, p2, -0x6

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefGPUMinBoot"

    invoke-static {v1, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefGPUMin"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/cz;->a(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/cz;->c(Lflar2/exkernelmanager/fragments/cz;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    if-eqz v0, :cond_0

    const-string v1, "prefGPUMinBoot"

    invoke-static {v1, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefGPUMin"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/cz;->a(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/df;->b:Lflar2/exkernelmanager/fragments/cz;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/cz;->c(Lflar2/exkernelmanager/fragments/cz;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
