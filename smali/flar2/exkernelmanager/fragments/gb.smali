.class public Lflar2/exkernelmanager/fragments/gb;
.super Landroid/app/Fragment;

# interfaces
.implements Lflar2/exkernelmanager/a/g;


# static fields
.field public static final a:Ljava/util/ArrayList;

.field public static final b:Ljava/util/ArrayList;


# instance fields
.field private c:Lflar2/exkernelmanager/utilities/m;

.field private d:Lflar2/exkernelmanager/utilities/e;

.field private e:Landroid/content/Context;

.field private f:Landroid/widget/ListView;

.field private g:Lflar2/exkernelmanager/a/a;

.field private h:Landroid/widget/CheckedTextView;

.field private i:I

.field private j:Landroid/widget/TextView;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->c:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->d:Lflar2/exkernelmanager/utilities/e;

    const/4 v0, 0x0

    iput v0, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/gb;)Landroid/widget/CheckedTextView;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->h:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private a()V
    .locals 2

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gb;->b()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->g:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/a;->clear()V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->g:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/a/a;->addAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->g:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/a;->notifyDataSetChanged()V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/gb;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/gb;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/gb;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    return v0
.end method

.method private b()Ljava/util/List;
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    sget-object v0, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->d:Lflar2/exkernelmanager/utilities/e;

    sget-object v4, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    iget v5, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->e:Landroid/content/Context;

    const v2, 0x7f0e00e4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v3

    :cond_1
    iget v0, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    if-nez v0, :cond_2

    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    const-string v7, "mhz"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    const-string v7, " "

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "mV"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lflar2/exkernelmanager/a/o;

    invoke-direct {v7}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v7, v10}, Lflar2/exkernelmanager/a/o;->a(I)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " MHz"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mV"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    sget-object v7, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x3

    invoke-virtual {v6, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v9

    const-string v7, " "

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x3

    invoke-virtual {v5, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Lflar2/exkernelmanager/a/o;

    invoke-direct {v7}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v7, v10}, Lflar2/exkernelmanager/a/o;->a(I)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " MHz"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mV"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 9

    const/4 v2, 0x0

    const-string v0, "prefVoltageBoot"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->h:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    aget-object v1, v1, v3

    invoke-virtual {v0, p1, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gb;->a()V

    sget-object v0, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    sget-object v1, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sget-object v1, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    sget-object v3, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    if-nez v3, :cond_1

    const-string v0, "prefVoltage"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string v3, "prefVoltSize"

    sget-object v4, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v3, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    array-length v4, v1

    move v3, v2

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v1, v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "prefVolt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v8, v0, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/gb;->b(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    const v0, 0x7f030038

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->e:Landroid/content/Context;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x4

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v1, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v2, "Voltage"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    const-string v0, "prefVoltagePath"

    iget v1, p0, Lflar2/exkernelmanager/fragments/gb;->i:I

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const v0, 0x7f0c007d

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->f:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->g:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->g:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p0}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/g;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->g:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sput-boolean v4, Lflar2/exkernelmanager/a/a;->b:Z

    const v0, 0x7f0c013b

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const v1, 0x7f0c013d

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0c013a

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckedTextView;

    iput-object v2, p0, Lflar2/exkernelmanager/fragments/gb;->h:Landroid/widget/CheckedTextView;

    const-string v2, "prefVoltageBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gb;->h:Landroid/widget/CheckedTextView;

    invoke-virtual {v2, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gb;->h:Landroid/widget/CheckedTextView;

    new-instance v3, Lflar2/exkernelmanager/fragments/gc;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/gc;-><init>(Lflar2/exkernelmanager/fragments/gb;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/fragments/gb;->setHasOptionsMenu(Z)V

    new-instance v2, Lflar2/exkernelmanager/fragments/gd;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gd;-><init>(Lflar2/exkernelmanager/fragments/gb;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/ge;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/fragments/ge;-><init>(Lflar2/exkernelmanager/fragments/gb;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v1}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationY(F)V

    const v0, 0x7f0c0139

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->k:Landroid/view/View;

    const v0, 0x7f0c013e

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->l:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c00b6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->j:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c00b7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->m:Landroid/view/View;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f030031

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->f:Landroid/widget/ListView;

    invoke-virtual {p1, v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->n:Landroid/view/View;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gb;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v7, p0, Lflar2/exkernelmanager/fragments/gb;->f:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/utilities/a/b;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gb;->k:Landroid/view/View;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gb;->l:Landroid/view/View;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gb;->m:Landroid/view/View;

    const v4, 0x7f040020

    const v5, 0x7f04001f

    invoke-direct/range {v0 .. v5}, Lflar2/exkernelmanager/utilities/a/b;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const-string v0, "prefFirstRunVoltage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "prefFirstRunVoltage"

    invoke-static {v0, v8}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gb;->a()V

    return-object v6
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gb;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/a/a;->b:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gb;->a()V

    return-void
.end method
