.class Lflar2/exkernelmanager/fragments/ao;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/fragments/CPUTimeActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/ao;->a:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    const-string v1, ""

    const-string v2, "prefCPUTimeSaveOffsets"

    invoke-static {v2, v6}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ao;->a:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->c(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    invoke-virtual {v2, v3, v0}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v6

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "prefCPUTimeOffsets"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xa

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefCPUTimeDeepSleepOffset"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ao;->a:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->a(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)V

    return-void
.end method
