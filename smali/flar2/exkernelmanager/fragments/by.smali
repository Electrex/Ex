.class Lflar2/exkernelmanager/fragments/by;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lflar2/exkernelmanager/fragments/ColorActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/by;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/by;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/by;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/by;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v2, v0

    sub-int v0, v1, v0

    const/16 v1, 0x1f4

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/by;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->g(Lflar2/exkernelmanager/fragments/ColorActivity;)Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/by;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->g(Lflar2/exkernelmanager/fragments/ColorActivity;)Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->setVisibility(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/by;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflar2/exkernelmanager/fragments/ColorActivity;->n:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/by;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    iget-boolean v0, v0, Lflar2/exkernelmanager/fragments/ColorActivity;->n:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lflar2/exkernelmanager/fragments/bz;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bz;-><init>(Lflar2/exkernelmanager/fragments/by;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
