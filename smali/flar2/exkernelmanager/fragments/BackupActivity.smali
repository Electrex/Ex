.class public Lflar2/exkernelmanager/fragments/BackupActivity;
.super Landroid/support/v7/app/ad;

# interfaces
.implements Lflar2/exkernelmanager/b/c;


# instance fields
.field n:Lflar2/exkernelmanager/b/a;

.field o:Ljava/lang/String;

.field p:Lflar2/exkernelmanager/utilities/h;

.field private q:Ljava/util/List;

.field private r:Landroid/support/v7/widget/RecyclerView;

.field private s:Landroid/support/v7/widget/cl;

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->q:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->t:Z

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/BackupActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->l()V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/BackupActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/BackupActivity;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/BackupActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->t:Z

    return p1
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/BackupActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->m()V

    return-void
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX/kernel_backups/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMM d hh:mm a"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/BackupActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->t:Z

    return v0
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/BackupActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->p()V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->setRequestedOrientation(I)V

    new-instance v0, Lflar2/exkernelmanager/fragments/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/i;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;Lflar2/exkernelmanager/fragments/a;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/BackupActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->o()V

    return-void
.end method

.method private k()V
    .locals 3

    const v0, 0x7f0c005b

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    invoke-virtual {v1}, Lflar2/exkernelmanager/b/a;->a()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "No backups"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    invoke-virtual {v1}, Lflar2/exkernelmanager/b/a;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "You have "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    invoke-virtual {v2}, Lflar2/exkernelmanager/b/a;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " backup"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "You have "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    invoke-virtual {v2}, Lflar2/exkernelmanager/b/a;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " backups"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private l()V
    .locals 6

    const/4 v3, 0x1

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const-string v1, "Backup name"

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const-string v2, "os.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setMaxLines(I)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ElementalX/kernel_backups/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    :cond_0
    sget-object v3, Lflar2/exkernelmanager/r;->b:[Ljava/lang/String;

    new-instance v4, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v4}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    sget-object v5, Lflar2/exkernelmanager/r;->b:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    if-nez v3, :cond_1

    const-string v1, "Device not supported"

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    :goto_0
    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void

    :cond_1
    const v4, 0x7f0e00ea

    new-instance v5, Lflar2/exkernelmanager/fragments/c;

    invoke-direct {v5, p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/c;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    goto :goto_0
.end method

.method private m()V
    .locals 3

    new-instance v0, Lflar2/exkernelmanager/b/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->o:Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->o:Ljava/lang/String;

    invoke-direct {p0, v2}, Lflar2/exkernelmanager/fragments/BackupActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lflar2/exkernelmanager/b/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/b/a;->a(ILflar2/exkernelmanager/b/f;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->k()V

    return-void
.end method

.method private n()V
    .locals 6

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX/kernel_backups"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->k()V

    return-void

    :cond_1
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    new-instance v4, Lflar2/exkernelmanager/b/f;

    invoke-direct {p0, v3}, Lflar2/exkernelmanager/fragments/BackupActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lflar2/exkernelmanager/b/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->q:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->q:Ljava/util/List;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/b/a;->a(Ljava/util/List;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private o()V
    .locals 7

    const/4 v1, 0x0

    iput-boolean v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->t:Z

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00fb

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/f;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/f;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v7/app/ab;->show()V

    new-instance v0, Lflar2/exkernelmanager/fragments/g;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lflar2/exkernelmanager/fragments/g;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;JJLandroid/support/v7/app/ab;)V

    invoke-virtual {v0}, Lflar2/exkernelmanager/fragments/g;->start()Landroid/os/CountDownTimer;

    return-void
.end method

.method private p()V
    .locals 1

    :try_start_0
    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {v0, p0}, Lflar2/exkernelmanager/utilities/f;->f(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->k()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/BackupActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const-string v2, "Restore backup and reboot?"

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const-string v2, "This will restore kernel from backup and reboot your device."

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/e;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/e;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const-string v2, "RESTORE"

    new-instance v3, Lflar2/exkernelmanager/fragments/d;

    invoke-direct {v3, p0, p1}, Lflar2/exkernelmanager/fragments/d;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->p:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BackupActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001a

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->setContentView(I)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BackupActivity;->overridePendingTransition(II)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Z)V

    const-string v0, "Kernel Backup"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/a;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/a;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->p:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c0059

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->p:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c005c

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->s:Landroid/support/v7/widget/cl;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->s:Landroid/support/v7/widget/cl;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/cl;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->r:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/am;

    invoke-direct {v1}, Landroid/support/v7/widget/am;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/cf;)V

    new-instance v0, Lflar2/exkernelmanager/b/a;

    invoke-direct {v0}, Lflar2/exkernelmanager/b/a;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/cc;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BackupActivity;->n:Lflar2/exkernelmanager/b/a;

    invoke-virtual {v0, p0}, Lflar2/exkernelmanager/b/a;->a(Lflar2/exkernelmanager/b/c;)V

    const v0, 0x7f0c005d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lflar2/exkernelmanager/fragments/b;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/b;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BackupActivity;->n()V

    return-void
.end method
