.class public Lflar2/exkernelmanager/fragments/ep;
.super Landroid/app/Fragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Lflar2/exkernelmanager/a/a;

.field private c:Landroid/content/Context;

.field private d:Lflar2/exkernelmanager/utilities/m;

.field private e:Lflar2/exkernelmanager/utilities/e;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ImageView;

.field private i:I

.field private j:I

.field private k:Landroid/graphics/RectF;

.field private l:Landroid/graphics/RectF;

.field private m:Landroid/view/animation/AccelerateDecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    return-void
.end method

.method public static a(FFF)F
    .locals 1

    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)I
    .locals 4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00d9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    rsub-int/lit8 v0, v0, 0x26

    :goto_0
    return v0

    :cond_0
    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00dd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    rsub-int/lit8 v0, v0, 0x16

    goto :goto_0

    :cond_1
    rsub-int/lit8 v0, v0, 0x26

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)I
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    const/16 v1, 0x1e

    if-ge v0, v1, :cond_1

    :goto_1
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_1
    add-int/lit16 v0, v0, -0x100

    goto :goto_1
.end method

.method private a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    return-object p1
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ep;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ep;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/ep;->j:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/ep;->j:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iget v1, p0, Lflar2/exkernelmanager/fragments/ep;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lflar2/exkernelmanager/fragments/ep;->a(FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->m:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/ep;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->h:Landroid/widget/ImageView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->m:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/ep;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->h:Landroid/widget/ImageView;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;F)V
    .locals 6

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p1}, Lflar2/exkernelmanager/fragments/ep;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p2}, Lflar2/exkernelmanager/fragments/ep;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    sub-float/2addr v0, v3

    mul-float/2addr v0, p3

    add-float/2addr v0, v3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, p3

    add-float/2addr v1, v3

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    mul-float/2addr v2, v5

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ep;->l:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ep;->k:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p3

    mul-float/2addr v3, v5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    sub-float v2, v3, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ep;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ep;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "2"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "2"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ep;->b()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ep;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->b:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private b()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/er;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/er;-><init>(Lflar2/exkernelmanager/fragments/ep;Lflar2/exkernelmanager/fragments/eq;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/er;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private c()Ljava/util/List;
    .locals 10

    const/4 v9, 0x3

    const v8, 0x7f02006b

    const v7, 0x7f020069

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_0

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e0137

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00e4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x325

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00c5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00e8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    const-string v2, "prefSoundLockedBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSoundLockedBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_headphone_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00ac

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x323

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v4, 0x7f0e00a2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sys/kernel/sound_control_3/gpl_headphone_gain"

    invoke-direct {p0, v3, v6}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/kernel/sound_control_3/gpl_headphone_gain"

    invoke-direct {p0, v2, v6}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "PREF_SOUND_HEADPHONE_GAINBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_HEADPHONE_GAINBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_headphone_pa_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00b1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00db

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x324

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v4, 0x7f0e00f4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sys/kernel/sound_control_3/gpl_headphone_pa_gain"

    invoke-direct {p0, v3}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/kernel/sound_control_3/gpl_headphone_pa_gain"

    invoke-direct {p0, v2}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "PREF_SOUND_HEADPHONE_PA_GAINBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_HEADPHONE_PA_GAINBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_speaker_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_mic_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_cam_mic_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00a3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_speaker_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00b1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00db

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x322

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v4, 0x7f0e0138

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sys/kernel/sound_control_3/gpl_speaker_gain"

    invoke-direct {p0, v3, v6}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/kernel/sound_control_3/gpl_speaker_gain"

    invoke-direct {p0, v2, v6}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "PREF_SOUND_SPEAKER_GAINBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_SPEAKER_GAINBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_mic_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x320

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v4, 0x7f0e00ab

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sys/kernel/sound_control_3/gpl_mic_gain"

    invoke-direct {p0, v3, v5}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/kernel/sound_control_3/gpl_mic_gain"

    invoke-direct {p0, v2, v5}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "PREF_SOUND_MIC_GAINBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_MIC_GAINBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/sound_control_3/gpl_cam_mic_gain"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x321

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v4, 0x7f0e003d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sys/kernel/sound_control_3/gpl_cam_mic_gain"

    invoke-direct {p0, v3, v5}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/kernel/sound_control_3/gpl_cam_mic_gain"

    invoke-direct {p0, v2, v5}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "PREF_SOUND_CAM_GAINBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_CAM_GAINBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_6
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/mic_boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x326

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mic boost: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/virtual/misc/soundcontrol/mic_boost"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/mic_boost"

    invoke-direct {p0, v2, v5}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "prefMicBoostBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefMicBoostBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->e:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x327

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Volume boost: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-direct {p0, v2, v5}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Z)I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v2, "prefVolBoostBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefVolBoostBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    return-object v0

    :cond_c
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const v3, 0x7f0e00c6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_e
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_10
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_4

    :cond_11
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_12
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_6

    :cond_13
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_14
    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto :goto_8
.end method


# virtual methods
.method public a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    const/4 v4, 0x1

    if-lt v2, v4, :cond_1

    iget v0, p0, Lflar2/exkernelmanager/fragments/ep;->i:I

    :cond_1
    neg-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f0b005d

    const/4 v6, 0x5

    const/4 v5, 0x1

    const v0, 0x7f030032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "Sound"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0, v5}, Lflar2/exkernelmanager/fragments/ep;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c007d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v3, v4}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->b:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ep;->b:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    sput-boolean v5, Lflar2/exkernelmanager/a/a;->b:Z

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b3

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_2

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b7

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->h:Landroid/widget/ImageView;

    const v3, 0x7f020086

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b4

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->g:Landroid/widget/TextView;

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->f:Landroid/widget/TextView;

    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ep;->i:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/ep;->i:I

    neg-int v0, v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lflar2/exkernelmanager/fragments/ep;->j:I

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->m:Landroid/view/animation/AccelerateDecelerateInterpolator;

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->a:Landroid/widget/ListView;

    new-instance v2, Lflar2/exkernelmanager/fragments/eq;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/eq;-><init>(Lflar2/exkernelmanager/fragments/ep;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    const-string v0, "prefSoundLocked"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefSoundLocked"

    const-string v2, "0"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ep;->b()V

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefFirstRunSettings"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_1
    return-object v1

    :cond_2
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ep;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->m:Landroid/view/animation/AccelerateDecelerateInterpolator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ep;->a(I)V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->b:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "prefSoundLocked"

    const-string v1, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ep;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x325
        :pswitch_0
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->b:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ep;->c:Landroid/content/Context;

    const-string v1, "No help for this item"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/a/a;->b:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ep;->b()V

    return-void
.end method
