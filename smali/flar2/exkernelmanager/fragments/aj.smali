.class Lflar2/exkernelmanager/fragments/aj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Lflar2/exkernelmanager/fragments/ac;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ac;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/aj;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aj;->a:[Ljava/lang/String;

    aget-object v0, v0, p2

    if-eqz v0, :cond_0

    const-string v1, "prefCPUGovBoot"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefCPUGov"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu5/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu6/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu7/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aj;->b:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;)V

    :cond_0
    return-void
.end method
