.class Lflar2/exkernelmanager/fragments/i;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/fragments/BackupActivity;

.field private b:Landroid/app/ProgressDialog;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/i;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/fragments/BackupActivity;Lflar2/exkernelmanager/fragments/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/i;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const-string v0, "records"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-virtual {v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Restore backup failed"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXKM: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/BackupActivity;->setRequestedOrientation(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->e(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/i;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/i;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/i;->b:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->b:Landroid/app/ProgressDialog;

    const-string v1, "Restoring..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/i;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method
