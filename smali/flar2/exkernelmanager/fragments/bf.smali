.class Lflar2/exkernelmanager/fragments/bf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Landroid/view/inputmethod/InputMethodManager;

.field final synthetic b:Lflar2/exkernelmanager/fragments/ColorActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/bf;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/bf;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/bf;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->o(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/bf;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->o(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/bf;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0, p1}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/View;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/bf;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method
