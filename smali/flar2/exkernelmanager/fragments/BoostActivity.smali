.class public Lflar2/exkernelmanager/fragments/BoostActivity;
.super Landroid/support/v7/app/ad;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field n:Lflar2/exkernelmanager/utilities/h;

.field private o:Lflar2/exkernelmanager/utilities/m;

.field private p:Lflar2/exkernelmanager/utilities/e;

.field private q:Lflar2/exkernelmanager/utilities/f;

.field private r:Landroid/widget/ListView;

.field private s:Lflar2/exkernelmanager/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->q:Lflar2/exkernelmanager/utilities/f;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/BoostActivity;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e009b

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v2, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/u;

    invoke-direct {v3, p0, v1, p1, p2}, Lflar2/exkernelmanager/fragments/u;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/BoostActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->k()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->k()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/BoostActivity;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010d

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-virtual {v2, v3, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/v;

    invoke-direct {v3, p0, v2, p1, p2}, Lflar2/exkernelmanager/fragments/v;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/BoostActivity;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->s:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030041

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v0, 0x7f0c0150

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    const v1, 0x7f0c0151

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    const v2, 0x7f0c0152

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    const v3, 0x7f0c0153

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v7, v8}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v7, v8, v9}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v7

    new-instance v5, Landroid/widget/ArrayAdapter;

    const v8, 0x1090009

    invoke-direct {v5, p0, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v1, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v4, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x4

    new-array v9, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0:"

    invoke-virtual {v8, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    const-string v10, "1:"

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v8, v5, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v4

    const/4 v4, 0x1

    const-string v5, "1:"

    invoke-virtual {v8, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    const-string v10, "2:"

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v8, v5, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v4

    const/4 v4, 0x2

    const-string v5, "2:"

    invoke-virtual {v8, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    const-string v10, "3:"

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v8, v5, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v4

    const/4 v4, 0x3

    const-string v5, "3:"

    invoke-virtual {v8, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v8, v5, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v4

    const/4 v5, 0x0

    array-length v10, v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v10, :cond_4

    aget-object v11, v7, v4

    const/4 v12, 0x0

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    const/4 v12, 0x1

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual {v1, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_1
    const/4 v12, 0x2

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    const/4 v12, 0x3

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_3
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    new-instance v4, Lflar2/exkernelmanager/fragments/w;

    invoke-direct {v4, p0, v7, p2}, Lflar2/exkernelmanager/fragments/w;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/x;

    invoke-direct {v0, p0, v7, p2}, Lflar2/exkernelmanager/fragments/x;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/y;

    invoke-direct {v0, p0, v7, p2}, Lflar2/exkernelmanager/fragments/y;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/z;

    invoke-direct {v0, p0, v7, p2}, Lflar2/exkernelmanager/fragments/z;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010c

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/s;

    invoke-direct {v3, p0, p1, p2}, Lflar2/exkernelmanager/fragments/s;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    new-instance v3, Lflar2/exkernelmanager/fragments/aa;

    invoke-direct {v3, p0, v8, p2, p1}, Lflar2/exkernelmanager/fragments/aa;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private k()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/ab;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/ab;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;Lflar2/exkernelmanager/fragments/r;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/ab;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private l()Ljava/util/List;
    .locals 10

    const v9, 0x7f0e007a

    const v8, 0x7f02006b

    const v7, 0x7f020069

    const/4 v6, 0x0

    const/4 v5, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v2, 0x7f0e004a

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/cpuboost_enable"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2bd

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "CPU Boost enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/cpuboost_enable"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, v9}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    const-string v2, "prefBoostEnabledBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostEnabledBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/boost_ms"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2bc

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Boost milliseconds"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/boost_ms"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBoostMSBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostMSBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2be

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Input boost frequency"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_3
    const-string v2, "prefBoostInputBoostFreqBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostInputBoostFreqBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/input_boost_ms"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2bf

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Input boost milliseconds"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_ms"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBoostInputBoostMSBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostInputBoostMSBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/load_based_syncs"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2c0

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Load based syncs"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/load_based_syncs"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p0, v9}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_6
    const-string v2, "prefBoostLoadBasedSyncsBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostLoadBasedSyncsBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/migration_load_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2c1

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Migration load threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_boost/parameters/migration_load_threshold"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBoostMigrationLoadThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostMigrationLoadThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2c2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Sync threshold frequency"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBoostSyncThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBoostSyncThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_9
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    return-object v0

    :cond_7
    const v2, 0x7f0e008d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_2

    :cond_a
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_b
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_4

    :cond_c
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_d
    const v2, 0x7f0e008d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_e
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_f
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_8

    :cond_10
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto :goto_9
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->setContentView(I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->overridePendingTransition(II)V

    const v0, 0x7f0e004c

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/fragments/r;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/r;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->n:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/BoostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->r:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->s:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->s:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->r:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->r:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->r:Landroid/widget/ListView;

    new-instance v1, Lflar2/exkernelmanager/fragments/t;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/t;-><init>(Lflar2/exkernelmanager/fragments/BoostActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->k()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->s:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "prefBoostMS"

    const-string v1, "/sys/module/cpu_boost/parameters/boost_ms"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "prefBoostEnable"

    const-string v1, "/sys/module/cpu_boost/parameters/cpuboost_enable"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefBoostInputBoostFreq"

    const-string v1, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "prefBoostInputBoostFreq"

    const-string v1, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "prefBoostInputBoostMS"

    const-string v1, "/sys/module/cpu_boost/parameters/input_boost_ms"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    const-string v0, "prefBoostLoadBasedSyncs"

    const-string v1, "/sys/module/cpu_boost/parameters/load_based_syncs"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    const-string v0, "prefBoostMigrationLoadThreshold"

    const-string v1, "/sys/module/cpu_boost/parameters/migration_load_threshold"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    const-string v0, "prefBoostSyncThreshold"

    const-string v1, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/BoostActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2c2
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/BoostActivity;->s:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    const/4 v0, 0x1

    return v0

    :sswitch_1
    const-string v0, "This is how you change doubletap2wake "

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2c1 -> :sswitch_0
        -0x2c0 -> :sswitch_0
        -0x2bf -> :sswitch_0
        -0x2be -> :sswitch_0
        -0x2bd -> :sswitch_0
        -0x2bc -> :sswitch_0
        -0x25 -> :sswitch_1
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/BoostActivity;->k()V

    return-void
.end method
