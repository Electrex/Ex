.class Lflar2/exkernelmanager/fragments/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflar2/exkernelmanager/fragments/BackupActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/BackupActivity;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/c;->d:Lflar2/exkernelmanager/fragments/BackupActivity;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/c;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lflar2/exkernelmanager/fragments/c;->b:Ljava/lang/String;

    iput-object p4, p0, Lflar2/exkernelmanager/fragments/c;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/c;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/c;->d:Lflar2/exkernelmanager/fragments/BackupActivity;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/c;->b:Ljava/lang/String;

    iput-object v1, v0, Lflar2/exkernelmanager/fragments/BackupActivity;->o:Ljava/lang/String;

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/ElementalX/kernel_backups/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/c;->d:Lflar2/exkernelmanager/fragments/BackupActivity;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/BackupActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v1}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/c;->d:Lflar2/exkernelmanager/fragments/BackupActivity;

    const-string v1, "File already exists"

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/c;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/c;->d:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflar2/exkernelmanager/fragments/BackupActivity;->o:Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v1, Lflar2/exkernelmanager/fragments/h;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/c;->d:Lflar2/exkernelmanager/fragments/BackupActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lflar2/exkernelmanager/fragments/h;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;Lflar2/exkernelmanager/fragments/a;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/c;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method
