.class public Lflar2/exkernelmanager/fragments/SleeperActivity;
.super Landroid/support/v7/app/e;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field n:Lflar2/exkernelmanager/utilities/h;

.field private o:Lflar2/exkernelmanager/utilities/m;

.field private p:Lflar2/exkernelmanager/utilities/e;

.field private q:Lflar2/exkernelmanager/utilities/f;

.field private r:Landroid/widget/ListView;

.field private s:Lflar2/exkernelmanager/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/e;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->q:Lflar2/exkernelmanager/utilities/f;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010d

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-virtual {v2, v3, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/el;

    invoke-direct {v3, p0, v2, p1, p2}, Lflar2/exkernelmanager/fragments/el;-><init>(Lflar2/exkernelmanager/fragments/SleeperActivity;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/SleeperActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->k()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e009b

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v2, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/en;

    invoke-direct {v3, p0, v1, p1, p2}, Lflar2/exkernelmanager/fragments/en;-><init>(Lflar2/exkernelmanager/fragments/SleeperActivity;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/f;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->q:Lflar2/exkernelmanager/utilities/f;

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->k()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/SleeperActivity;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->m()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->s:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private k()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/eo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/eo;-><init>(Lflar2/exkernelmanager/fragments/SleeperActivity;Lflar2/exkernelmanager/fragments/ej;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/eo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private l()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e00ad

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const v3, 0x7f0e00d6

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0e005d

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0e007a

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/em;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/em;-><init>(Lflar2/exkernelmanager/fragments/SleeperActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private m()Ljava/util/List;
    .locals 9

    const v4, 0x7f0e007a

    const v8, 0x7f02006b

    const v7, 0x7f020069

    const/4 v6, 0x0

    const/4 v5, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v2, 0x7f0e0051

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/system/bin/mpdecision"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f5

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e00ad

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->q:Lflar2/exkernelmanager/utilities/f;

    const-string v3, "busybox pidof mpdecision"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_19

    const-string v2, "mpdecision"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    const-string v2, "prefHotplugBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefHotplugBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper/up_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2ef

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Up threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/up_threshold"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSleeperUpThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperUpThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper/plug_all"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f8

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Run with all cores online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/plug_all"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    const v2, 0x7f0e008d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_4
    const-string v2, "prefSleeperPlugAllBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperPlugAllBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper/up_count_max"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Up count (tenths of a second)"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/up_count_max"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSleeperUpCountMaxBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperUpCountMaxBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_6
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper/down_count_max"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Down count (tenths of a second)"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/down_count_max"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSleeperDownCountMaxBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperDownCountMaxBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f1

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Max cores online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSleeperMaxOnlineBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperMaxOnlineBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper/suspend_max_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f0

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Cores online during suspend"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/suspend_max_online"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSleeperSuspendMaxOnlineBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperSuspendMaxOnlineBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_25

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_9
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x44c

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e008c

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/enabled"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_a
    const-string v2, "prefEnabledBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefEnabledBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_27

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/cores_on_touch"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x44d

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Cores on touch"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/cores_on_touch"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefCoresOnTouchBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCoresOnTouchBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x44e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Cpufreq unplug limit"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefCpufreqUnplugLimitBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCpufreqUnplugLimitBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_d
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/first_level"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x44f

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "First level"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/first_level"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefFirstLevelBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefFirstLevelBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/high_load_counter"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x450

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "High load counter"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/high_load_counter"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefHighLoadCounterBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefHighLoadCounterBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2b

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x451

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Load threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefLoadThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefLoadThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2c

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/max_load_counter"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x452

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Max load counter"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/max_load_counter"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefMaxLoadCounterBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefMaxLoadCounterBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_11
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/min_time_cpu_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x453

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Min time cpu online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/min_time_cpu_online"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefMinTimeCpuOnlineBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefMinTimeCpuOnlineBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_12
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/timer"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x454

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Timer"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/misc/mako_hotplug_control/timer"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefTimerBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefTimerBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2f

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_13
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control/suspend_frequency"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x455

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Suspend frequency"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/class/misc/mako_hotplug_control/suspend_frequency"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSuspendFrequencyBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSuspendFrequencyBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_30

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_14
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/powersaver_mode"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b1

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Powersaver mode"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/powersaver_mode"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugPowersaverModeBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugPowersaverModeBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_15
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/min_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Min online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/min_online"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugMinOnlineBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugMinOnlineBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_32

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_16
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/max_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Max online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/max_online"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlug/max_onlineBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlug/max_onlineBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_33

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_17
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/max_cores_screenoff"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b4

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Max cores screenoff"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/max_cores_screenoff"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugMaxCoresScreenoffBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugMaxCoresScreenoffBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_34

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_18
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/max_freq_screenoff"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b5

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Max freq screenoff"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/module/blu_plug/parameters/max_freq_screenoff"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugMaxFreqScreenoffBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugMaxFreqScreenoffBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_35

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_19
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_15
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/up_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b6

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Up threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/up_threshold"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugUpThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugUpThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_36

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_16
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/up_timer_cnt"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b7

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Up timer count"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/up_timer_cnt"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugUpTimerCntBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugUpTimerCntBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_37

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_17
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters/down_timer_cnt"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b8

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "Down timer count"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/blu_plug/parameters/down_timer_cnt"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBluPlugDownTimerCntBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBluPlugDownTimerCntBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_38

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_18
    return-object v0

    :cond_19
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const v2, 0x7f0e005d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1b
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_1c
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f4

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e008c

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_1d
    const-string v2, "prefSleeperEnabledBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSleeperEnabledBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_1d
    const v2, 0x7f0e008d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto :goto_1d

    :cond_1e
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto :goto_1e

    :cond_1f
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_20
    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_21
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_22
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_6

    :cond_23
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_24
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_8

    :cond_25
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_9

    :cond_26
    const v2, 0x7f0e008d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_27
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_b

    :cond_28
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_c

    :cond_29
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_d

    :cond_2a
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_e

    :cond_2b
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_f

    :cond_2c
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_10

    :cond_2d
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_11

    :cond_2e
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_12

    :cond_2f
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_13

    :cond_30
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_14

    :cond_31
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_15

    :cond_32
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_16

    :cond_33
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_17

    :cond_34
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_18

    :cond_35
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_19

    :cond_36
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1a

    :cond_37
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1b

    :cond_38
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1c
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/e;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/e;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030025

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->setContentView(I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->overridePendingTransition(II)V

    const v0, 0x7f0e0053

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/fragments/ej;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/ej;-><init>(Lflar2/exkernelmanager/fragments/SleeperActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->n:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c00bd

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->r:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->s:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->s:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->r:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->r:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->r:Landroid/widget/ListView;

    new-instance v1, Lflar2/exkernelmanager/fragments/ek;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/ek;-><init>(Lflar2/exkernelmanager/fragments/SleeperActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->k()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->s:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const-string v0, "prefSleeperUpThreshold"

    const-string v1, "/sys/devices/platform/msm_sleeper/up_threshold"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "prefSleeperEnabled"

    const-string v1, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    const-string v0, "prefSleeperUpCountMax"

    const-string v1, "/sys/devices/platform/msm_sleeper/up_count_max"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    const-string v0, "prefSleeperDownCountMax"

    const-string v1, "/sys/devices/platform/msm_sleeper/down_count_max"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    const-string v0, "prefSleeperMaxOnline"

    const-string v1, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    const-string v0, "prefSleeperSuspendMaxOnline"

    const-string v1, "/sys/devices/platform/msm_sleeper/suspend_max_online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->l()V

    goto :goto_0

    :sswitch_7
    const-string v0, "prefSleeperPlugAll"

    const-string v1, "/sys/devices/platform/msm_sleeper/plug_all"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_8
    const-string v0, "prefEnabled"

    const-string v1, "/sys/class/misc/mako_hotplug_control/enabled"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_9
    const-string v0, "prefCoresOnTouch"

    const-string v1, "/sys/class/misc/mako_hotplug_control/cores_on_touch"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_a
    const-string v0, "prefCpufreqUnplugLimit"

    const-string v1, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_b
    const-string v0, "prefFirstLevel"

    const-string v1, "/sys/class/misc/mako_hotplug_control/first_level"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_c
    const-string v0, "prefHighLoadCounter"

    const-string v1, "/sys/class/misc/mako_hotplug_control/high_load_counter"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_d
    const-string v0, "prefLoadThreshold"

    const-string v1, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_e
    const-string v0, "prefMaxLoadCounter"

    const-string v1, "/sys/class/misc/mako_hotplug_control/max_load_counter"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_f
    const-string v0, "prefMinTimeCpuOnline"

    const-string v1, "/sys/class/misc/mako_hotplug_control/min_time_cpu_online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_10
    const-string v0, "prefTimer"

    const-string v1, "/sys/class/misc/mako_hotplug_control/timer"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "prefSuspendFrequency"

    const-string v1, "/sys/class/misc/mako_hotplug_control/suspend_frequency"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_12
    const-string v0, "prefBluPlugEnabled"

    const-string v1, "/sys/module/blu_plug/parameters/enabled"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_13
    const-string v0, "prefBluPlugPowersaverMode"

    const-string v1, "/sys/module/blu_plug/parameters/powersaver_mode"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_14
    const-string v0, "prefBluPlugMinOnline"

    const-string v1, "/sys/module/blu_plug/parameters/min_online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "prefBluPlug/max_online"

    const-string v1, "/sys/module/blu_plug/parameters/max_online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_16
    const-string v0, "prefBluPlugMaxCoresScreenoff"

    const-string v1, "/sys/module/blu_plug/parameters/max_cores_screenoff"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_17
    const-string v0, "prefBluPlugMaxFreqScreenoff"

    const-string v1, "/sys/module/blu_plug/parameters/max_freq_screenoff"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_18
    const-string v0, "prefBluPlugUpThreshold"

    const-string v1, "/sys/module/blu_plug/parameters/up_threshold"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_19
    const-string v0, "prefBluPlugUpTimerCnt"

    const-string v1, "/sys/module/blu_plug/parameters/up_timer_cnt"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1a
    const-string v0, "prefBluPlugDownTimerCnt"

    const-string v1, "/sys/module/blu_plug/parameters/down_timer_cnt"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4b8 -> :sswitch_1a
        -0x4b7 -> :sswitch_19
        -0x4b6 -> :sswitch_18
        -0x4b5 -> :sswitch_17
        -0x4b4 -> :sswitch_16
        -0x4b3 -> :sswitch_15
        -0x4b2 -> :sswitch_14
        -0x4b1 -> :sswitch_13
        -0x4b0 -> :sswitch_12
        -0x455 -> :sswitch_11
        -0x454 -> :sswitch_10
        -0x453 -> :sswitch_f
        -0x452 -> :sswitch_e
        -0x451 -> :sswitch_d
        -0x450 -> :sswitch_c
        -0x44f -> :sswitch_b
        -0x44e -> :sswitch_a
        -0x44d -> :sswitch_9
        -0x44c -> :sswitch_8
        -0x2f8 -> :sswitch_7
        -0x2f5 -> :sswitch_6
        -0x2f4 -> :sswitch_1
        -0x2f3 -> :sswitch_3
        -0x2f2 -> :sswitch_2
        -0x2f1 -> :sswitch_4
        -0x2f0 -> :sswitch_5
        -0x2ef -> :sswitch_0
    .end sparse-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/SleeperActivity;->s:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    const/4 v0, 0x1

    return v0

    :sswitch_1
    const-string v0, "This is how you change doubletap2wake "

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2f4 -> :sswitch_0
        -0x2f3 -> :sswitch_0
        -0x2f2 -> :sswitch_0
        -0x2f1 -> :sswitch_0
        -0x2f0 -> :sswitch_0
        -0x2ef -> :sswitch_0
        -0x25 -> :sswitch_1
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/e;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->k()V

    return-void
.end method
