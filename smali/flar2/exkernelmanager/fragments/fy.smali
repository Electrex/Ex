.class Lflar2/exkernelmanager/fragments/fy;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/fragments/ey;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/fragments/ey;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/fy;->a:Lflar2/exkernelmanager/fragments/ey;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/fragments/ey;Lflar2/exkernelmanager/fragments/ez;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/fy;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/net/URL;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/fy;->a:Lflar2/exkernelmanager/fragments/ey;

    invoke-virtual {v0}, Lflar2/exkernelmanager/fragments/ey;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/fy;->a:Lflar2/exkernelmanager/fragments/ey;

    invoke-virtual {v0, p1}, Lflar2/exkernelmanager/fragments/ey;->a(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/fy;->a:Lflar2/exkernelmanager/fragments/ey;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ey;->c(Lflar2/exkernelmanager/fragments/ey;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/fy;->a:Lflar2/exkernelmanager/fragments/ey;

    const-string v1, "Not available"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/ey;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/fy;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/fy;->a(Ljava/lang/String;)V

    return-void
.end method
