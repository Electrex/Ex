.class public Lflar2/exkernelmanager/fragments/ac;
.super Landroid/app/Fragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field a:Lflar2/exkernelmanager/utilities/m;

.field b:Lflar2/exkernelmanager/utilities/f;

.field c:Lflar2/exkernelmanager/utilities/e;

.field d:Landroid/widget/ListView;

.field private e:Lflar2/exkernelmanager/a/a;

.field private f:Lflar2/exkernelmanager/powersave/a;

.field private g:Lflar2/exkernelmanager/performance/a;

.field private h:Landroid/content/Context;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/ImageView;

.field private n:I

.field private o:I

.field private p:Landroid/graphics/RectF;

.field private q:Landroid/graphics/RectF;

.field private r:Landroid/view/animation/AccelerateDecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    return-void
.end method

.method private static a(FFF)F
    .locals 1

    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    return-object p1
.end method

.method private a(I)V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/ac;->o:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/ac;->o:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iget v1, p0, Lflar2/exkernelmanager/fragments/ac;->o:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lflar2/exkernelmanager/fragments/ac;->a(FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->l:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->r:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/ac;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->m:Landroid/widget/ImageView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->l:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->r:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/ac;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->m:Landroid/widget/ImageView;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;F)V
    .locals 6

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p1}, Lflar2/exkernelmanager/fragments/ac;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p2}, Lflar2/exkernelmanager/fragments/ac;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    sub-float/2addr v0, v3

    mul-float/2addr v0, p3

    add-float/2addr v0, v3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, p3

    add-float/2addr v1, v3

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    mul-float/2addr v2, v5

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ac;->q:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ac;->p:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p3

    mul-float/2addr v3, v5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    sub-float v2, v3, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ac;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ac;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ac;->a(I)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ac;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e0045

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ac;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0, p2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private a(Z)V
    .locals 4

    if-eqz p1, :cond_0

    const-string v0, "444"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "prefCPUMin"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    :try_start_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/e/a/a/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    return-void

    :cond_0
    const-string v0, "666"

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/e/a/a/a;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ac;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "666"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "666"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "666"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(I)V
    .locals 4

    const v3, 0x7f0e00d9

    const-string v0, "prefMPDecisionBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    packed-switch p1, :pswitch_data_0

    :goto_0
    const-string v0, "prefMPDecision"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Disabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "start mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMPDecision_NW"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMPDecision_NS"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_ns"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMPDecision_TW"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_tw"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision"

    const-string v1, "Disabled"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "prefMPDecision_NW"

    const-string v1, "0 1.9 2.2 3.0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_NS"

    const-string v1, "0 0.9 1.9 3.0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_TW"

    const-string v1, "0 90 90 90"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision"

    const-string v1, "Aggressive"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "prefMPDecision_NW"

    const-string v1, "0 1.9 2.7 3.5"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_NS"

    const-string v1, "0 1.1 2.1 3.1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_TW"

    const-string v1, "0 140 90 90"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision"

    const-string v1, "Default"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "prefMPDecision_NW"

    const-string v1, "0 2.0 3.1 4.5"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_NS"

    const-string v1, "0 1.1 2.1 3.5"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_TW"

    const-string v1, "0 140 140 140"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision"

    const-string v1, "Battery Saving"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision"

    const-string v1, "Disabled"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "start mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefMPDecision"

    const-string v1, "Enabled"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ac;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ac;->b(I)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e009c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v2, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/ae;

    invoke-direct {v3, p0, v1, p1, p2}, Lflar2/exkernelmanager/fragments/ae;-><init>(Lflar2/exkernelmanager/fragments/ac;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/ac;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/ak;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/ak;-><init>(Lflar2/exkernelmanager/fragments/ac;Lflar2/exkernelmanager/fragments/ad;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private c(I)V
    .locals 3

    const-string v0, "prefThermBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "stop thermal-engine"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "start thermal-engine"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void

    :pswitch_0
    const-string v0, "prefTherm"

    const-string v1, "warmer"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "THERM="

    const-string v2, "THERM=1"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v2, "N5_elex"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "prefTherm"

    const-string v1, "stock"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "THERM="

    const-string v2, "THERM=3"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v2, "N5_stock"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "prefTherm"

    const-string v1, "cooler"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "THERM="

    const-string v2, "THERM=2"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v2, "N5_cool"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    const-string v1, "HTC_One_m8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    const-string v0, "prefTherm"

    const-string v1, "warmer"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "THERM="

    const-string v2, "THERM=1"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v2, "m8_elex"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "prefTherm"

    const-string v1, "stock"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "THERM="

    const-string v2, "THERM=2"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v2, "m8_stock"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    const-string v0, "prefTherm"

    const-string v1, "cooler"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v2, "m8_cool"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/ac;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ac;->c(I)V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/ac;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->p()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 3

    const-string v0, "prefCPUMin"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "prefCPUMin"

    const-string v1, "300000"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const-string v0, "prefCPUMin"

    const-string v1, "384000"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "prefCPUMin"

    const-string v1, "204000"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v0, "prefCPUMin"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d(I)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e010d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-virtual {v2, v3, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/ah;

    invoke-direct {v3, p0, v2, p1}, Lflar2/exkernelmanager/fragments/ah;-><init>(Lflar2/exkernelmanager/fragments/ac;[Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/ac;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->e:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "prefPowersaver"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefPerformance"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefPerformance"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->g:Lflar2/exkernelmanager/performance/a;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    :cond_0
    const-string v0, "prefPowersaver"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->f:Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void

    :cond_1
    const-string v0, "prefPowersaver"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->f:Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    goto :goto_0
.end method

.method private e(I)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e010d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies"

    invoke-virtual {v2, v3, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/ai;

    invoke-direct {v3, p0, v2, p1}, Lflar2/exkernelmanager/fragments/ai;-><init>(Lflar2/exkernelmanager/fragments/ac;[Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private f()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "prefPerformance"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefPowersaver"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefPowersaver"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->f:Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    :cond_0
    const-string v0, "prefPerformance"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->g:Lflar2/exkernelmanager/performance/a;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void

    :cond_1
    const-string v0, "prefPerformance"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->g:Lflar2/exkernelmanager/performance/a;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    goto :goto_0
.end method

.method private g()V
    .locals 3

    const-string v0, "prefCPUQuietEnabledBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    const-string v2, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "userspace"

    const-string v2, "/sys/devices/system/cpu/cpuquiet/current_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCPUQuietEnabled"

    const-string v1, "0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCPUQuietGovernor"

    const-string v1, "userspace"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "balanced"

    const-string v2, "/sys/devices/system/cpu/cpuquiet/current_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCPUQuietEnabled"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCPUQuietGovernor"

    const-string v1, "balanced"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private h()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v1, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00d7

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v0, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "Disabled"

    aput-object v2, v0, v3

    const-string v2, "Aggressive"

    aput-object v2, v0, v4

    const-string v2, "Default"

    aput-object v2, v0, v5

    const/4 v2, 0x3

    const-string v3, "Battery Saving"

    aput-object v3, v0, v2

    :goto_0
    new-instance v2, Lflar2/exkernelmanager/fragments/af;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/af;-><init>(Lflar2/exkernelmanager/fragments/ac;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v1}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void

    :cond_0
    new-array v0, v5, [Ljava/lang/String;

    const-string v2, "Disabled"

    aput-object v2, v0, v3

    const-string v2, "Enabled"

    aput-object v2, v0, v4

    goto :goto_0
.end method

.method private i()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e014a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "ElementalX"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "Stock"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Extra cool"

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/ag;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ag;-><init>(Lflar2/exkernelmanager/fragments/ac;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->b()Landroid/support/v7/app/ab;

    return-void
.end method

.method private j()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private k()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/GovernorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private l()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/GovernorC2Activity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private m()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/BoostActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private n()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private o()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e010e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"

    invoke-virtual {v1, v2, v3, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/aj;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/aj;-><init>(Lflar2/exkernelmanager/fragments/ac;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private p()Ljava/util/List;
    .locals 10

    const/4 v9, 0x2

    const v8, 0x7f02006b

    const v7, 0x7f020069

    const/4 v6, 0x0

    const/4 v5, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "unknown"

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v5, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00a0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0xa

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00cd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    const-string v2, "prefCPUMaxBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCPUMaxBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0xb

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00d2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefHTC"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_15

    const-string v2, "prefCPUMin"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "300000"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "prefCPUMin"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "384000"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    invoke-direct {p0, v5}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    :goto_2
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v4, 0x7f0e00df

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "prefCPUMinBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCPUMinBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :cond_2
    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x73

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00cc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefCPUC2MaxBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCPUC2MaxBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x74

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00d1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefCPUC2MinBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCPUC2MinBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0xc

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00cf

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefMaxScroffBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefMaxScroffBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_6
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x72

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0055

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2f6

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0054

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e007a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_7
    const-string v2, "prefCPUQuietEnabledBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCPUQuietEnabledBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/system/bin/mpdecision"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_21

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x13

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00d6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v3, "busybox pidof mpdecision"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1c

    const-string v2, "prefMPDecision"

    const-string v3, "Disabled"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_9
    const-string v2, "prefMPDecision"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefMPDecisionBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefMPDecisionBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_b
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/platform/msm_sleeper"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2ee

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0052

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/blu_plug/parameters"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/misc/mako_hotplug_control"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x2ee

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0052

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00a6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0xd

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0050

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefCPUGovBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefCPUGovBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0xe

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e004d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x75

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0056

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x71

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e004b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/system/etc/elementalx.conf"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-ge v1, v2, :cond_c

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e00d9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_b
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_c
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0149

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0xf

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e0148

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-le v2, v3, :cond_25

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v4, 0x7f0e00d9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v3, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_25

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "THERM="

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "THERM=1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    const-string v2, "ElementalX"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :cond_d
    :goto_d
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v4, 0x7f0e00d9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v4, 0x7f0e00b1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v3, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_e
    const-string v2, "prefThermBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefThermBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    :cond_f
    :goto_e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x7

    if-le v1, v2, :cond_11

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xb

    if-ge v1, v2, :cond_11

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    return-object v0

    :cond_12
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_15
    invoke-direct {p0, v6}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    goto/16 :goto_2

    :cond_16
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_17
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_4

    :cond_18
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_19
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_6

    :cond_1a
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_1b
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_8

    :cond_1c
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0 1.9 2.2 3.0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const-string v2, "prefMPDecision"

    const-string v3, "Aggressive"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_1d
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0 2.0 3.1 4.5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    const-string v2, "prefMPDecision"

    const-string v3, "Battery Saving"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_1e
    const-string v2, "prefMPDecision"

    const-string v3, "Default"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_1f
    const-string v2, "prefMPDecision"

    const-string v3, "Enabled"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_20
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_a

    :cond_21
    const-string v1, "prefMPDecision"

    const-string v2, "Disabled"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    :cond_22
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_c

    :cond_23
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "THERM="

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "THERM=2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    const-string v2, "Extra Cool"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_24
    const-string v2, "Stock"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_25
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v4, 0x7f0e00b1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v3, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_27

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "THERM="

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "THERM=1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    const-string v2, "ElementalX"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_26
    const-string v2, "Stock"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_27
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v3, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_28
    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_e
.end method


# virtual methods
.method public a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->d:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    const/4 v4, 0x1

    if-lt v2, v4, :cond_1

    iget v0, p0, Lflar2/exkernelmanager/fragments/ac;->n:I

    :cond_1
    neg-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const v6, 0x7f0b005d

    const/4 v5, 0x1

    const v0, 0x7f030032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "CPU"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0, v5}, Lflar2/exkernelmanager/fragments/ac;->setHasOptionsMenu(Z)V

    new-instance v0, Lflar2/exkernelmanager/powersave/a;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    invoke-direct {v0, v2}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->f:Lflar2/exkernelmanager/powersave/a;

    new-instance v0, Lflar2/exkernelmanager/performance/a;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    invoke-direct {v0, v2}, Lflar2/exkernelmanager/performance/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->g:Lflar2/exkernelmanager/performance/a;

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->d()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v3, v2, v5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    sput-boolean v5, Lflar2/exkernelmanager/a/a;->b:Z

    const v0, 0x7f0c007d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->d:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v3, v4}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->e:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->d:Landroid/widget/ListView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ac;->e:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->d:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090007

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->m:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->m:Landroid/widget/ImageView;

    const v3, 0x7f02006f

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b4

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->l:Landroid/widget/TextView;

    aget-object v3, v2, v5

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->k:Landroid/widget/TextView;

    aget-object v2, v2, v5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ac;->n:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/ac;->n:I

    neg-int v0, v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lflar2/exkernelmanager/fragments/ac;->o:I

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->r:Landroid/view/animation/AccelerateDecelerateInterpolator;

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->d:Landroid/widget/ListView;

    new-instance v2, Lflar2/exkernelmanager/fragments/ad;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ad;-><init>(Lflar2/exkernelmanager/fragments/ac;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :try_start_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/e/a/a/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefFirstRunSettings"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    const-string v0, "prefThermPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    return-object v1

    :cond_1
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ac;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/e/a/a/a;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->r:Landroid/view/animation/AccelerateDecelerateInterpolator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->a(I)V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->e:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->d(I)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->d(I)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->e(I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->e(I)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0, v2}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->d(I)V

    const-string v0, "prefHTC"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "prefCPUMin"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "300000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefCPUMin"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "384000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v2}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ac;->a(Z)V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->e()V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->f()V

    goto :goto_0

    :sswitch_7
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->h()V

    goto :goto_0

    :sswitch_8
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->j()V

    goto :goto_0

    :sswitch_9
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->o()V

    goto :goto_0

    :sswitch_a
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->k()V

    goto :goto_0

    :sswitch_b
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->l()V

    goto :goto_0

    :sswitch_c
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->m()V

    goto :goto_0

    :sswitch_d
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->n()V

    goto :goto_0

    :sswitch_e
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->i:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const v2, 0x7f0e00d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->i()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefTherm"

    sget-object v1, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/ac;->j:I

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->g()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2f6 -> :sswitch_f
        -0x2ee -> :sswitch_d
        -0xc7 -> :sswitch_6
        -0x75 -> :sswitch_b
        -0x74 -> :sswitch_3
        -0x73 -> :sswitch_2
        -0x72 -> :sswitch_8
        -0x71 -> :sswitch_c
        -0x13 -> :sswitch_7
        -0x12 -> :sswitch_5
        -0xf -> :sswitch_e
        -0xe -> :sswitch_a
        -0xd -> :sswitch_9
        -0xc -> :sswitch_4
        -0xb -> :sswitch_1
        -0xa -> :sswitch_0
    .end sparse-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->e:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ac;->h:Landroid/content/Context;

    const-string v1, "No help for this item"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    const/4 v0, 0x1

    return v0

    :sswitch_0
    const-string v0, "Max CPU frequency"

    const-string v1, "Choose the maximum CPU frequency to be used"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "Min CPU frequency"

    const-string v1, "This is for information only.  It is best not to set the minimum frequency."

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    const-string v0, "Max screen off frequency"

    const-string v1, "Choose the maximum CPU frequency to be used while the screen is off.  It is recommended not to set this too low."

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    const-string v0, "MPDecision settings"

    const-string v1, "Controls when CPUs come online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    const-string v0, "Powersave mode"

    const-string v1, "Reduce max CPU frequency, dim screen, turn off vibration and disable wake functions to save battery"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    const-string v0, "Performance mode"

    const-string v1, "Maximize CPU and GPU frequencies and put all CPU cores online"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_6
    const-string v0, "CPU Governor"

    const-string v1, "The governor controls frequency scaling for the CPU"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_7
    const-string v0, "Thermal throttling"

    const-string v1, "To prevent damage, the CPU frequency is reduced when the device gets hot.  This setting controls the temperature at which throttling begins.  Cooler means throttling starts earlier, thus keeping your device cooler at the expense of performance."

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xc7 -> :sswitch_5
        -0x13 -> :sswitch_3
        -0x12 -> :sswitch_4
        -0xf -> :sswitch_7
        -0xd -> :sswitch_6
        -0xc -> :sswitch_2
        -0xb -> :sswitch_1
        -0xa -> :sswitch_0
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0c017f
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/a/a;->b:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ac;->c()V

    return-void
.end method
