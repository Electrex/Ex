.class Lflar2/exkernelmanager/fragments/h;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/fragments/BackupActivity;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/h;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/fragments/BackupActivity;Lflar2/exkernelmanager/fragments/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/h;-><init>(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    const-string v0, "No such file"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/h;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-virtual {v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Save backup failed"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/h;->a:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->b(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/h;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/h;->a(Ljava/lang/String;)V

    return-void
.end method
