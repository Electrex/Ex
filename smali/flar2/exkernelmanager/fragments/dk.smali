.class public Lflar2/exkernelmanager/fragments/dk;
.super Landroid/app/Fragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Lflar2/exkernelmanager/a/a;

.field private c:Landroid/content/Context;

.field private d:Lflar2/exkernelmanager/utilities/m;

.field private e:Lflar2/exkernelmanager/utilities/f;

.field private f:Lflar2/exkernelmanager/utilities/e;

.field private g:I

.field private h:I

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ImageView;

.field private l:I

.field private m:I

.field private n:Landroid/graphics/RectF;

.field private o:Landroid/graphics/RectF;

.field private p:Landroid/view/animation/AccelerateDecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    iput v1, p0, Lflar2/exkernelmanager/fragments/dk;->g:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    return-void
.end method

.method public static a(FFF)F
    .locals 1

    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    return-object p1
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/dk;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method private a(I)V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/dk;->m:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/dk;->m:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iget v1, p0, Lflar2/exkernelmanager/fragments/dk;->m:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lflar2/exkernelmanager/fragments/dk;->a(FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->p:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/dk;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->k:Landroid/widget/ImageView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->p:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/dk;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->k:Landroid/widget/ImageView;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;F)V
    .locals 6

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p1}, Lflar2/exkernelmanager/fragments/dk;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p2}, Lflar2/exkernelmanager/fragments/dk;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    sub-float/2addr v0, v3

    mul-float/2addr v0, p3

    add-float/2addr v0, v3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, p3

    add-float/2addr v1, v3

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    mul-float/2addr v2, v5

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/dk;->o:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/dk;->n:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p3

    mul-float/2addr v3, v5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    sub-float v2, v3, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/dk;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/dk;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v2, 0x7f0e009b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v2, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/dm;

    invoke-direct {v3, p0, v1, p1, p2}, Lflar2/exkernelmanager/fragments/dm;-><init>(Lflar2/exkernelmanager/fragments/dk;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private b()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/ds;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/ds;-><init>(Lflar2/exkernelmanager/fragments/dk;Lflar2/exkernelmanager/fragments/dl;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/ds;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/dk;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/dk;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->k()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->c(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapoff "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "swapoff /dev/block/zram1"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "swapoff /dev/block/zram2"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "swapoff /dev/block/zram3"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "swapoff "

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapon "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "swapon /dev/block/zram1"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "swapon /dev/block/zram2"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "swapon /dev/block/zram3"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "swapon "

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/dk;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->b:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private d()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/BatteryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/dk;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v2, 0x7f0e0110

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/block/mmcblk0/queue/scheduler"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/dn;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/dn;-><init>(Lflar2/exkernelmanager/fragments/dk;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private f()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v2, 0x7f0e010a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/leds/charging/trigger"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/do;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/do;-><init>(Lflar2/exkernelmanager/fragments/dk;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private g()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v2, 0x7f0e00fa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "128"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "256"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "512"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "1024"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "2048"

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/dp;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/dp;-><init>(Lflar2/exkernelmanager/fragments/dk;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v2, 0x7f0e0146

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/proc/sys/net/ipv4/tcp_available_congestion_control"

    invoke-virtual {v1, v2, v3, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/dq;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/dq;-><init>(Lflar2/exkernelmanager/fragments/dk;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private i()V
    .locals 2

    const-string v0, "prefExFATBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "lsmod"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "exfat"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "rmmod exfat"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefExFAT"

    const-string v1, "0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "insmod /system/lib/modules/exfat.ko"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefExFAT"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private j()V
    .locals 8

    const/4 v3, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v2, 0x7f0e002e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "4200mV (~93%)"

    aput-object v2, v1, v4

    const-string v2, "4100mV (~83%)"

    aput-object v2, v1, v5

    const-string v2, "4000mV (~73%)"

    aput-object v2, v1, v6

    const-string v2, "4300 mV (Disabled)"

    aput-object v2, v1, v7

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "4200"

    aput-object v3, v2, v4

    const-string v3, "4100"

    aput-object v3, v2, v5

    const-string v3, "4000"

    aput-object v3, v2, v6

    const-string v3, "4300"

    aput-object v3, v2, v7

    new-instance v3, Lflar2/exkernelmanager/fragments/dr;

    invoke-direct {v3, p0, v2}, Lflar2/exkernelmanager/fragments/dr;-><init>(Lflar2/exkernelmanager/fragments/dk;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private k()Ljava/util/List;
    .locals 11

    const v10, 0x7f0e007a

    const v9, 0x7f02006b

    const v8, 0x7f020069

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_0

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00bb

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x32

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00bc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSchedBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSchedBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x33

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00fa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/block/mmcblk0/queue/read_ahead_kb"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefReadaheadBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefReadaheadBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/sync/parameters/fsync_enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x34

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00a1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/sync/parameters/fsync_enabled"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_2
    const-string v2, "prefFsyncBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefFsyncBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "ls /system/lib/modules/"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "exfat.ko"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00b1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x35

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e009d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "lsmod"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "exfat"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefExFAT"

    const-string v3, "1"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    const-string v2, "prefExFATBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefExFATBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e016a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x36

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget v2, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    if-ne v2, v6, :cond_17

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "[^0-9]"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    int-to-float v4, v2

    const v5, 0x4541c000    # 3100.0f

    div-float/2addr v4, v5

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v3, 0x76c

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/a/o;->e(I)V

    add-int/lit16 v2, v2, -0x4b0

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    :goto_6
    const-string v2, "prefVibBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefVibBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00f5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x45

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0030

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x37

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0169

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_8
    const-string v2, "prefFCBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefFCBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_9
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/i2c-0/0-006a/float_voltage"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x38

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e002f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/i2c-0/0-006a/float_voltage"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "4300"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_a
    const-string v2, "prefBLEBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBLEBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/msm_otg/parameters/usbhost_charge_mode"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00eb

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/msm_otg/parameters/usbhost_charge_mode"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_c
    const-string v2, "prefOTGCBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefOTGCBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_d
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/block/zram0"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "prefSubVersion"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00d0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0177

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v2}, Lflar2/exkernelmanager/utilities/m;->a()Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_e
    const-string v2, "prefzRam0Boot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefzRam0Boot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x40

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0139

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/proc/sys/vm/swappiness"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSwappinessBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSwappinessBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00d5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/dk;->g:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3a

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00ca

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/dk;->g:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/dk;->g:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    :cond_8
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_11
    const-string v2, "prefLidBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefLidBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_12
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3b

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0147

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefTCPCongBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefTCPCongBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_13
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/android_touch/logo2menu"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3c

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00c7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/android_touch/logo2menu"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_14
    const-string v2, "prefL2MBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefL2MBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_15
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/leds/button-backlight/blink_buttons"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3d

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0038

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/leds/button-backlight/blink_buttons"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_16
    const-string v2, "prefBLNBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefBLNBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_17
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/leds/charging/trigger"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x41

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0042

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/leds/charging/trigger"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefChargeLEDBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefChargeLEDBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_18
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/wakeup/parameters/enable_si_ws"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x42

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e0111

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/wakeup/parameters/enable_si_ws"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_19
    const-string v2, "prefSensorIndBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSensorIndBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2b

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->f:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/proc/sys/kernel/rr_interval"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x44

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e00d4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/proc/sys/kernel/rr_interval"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefBFSRRIntervalBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSensorIndBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2c

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x7

    if-le v1, v2, :cond_10

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xb

    if-ge v1, v2, :cond_10

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    return-object v0

    :cond_11
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_13
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_14
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_15
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefExFAT"

    const-string v3, "0"

    invoke-static {v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_16
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_17
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v4, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    iget v5, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->e(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(I)V

    goto/16 :goto_6

    :cond_18
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_19
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_1a
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_9

    :cond_1b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v4, "/sys/devices/i2c-0/0-006a/float_voltage"

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mV"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_1c
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_b

    :cond_1d
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_1e
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_d

    :cond_1f
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_e

    :cond_20
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_f

    :cond_21
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_10

    :cond_22
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_11

    :cond_23
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_12

    :cond_24
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_13

    :cond_25
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const v3, 0x7f0e008d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_14

    :cond_26
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_15

    :cond_27
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_16

    :cond_28
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_17

    :cond_29
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_18

    :cond_2a
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_19

    :cond_2b
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1a

    :cond_2c
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1b
.end method


# virtual methods
.method public a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    const/4 v4, 0x1

    if-lt v2, v4, :cond_1

    iget v0, p0, Lflar2/exkernelmanager/fragments/dk;->l:I

    :cond_1
    neg-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f0b005d

    const/4 v6, 0x6

    const/4 v5, 0x1

    const v0, 0x7f030032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "Miscellaneous"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0, v5}, Lflar2/exkernelmanager/fragments/dk;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c007d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v3, v4}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->b:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/dk;->b:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    sput-boolean v5, Lflar2/exkernelmanager/a/a;->b:Z

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_1

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b7

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->k:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->k:Landroid/widget/ImageView;

    const v3, 0x7f02007a

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b4

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->j:Landroid/widget/TextView;

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->i:Landroid/widget/TextView;

    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/dk;->l:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/dk;->l:I

    neg-int v0, v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/dk;->e:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lflar2/exkernelmanager/fragments/dk;->m:I

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->p:Landroid/view/animation/AccelerateDecelerateInterpolator;

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->a:Landroid/widget/ListView;

    new-instance v2, Lflar2/exkernelmanager/fragments/dl;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/dl;-><init>(Lflar2/exkernelmanager/fragments/dk;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/dk;->g:I

    const-string v0, "preflidPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/dk;->g:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->c()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->d:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    const-string v0, "prefvibPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/dk;->h:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefFirstRunSettings"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-object v1

    :cond_1
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/dk;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->p:Landroid/view/animation/AccelerateDecelerateInterpolator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/dk;->a(I)V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->b:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->e()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->g()V

    goto :goto_0

    :pswitch_3
    const-string v0, "prefFsync"

    const-string v1, "/sys/module/sync/parameters/fsync_enabled"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->i()V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->d()V

    goto :goto_0

    :pswitch_6
    const-string v0, "prefFC"

    const-string v1, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->j()V

    goto :goto_0

    :pswitch_8
    const-string v0, "prefOTGC"

    const-string v1, "/sys/module/msm_otg/parameters/usbhost_charge_mode"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_9
    const-string v0, "prefLid"

    sget-object v1, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    const-string v2, "preflidPath"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_a
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->h()V

    goto :goto_0

    :pswitch_b
    const-string v0, "prefL2M"

    const-string v1, "/sys/android_touch/logo2menu"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_c
    const-string v0, "prefBLN"

    const-string v1, "/sys/class/leds/button-backlight/blink_buttons"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_d
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->f()V

    goto :goto_0

    :pswitch_e
    const-string v0, "prefzRam0"

    const-string v1, "/dev/block/zram0"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_f
    const-string v0, "prefSwappiness"

    const-string v1, "/proc/sys/vm/swappiness"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_10
    const-string v0, "prefSensorInd"

    const-string v1, "/sys/module/wakeup/parameters/enable_si_ws"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_11
    const-string v0, "prefBFSRRInterval"

    const-string v1, "/proc/sys/kernel/rr_interval"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/dk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x45
        :pswitch_5
        :pswitch_11
        :pswitch_0
        :pswitch_10
        :pswitch_d
        :pswitch_f
        :pswitch_0
        :pswitch_e
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->b:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/dk;->c:Landroid/content/Context;

    const-string v1, "No help for this item"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/a/a;->b:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/dk;->b()V

    return-void
.end method
