.class public Lflar2/exkernelmanager/fragments/gf;
.super Landroid/app/Fragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lflar2/exkernelmanager/utilities/m;

.field private c:Lflar2/exkernelmanager/utilities/e;

.field private d:Lflar2/exkernelmanager/utilities/f;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Landroid/widget/ListView;

.field private l:Lflar2/exkernelmanager/a/a;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/ImageView;

.field private p:I

.field private q:I

.field private r:Landroid/graphics/RectF;

.field private s:Landroid/graphics/RectF;

.field private t:Landroid/view/animation/AccelerateDecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->d:Lflar2/exkernelmanager/utilities/f;

    iput v1, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/gf;->f:I

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    return-void
.end method

.method public static a(FFF)F
    .locals 1

    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    return-object p1
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/gf;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/gf;->q:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/gf;->q:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iget v1, p0, Lflar2/exkernelmanager/fragments/gf;->q:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lflar2/exkernelmanager/fragments/gf;->a(FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->m:Landroid/widget/TextView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->n:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->t:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/gf;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->o:Landroid/widget/ImageView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->n:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->t:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/gf;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->o:Landroid/widget/ImageView;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;F)V
    .locals 6

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p1}, Lflar2/exkernelmanager/fragments/gf;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p2}, Lflar2/exkernelmanager/fragments/gf;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    sub-float/2addr v0, v3

    mul-float/2addr v0, p3

    add-float/2addr v0, v3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, p3

    add-float/2addr v1, v3

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    mul-float/2addr v2, v5

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->s:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p3

    mul-float/2addr v3, v5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    sub-float v2, v3, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/gf;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/gf;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->c()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "AUTO"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "AUTO"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUTO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "OFF"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OFF"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/gf;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/gf;->f:I

    const-string v0, "prefs2wPath"

    iget v1, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefdt2wPath"

    iget v1, p0, Lflar2/exkernelmanager/fragments/gf;->f:I

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    sget-object v0, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    iget v1, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    sget-object v0, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    iget v1, p0, Lflar2/exkernelmanager/fragments/gf;->f:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    sget-object v0, Lflar2/exkernelmanager/r;->o:[Ljava/lang/String;

    iget v1, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->i:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/gq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/gq;-><init>(Lflar2/exkernelmanager/fragments/gf;Lflar2/exkernelmanager/fragments/gg;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/gq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/gf;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->c()V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/gf;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    return v0
.end method

.method private d()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e007f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e007a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e0090

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e0091

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/gi;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gi;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/gf;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/gf;->f:I

    return v0
.end method

.method private e()V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e013e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-array v1, v7, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e0142

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e0141

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e0143

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e0140

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    new-array v2, v7, [Z

    fill-array-data v2, :array_0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v5, :cond_0

    aput-boolean v5, v2, v8

    :cond_0
    and-int/lit8 v4, v3, 0x2

    if-ne v4, v6, :cond_1

    aput-boolean v5, v2, v5

    :cond_1
    and-int/lit8 v4, v3, 0x4

    if-ne v4, v7, :cond_2

    aput-boolean v5, v2, v6

    :cond_2
    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    aput-boolean v5, v2, v9

    :cond_3
    new-instance v3, Lflar2/exkernelmanager/fragments/gj;

    invoke-direct {v3, p0, v2}, Lflar2/exkernelmanager/fragments/gj;-><init>(Lflar2/exkernelmanager/fragments/gf;[Z)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/gk;

    invoke-direct {v3, p0, v2}, Lflar2/exkernelmanager/fragments/gk;-><init>(Lflar2/exkernelmanager/fragments/gf;[Z)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic f(Lflar2/exkernelmanager/fragments/gf;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v1, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e013a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v0, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e00df

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v8, [Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e007a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e00f3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e00bd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e00f2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    :goto_0
    new-instance v2, Lflar2/exkernelmanager/fragments/gl;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gl;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v1}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void

    :cond_0
    new-array v0, v8, [Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e007a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e0142

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e0141

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v3, 0x7f0e013f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    goto :goto_0
.end method

.method static synthetic g(Lflar2/exkernelmanager/fragments/gf;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->l:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private g()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e007e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e007a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e0089

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e008a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e008b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/gm;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gm;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private h()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e007f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e007a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e0089

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e008a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e008b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/gn;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gn;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private i()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e013e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e007a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e0144

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e013b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/go;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/go;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private j()V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e013c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e00f2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e00f3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e00bd

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/gp;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gp;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private k()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e0172

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e0171

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/android_touch/wake_timeout"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/gh;

    invoke-direct {v3, p0, v1}, Lflar2/exkernelmanager/fragments/gh;-><init>(Lflar2/exkernelmanager/fragments/gf;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private l()Ljava/util/List;
    .locals 11

    const v10, 0x7f0e007a

    const v9, 0x7f02006b

    const v8, 0x7f020069

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e016f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v4, "/sys/android_touch/wake_gestures"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00b1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v0, v4, :cond_2

    :cond_1
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x1e

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e016e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/wake_gestures"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    const-string v4, "prefWGBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefWGBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x25

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e007f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "N"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    :cond_3
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_2
    const-string v4, "prefDT2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefDT2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1b

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_3
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x24

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_4
    const-string v4, "prefS2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefS2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x23

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_6
    const-string v4, "prefS2SBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefS2SBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_26

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_6
    :goto_8
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_key/doubletap2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    new-instance v4, Lflar2/exkernelmanager/a/o;

    invoke-direct {v4}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v4, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v5, -0x26

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e007e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "/sys/android_key/doubletap2sleep"

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_47

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e008e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_9
    const-string v5, "prefDT2SBoot"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v5, "prefDT2SBoot"

    invoke-static {v5}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4a

    invoke-virtual {v4, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v4, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_a
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->i:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x27

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00f1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->i:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4b

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_b
    const-string v4, "prefPDBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefPDBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4c

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_c
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_8
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/camera_gesture"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x28

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e003e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/camera_gesture"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4d

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_d
    const-string v4, "prefWGCamBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefWGCamBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4e

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_e
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_9
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/vib_strength"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00a4

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x29

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/vib_strength"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x3fd55555

    int-to-float v7, v4

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const/16 v5, 0x3c

    invoke-virtual {v0, v5}, Lflar2/exkernelmanager/a/o;->e(I)V

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(I)V

    const-string v4, "prefWGVibBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefWGVibBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4f

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_f
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_a
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/orientation"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x2a

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/orientation"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_50

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0104

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_10
    const-string v4, "prefOrientBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefOrientBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_52

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_11
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_b
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/shortsweep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x2b

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0134

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/shortsweep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_53

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_12
    const-string v4, "prefShortSweepBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefShortSweepBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_54

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_13
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_c
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/wake_timeout"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x2c

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e016d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/wake_timeout"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_55

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_14
    const-string v4, "prefWTOBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefWTOBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_56

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_15
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_d
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/module/qpnp_power_on/parameters/pwrkey_suspend"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/pwrkey_suspend"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    :cond_e
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x2d

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00f9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/module/qpnp_power_on/parameters/pwrkey_suspend"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "N"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_57

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_16
    const-string v4, "prefPwrKeySuspendBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefPwrKeySuspendBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_59

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_17
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_f
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/lid_suspend"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x2e

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00cb

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/lid_suspend"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5a

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_18
    const-string v4, "prefWGLidBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefWGLidBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5b

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_19
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_10
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/android_touch/logo2wake"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x2f

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00c8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/logo2wake"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5c

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_1a
    const-string v4, "prefL2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefL2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5d

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1b
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_11
    if-eqz v0, :cond_12

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v4, 0x7f0e00e4

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x7

    if-le v0, v2, :cond_13

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0xb

    if-ge v0, v2, :cond_13

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    return-object v3

    :cond_14
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_16
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_17

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Y"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    :cond_17
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e00d9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e00af

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    :cond_18
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0090

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_19
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_1a
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0091

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_1b
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_1c
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    and-int/lit8 v6, v4, 0x1

    if-ne v6, v2, :cond_1d

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v7, 0x7f0e0102

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1d
    and-int/lit8 v6, v4, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1e

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v7, 0x7f0e00bf

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1e
    and-int/lit8 v6, v4, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1f

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v7, 0x7f0e0165

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1f
    and-int/lit8 v4, v4, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_20

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e0080

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_20
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_21
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_22
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0094

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_23
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e00dd

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_24
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0098

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_25
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_26
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_27
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00b4

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x25

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e007f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_1c
    const-string v4, "prefDT2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefDT2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2b

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1d
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_1e
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x24

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_1f
    const-string v4, "prefS2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefS2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2e

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_20
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto/16 :goto_8

    :cond_28
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1c

    :cond_29
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0092

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1c

    :cond_2a
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0097

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1c

    :cond_2b
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1d

    :cond_2c
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e009a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1f

    :cond_2d
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0099

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1f

    :cond_2e
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_20

    :cond_2f
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00df

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e00db

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    :cond_30
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x24

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_32

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_21
    const-string v4, "prefS2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefS2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_37

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_22
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x23

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_39

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e00df

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_38

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0096

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_23
    const-string v4, "prefS2SBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefS2SBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3e

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_24
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_25
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_44

    iget v4, p0, Lflar2/exkernelmanager/fragments/gf;->f:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_44

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_44

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x25

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e007f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "OFF"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_31

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ON"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_42

    :cond_31
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_26
    const-string v4, "prefDT2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefDT2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_43

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_27
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto/16 :goto_8

    :cond_32
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    and-int/lit8 v6, v4, 0x1

    if-ne v6, v2, :cond_33

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v7, 0x7f0e0102

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_33
    and-int/lit8 v6, v4, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_34

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v7, 0x7f0e00bf

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_34
    and-int/lit8 v6, v4, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_35

    iget-object v6, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v7, 0x7f0e0165

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_35
    and-int/lit8 v4, v4, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_36

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e0080

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_36
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_21

    :cond_37
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_22

    :cond_38
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_23

    :cond_39
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e00df

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3a

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0093

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_23

    :cond_3a
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0094

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_23

    :cond_3b
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e00df

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3c

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0095

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_23

    :cond_3c
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0098

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_23

    :cond_3d
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_23

    :cond_3e
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_24

    :cond_3f
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x24

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e013e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_40

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_28
    const-string v4, "prefS2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefS2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_41

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_29
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto/16 :goto_25

    :cond_40
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto :goto_28

    :cond_41
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto :goto_29

    :cond_42
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_26

    :cond_43
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_27

    :cond_44
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x25

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e007f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_45

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_2a
    const-string v4, "prefDT2WBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefDT2WBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_46

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_2b
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto/16 :goto_8

    :cond_45
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto :goto_2a

    :cond_46
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto :goto_2b

    :cond_47
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "/sys/android_key/doubletap2sleep"

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_48

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e0092

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_48
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "/sys/android_key/doubletap2sleep"

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "3"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_49

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e0097

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_49
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v5, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_4a
    invoke-virtual {v4, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v4, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_a

    :cond_4b
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_b

    :cond_4c
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_c

    :cond_4d
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_4e
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_e

    :cond_4f
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_f

    :cond_50
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/orientation"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_51

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0105

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_10

    :cond_51
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e0106

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_10

    :cond_52
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_11

    :cond_53
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_12

    :cond_54
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_13

    :cond_55
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "/sys/android_touch/wake_timeout"

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v6, 0x7f0e0170

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_14

    :cond_56
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_15

    :cond_57
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/android_touch/pwrkey_suspend"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_58

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_16

    :cond_58
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_16

    :cond_59
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_17

    :cond_5a
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_18

    :cond_5b
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_19

    :cond_5c
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1a

    :cond_5d
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1b

    :cond_5e
    move v0, v2

    goto/16 :goto_25

    :cond_5f
    move v0, v2

    goto/16 :goto_1e
.end method


# virtual methods
.method public a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    const/4 v4, 0x1

    if-lt v2, v4, :cond_1

    iget v0, p0, Lflar2/exkernelmanager/fragments/gf;->p:I

    :cond_1
    neg-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f0b005d

    const/4 v6, 0x3

    const/4 v5, 0x1

    const v0, 0x7f030032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "Wake"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0, v5}, Lflar2/exkernelmanager/fragments/gf;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    const v0, 0x7f0c007d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v3, v4}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->l:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/gf;->l:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    sput-boolean v5, Lflar2/exkernelmanager/a/a;->b:Z

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_1

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b7

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->o:Landroid/widget/ImageView;

    const v3, 0x7f020090

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b4

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->n:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->n:Landroid/widget/TextView;

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->m:Landroid/widget/TextView;

    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/gf;->p:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/gf;->p:I

    neg-int v0, v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/gf;->d:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lflar2/exkernelmanager/fragments/gf;->q:I

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->t:Landroid/view/animation/AccelerateDecelerateInterpolator;

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->k:Landroid/widget/ListView;

    new-instance v2, Lflar2/exkernelmanager/fragments/gg;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/gg;-><init>(Lflar2/exkernelmanager/fragments/gf;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->b()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->c()V

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefFirstRunSettings"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-object v1

    :cond_1
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/gf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->t:Landroid/view/animation/AccelerateDecelerateInterpolator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/gf;->a(I)V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    const v3, 0x7f0e00b4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->l:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "prefWG"

    const-string v1, "/sys/android_touch/wake_gestures"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefS2S"

    const-string v1, "/sys/android_touch/sweep2sleep"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->f()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/android_touch/wake_gestures"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e00df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e00db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->e()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->i()V

    goto :goto_0

    :cond_3
    const-string v0, "prefS2W"

    sget-object v1, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/gf;->e:I

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/android_touch/wake_gestures"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e00d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e00af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->d()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->h()V

    goto/16 :goto_0

    :cond_6
    const-string v0, "prefDT2W"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->g()V

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "prefPD"

    sget-object v1, Lflar2/exkernelmanager/r;->o:[Ljava/lang/String;

    const-string v2, "prefs2wPath"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    const-string v0, "prefWGCam"

    const-string v1, "/sys/android_touch/camera_gesture"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const v2, 0x7f0e00e7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :pswitch_9
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->j()V

    goto/16 :goto_0

    :pswitch_a
    const-string v0, "prefShortSweep"

    const-string v1, "/sys/android_touch/shortsweep"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_b
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->k()V

    goto/16 :goto_0

    :pswitch_c
    const-string v0, "prefPwrKeySuspend"

    const-string v1, "/sys/module/qpnp_power_on/parameters/pwrkey_suspend"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefPwrKeySuspend"

    const-string v1, "/sys/android_touch/pwrkey_suspend"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_d
    const-string v0, "prefWGLid"

    const-string v1, "/sys/android_touch/lid_suspend"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_e
    const-string v0, "prefL2W"

    const-string v1, "/sys/android_touch/logo2wake"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/gf;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch -0x2f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->l:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const-string v1, "No help for this item"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/gf;->a:Landroid/content/Context;

    const-string v1, "This is how you change doubletap2wake "

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x25
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/a/a;->b:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/gf;->c()V

    return-void
.end method
