.class Lflar2/exkernelmanager/fragments/aq;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Landroid/widget/Button;

.field final synthetic c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/CPUTimeActivity;Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/aq;->a:Landroid/widget/Button;

    iput-object p3, p0, Lflar2/exkernelmanager/fragments/aq;->b:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->d(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->d(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->d(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->d(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    :goto_2
    move v2, v1

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->b(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-virtual {v0}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->k()I

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->f(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/view/View;

    move-result-object v1

    neg-int v2, v0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->e(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->g(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/view/View;

    move-result-object v1

    neg-int v2, v0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->e(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->h(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->e(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setTranslationY(F)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->f(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/aq;->c:Lflar2/exkernelmanager/fragments/CPUTimeActivity;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->e(Lflar2/exkernelmanager/fragments/CPUTimeActivity;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v4}, Lflar2/exkernelmanager/fragments/CPUTimeActivity;->a(Lflar2/exkernelmanager/fragments/CPUTimeActivity;FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aq;->a:Landroid/widget/Button;

    mul-float v2, v0, v5

    sub-float v2, v4, v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setAlpha(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/aq;->b:Landroid/widget/Button;

    mul-float/2addr v0, v5

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setAlpha(F)V

    return-void

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v3, v2

    goto/16 :goto_1

    :cond_3
    move v1, v2

    goto/16 :goto_2
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method
