.class public Lflar2/exkernelmanager/fragments/es;
.super Landroid/app/Fragment;


# instance fields
.field a:Landroid/view/ViewGroup;

.field b:Landroid/support/v7/widget/CardView;

.field c:Landroid/support/v7/widget/CardView;

.field d:Landroid/support/v7/widget/CardView;

.field e:Landroid/support/v7/widget/CardView;

.field f:Landroid/support/v7/widget/CardView;

.field private g:Landroid/content/Context;

.field private h:Lflar2/exkernelmanager/utilities/e;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->h:Lflar2/exkernelmanager/utilities/e;

    return-void
.end method

.method private a()V
    .locals 5

    const v4, 0x7f04001e

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->g:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/es;->b:Landroid/support/v7/widget/CardView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/CardView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->g:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/es;->c:Landroid/support/v7/widget/CardView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/CardView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->g:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v2, 0x177

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/es;->d:Landroid/support/v7/widget/CardView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/CardView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->g:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/es;->e:Landroid/support/v7/widget/CardView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/CardView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->g:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v2, 0x1a9

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/es;->f:Landroid/support/v7/widget/CardView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/CardView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/es;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/es;->b()V

    return-void
.end method

.method private b()V
    .locals 5

    const v4, 0x7f0a0006

    const-string v0, "/sdcard/last_kmsg.txt"

    new-instance v1, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v1}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cat /proc/last_kmsg > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "No such file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cat /sys/fs/pstore/console-ramoops >> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "No such file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/c/a/n;->a(Landroid/content/Context;)Lcom/c/a/n;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/c/a/n;->a(I)Lcom/c/a/n;

    move-result-object v0

    const-string v1, "No log available"

    invoke-virtual {v0, v1}, Lcom/c/a/n;->a(Ljava/lang/CharSequence;)Lcom/c/a/n;

    move-result-object v0

    invoke-static {v0}, Lcom/c/a/aa;->a(Lcom/c/a/n;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/c/a/n;->a(Landroid/content/Context;)Lcom/c/a/n;

    move-result-object v1

    sget-object v2, Lcom/c/a/x;->c:Lcom/c/a/x;

    invoke-virtual {v1, v2}, Lcom/c/a/n;->a(Lcom/c/a/x;)Lcom/c/a/n;

    move-result-object v1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/c/a/n;->a(I)Lcom/c/a/n;

    move-result-object v1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/c/a/n;->b(I)Lcom/c/a/n;

    move-result-object v1

    const-string v2, "CLOSE"

    invoke-virtual {v1, v2}, Lcom/c/a/n;->b(Ljava/lang/CharSequence;)Lcom/c/a/n;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saved as "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/c/a/n;->a(Ljava/lang/CharSequence;)Lcom/c/a/n;

    move-result-object v0

    invoke-static {v0}, Lcom/c/a/aa;->a(Lcom/c/a/n;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x8

    const v0, 0x7f030035

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0c011d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->a:Landroid/view/ViewGroup;

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->g:Landroid/content/Context;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "Tools"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/fragments/es;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v3, v2, v5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->j:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090007

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->j:Landroid/widget/ImageView;

    const v3, 0x7f020089

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->i:Landroid/widget/TextView;

    aget-object v2, v2, v5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f0c011e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->b:Landroid/support/v7/widget/CardView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->b:Landroid/support/v7/widget/CardView;

    new-instance v2, Lflar2/exkernelmanager/fragments/et;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/et;-><init>(Lflar2/exkernelmanager/fragments/es;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0120

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->c:Landroid/support/v7/widget/CardView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->h:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    new-instance v3, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v3}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    sget-object v4, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->c:Landroid/support/v7/widget/CardView;

    new-instance v2, Lflar2/exkernelmanager/fragments/eu;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/eu;-><init>(Lflar2/exkernelmanager/fragments/es;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    const v0, 0x7f0c0122

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->d:Landroid/support/v7/widget/CardView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->d:Landroid/support/v7/widget/CardView;

    new-instance v2, Lflar2/exkernelmanager/fragments/ev;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ev;-><init>(Lflar2/exkernelmanager/fragments/es;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0124

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->e:Landroid/support/v7/widget/CardView;

    const-string v0, "prefHTC"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->e:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/CardView;->setVisibility(I)V

    :goto_2
    const v0, 0x7f0c0126

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/es;->f:Landroid/support/v7/widget/CardView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->f:Landroid/support/v7/widget/CardView;

    new-instance v2, Lflar2/exkernelmanager/fragments/ex;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ex;-><init>(Lflar2/exkernelmanager/fragments/es;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/es;->a()V

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefFirstRunSettings"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-object v1

    :cond_1
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/es;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->i:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->c:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/CardView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/es;->e:Landroid/support/v7/widget/CardView;

    new-instance v2, Lflar2/exkernelmanager/fragments/ew;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ew;-><init>(Lflar2/exkernelmanager/fragments/es;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method
