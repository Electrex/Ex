.class public Lflar2/exkernelmanager/fragments/GovernorActivity;
.super Landroid/support/v7/app/ad;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field n:Lflar2/exkernelmanager/utilities/h;

.field private o:Lflar2/exkernelmanager/utilities/m;

.field private p:Lflar2/exkernelmanager/utilities/e;

.field private q:Lflar2/exkernelmanager/utilities/f;

.field private r:Ljava/lang/String;

.field private s:Landroid/widget/ListView;

.field private t:Lflar2/exkernelmanager/a/a;

.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->q:Lflar2/exkernelmanager/utilities/f;

    const/4 v0, 0x0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->r:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/GovernorActivity;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e009b

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v2, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/cg;

    invoke-direct {v3, p0, v1, p1, p2}, Lflar2/exkernelmanager/fragments/cg;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/GovernorActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->k()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->k()V

    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/GovernorActivity;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->m()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010d

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-virtual {v2, v3, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/ch;

    invoke-direct {v3, p0, v2, p1, p2}, Lflar2/exkernelmanager/fragments/ch;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/GovernorActivity;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->t:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030041

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v0, 0x7f0c0150

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    const v1, 0x7f0c0151

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    const v2, 0x7f0c0152

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    const v3, 0x7f0c0153

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v7, v8}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v7, v8, v9}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v7

    new-instance v5, Landroid/widget/ArrayAdapter;

    const v8, 0x1090009

    invoke-direct {v5, p0, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v1, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v4, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, ","

    invoke-virtual {v8, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const/4 v5, 0x0

    array-length v10, v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v10, :cond_4

    aget-object v11, v7, v4

    const/4 v12, 0x0

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    const/4 v12, 0x1

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual {v1, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_1
    const/4 v12, 0x2

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    const/4 v12, 0x3

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_3
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    new-instance v4, Lflar2/exkernelmanager/fragments/ci;

    invoke-direct {v4, p0, v7, p2}, Lflar2/exkernelmanager/fragments/ci;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/cj;

    invoke-direct {v0, p0, p2, v7}, Lflar2/exkernelmanager/fragments/cj;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/ck;

    invoke-direct {v0, p0, p2, v7}, Lflar2/exkernelmanager/fragments/ck;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/cl;

    invoke-direct {v0, p0, p2, v7}, Lflar2/exkernelmanager/fragments/cl;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010c

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/ce;

    invoke-direct {v3, p0, p1, p2}, Lflar2/exkernelmanager/fragments/ce;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    new-instance v3, Lflar2/exkernelmanager/fragments/cm;

    invoke-direct {v3, p0, v8, p2, p1}, Lflar2/exkernelmanager/fragments/cm;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private k()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/cn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/cn;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Lflar2/exkernelmanager/fragments/cd;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/cn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private l()V
    .locals 3

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->c(Ljava/lang/String;)V

    return-void
.end method

.method private m()Ljava/util/List;
    .locals 11

    const/4 v10, 0x2

    const v9, 0x7f02006b

    const v8, 0x7f020069

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v2, 0x7f0e004e

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "gboost"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x46

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "gboost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "gboost"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    const v2, 0x7f0e007a

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    const-string v2, "prefGBoostBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefGBoostBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_event_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x48

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "input_event_min_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "input_event_min_freq"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v5, v2, v7

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v5, v2, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const/4 v5, 0x3

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefInputMinFreqBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefInputMinFreqBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_27

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "active_floor_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "active_floor_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "active_floor_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefActiveFloorFreqBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefActiveFloorFreqBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39b

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "input_min_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_INPUT_MIN_FREQ2Boot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_INPUT_MIN_FREQ2Boot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "gboost_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39c

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "gboost_min_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "gboost_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_GBOOST_MIN_FREQBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_GBOOST_MIN_FREQBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "max_screen_off_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39d

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "max_screen_off_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max_screen_off_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_MAX_SCREEN_OFF_FREQBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_MAX_SCREEN_OFF_FREQBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2b

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_6
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_event_timeout"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x49

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "input_event_timeout"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_event_timeout"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefInputTimeoutBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefInputTimeoutBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2c

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ui_sampling_rate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4a

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "ui_sampling_rate"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ui_sampling_rate"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefUISamplingRateBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefUISamplingRateBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "two_phase_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4b

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "two_phase_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "two_phase_freq"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    if-le v3, v6, :cond_2e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v5, v2, v7

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v5, v2, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    const/4 v5, 0x3

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_9
    const-string v2, "prefTwoPhaseFreqBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefTwoPhaseFreqBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2f

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "up_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4c

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "up_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "up_threshold"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefUpThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefUpThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_30

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "down_differential"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4d

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "down_differential"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "down_differential"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefDownDifferentialBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefDownDifferentialBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HTC_One_m"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sync_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "sync_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sync_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefSyncFreqBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefSyncFreqBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_32

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_d
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "optimal_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x4f

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "optimal_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "optimal_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefOptFreqBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefOptFreqBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_33

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "shortcut"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x47

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "shortcut"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "shortcut"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefShortcutBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefShortcutBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_34

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sampling_rate_min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x384

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "sampling_rate_min"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sampling_rate_min"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_SAMPLING_RATE_MINBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SAMPLING_RATE_MINBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_35

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "freq_down_step_barriar"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x385

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "freq_down_step_barrier"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_down_step_barriar"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_FREQ_DOWN_STEP_BARRIARBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_FREQ_DOWN_STEP_BARRIARBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_36

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_11
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "freq_down_step"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x386

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "freq_down_step"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "freq_down_step"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_FREQ_DOWN_STEPBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_FREQ_DOWN_STEPBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_37

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_12
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sampling_rate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x387

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "sampling_rate"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sampling_rate"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_SAMPLING_RATEBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SAMPLING_RATEBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_38

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_13
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sampling_down_factor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x388

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "sampling_down_factor"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sampling_down_factor"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_SAMPLING_DOWN_FACTORBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_SAMPLING_DOWN_FACTORBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_39

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_14
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "up_threshold_any_cpu_load"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x38a

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "up_threshold_any_cpu_load"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "up_threshold_any_cpu_load"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_UP_THRESHOLD_ANY_CPU_LOADBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_UP_THRESHOLD_ANY_CPU_LOADBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3a

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_15
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "target_loads"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x38b

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "target_loads"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "target_loads"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_TARGET_LOADSBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_TARGET_LOADSBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3b

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_16
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "timer_slack"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x38c

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "timer_slack"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "timer_slack"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_TIMER_SLACKBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_TIMER_SLACKBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3c

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_17
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_15
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "hispeed_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x38e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "hispeed_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "hispeed_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_HISPEED_FREQBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_HISPEED_FREQBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_18
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_16
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "timer_rate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x38f

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "timer_rate"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "timer_rate"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_TIMER_RATEBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_TIMER_RATEBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3e

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_19
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_17
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "above_hispeed_delay"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x390

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "above_hispeed_delay"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "above_hispeed_delay"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_ABOVE_HISPEED_DELAYBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_ABOVE_HISPEED_DELAYBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3f

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_18
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "boostpulse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x391

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "boostpulse"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "boostpulse"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_BOOSTPULSEBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_BOOSTPULSEBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_40

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_19
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "boostpulse_duration"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x392

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "boostpulse_duration"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "boostpulse_duration"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_BOOSTPULSE_DURATIONBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_BOOSTPULSE_DURATIONBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_41

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1a
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "go_hispeed_load"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x393

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "go_hispeed_load"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "go_hispeed_load"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_GO_HISPEED_LOADBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_GO_HISPEED_LOADBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_42

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1d
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1b
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x394

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "input_boost_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_boost_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_INPUT_BOOST_FREQBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_INPUT_BOOST_FREQBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_43

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1c
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "min_sample_time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x395

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "min_sample_time"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "min_sample_time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_MIN_SAMPLE_TIMEBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_MIN_SAMPLE_TIMEBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_44

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_1f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1d
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "up_threshold_any_cpu_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x396

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "up_threshold_any_cpu_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold_any_cpu_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_UP_THRESHOLD_ANY_CPU_FREQBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_UP_THRESHOLD_ANY_CPU_FREQBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_45

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_20
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1e
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "down_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x398

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "down_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "down_threshold"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_DOWN_THRESHOLDBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_DOWN_THRESHOLDBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_46

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_21
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1f
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "freq_step"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x399

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "freq_step"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "freq_step"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_FREQ_STEPBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_FREQ_STEPBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_47

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_22
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_20
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "boost"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39a

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "boost"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "PREF_BOOSTBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "PREF_BOOSTBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_48

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_23
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_21
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_boost_duration"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3a1

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "input boost duration"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "input_boost_duration"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefInputBoostDurationBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefInputBoostDurationBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_49

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_24
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_22
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "low_load_down_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x39f

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "low load down threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "low_load_down_threshold"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefLowLoadDownThresholdBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefLowLoadDownThresholdBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4a

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_25
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_23
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "max_freq_hysteresis"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x3a0

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "max freq hysteresis"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "max_freq_hysteresis"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v2, "prefMaxFreqHysteresisBoot"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v2, "prefMaxFreqHysteresisBoot"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4b

    invoke-virtual {v1, v6}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_26
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_24
    return-object v0

    :cond_25
    const v2, 0x7f0e008d

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_26
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1

    :cond_27
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_2

    :cond_28
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_29
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_4

    :cond_2a
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_5

    :cond_2b
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_6

    :cond_2c
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_2d
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_8

    :cond_2e
    iget-object v3, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    aget-object v2, v2, v7

    invoke-virtual {v3, v2}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_2f
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_a

    :cond_30
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_b

    :cond_31
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_c

    :cond_32
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_d

    :cond_33
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_e

    :cond_34
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_f

    :cond_35
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_10

    :cond_36
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_11

    :cond_37
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_12

    :cond_38
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_13

    :cond_39
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_14

    :cond_3a
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_15

    :cond_3b
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_16

    :cond_3c
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_17

    :cond_3d
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_18

    :cond_3e
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_19

    :cond_3f
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1a

    :cond_40
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1b

    :cond_41
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1c

    :cond_42
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1d

    :cond_43
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1e

    :cond_44
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_1f

    :cond_45
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_20

    :cond_46
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_21

    :cond_47
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_22

    :cond_48
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_23

    :cond_49
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_24

    :cond_4a
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_25

    :cond_4b
    invoke-virtual {v1, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_26
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->setContentView(I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->overridePendingTransition(II)V

    const v0, 0x7f0e004f

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/fragments/cd;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/cd;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->n:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c00af

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->s:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->t:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->s:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->t:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->s:Landroid/widget/ListView;

    new-instance v1, Lflar2/exkernelmanager/fragments/cf;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/cf;-><init>(Lflar2/exkernelmanager/fragments/GovernorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->r:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sys/devices/system/cpu/cpufreq/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->p:Lflar2/exkernelmanager/utilities/e;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sys/devices/system/cpu/cpu0/cpufreq/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    :cond_0
    const-string v0, "prefGovPath"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->l()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->k()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->t:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const-string v0, "prefGBoost"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gboost"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "prefShortcut"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "shortcut"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    const-string v0, "prefSyncFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sync_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    const-string v0, "prefOptFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "optimal_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "two_phase_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefTwoPhaseFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "two_phase_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_0
    const-string v0, "prefTwoPhaseFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "two_phase_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->o:Lflar2/exkernelmanager/utilities/m;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_event_min_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "prefInputMinFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_event_min_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    const-string v0, "prefInputMinFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_event_min_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "prefActiveFloorFreq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "active_floor_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "PREF_INPUT_MIN_FREQ2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_min_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "PREF_GBOOST_MIN_FREQ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gboost_min_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "PREF_MAX_SCREEN_OFF_FREQ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "max_screen_off_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "prefInputTimeout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_event_timeout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_b
    const-string v0, "prefUISamplingRate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ui_sampling_rate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "prefUpThreshold"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "up_threshold"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "prefDownDifferential"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "down_differential"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "PREF_SAMPLING_RATE_MIN"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sampling_rate_min"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "PREF_FREQ_DOWN_STEP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "freq_down_step"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_10
    const-string v0, "PREF_SAMPLING_RATE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sampling_rate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "PREF_SAMPLING_DOWN_FACTOR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sampling_down_factor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_12
    const-string v0, "PREF_UP_THRESHOLD_ANY_CPU_LOAD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "up_threshold_any_cpu_load"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_13
    const-string v0, "PREF_TARGET_LOADS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "target_loads"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_14
    const-string v0, "PREF_TIMER_SLACK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timer_slack"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "PREF_TIMER_RATE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timer_rate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_16
    const-string v0, "PREF_ABOVE_HISPEED_DELAY"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "above_hispeed_delay"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_17
    const-string v0, "PREF_BOOSTPULSE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "boostpulse"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_18
    const-string v0, "PREF_BOOSTPULSE_DURATION"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "boostpulse_duration"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_19
    const-string v0, "PREF_GO_HISPEED_LOAD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "go_hispeed_load"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1a
    const-string v0, "PREF_MIN_SAMPLE_TIME"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "min_sample_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1b
    const-string v0, "PREF_DOWN_THRESHOLD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "down_threshold"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1c
    const-string v0, "PREF_FREQ_STEP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "freq_step"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1d
    const-string v0, "PREF_BOOST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "boost"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1e
    const-string v0, "PREF_HISPEED_FREQ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hispeed_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1f
    const-string v0, "PREF_INPUT_BOOST_FREQ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_boost_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_20
    const-string v0, "PREF_UP_THRESHOLD_ANY_CPU_FREQ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "up_threshold_any_cpu_freq"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_21
    const-string v0, "PREF_FREQ_DOWN_STEP_BARRIAR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "freq_down_step_barriar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_22
    const-string v0, "prefLowLoadDownThreshold"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "low_load_down_threshold"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_23
    const-string v0, "prefMaxFreqHysteresis"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "max_freq_hysteresis"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_24
    const-string v0, "prefInputBoostDuration"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "input_boost_duration"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/GovernorActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3a1 -> :sswitch_24
        -0x3a0 -> :sswitch_23
        -0x39f -> :sswitch_22
        -0x39e -> :sswitch_6
        -0x39d -> :sswitch_9
        -0x39c -> :sswitch_8
        -0x39b -> :sswitch_7
        -0x39a -> :sswitch_1d
        -0x399 -> :sswitch_1c
        -0x398 -> :sswitch_1b
        -0x396 -> :sswitch_20
        -0x395 -> :sswitch_1a
        -0x394 -> :sswitch_1f
        -0x393 -> :sswitch_19
        -0x392 -> :sswitch_18
        -0x391 -> :sswitch_17
        -0x390 -> :sswitch_16
        -0x38f -> :sswitch_15
        -0x38e -> :sswitch_1e
        -0x38c -> :sswitch_14
        -0x38b -> :sswitch_13
        -0x38a -> :sswitch_12
        -0x388 -> :sswitch_11
        -0x387 -> :sswitch_10
        -0x386 -> :sswitch_f
        -0x385 -> :sswitch_21
        -0x384 -> :sswitch_e
        -0x4f -> :sswitch_3
        -0x4e -> :sswitch_2
        -0x4d -> :sswitch_d
        -0x4c -> :sswitch_c
        -0x4b -> :sswitch_4
        -0x4a -> :sswitch_b
        -0x49 -> :sswitch_a
        -0x48 -> :sswitch_5
        -0x47 -> :sswitch_1
        -0x46 -> :sswitch_0
    .end sparse-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/GovernorActivity;->t:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    const-string v0, "This is how you change doubletap2wake "

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x25
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/GovernorActivity;->k()V

    return-void
.end method
