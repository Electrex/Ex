.class public Lflar2/exkernelmanager/fragments/cz;
.super Landroid/app/Fragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private a:Lflar2/exkernelmanager/utilities/m;

.field private b:Lflar2/exkernelmanager/utilities/e;

.field private c:Lflar2/exkernelmanager/utilities/f;

.field private d:Landroid/content/Context;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Landroid/widget/ListView;

.field private l:Lflar2/exkernelmanager/a/a;

.field private m:Ljava/lang/String;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/ImageView;

.field private q:I

.field private r:I

.field private s:Landroid/graphics/RectF;

.field private t:Landroid/graphics/RectF;

.field private u:Landroid/view/animation/AccelerateDecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    iput v1, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/cz;->g:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    iput v1, p0, Lflar2/exkernelmanager/fragments/cz;->j:I

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    return-void
.end method

.method public static a(FFF)F
    .locals 1

    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    return-object p1
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method private a(I)V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->r:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    neg-int v1, p1

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->r:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iget v1, p0, Lflar2/exkernelmanager/fragments/cz;->r:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lflar2/exkernelmanager/fragments/cz;->a(FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->n:Landroid/widget/TextView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->o:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->u:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/cz;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->p:Landroid/widget/ImageView;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->o:Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->u:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v3, v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lflar2/exkernelmanager/fragments/cz;->a(Landroid/view/View;Landroid/view/View;F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->p:Landroid/widget/ImageView;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;F)V
    .locals 6

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p1}, Lflar2/exkernelmanager/fragments/cz;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    invoke-direct {p0, v0, p2}, Lflar2/exkernelmanager/fragments/cz;->a(Landroid/graphics/RectF;Landroid/view/View;)Landroid/graphics/RectF;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    sub-float/2addr v0, v3

    mul-float/2addr v0, p3

    add-float/2addr v0, v3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, p3

    add-float/2addr v1, v3

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    mul-float/2addr v2, v5

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->t:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->s:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p3

    mul-float/2addr v3, v5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    sub-float v2, v3, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/cz;I)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/cz;->a(I)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/cz;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/cz;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e0173

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e007c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v0, "prefCoolerColors"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v1, 0x7f0e0049

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/cz;->a(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    return-void

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->j:I

    const-string v0, "prefkcalPath"

    iget v1, p0, Lflar2/exkernelmanager/fragments/cz;->j:I

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/cz;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "prefCCProfile"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX/color_profiles/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    aget-object v2, v0, v4

    const-string v3, "/sys/module/dsi_panel/kgamma_bn"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    const-string v3, "/sys/module/dsi_panel/kgamma_bp"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x2

    aget-object v2, v0, v2

    const-string v3, "/sys/module/dsi_panel/kgamma_gn"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x3

    aget-object v2, v0, v2

    const-string v3, "/sys/module/dsi_panel/kgamma_gp"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x4

    aget-object v2, v0, v2

    const-string v3, "/sys/module/dsi_panel/kgamma_rn"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x5

    aget-object v2, v0, v2

    const-string v3, "/sys/module/dsi_panel/kgamma_rp"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x6

    aget-object v0, v0, v2

    const-string v2, "/sys/module/dsi_panel/kgamma_w"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e0108

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Boot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "4"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "4"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e0108

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    invoke-direct {p0, p1, p2}, Lflar2/exkernelmanager/fragments/cz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    invoke-virtual {v0, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "Y"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "N"

    invoke-virtual {v0, v1, p2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "N"

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/cz;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    return v0
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->j:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e002b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v4, 0x7f0e007b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v4, 0x7f0e0078

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v4, 0x7f0e0079

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lflar2/exkernelmanager/fragments/db;

    invoke-direct {v2, p0, p2, p1}, Lflar2/exkernelmanager/fragments/db;-><init>(Lflar2/exkernelmanager/fragments/cz;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/e;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    return-object v0
.end method

.method private d()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/dj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/dj;-><init>(Lflar2/exkernelmanager/fragments/cz;Lflar2/exkernelmanager/fragments/da;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/dj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/cz;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    return-object v0
.end method

.method private e()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/cz;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic f(Lflar2/exkernelmanager/fragments/cz;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e002c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e002d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/dc;

    invoke-direct {v3, p0, v1}, Lflar2/exkernelmanager/fragments/dc;-><init>(Lflar2/exkernelmanager/fragments/cz;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic g(Lflar2/exkernelmanager/fragments/cz;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    return v0
.end method

.method private g()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e010d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v2, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v3, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3, v5, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/dd;

    invoke-direct {v3, p0, v2}, Lflar2/exkernelmanager/fragments/dd;-><init>(Lflar2/exkernelmanager/fragments/cz;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic h(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/utilities/f;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    return-object v0
.end method

.method private h()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e010d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2, v6, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/kernel/tegra_gpu/gpu_floor_rate"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v3, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3, v5, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    aget-object v4, v1, v5

    aput-object v4, v3, v5

    aget-object v4, v1, v6

    aput-object v4, v3, v6

    aget-object v4, v1, v7

    aput-object v4, v3, v7

    aget-object v4, v1, v8

    aput-object v4, v3, v8

    aget-object v1, v1, v9

    aput-object v1, v3, v9

    new-instance v1, Lflar2/exkernelmanager/fragments/de;

    invoke-direct {v1, p0, v2}, Lflar2/exkernelmanager/fragments/de;-><init>(Lflar2/exkernelmanager/fragments/cz;[Ljava/lang/String;)V

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    :goto_0
    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void

    :cond_0
    new-instance v2, Lflar2/exkernelmanager/fragments/df;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/df;-><init>(Lflar2/exkernelmanager/fragments/cz;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    goto :goto_0
.end method

.method static synthetic i(Lflar2/exkernelmanager/fragments/cz;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v1, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e010f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v0, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/kgsl/kgsl-3d0/devfreq/available_governors"

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/class/kgsl/kgsl-3d0/devfreq/available_governors"

    invoke-virtual {v0, v2, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Lflar2/exkernelmanager/fragments/dg;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/dg;-><init>(Lflar2/exkernelmanager/fragments/cz;[Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v1}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void

    :cond_0
    const-string v0, "os.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "ElementalX"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v3, 0x7f0e00dd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v3, 0x7f0e00af

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "ondemand"

    aput-object v2, v0, v4

    const-string v2, "simple"

    aput-object v2, v0, v5

    const-string v2, "performance"

    aput-object v2, v0, v6

    goto :goto_0

    :cond_2
    new-array v0, v6, [Ljava/lang/String;

    const-string v2, "ondemand"

    aput-object v2, v0, v4

    const-string v2, "performance"

    aput-object v2, v0, v5

    goto :goto_0
.end method

.method static synthetic j(Lflar2/exkernelmanager/fragments/cz;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->l:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private j()V
    .locals 2

    const-string v0, "prefKCALModuleBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "lsmod"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "kcal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lflar2/exkernelmanager/fragments/dh;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/dh;-><init>(Lflar2/exkernelmanager/fragments/cz;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "lsmod"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "kcal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "prefKCALModule"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->b()V

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    return-void

    :cond_1
    const-string v0, "prefKCALModule"

    const-string v1, "0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private k()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v2, 0x7f0e010b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/ElementalX/color_profiles"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/di;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/di;-><init>(Lflar2/exkernelmanager/fragments/cz;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    const-string v0, "prefCCProfileBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private l()Ljava/util/List;
    .locals 11

    const v10, 0x7f0e007a

    const v9, 0x7f02006b

    const v8, 0x7f020069

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e00a8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v4, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v5, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x14

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e00ce

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v4, "prefGPUMaxBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefGPUMaxBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_0
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v4, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v5, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v4, "/sys/kernel/tegra_gpu/gpu_floor_rate"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x15

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e00d3

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e00df

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/kernel/tegra_gpu/gpu_floor_state"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    const-string v4, "72 MHz"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_1
    const-string v4, "prefGPUMinBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefGPUMinBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1c

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v4, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    iget v5, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x16

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e00a7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v5, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v4, "prefGPUGovBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefGPUGovBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1d

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_3
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move v0, v1

    :goto_4
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/module/mdss_dsi/parameters/color_preset"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    new-instance v4, Lflar2/exkernelmanager/a/o;

    invoke-direct {v4}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v4, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e0109

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x18

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e0029

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "N"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    :cond_6
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :cond_7
    :goto_5
    const-string v4, "prefBacklightBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefBacklightBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_6
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_8
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Y"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0xf1

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e002a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v4, "prefBacklightMinBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefBacklightMinBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_22

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_7
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_9
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/module/mdss_dsi/parameters/color_preset"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x19

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e0048

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/module/mdss_dsi/parameters/color_preset"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_8
    const-string v4, "prefCoolerColorsBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefCoolerColorsBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_24

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_9
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_a
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0x1a

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e00ae

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_a
    const-string v4, "prefHTCColorBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefHTCColorBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_26

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_b
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_b
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "/system/lib/modules/msm_kcal_ctrl.ko"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->j:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_c
    new-instance v4, Lflar2/exkernelmanager/a/o;

    invoke-direct {v4}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v4, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e0046

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v5, "ls /system/lib/modules/"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "kcal"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    new-instance v4, Lflar2/exkernelmanager/a/o;

    invoke-direct {v4}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v4, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v5, -0x1b

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e0047

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    const-string v6, "lsmod"

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "msm_kcal"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_27

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e008d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v5, "prefKCALModule"

    const-string v6, "1"

    invoke-static {v5, v6}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_c
    const-string v5, "prefKCALModuleBoot"

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v5, "prefKCALModuleBoot"

    invoke-static {v5}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_28

    invoke-virtual {v4, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v4, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_d
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->j:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    new-instance v4, Lflar2/exkernelmanager/a/o;

    invoke-direct {v4}, Lflar2/exkernelmanager/a/o;-><init>()V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v5, -0xd4

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e001e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e00d9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v4, -0xd3

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(I)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e00c4

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    const-string v4, "prefCCProfile"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v4, "prefCCProfileBoot"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->d(Ljava/lang/String;)V

    const-string v4, "prefCCProfileBoot"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    :goto_e
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_10
    if-eqz v0, :cond_11

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v4, 0x7f0e00e6

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x7

    if-le v0, v2, :cond_12

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0xb

    if-ge v0, v2, :cond_12

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflar2/exkernelmanager/a/o;

    invoke-direct {v0}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    return-object v3

    :cond_13
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_0

    :cond_14
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "/sys/kernel/tegra_gpu/gpu_floor_rate"

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_15
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e00af

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v2, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    aget-object v5, v4, v1

    const-string v6, "585"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_17

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_16
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x7

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_17
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_18
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v6, 0x7f0e00dd

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v2, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    aget-object v5, v4, v1

    const-string v6, "585"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1a

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_19
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x6

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1a
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1b
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v5, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v2, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v6, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v7, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1c
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_2

    :cond_1d
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_3

    :cond_1e
    iget v4, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_20

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e0078

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_1f
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v5, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget v6, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e0079

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_20
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_21
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_6

    :cond_22
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_7

    :cond_23
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_24
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_9

    :cond_25
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v5, 0x7f0e008d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_26
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_b

    :cond_27
    iget-object v5, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    invoke-virtual {v5, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    const-string v5, "prefKCALModule"

    const-string v6, "0"

    invoke-static {v5, v6}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_28
    invoke-virtual {v4, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v4, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_d

    :cond_29
    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/a/o;->a(Z)V

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->c(I)V

    goto/16 :goto_e

    :cond_2a
    move v0, v2

    goto/16 :goto_4
.end method


# virtual methods
.method public a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    const/4 v4, 0x1

    if-lt v2, v4, :cond_1

    iget v0, p0, Lflar2/exkernelmanager/fragments/cz;->q:I

    :cond_1
    neg-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f0b005d

    const/4 v6, 0x2

    const/4 v5, 0x1

    const v0, 0x7f030032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "Graphics"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0, v5}, Lflar2/exkernelmanager/fragments/cz;->setHasOptionsMenu(Z)V

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c007d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v3, v4}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->l:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/cz;->l:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    sput-boolean v5, Lflar2/exkernelmanager/a/a;->b:Z

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_3

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b7

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b5

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->p:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->p:Landroid/widget/ImageView;

    const v3, 0x7f020075

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b4

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->o:Landroid/widget/TextView;

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0c00b6

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->n:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->n:Landroid/widget/TextView;

    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->q:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/cz;->q:I

    neg-int v0, v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->r:I

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->u:Landroid/view/animation/AccelerateDecelerateInterpolator;

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->k:Landroid/widget/ListView;

    new-instance v2, Lflar2/exkernelmanager/fragments/da;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/da;-><init>(Lflar2/exkernelmanager/fragments/cz;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->m:Ljava/lang/String;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const v3, 0x7f0e00d9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->c:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/f;->c(Landroid/content/Context;)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->g:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    :cond_1
    const-string v0, "prefgpuMaxPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->e:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefgpuMinPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->f:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefgpuAvailPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->g:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefgpuGovPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->h:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->a:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    const-string v0, "prefbacklightPath"

    iget v2, p0, Lflar2/exkernelmanager/fragments/cz;->i:I

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->b()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->c()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    sget-object v0, Lflar2/exkernelmanager/utilities/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefFirstRunSettings"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_2
    return-object v1

    :cond_3
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/cz;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->u:Landroid/view/animation/AccelerateDecelerateInterpolator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/cz;->a(I)V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->l:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->g()V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->h()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->i()V

    goto :goto_0

    :sswitch_4
    const-string v0, "prefBacklight"

    sget-object v1, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    const-string v2, "prefbacklightPath"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/cz;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->f()V

    goto :goto_0

    :sswitch_6
    const-string v0, "prefCoolerColors"

    const-string v1, "/sys/module/mdss_dsi/parameters/color_preset"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/cz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_7
    const-string v0, "prefHTCColor"

    const-string v1, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/cz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_8
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->j()V

    goto :goto_0

    :sswitch_9
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->e()V

    goto :goto_0

    :sswitch_a
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->k()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0xf1 -> :sswitch_5
        -0xd4 -> :sswitch_9
        -0xd3 -> :sswitch_a
        -0xd2 -> :sswitch_0
        -0x1d -> :sswitch_0
        -0x1c -> :sswitch_0
        -0x1b -> :sswitch_8
        -0x1a -> :sswitch_7
        -0x19 -> :sswitch_6
        -0x18 -> :sswitch_4
        -0x17 -> :sswitch_0
        -0x16 -> :sswitch_3
        -0x15 -> :sswitch_2
        -0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->l:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const-string v1, "No help for this item"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cz;->d:Landroid/content/Context;

    const-string v1, "This is how you change doubletap2wake "

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x25
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x0

    sput-boolean v0, Lflar2/exkernelmanager/a/a;->b:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/cz;->d()V

    return-void
.end method
