.class public Lflar2/exkernelmanager/fragments/ModesActivity;
.super Landroid/support/v7/app/ad;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field n:I

.field o:Lflar2/exkernelmanager/utilities/h;

.field private p:Lflar2/exkernelmanager/utilities/m;

.field private q:Lflar2/exkernelmanager/utilities/e;

.field private r:Lflar2/exkernelmanager/utilities/f;

.field private s:Landroid/widget/ListView;

.field private t:Lflar2/exkernelmanager/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->r:Lflar2/exkernelmanager/utilities/f;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ModesActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->k()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010d

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    invoke-virtual {v2, p2, v3, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/dw;

    invoke-direct {v3, p0, v2, p1}, Lflar2/exkernelmanager/fragments/dw;-><init>(Lflar2/exkernelmanager/fragments/ModesActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ModesActivity;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->m()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->k()V

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/ModesActivity;)Lflar2/exkernelmanager/a/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->t:Lflar2/exkernelmanager/a/a;

    return-object v0
.end method

.method private k()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/fragments/dx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/dx;-><init>(Lflar2/exkernelmanager/fragments/ModesActivity;Lflar2/exkernelmanager/fragments/dt;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/dx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010e

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"

    invoke-virtual {v1, v2, v3, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/dv;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/dv;-><init>(Lflar2/exkernelmanager/fragments/ModesActivity;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private m()Ljava/util/List;
    .locals 9

    const/4 v8, 0x0

    const v7, 0x7f0e008d

    const v6, 0x7f0e007a

    const/4 v5, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "prefSubVersion"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Sense"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v2, 0x7f0e00f8

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x259

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e00c2

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "prefCPUMaxPrefPS"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x25a

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e00c3

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "prefGPUMaxPrefPS"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v4, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x25b

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e00c9

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    const-string v2, "prefDimmerPS"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v4, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x25c

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const-string v2, "prefHTC"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x7f0e00fe

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    :goto_1
    const-string v2, "prefVibOptPS"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00d9

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00dd

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x258

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e014f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    const-string v2, "prefWakePS"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v8}, Lflar2/exkernelmanager/a/o;->a(I)V

    const v2, 0x7f0e00ef

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x25d

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e00cd

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "prefCPUMaxPrefPF"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x25e

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e00ce

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "prefGPUMaxPrefPF"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00df

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00db

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Lflar2/exkernelmanager/a/o;

    invoke-direct {v1}, Lflar2/exkernelmanager/a/o;-><init>()V

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/a/o;->a(I)V

    const/16 v2, -0x25f

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(I)V

    const v2, 0x7f0e0050

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    const-string v2, "prefCPUGovPF"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0

    :cond_5
    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const v2, 0x7f0e0150

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/a/o;->b(Ljava/lang/String;)V

    goto/16 :goto_3
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->o:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->setContentView(I)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->overridePendingTransition(II)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f0e001f

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/fragments/dt;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/dt;-><init>(Lflar2/exkernelmanager/fragments/ModesActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->o:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c00ba

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->o:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->s:Landroid/widget/ListView;

    new-instance v0, Lflar2/exkernelmanager/a/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/a/a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->t:Lflar2/exkernelmanager/a/a;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->s:Landroid/widget/ListView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->t:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->s:Landroid/widget/ListView;

    new-instance v1, Lflar2/exkernelmanager/fragments/du;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/du;-><init>(Lflar2/exkernelmanager/fragments/ModesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->n:I

    const-string v0, "prefCPUMaxPrefPF"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefCPUMaxPrefPF"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->q:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->n:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, v3, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefGPUMaxPrefPF"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00df

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->n:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const v2, 0x32c87d00

    if-ge v1, v2, :cond_2

    const-string v1, "prefGPUMaxPrefPF"

    const/16 v2, 0xe

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->k()V

    return-void

    :cond_2
    const-string v0, "prefGPUMaxPrefPF"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->n:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00dd

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    array-length v1, v0

    if-le v1, v4, :cond_4

    const-string v1, "prefGPUMaxPrefPF"

    const/4 v2, 0x4

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v1, "prefGPUMaxPrefPF"

    aget-object v0, v0, v3

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e00af

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ModesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    array-length v1, v0

    if-le v1, v4, :cond_6

    const-string v1, "prefGPUMaxPrefPF"

    const/4 v2, 0x5

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v1, "prefGPUMaxPrefPF"

    aget-object v0, v0, v3

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string v1, "prefGPUMaxPrefPF"

    aget-object v0, v0, v3

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->t:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "prefCPUMaxPrefPS"

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "prefGPUMaxPrefPS"

    sget-object v1, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->n:I

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "prefDimmerPS"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "prefVibOptPS"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    const-string v0, "prefWakePS"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ModesActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    const-string v0, "prefCPUMaxPrefPF"

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    const-string v0, "prefGPUMaxPrefPF"

    sget-object v1, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->n:I

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ModesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->l()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x25f
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ModesActivity;->t:Lflar2/exkernelmanager/a/a;

    invoke-virtual {v0, p3}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    const-string v0, "This is how you change doubletap2wake "

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x25
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ModesActivity;->k()V

    return-void
.end method
