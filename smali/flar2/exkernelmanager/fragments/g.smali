.class Lflar2/exkernelmanager/fragments/g;
.super Landroid/os/CountDownTimer;


# instance fields
.field final synthetic a:Landroid/support/v7/app/ab;

.field final synthetic b:Lflar2/exkernelmanager/fragments/BackupActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/BackupActivity;JJLandroid/support/v7/app/ab;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/g;->b:Lflar2/exkernelmanager/fragments/BackupActivity;

    iput-object p6, p0, Lflar2/exkernelmanager/fragments/g;->a:Landroid/support/v7/app/ab;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/g;->a:Landroid/support/v7/app/ab;

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->dismiss()V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/g;->b:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->c(Lflar2/exkernelmanager/fragments/BackupActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/g;->b:Lflar2/exkernelmanager/fragments/BackupActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/BackupActivity;->d(Lflar2/exkernelmanager/fragments/BackupActivity;)V

    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 5

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/g;->a:Landroid/support/v7/app/ab;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "System will reboot in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seconds"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ab;->a(Ljava/lang/CharSequence;)V

    return-void
.end method
