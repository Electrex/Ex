.class Lflar2/exkernelmanager/fragments/cb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/fragments/ColorActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/cb;->a:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    const-string v0, "pref_KcalGreyscale"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefKcalSat"

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/cb;->a:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->i(Lflar2/exkernelmanager/fragments/ColorActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cb;->a:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->i(Lflar2/exkernelmanager/fragments/ColorActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "128"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/cb;->a:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->i(Lflar2/exkernelmanager/fragments/ColorActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "prefKcalSat"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
