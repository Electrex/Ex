.class Lflar2/exkernelmanager/fragments/em;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/fragments/SleeperActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/SleeperActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const v5, 0x7f0e00dd

    const v4, 0x7f0e00db

    const v3, 0x7f0e00d9

    const-string v0, "prefHotplugBoot"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    packed-switch p2, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->b(Lflar2/exkernelmanager/fragments/SleeperActivity;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "0"

    const-string v2, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/f;

    move-result-object v0

    const-string v1, "start mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefHotplug"

    const-string v1, "mpdecision"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/f;

    move-result-object v0

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-virtual {v1, v4}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "1"

    const-string v2, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefHotplug"

    const-string v1, "custom"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "384000"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "384000"

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "0"

    const-string v2, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->c(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/f;

    move-result-object v0

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-virtual {v1, v4}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "300000"

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefHotplug"

    const-string v1, "disabled"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-virtual {v1, v5}, Lflar2/exkernelmanager/fragments/SleeperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "384000"

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/em;->a:Lflar2/exkernelmanager/fragments/SleeperActivity;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/SleeperActivity;->a(Lflar2/exkernelmanager/fragments/SleeperActivity;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v0

    const-string v1, "384000"

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
