.class Lflar2/exkernelmanager/fragments/ah;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lflar2/exkernelmanager/fragments/ac;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ac;[Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/ah;->a:[Ljava/lang/String;

    iput p3, p0, Lflar2/exkernelmanager/fragments/ah;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->a:[Ljava/lang/String;

    aget-object v3, v0, p2

    if-eqz v3, :cond_0

    iget v0, p0, Lflar2/exkernelmanager/fragments/ah;->b:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v0, "prefCPUMaxBoot"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefCPUMax"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->c:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    const-string v0, "prefCPUMinBoot"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefCPUMin"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0, v2}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->c(Lflar2/exkernelmanager/fragments/ac;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v4}, Lflar2/exkernelmanager/fragments/ac;->b(Lflar2/exkernelmanager/fragments/ac;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e00d9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "prefMPDecision"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "Disabled"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v4, "stop mpdecision"

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move v0, v1

    :goto_2
    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v4, v3, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "1"

    const-string v6, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v4, v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v4, v3, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "1"

    const-string v6, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v4, v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq"

    invoke-virtual {v4, v3, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "1"

    const-string v6, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v4, v5, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v4, v4, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v5, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq"

    invoke-virtual {v4, v3, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->b:Lflar2/exkernelmanager/utilities/f;

    const-string v3, "start mpdecision"

    invoke-virtual {v0, v3}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0, v2}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;Z)V

    :cond_3
    const-string v0, "prefHTC"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "prefCPUMin"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "300000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "prefCPUMin"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "384000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0, v1}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;Z)V

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;)V

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "prefMaxScroffBoot"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefMaxScroff"

    invoke-static {v0, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v0, v0, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ah;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0xc
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
