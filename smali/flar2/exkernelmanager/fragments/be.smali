.class Lflar2/exkernelmanager/fragments/be;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lflar2/exkernelmanager/fragments/ColorActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/be;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged()V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/be;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->b(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/view/View;

    move-result-object v1

    neg-int v2, v0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->c(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/view/View;

    move-result-object v1

    neg-int v2, v0

    iget-object v3, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v3}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-virtual {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->d(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setTranslationY(F)V

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->b(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v4}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;FFF)F

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->e(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/Switch;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setAlpha(F)V

    return-void

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->d(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x3

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/be;->b:Lflar2/exkernelmanager/fragments/ColorActivity;

    invoke-static {v2}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Lflar2/exkernelmanager/fragments/ColorActivity;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setTranslationY(F)V

    goto :goto_0
.end method
