.class public Lflar2/exkernelmanager/fragments/ColorActivity;
.super Landroid/support/v7/app/ad;


# instance fields
.field private A:Landroid/widget/EditText;

.field private B:Landroid/widget/EditText;

.field private C:Landroid/widget/EditText;

.field private D:Landroid/widget/EditText;

.field private E:Landroid/widget/EditText;

.field private F:Landroid/widget/EditText;

.field private G:Landroid/widget/EditText;

.field private H:Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

.field private I:Landroid/support/v7/widget/Toolbar;

.field private J:Landroid/view/View;

.field private K:Landroid/view/View;

.field private L:I

.field private M:I

.field private N:I

.field private O:Z

.field private P:I

.field n:Z

.field o:Lflar2/exkernelmanager/utilities/h;

.field private p:Lflar2/exkernelmanager/utilities/m;

.field private q:Lflar2/exkernelmanager/utilities/f;

.field private r:Landroid/widget/Switch;

.field private s:Landroid/widget/Switch;

.field private t:Landroid/widget/SeekBar;

.field private u:Landroid/widget/SeekBar;

.field private v:Landroid/widget/SeekBar;

.field private w:Landroid/widget/SeekBar;

.field private x:Landroid/widget/SeekBar;

.field private y:Landroid/widget/SeekBar;

.field private z:Landroid/widget/SeekBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->q:Lflar2/exkernelmanager/utilities/f;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    return-void
.end method

.method private a(FFF)F
    .locals 1

    invoke-static {p1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ColorActivity;FFF)F
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(FFF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ColorActivity;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->N:I

    return v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    move-object v0, p1

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->l()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_1
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    :sswitch_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->t:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    const-string v1, "prefRed"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->u:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    const-string v1, "prefGreen"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->v:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    const-string v1, "prefBlue"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_3
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v1, v1, 0xdf

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v2, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->w:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    const-string v0, "prefKcalSat"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_4
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v1, v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_val"

    invoke-virtual {v2, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->x:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    const-string v0, "prefKcalVal"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_5
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v1, v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_cont"

    invoke-virtual {v2, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->y:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    const-string v0, "prefKcalCont"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_hue"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->z:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    const-string v1, "prefKcalHue"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c0086 -> :sswitch_0
        0x7f0c008a -> :sswitch_1
        0x7f0c008e -> :sswitch_2
        0x7f0c0093 -> :sswitch_3
        0x7f0c0097 -> :sswitch_4
        0x7f0c009b -> :sswitch_5
        0x7f0c009f -> :sswitch_6
    .end sparse-switch
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ColorActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ColorActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->J:Landroid/view/View;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX/kcal_profiles/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    aget-object v2, v0, v5

    sget-object v3, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->P:I

    aget-object v3, v3, v4

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v1, :cond_1

    aget-object v0, v0, v6

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    aget-object v2, v0, v5

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    aget-object v2, v0, v6

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_val"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x2

    aget-object v2, v0, v2

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_cont"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_hue"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->K:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/support/v7/widget/Toolbar;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->I:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->r:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic f(Lflar2/exkernelmanager/fragments/ColorActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->m()V

    return-void
.end method

.method static synthetic g(Lflar2/exkernelmanager/fragments/ColorActivity;)Lcom/getbase/floatingactionbutton/FloatingActionsMenu;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->H:Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

    return-object v0
.end method

.method static synthetic h(Lflar2/exkernelmanager/fragments/ColorActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->n()V

    return-void
.end method

.method static synthetic i(Lflar2/exkernelmanager/fragments/ColorActivity;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method

.method static synthetic j(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->A:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic k(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->B:Landroid/widget/EditText;

    return-object v0
.end method

.method private k()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x100

    const/16 v2, 0xff

    const-string v0, "pref_KcalGreyscale"

    invoke-static {v0, v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_2

    const-string v0, "prefRed"

    const-string v1, "256"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefGreen"

    const-string v1, "256"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefBlue"

    const-string v1, "256"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_0

    const-string v0, "prefKcalSat"

    const-string v1, "255"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefKcalVal"

    const-string v1, "255"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefKcalCont"

    const-string v1, "255"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefKcalHue"

    const-string v1, "0"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v0, v3, v1}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :goto_1
    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "255"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "255"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_val"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "255"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_cont"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_hue"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->l()V

    return-void

    :cond_2
    const-string v0, "prefRed"

    const-string v1, "255"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefGreen"

    const-string v1, "255"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefBlue"

    const-string v1, "255"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v0, v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v0, v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v0, v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    goto :goto_1
.end method

.method static synthetic l(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->C:Landroid/widget/EditText;

    return-object v0
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_enable"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "35"

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_min"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefKcalACCBoot"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->r:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_0
    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "128"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->t:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->u:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->v:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->w:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit16 v1, v1, -0xdf

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->x:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_val"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x7f

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->y:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_cont"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x7f

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->z:Landroid/widget/SeekBar;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/kcal_ctrl.0/kcal_hue"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->A:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->t:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->B:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->u:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->C:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->v:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->D:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->w:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->E:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->x:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->F:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->G:Landroid/widget/EditText;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->z:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method static synthetic m(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->D:Landroid/widget/EditText;

    return-object v0
.end method

.method private m()V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0107

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const-string v2, "myprofile"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setMaxLines(I)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    const v2, 0x7f0e00ea

    new-instance v3, Lflar2/exkernelmanager/fragments/bq;

    invoke-direct {v3, p0, v1}, Lflar2/exkernelmanager/fragments/bq;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    new-instance v1, Lflar2/exkernelmanager/fragments/br;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/br;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x320

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    return-void
.end method

.method static synthetic n(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    return-object v0
.end method

.method private n()V
    .locals 6

    const v5, 0x7f0e00ea

    const/4 v4, 0x0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX/kcal_profiles"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e010b

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    invoke-virtual {v0, v5, v4}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const v1, 0x7f0e00e3

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    new-instance v1, Lflar2/exkernelmanager/fragments/bs;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bs;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030039

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Landroid/support/v7/app/ac;

    invoke-direct {v3, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    const-string v0, "Load profile"

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    invoke-virtual {v3, v5, v4}, Landroid/support/v7/app/ac;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    const v0, 0x7f0c013f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-boolean v4, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-nez v4, :cond_1

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    new-instance v0, Lflar2/exkernelmanager/fragments/bu;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/fragments/bu;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ac;->a(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v3}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v3

    new-instance v4, Landroid/widget/ArrayAdapter;

    const v0, 0x1090003

    const v5, 0x1020014

    invoke-direct {v4, p0, v0, v5, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    const v0, 0x7f0c0140

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lflar2/exkernelmanager/fragments/bv;

    invoke-direct {v2, p0, v1, v3}, Lflar2/exkernelmanager/fragments/bv;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;[Ljava/lang/String;Landroid/support/v7/app/ab;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v3}, Landroid/support/v7/app/ab;->show()V

    invoke-virtual {v3}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    goto :goto_0

    :cond_1
    const-string v4, "prefKcalTestImage"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "colorscale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const v4, 0x7f020049

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_2
    new-instance v4, Lflar2/exkernelmanager/fragments/bt;

    invoke-direct {v4, p0, v0}, Lflar2/exkernelmanager/fragments/bt;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    const-string v5, "healthyfood"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const v4, 0x7f02005a

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_4
    const-string v5, "grayscale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const v4, 0x7f020059

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_5
    const-string v5, "macbeth"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const v4, 0x7f020097

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_6
    const-string v5, "babies"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f02003c

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method

.method static synthetic o(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->E:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic p(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->F:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic q(Lflar2/exkernelmanager/fragments/ColorActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->G:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic r(Lflar2/exkernelmanager/fragments/ColorActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    return v0
.end method

.method static synthetic s(Lflar2/exkernelmanager/fragments/ColorActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->l()V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->o:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    const/16 v6, 0xff

    const/16 v5, 0x8

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->setContentView(I)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->overridePendingTransition(II)V

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    const-string v1, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->p:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->P:I

    const-string v0, "prefkcalPath"

    iget v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->P:I

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->I:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->I:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f0e014c

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/fragments/at;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/fragments/at;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->o:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->o:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c00a4

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->J:Landroid/view/View;

    const v0, 0x7f0c00a6

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->K:Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->M:I

    iget v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->M:I

    neg-int v0, v0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->N:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_1
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_c

    :goto_0
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/fragments/be;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/fragments/be;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    const v0, 0x7f0c00a0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->H:Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

    new-instance v0, Lcom/d/a/b;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->H:Lcom/getbase/floatingactionbutton/FloatingActionsMenu;

    const v3, 0x7f04000e

    const v4, 0x7f04000d

    invoke-direct {v0, v2, v3, v4}, Lcom/d/a/b;-><init>(Landroid/view/View;II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0c00a1

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lflar2/exkernelmanager/fragments/bp;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bp;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c00a2

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lflar2/exkernelmanager/fragments/bw;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bw;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00db

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->q:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "Nexus6"

    invoke-virtual {v0, p0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    :goto_1
    const-string v0, "prefKcalTestImage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "prefKcalTestImage"

    const-string v1, "macbeth"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const v0, 0x7f0c008f

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v1, "prefKcalTestImage"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "macbeth"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const v1, 0x7f020097

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_4
    :goto_2
    new-instance v1, Lflar2/exkernelmanager/fragments/bx;

    invoke-direct {v1, p0, v0}, Lflar2/exkernelmanager/fragments/bx;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const v1, 0x7f0c007f

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/by;

    invoke-direct {v3, p0, v1}, Lflar2/exkernelmanager/fragments/by;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const v1, 0x7f0c0081

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestFocus()Z

    const v1, 0x7f0c00a5

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->r:Landroid/widget/Switch;

    const v1, 0x7f0c0082

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setVisibility(I)V

    :cond_5
    const v1, 0x7f0c0085

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->t:Landroid/widget/SeekBar;

    const v1, 0x7f0c0089

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->u:Landroid/widget/SeekBar;

    const v1, 0x7f0c008d

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->v:Landroid/widget/SeekBar;

    const v1, 0x7f0c0092

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->w:Landroid/widget/SeekBar;

    const v1, 0x7f0c0096

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->x:Landroid/widget/SeekBar;

    const v1, 0x7f0c009a

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->y:Landroid/widget/SeekBar;

    const v1, 0x7f0c009e

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->z:Landroid/widget/SeekBar;

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->t:Landroid/widget/SeekBar;

    invoke-virtual {v1, v6}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->u:Landroid/widget/SeekBar;

    invoke-virtual {v1, v6}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->v:Landroid/widget/SeekBar;

    invoke-virtual {v1, v6}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->w:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->x:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->z:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    const v1, 0x7f0c0090

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0c0094

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0c009c

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0c0098

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    const v1, 0x7f0c0086

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->A:Landroid/widget/EditText;

    const v1, 0x7f0c008a

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->B:Landroid/widget/EditText;

    const v1, 0x7f0c008e

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->C:Landroid/widget/EditText;

    const v1, 0x7f0c0093

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->D:Landroid/widget/EditText;

    const v1, 0x7f0c0097

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->E:Landroid/widget/EditText;

    const v1, 0x7f0c009b

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->F:Landroid/widget/EditText;

    const v1, 0x7f0c009f

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->G:Landroid/widget/EditText;

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->D:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->E:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->F:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->G:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setVisibility(I)V

    :cond_7
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->l()V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->r:Landroid/widget/Switch;

    new-instance v2, Lflar2/exkernelmanager/fragments/ca;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ca;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->s:Landroid/widget/Switch;

    new-instance v2, Lflar2/exkernelmanager/fragments/cb;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/cb;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_8
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->t:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/cc;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/cc;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->u:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/au;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/au;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->v:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/av;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/av;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->w:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/aw;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/aw;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->x:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/ax;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ax;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->y:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/ay;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ay;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->z:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/fragments/az;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/az;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_9
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->A:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/ba;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/ba;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->C:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/bb;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/bb;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->B:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/bc;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/bc;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-boolean v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->D:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/bd;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/bd;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->E:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/bf;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/bf;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->F:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/bg;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/bg;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->G:Landroid/widget/EditText;

    new-instance v2, Lflar2/exkernelmanager/fragments/bh;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/fragments/bh;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :cond_a
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->A:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bi;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bi;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->B:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bj;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bj;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->C:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bk;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bk;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->O:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->D:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bl;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bl;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->F:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bm;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bm;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->E:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bn;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bn;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->G:Landroid/widget/EditText;

    new-instance v1, Lflar2/exkernelmanager/fragments/bo;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/bo;-><init>(Lflar2/exkernelmanager/fragments/ColorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    return-void

    :cond_c
    iput v2, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->L:I

    goto/16 :goto_0

    :cond_d
    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00dd

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ColorActivity;->q:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "Nexus7"

    invoke-virtual {v0, p0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    const-string v2, "healthyfood"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    :cond_f
    const-string v2, "grayscale"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    const v1, 0x7f020059

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    :cond_10
    const-string v2, "colorscale"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const v1, 0x7f020049

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    :cond_11
    const-string v2, "babies"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ar;->a(Landroid/app/Activity;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->k()V

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/UserSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ColorActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0173 -> :sswitch_2
        0x7f0c0174 -> :sswitch_3
        0x7f0c017c -> :sswitch_1
    .end sparse-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/support/v7/app/ad;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ColorActivity;->l()V

    return-void
.end method
