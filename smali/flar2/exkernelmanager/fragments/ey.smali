.class public Lflar2/exkernelmanager/fragments/ey;
.super Landroid/app/Fragment;


# instance fields
.field a:Lflar2/exkernelmanager/utilities/d;

.field b:Landroid/view/MenuItem;

.field c:Landroid/view/MenuItem;

.field d:Landroid/view/MenuItem;

.field e:Landroid/view/MenuItem;

.field f:Landroid/view/MenuItem;

.field g:Landroid/view/MenuItem;

.field h:Lflar2/exkernelmanager/utilities/f;

.field private i:Landroid/content/Context;

.field private j:Landroid/widget/Button;

.field private k:Landroid/view/View;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/widget/RadioButton;

.field private p:Landroid/widget/RadioButton;

.field private q:Z

.field private r:Landroid/content/BroadcastReceiver;

.field private s:J

.field private t:Landroid/app/DownloadManager;

.field private u:Z

.field private v:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/d;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/d;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->a:Lflar2/exkernelmanager/utilities/d;

    iput v1, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    iput-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->m:Ljava/lang/String;

    iput-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->n:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflar2/exkernelmanager/fragments/ey;->q:Z

    iput-boolean v1, p0, Lflar2/exkernelmanager/fragments/ey;->u:Z

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->h:Lflar2/exkernelmanager/utilities/f;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ey;I)I
    .locals 0

    iput p1, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    return p1
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ey;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/ey;->m:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    const-wide/16 v4, 0x1f4

    const/4 v2, 0x1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->o:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AOSP"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ey;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->a:Lflar2/exkernelmanager/utilities/d;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/d;->b()V

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, Lflar2/exkernelmanager/fragments/fw;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/fw;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->p:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ey;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->a:Lflar2/exkernelmanager/utilities/d;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/d;->b()V

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, Lflar2/exkernelmanager/fragments/fx;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/fx;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ey;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->d()V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ey;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/fragments/ey;Z)Z
    .locals 0

    iput-boolean p1, p0, Lflar2/exkernelmanager/fragments/ey;->q:Z

    return p1
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ey;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v1, 0x7f04000c

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    iget v0, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e0087

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    const-string v0, "prefHTC"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "franco"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "faux"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "leanKernel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ricked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UBER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "haos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "furnace"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Code_Blue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->f()V

    goto/16 :goto_0

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->g()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ey;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ey;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/fragments/ey;Z)Z
    .locals 0

    iput-boolean p1, p0, Lflar2/exkernelmanager/fragments/ey;->u:Z

    return p1
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/ey;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method static synthetic c(Lflar2/exkernelmanager/fragments/ey;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/ey;->n:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0021

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0020

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0045

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fa;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fa;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic d(Lflar2/exkernelmanager/fragments/ey;)J
    .locals 2

    iget-wide v0, p0, Lflar2/exkernelmanager/fragments/ey;->s:J

    return-wide v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URLEncoder.encode() failed for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()V
    .locals 5

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    const v1, 0x7f0e003a

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v0, Lflar2/exkernelmanager/fragments/fy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/fy;-><init>(Lflar2/exkernelmanager/fragments/ey;Lflar2/exkernelmanager/fragments/ez;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prefCheckVersion"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "latest"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/fy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private e()V
    .locals 4

    new-instance v0, Lflar2/exkernelmanager/fragments/fz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/fragments/fz;-><init>(Lflar2/exkernelmanager/fragments/ey;Lflar2/exkernelmanager/fragments/ez;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "prefCheckChangelog"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/fragments/fz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic e(Lflar2/exkernelmanager/fragments/ey;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->h()V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".zip"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/ElementalX/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-express.zip"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, "express"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefAutoReboot"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->l()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->k()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "prefAutoReboot"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->l()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->k()V

    goto :goto_0
.end method

.method static synthetic f(Lflar2/exkernelmanager/fragments/ey;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0023

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0022

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0045

    new-instance v3, Lflar2/exkernelmanager/fragments/fb;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fb;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 4

    const-string v0, "express"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "prefChecksum"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-express"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Lflar2/exkernelmanager/fragments/ga;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lflar2/exkernelmanager/fragments/ga;-><init>(Lflar2/exkernelmanager/fragments/ey;Lflar2/exkernelmanager/fragments/ez;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/fragments/ga;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_0
    const-string v0, "prefChecksum"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, -0x2

    const/4 v6, 0x1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    const v0, 0x7f0c00dc

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c00d5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const v1, 0x7f0c00d7

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const-string v4, "prefInstaller"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "express"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    invoke-virtual {v1, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_0
    new-instance v4, Lflar2/exkernelmanager/fragments/fc;

    invoke-direct {v4, p0}, Lflar2/exkernelmanager/fragments/fc;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lflar2/exkernelmanager/fragments/fd;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/fragments/fd;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c00d9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const-string v1, "prefAutoReboot"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    new-instance v1, Lflar2/exkernelmanager/fragments/fe;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/fe;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v1, 0x7f0e0081

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/fragments/fg;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/fg;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v3, v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e003f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/fragments/ff;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ff;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v1, 0x7f0e0082

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    invoke-virtual {v3}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v7}, Landroid/view/Window;->setLayout(II)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    invoke-virtual {v1, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/app/ab;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1, v7}, Landroid/view/Window;->setLayout(II)V

    goto :goto_1
.end method

.method static synthetic g(Lflar2/exkernelmanager/fragments/ey;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->g()V

    return-void
.end method

.method private g(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1}, Lflar2/exkernelmanager/utilities/l;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x2

    new-instance v0, Landroid/app/DownloadManager$Query;

    invoke-direct {v0}, Landroid/app/DownloadManager$Query;-><init>()V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->t:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "status"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "local_filename"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->i()V

    iput v3, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ey;->g(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->i()V

    iput v3, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    goto :goto_0

    :cond_1
    const-string v0, "prefAutoReboot"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->l()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->j()V

    iput v3, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic h(Lflar2/exkernelmanager/fragments/ey;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->l()V

    return-void
.end method

.method private h(Ljava/lang/String;)V
    .locals 1

    const-string v0, "prefSubVersion"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private i()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0085

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0086

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fi;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fi;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fh;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fh;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private i(Ljava/lang/String;)V
    .locals 1

    const-string v0, "prefDownloadFileName"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic i(Lflar2/exkernelmanager/fragments/ey;)Z
    .locals 1

    iget-boolean v0, p0, Lflar2/exkernelmanager/fragments/ey;->u:Z

    return v0
.end method

.method private j()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0083

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0084

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fl;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fl;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fj;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fj;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic j(Lflar2/exkernelmanager/fragments/ey;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->m()V

    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 1

    const-string v0, "prefLatestVersion"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private k()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0024

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0084

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fn;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fn;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fm;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fm;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private k(Ljava/lang/String;)V
    .locals 7

    const v6, 0x7f0e0133

    const/4 v2, 0x1

    const/4 v0, 0x0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    const-string v4, "ElementalX Update!"

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New ElementalX Kernel for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nhttp://elementalx.org/devices/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "see_all"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ey;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "twitter"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "https://twitter.com/intent/tweet?text=%s&url=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New ElementalX Kernel for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lflar2/exkernelmanager/fragments/ey;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://elementalx.org/devices/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lflar2/exkernelmanager/fragments/ey;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.twitter"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_5
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move v0, v2

    :goto_3
    move v1, v0

    goto :goto_2

    :cond_6
    move v1, v0

    :cond_7
    if-eqz v1, :cond_0

    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/fragments/ey;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method private l()V
    .locals 7

    const/4 v3, 0x0

    new-instance v1, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v1}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    const-string v0, "mkdir /cache/recovery"

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "touch /cache/recovery/command"

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    const-string v0, "777"

    const-string v2, "/cache/recovery/command"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/e/a/a/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    const-string v0, "echo boot-recovery > /cache/recovery/command"

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefInstaller"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "express"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "echo --update_package=/sdcard/ElementalX/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-express.zip >> /cache/recovery/command"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    iput-boolean v3, p0, Lflar2/exkernelmanager/fragments/ey;->u:Z

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00fb

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/fragments/fo;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/fragments/fo;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v7/app/ab;->show()V

    new-instance v0, Lflar2/exkernelmanager/fragments/fp;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lflar2/exkernelmanager/fragments/fp;-><init>(Lflar2/exkernelmanager/fragments/ey;JJLandroid/support/v7/app/ab;)V

    invoke-virtual {v0}, Lflar2/exkernelmanager/fragments/fp;->start()Landroid/os/CountDownTimer;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/e/a/a/a;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "echo --update_package=/sdcard/ElementalX/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".zip >> /cache/recovery/command"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method

.method private m()V
    .locals 2

    :try_start_0
    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->e(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 1

    const-string v0, "prefSubVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 1

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 1

    const-string v0, "prefDownloadFileName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 1

    const-string v0, "prefCurrentKernel"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 1

    const-string v0, "prefLatestVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ElementalX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->b:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->c:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->c:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_2
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hangout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hangout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->f:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->f:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_4
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "apps.plus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "apps.plus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->e:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->e:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_6
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mail"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mail"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->g:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->g:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_8
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_9
    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->d:Landroid/view/MenuItem;

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->d:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->b:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_b
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "nexus-5"

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "nexus-7-2013"

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "htc-one-m8"

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "htc-one-m7"

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "htc-one-s"

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "nexus-6"

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "nexus-9"

    goto :goto_0

    :cond_6
    const-string v0, ""

    goto :goto_0
.end method

.method private u()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "http://forum.xda-developers.com/google-nexus-5/orig-development/kernel-elementalx-n5-0-44-t2519607"

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "http://forum.xda-developers.com/showthread.php?t=2389022"

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "http://forum.xda-developers.com/showthread.php?t=2705613"

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "http://forum.xda-developers.com/showthread.php?t=2249774"

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "http://forum.xda-developers.com/showthread.php?t=2083229"

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "http://forum.xda-developers.com/nexus-6/orig-development/kernel-elementalx-n6-0-01-alpha-t2954680"

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "http://forum.xda-developers.com/nexus-9/orig-development/kernel-elementalx-n9-0-02-alpha-t2931657"

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v2, 0x7f0e00b3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "http://forum.xda-developers.com/one-m9/orig-development/kernel-elementalx-m9-0-1-t3087586"

    goto :goto_0

    :cond_7
    const-string v0, "http://forum.xda-developers.com/"

    goto/16 :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lflar2/exkernelmanager/fragments/ey;->i(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v1

    const-string v2, "E"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    throw v0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->j(Ljava/lang/String;)V

    const-string v0, "Not available"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    const v1, 0x7f0e0039

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iput v2, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->s()V

    return-void

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->a()V

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->e()V

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    const v1, 0x7f0e003b

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iput v4, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->a()V

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->e()V

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    const v1, 0x7f0e0037

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iput v4, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "express"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://elementalx.org/kernels/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-express.zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://elementalx.org/kernels/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    const-string v2, "download"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->t:Landroid/app/DownloadManager;

    new-instance v0, Landroid/app/DownloadManager$Request;

    invoke-direct {v0, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/fragments/ey;->f(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    const-string v1, "express"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ElementalX"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-express.zip"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    :goto_0
    const/4 v1, 0x1

    iput v1, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->t:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    iput-wide v0, p0, Lflar2/exkernelmanager/fragments/ey;->s:J

    return-void

    :cond_0
    const-string v1, "ElementalX"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".zip"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const v0, 0x7f0c0180

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    const v0, 0x7f0c017f

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    if-eqz p1, :cond_1

    const v0, 0x7f0c0175

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->b:Landroid/view/MenuItem;

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->b:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz p1, :cond_2

    const v0, 0x7f0c0177

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->c:Landroid/view/MenuItem;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->c:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c0176

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->e:Landroid/view/MenuItem;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->e:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c0178

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->d:Landroid/view/MenuItem;

    const v0, 0x7f0c0179

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->f:Landroid/view/MenuItem;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->f:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c017a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->g:Landroid/view/MenuItem;

    :cond_2
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p3}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030037

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x7

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget-object v2, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "Update"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0c00b6

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0c00b5

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->x:Landroid/widget/ImageView;

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    new-instance v2, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v2}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/f;->b(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->h:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/f;->d(Landroid/content/Context;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->o()Ljava/lang/String;

    const v0, 0x7f0c012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->r()Ljava/lang/String;

    const v0, 0x7f0c0133

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    invoke-virtual {v0, v5, v7}, Landroid/widget/Button;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    const v2, 0x7f0e0036

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    new-instance v2, Lflar2/exkernelmanager/fragments/ez;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/ez;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput v4, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    const v0, 0x7f0c0131

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->k:Landroid/view/View;

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->b()V

    const v0, 0x7f0c0135

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->o:Landroid/widget/RadioButton;

    const v0, 0x7f0c0136

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->p:Landroid/widget/RadioButton;

    const-string v0, "prefHTC"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v3, 0x7f0e00df

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->i:Landroid/content/Context;

    const v3, 0x7f0e00db

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->o:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setVisibility(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->p:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setVisibility(I)V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->n()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CAF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->p:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->o:Landroid/widget/RadioButton;

    new-instance v2, Lflar2/exkernelmanager/fragments/fk;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/fk;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->p:Landroid/widget/RadioButton;

    new-instance v2, Lflar2/exkernelmanager/fragments/fq;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/fq;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f0c012c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    const v0, 0x7f0c012f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeColor(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v2, Lflar2/exkernelmanager/fragments/fr;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/fr;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/bu;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->a:Lflar2/exkernelmanager/utilities/d;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/d;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "incompatible"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->c()V

    :cond_2
    new-instance v0, Lflar2/exkernelmanager/fragments/ft;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/fragments/ft;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    iput-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->r:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lflar2/exkernelmanager/fragments/ey;->r:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Lflar2/exkernelmanager/fragments/ey;->setHasOptionsMenu(Z)V

    const v0, 0x7f0c0137

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionButton;

    new-instance v2, Lflar2/exkernelmanager/fragments/fu;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/fragments/fu;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    invoke-virtual {v0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1

    :array_0
    .array-data 4
        0x7f0a000f
        0x7f0a0006
        0x7f0a000f
    .end array-data
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ey;->r:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    sget-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const-string v1, "facebook"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->k(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "apps.plus"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->k(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "twitter"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->k(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v1, "hangout"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->k(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    const-string v1, "mail"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->k(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    const-string v1, "see_all"

    invoke-direct {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->k(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0176
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/fragments/ey;->b()V

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/fragments/ey;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, Lflar2/exkernelmanager/fragments/fv;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/fragments/fv;-><init>(Lflar2/exkernelmanager/fragments/ey;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ey;->j:Landroid/widget/Button;

    const v1, 0x7f0e00e1

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/fragments/ey;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput v0, p0, Lflar2/exkernelmanager/fragments/ey;->l:I

    goto :goto_0
.end method
