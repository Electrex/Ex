.class Lflar2/exkernelmanager/fragments/ai;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lflar2/exkernelmanager/fragments/ac;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/fragments/ac;[Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iput-object p2, p0, Lflar2/exkernelmanager/fragments/ai;->a:[Ljava/lang/String;

    iput p3, p0, Lflar2/exkernelmanager/fragments/ai;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ai;->a:[Ljava/lang/String;

    aget-object v0, v0, p2

    if-eqz v0, :cond_0

    iget v1, p0, Lflar2/exkernelmanager/fragments/ai;->b:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v1, "prefCPUC2MaxBoot"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefCPUC2Max"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu5/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu6/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu7/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "prefCPUC2MinBoot"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefCPUC2Min"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu5/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu6/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    iget-object v1, v1, Lflar2/exkernelmanager/fragments/ac;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu7/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/fragments/ai;->c:Lflar2/exkernelmanager/fragments/ac;

    invoke-static {v0}, Lflar2/exkernelmanager/fragments/ac;->a(Lflar2/exkernelmanager/fragments/ac;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch -0x74
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
