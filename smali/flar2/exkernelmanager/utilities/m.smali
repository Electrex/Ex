.class public Lflar2/exkernelmanager/utilities/m;
.super Ljava/lang/Object;


# instance fields
.field a:Lflar2/exkernelmanager/utilities/f;

.field b:Lflar2/exkernelmanager/utilities/e;

.field c:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/m;->a:Lflar2/exkernelmanager/utilities/f;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/m;->b:Lflar2/exkernelmanager/utilities/e;

    return-void
.end method


# virtual methods
.method public a([Ljava/lang/String;)I
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    new-instance v2, Ljava/io/File;

    aget-object v3, p1, v0

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/m;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0, v3, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "NA"

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x200

    invoke-direct {v1, v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->a:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method public a(ILflar2/exkernelmanager/utilities/n;)V
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/m;->b:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v3

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v3

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v3

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefkcalPath"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1"

    sget-object v2, Lflar2/exkernelmanager/r;->p:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {p0, v1, v0}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "prefkcalPath"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->c:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->c:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_1
    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0108

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/m;->c:Landroid/widget/Toast;

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->c:Landroid/widget/Toast;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->c:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->a:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "echo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public final a()Z
    .locals 5

    const/4 v0, 0x0

    const-string v1, "/proc/swaps"

    if-eqz v1, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lflar2/exkernelmanager/utilities/m;->b:Lflar2/exkernelmanager/utilities/e;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cat "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "zram0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0xa

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x200

    invoke-direct {v3, v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_1
    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "NA"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/utilities/m;->a:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
