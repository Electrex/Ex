.class public Lflar2/exkernelmanager/utilities/CircleIndicator;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Landroid/support/v4/view/do;


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:Landroid/support/v4/view/do;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Landroid/animation/Animator;

.field private l:Landroid/animation/Animator;

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const v1, 0x7f0200a2

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const v0, 0x7f050002

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->g:I

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->h:I

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->i:I

    const/4 v0, 0x0

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->j:I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const v1, 0x7f0200a2

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f050002

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->g:I

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->h:I

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->i:I

    const/4 v0, 0x0

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->j:I

    invoke-direct {p0, p1, p2}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(ILandroid/animation/Animator;)V
    .locals 3

    invoke-virtual {p2}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/animation/Animator;->end()V

    :cond_0
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->d:I

    iget v2, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->e:I

    invoke-virtual {p0, v1, v0, v2}, Lflar2/exkernelmanager/utilities/CircleIndicator;->addView(Landroid/view/View;II)V

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->c:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v2, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->c:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    invoke-virtual {p2}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->setOrientation(I)V

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->setGravity(I)V

    invoke-direct {p0, p1, p2}, Lflar2/exkernelmanager/utilities/CircleIndicator;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->f:I

    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->k:Landroid/animation/Animator;

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->f:I

    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    new-instance v1, Lflar2/exkernelmanager/utilities/c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lflar2/exkernelmanager/utilities/c;-><init>(Lflar2/exkernelmanager/utilities/CircleIndicator;Lflar2/exkernelmanager/utilities/b;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->g:I

    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    goto :goto_0
.end method

.method private a(Landroid/support/v4/view/ViewPager;)V
    .locals 3

    invoke-virtual {p0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->removeAllViews()V

    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/bo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/bo;->b()I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->m:I

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->m:I

    if-gtz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->h:I

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->k:Landroid/animation/Animator;

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(ILandroid/animation/Animator;)V

    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->m:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->i:I

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    invoke-direct {p0, v1, v2}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(ILandroid/animation/Animator;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/high16 v4, 0x40a00000    # 5.0f

    const/4 v3, -0x1

    if-eqz p2, :cond_0

    sget-object v0, Lflar2/exkernelmanager/t;->CircleIndicator:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->d:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->e:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->c:I

    const/4 v1, 0x3

    const v2, 0x7f050002

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->f:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->g:I

    const/4 v1, 0x5

    const v2, 0x7f0200a2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->h:I

    const/4 v1, 0x6

    iget v2, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->i:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->d:I

    if-ne v0, v3, :cond_1

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(F)I

    move-result v0

    :goto_0
    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->d:I

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->e:I

    if-ne v0, v3, :cond_2

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(F)I

    move-result v0

    :goto_1
    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->e:I

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->c:I

    if-ne v0, v3, :cond_3

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(F)I

    move-result v0

    :goto_2
    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->c:I

    return-void

    :cond_1
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->d:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->e:I

    goto :goto_1

    :cond_3
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->c:I

    goto :goto_2
.end method


# virtual methods
.method public a(F)I
    .locals 2

    invoke-virtual {p0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    invoke-interface {v0, p1}, Landroid/support/v4/view/do;->a(I)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->k:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->k:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    :cond_2
    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->j:I

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->i:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->l:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->m:I

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_3

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/utilities/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->h:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->k:Landroid/animation/Animator;

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->k:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    :cond_3
    iput p1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->j:I

    return-void
.end method

.method public a(IFI)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/do;->a(IFI)V

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    invoke-interface {v0, p1}, Landroid/support/v4/view/do;->b(I)V

    :cond_0
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/do;)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "can not find Viewpager , setViewPager first"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->b:Landroid/support/v4/view/do;

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/do;)V

    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    iput-object p1, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->a:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->j:I

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(Landroid/support/v4/view/ViewPager;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/do;)V

    iget v0, p0, Lflar2/exkernelmanager/utilities/CircleIndicator;->j:I

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->a(I)V

    return-void
.end method
