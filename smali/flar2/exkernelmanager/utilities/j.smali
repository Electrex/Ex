.class final Lflar2/exkernelmanager/utilities/j;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/utilities/h;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/utilities/h;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/utilities/j;->a:Lflar2/exkernelmanager/utilities/h;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/utilities/h;Lflar2/exkernelmanager/utilities/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/utilities/j;-><init>(Lflar2/exkernelmanager/utilities/h;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    const/4 v0, 0x0

    const/high16 v4, 0x42c80000    # 100.0f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    const/high16 v2, 0x43480000    # 200.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/j;->a:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->a()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/utilities/j;->a:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->c()V

    goto :goto_1
.end method
