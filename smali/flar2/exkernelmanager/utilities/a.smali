.class public Lflar2/exkernelmanager/utilities/a;
.super Ljava/lang/Object;


# instance fields
.field a:Lflar2/exkernelmanager/utilities/e;

.field b:Lflar2/exkernelmanager/utilities/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    return-void
.end method


# virtual methods
.method public a(IZ)V
    .locals 6

    const v5, 0xf4240

    const/4 v0, 0x1

    const-string v1, "prefGovPath"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "prefC2GovPath"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    const-string v0, "prefCPUMaxBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "prefCPUMax"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "prefCPUMax"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "prefCPUMinBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCPUMin"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    const-string v0, "prefCPUC2MaxBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCPUC2Max"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    const-string v0, "prefCPUC2MinBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCPUC2Min"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    const-string v0, "prefCPU0OCBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "CPU0="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CPU0="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefCPU0OC"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "prefCPU1OCBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "CPU1="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CPU1="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefCPU1OC"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "prefCPU2OCBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "CPU2="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CPU2="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefCPU2OC"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "prefCPU3OCBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "CPU3="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CPU3="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefCPU3OC"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "prefL2OCBootBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "OPT="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OPT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefL2OCBoot"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "prefTouchboostBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "prefMaxScroffBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefMaxScroff"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "MAXSCROFF="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MAXSCROFF="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefMaxScroff"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_b
    const-string v0, "prefCPUGovBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCPUGov"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ondemand"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "2"

    :cond_2
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "CPU_GOV="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CPU_GOV="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "prefPowersaverBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "prefMPDecisionBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefMPDecision_NW"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_NS"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_ns"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefMPDecision_TW"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/power/pnpmgr/hotplug/mp_tw"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "prefThermBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefTherm"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    const-string v3, "prefThermPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "prefGPUMaxBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefGPUMax"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    const-string v3, "prefgpuMaxPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    const-string v2, "prefgpuMaxPath"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "GPU_FREQ="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GPU_FREQ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-int v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "GPU_OC="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GPU_OC="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-int/2addr v0, v5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_10
    const-string v0, "prefGPUMinBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Nexus9"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "prefGPUMin"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/kernel/tegra_gpu/gpu_floor_rate"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    const-string v0, "prefGPUMin"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    const-string v3, "prefgpuMinPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "prefGPUGovBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefGPUGov"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    const-string v3, "prefgpuGovPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "1"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    const-string v3, "prefgpuGovPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "simple"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "2"

    :cond_4
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "GPU_GOV="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GPU_GOV="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_12
    const-string v0, "prefBacklightBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBacklight"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    const-string v3, "prefbacklightPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v0, "prefBacklight"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "0"

    :goto_1
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "BLD="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BLD="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "BL="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BL="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "1"

    goto :goto_1

    :sswitch_13
    const-string v0, "prefBacklightMinBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBacklightMin"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_14
    const-string v0, "prefCoolerColorsBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCoolerColors"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/mdss_dsi/parameters/color_preset"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "COLOR="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "COLOR="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefCoolerColors"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "prefHTCColorBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefHTCColor"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "COLOR_ENHANCE="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "COLOR_ENHANCE="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefHTCColor"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_16
    const-string v0, "prefKCALModuleBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "COLOR="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "COLOR="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefKCALModule"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_17
    const-string v0, "prefWGBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefWG"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/wake_gestures"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "WG="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WG="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWG"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_18
    const-string v0, "prefS2SBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefS2S"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "S2S="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "S2S="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefS2S"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_19
    const-string v0, "prefS2WBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefS2W"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    const-string v3, "prefs2wPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "S2W="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "S2W="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefS2W"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "SWEEP2WAKE="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SWEEP2WAKE="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefS2W"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1a
    const-string v0, "prefDT2WBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefDT2W"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    const-string v3, "prefdt2wPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "DT2W="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DT2W="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefDT2W"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1b
    const-string v0, "prefPDBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefPD"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->o:[Ljava/lang/String;

    const-string v3, "prefs2wPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "POCKET="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "POCKET="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefPD"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1c
    const-string v0, "prefWGCamBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefWGCam"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/camera_gesture"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "CAM="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CAM="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWGCam"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1d
    const-string v0, "prefWGVibBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefWGVib"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/vib_strength"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "WVIB="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WVIB="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWGVib"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "VIB_STRENGTH="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VIB_STRENGTH="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWGVib"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HTC_One_m7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "VIB="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VIB="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWGVib"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1e
    const-string v0, "prefOrientBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefOrient"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/orientation"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "ORIENTATION="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ORIENTATION="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefOrient"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1f
    const-string v0, "prefShortSweepBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefShortSweep"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/shortsweep"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "SHORTSWEEP="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SHORTSWEEP="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefShortSweep"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_20
    const-string v0, "prefWTOBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefWTO"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/wake_timeout"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "TIMEOUT="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TIMEOUT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWTO"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_21
    const-string v0, "prefPwrKeySuspendBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Nexus5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "prefPwrKeySuspend"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/qpnp_power_on/parameters/pwrkey_suspend"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/qpnp_power_on/parameters/pwrkey_suspend"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "1"

    :cond_6
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "PWR_KEY="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PWR_KEY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "prefPwrKeySuspend"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/pwrkey_suspend"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "PWR_KEY="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PWR_KEY="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefPwrKeySuspend"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_22
    const-string v0, "prefWGLidBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefWGLid"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/lid_suspend"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "LID_SUS="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LID_SUS="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefWGLid"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_23
    const-string v0, "prefL2WBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefL2W"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/logo2wake"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "LOGO2WAKE="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LOGO2WAKE="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefL2W"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_24
    const-string v0, "prefSchedBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v0, "prefSched"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "2"

    const-string v2, "cfq"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v0, "1"

    :cond_8
    :goto_2
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "SCHED="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SCHED="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v2, "deadline"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v0, "3"

    goto :goto_2

    :cond_a
    const-string v2, "fiops"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v0, "4"

    goto :goto_2

    :cond_b
    const-string v2, "sio"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v0, "5"

    goto :goto_2

    :sswitch_25
    const-string v0, "prefReadaheadBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefReadahead"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/block/mmcblk0/queue/read_ahead_kb"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "READAHEAD="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "READAHEAD="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefReadahead"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_26
    const-string v0, "prefFsyncBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefFsync"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/sync/parameters/fsync_enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    const-string v0, "prefFsync"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "0"

    :goto_3
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "FSYNC="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FSYNC="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "1"

    goto :goto_3

    :sswitch_27
    const-string v0, "prefExFATBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "EXFAT="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EXFAT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefExFAT"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_28
    const-string v1, "prefVibBoot"

    invoke-static {v1, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v1, "prefvibPath"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_d

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v1, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    const-string v2, "prefvibPath"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefVib"

    const-string v2, "[^0-9]"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "SVIB="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SVIB="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefVib"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "GVIB="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GVIB="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefVib"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v0, "prefVib"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    const-string v3, "prefvibPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :sswitch_29
    const-string v0, "prefzRam0Boot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefzRam0"

    const-string v1, "prefzRam0"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2a
    const-string v0, "prefSwappinessBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSwappiness"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/proc/sys/vm/swappiness"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2b
    const-string v0, "prefFCBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefFC"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "FC="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FC="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefFC"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2c
    const-string v0, "prefBLEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBLE"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/i2c-0/0-006a/float_voltage"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "BLE="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BLE="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefBLE"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2d
    const-string v0, "prefOTGCBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefOTGC"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/msm_otg/parameters/usbhost_charge_mode"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    const-string v1, "prefOTGC"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v0, "1"

    :cond_e
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "OTGCM="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OTGCM="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2e
    const-string v0, "prefLidBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefLid"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    const-string v3, "preflidPath"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0"

    const-string v1, "prefLid"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "N"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "prefLid"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_f
    const-string v0, "1"

    :cond_10
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "COVER="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "COVER="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "LID="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2f
    const-string v0, "prefTCPCongBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefTCPCong"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_30
    const-string v0, "prefL2MBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefL2M"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_touch/logo2menu"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "LOGO2MENU="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LOGO2MENU="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefL2M"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_31
    const-string v0, "prefBLNBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBLN"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/leds/button-backlight/blink_buttons"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "BLN="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BLN="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefBLN"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_32
    const-string v0, "prefChargeLEDBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "/sys/class/leds/charging/trigger"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/leds/charging/trigger"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/class/leds/charging/trigger"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefChargeLED"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_33
    const-string v0, "prefDT2SBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefDT2S"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/android_key/doubletap2sleep"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "DT2S="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DT2S="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefDT2S"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_34
    const-string v1, "prefRedBoot"

    invoke-static {v1, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v1, "prefRed"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HTC_One_m7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Nexus7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_11
    const-string v1, "prefKCALModuleBoot"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefKCALModule"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "COLOR="

    const-string v2, "COLOR=1"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_35
    const-string v1, "prefGreenBoot"

    invoke-static {v1, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v1, "prefGreen"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HTC_One_m7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Nexus7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_12
    const-string v1, "prefKCALModuleBoot"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefKCALModule"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "COLOR="

    const-string v2, "COLOR=1"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_36
    const-string v1, "prefBlueBoot"

    invoke-static {v1, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v1, "prefBlue"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Lflar2/exkernelmanager/utilities/n;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HTC_One_m7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Nexus7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_13
    const-string v1, "prefKCALModuleBoot"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefKCALModule"

    const-string v1, "1"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "COLOR="

    const-string v2, "COLOR=1"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_37
    const-string v0, "prefCCProfileBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :sswitch_38
    const-string v0, "prefGBoostBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefGBoost"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "gboost"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "GBOOST="

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GBOOST="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prefGBoost"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_39
    const-string v0, "prefInputMinFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefInputMinFreq"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "input_event_min_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3a
    const-string v0, "prefActiveFloorFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefActiveFloorFreq"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "active_floor_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3b
    const-string v0, "PREF_INPUT_MIN_FREQ2Boot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_INPUT_MIN_FREQ2"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "input_min_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3c
    const-string v0, "PREF_GBOOST_MIN_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_GBOOST_MIN_FREQ"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "gboost_min_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3d
    const-string v0, "PREF_MAX_SCREEN_OFF_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_MAX_SCREEN_OFF_FREQ"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "max_screen_off_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3e
    const-string v0, "prefInputTimeoutBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefInputTimeout"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "input_event_timeout"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3f
    const-string v0, "prefUISamplingRateBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefUISamplingRate"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "ui_sampling_rate"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_40
    const-string v0, "prefTwoPhaseFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefTwoPhaseFreq"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "two_phase_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_41
    const-string v0, "prefUpThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefUpThreshold"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "up_threshold"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_42
    const-string v0, "prefDownDifferentialBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefDownDifferential"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "down_differential"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_43
    const-string v0, "prefSyncFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSyncFreq"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "sync_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_44
    const-string v0, "prefOptFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefOptFreq"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "optimal_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_45
    const-string v0, "PREF_SAMPLING_RATE_MINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_SAMPLING_RATE_MIN"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "sampling_rate_min"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_46
    const-string v0, "PREF_FREQ_DOWN_STEP_BARRIARBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_FREQ_DOWN_STEP_BARRIAR"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "freq_down_step_barriar"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_47
    const-string v0, "PREF_FREQ_DOWN_STEPBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_FREQ_DOWN_STEP"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "freq_down_step"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_48
    const-string v0, "PREF_SAMPLING_RATEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_SAMPLING_RATE"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "sampling_rate"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_49
    const-string v0, "PREF_SAMPLING_DOWN_FACTORBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_SAMPLING_DOWN_FACTOR"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "sampling_down_factor"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4a
    const-string v0, "PREF_UP_THRESHOLD_ANY_CPU_LOADBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_UP_THRESHOLD_ANY_CPU_LOAD"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "up_threshold_any_cpu_load"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4b
    const-string v0, "PREF_TARGET_LOADSBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_TARGET_LOADS"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "target_loads"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4c
    const-string v0, "PREF_TIMER_SLACKBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_TIMER_SLACK"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "timer_slack"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4d
    const-string v0, "PREF_HISPEED_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_HISPEED_FREQ"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "hispeed_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4e
    const-string v0, "PREF_TIMER_RATEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_TIMER_RATE"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "timer_rate"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4f
    const-string v0, "PREF_ABOVE_HISPEED_DELAYBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_ABOVE_HISPEED_DELAY"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "above_hispeed_delay"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_50
    const-string v0, "PREF_BOOSTPULSEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_BOOSTPULSE"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "boostpulse"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_51
    const-string v0, "PREF_BOOSTPULSE_DURATIONBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_BOOSTPULSE_DURATION"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "boostpulse_duration"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_52
    const-string v0, "PREF_GO_HISPEED_LOADBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_GO_HISPEED_LOAD"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "go_hispeed_load"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_53
    const-string v0, "PREF_INPUT_BOOST_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_INPUT_BOOST_FREQ"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "input_boost_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_54
    const-string v0, "PREF_MIN_SAMPLE_TIMEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_MIN_SAMPLE_TIME"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "min_sample_time"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_55
    const-string v0, "PREF_UP_THRESHOLD_ANY_CPU_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_UP_THRESHOLD_ANY_CPU_FREQ"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "up_threshold_any_cpu_freq"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_56
    const-string v0, "PREF_DOWN_THRESHOLDBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_DOWN_THRESHOLD"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "down_threshold"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_57
    const-string v0, "PREF_FREQ_STEPBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_FREQ_STEP"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "freq_step"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_58
    const-string v0, "PREF_BOOSTBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREF_BOOST"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "boost"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_59
    const-string v0, "prefLowLoadDownThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefLowLoadDownThreshold"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "low_load_down_threshold"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5a
    const-string v0, "prefMaxFreqHysteresisBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefMaxFreqHysteresis"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "max_freq_hysteresis"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5b
    const-string v0, "prefInputBoostDurationBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefInputBoostDuration"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "input_boost_duration"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5c
    const-string v0, "prefC2GBoostBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2GBoost"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "gboost"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5d
    const-string v0, "prefC2InputMinFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2InputMinFreq"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_event_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5e
    const-string v0, "prefC2ActiveFloorFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2ActiveFloorFreq"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "active_floor_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5f
    const-string v0, "PREFC2_INPUT_MIN_FREQ2Boot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_INPUT_MIN_FREQ2"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_60
    const-string v0, "PREFC2_GBOOST_MIN_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_GBOOST_MIN_FREQ"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "gboost_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_61
    const-string v0, "PREFC2_MAX_SCREEN_OFF_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_MAX_SCREEN_OFF_FREQ"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "max_screen_off_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_62
    const-string v0, "prefC2InputTimeoutBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2InputTimeout"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_event_timeout"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_63
    const-string v0, "prefC2UISamplingRateBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2UISamplingRate"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ui_sampling_rate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_64
    const-string v0, "prefC2TwoPhaseFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2TwoPhaseFreq"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "two_phase_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_65
    const-string v0, "prefC2UpThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2UpThreshold"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "up_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_66
    const-string v0, "prefC2DownDifferentialBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2DownDifferential"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "down_differential"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_67
    const-string v0, "prefC2SyncFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2SyncFreq"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sync_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_68
    const-string v0, "prefC2OptFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2OptFreq"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "optimal_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_69
    const-string v0, "PREFC2_SAMPLING_RATE_MINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_SAMPLING_RATE_MIN"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sampling_rate_min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6a
    const-string v0, "PREFC2_FREQ_DOWN_STEP_BARRIARBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_FREQ_DOWN_STEP_BARRIAR"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "freq_down_step_barriar"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6b
    const-string v0, "PREFC2_FREQ_DOWN_STEPBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_FREQ_DOWN_STEP"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "freq_down_step"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6c
    const-string v0, "PREFC2_SAMPLING_RATEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_SAMPLING_RATE"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sampling_rate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6d
    const-string v0, "PREFC2_SAMPLING_DOWN_FACTORBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_SAMPLING_DOWN_FACTOR"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sampling_down_factor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6e
    const-string v0, "PREFC2_UP_THRESHOLD_ANY_CPU_LOADBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_UP_THRESHOLD_ANY_CPU_LOAD"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "up_threshold_any_cpu_load"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6f
    const-string v0, "PREFC2_TARGET_LOADSBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_TARGET_LOADS"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "target_loads"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_70
    const-string v0, "PREFC2_TIMER_SLACKBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_TIMER_SLACK"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "timer_slack"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_71
    const-string v0, "PREFC2_HISPEED_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_HISPEED_FREQ"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "hispeed_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_72
    const-string v0, "PREFC2_TIMER_RATEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_TIMER_RATE"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "timer_rate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_73
    const-string v0, "PREFC2_ABOVE_HISPEED_DELAYBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_ABOVE_HISPEED_DELAY"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "above_hispeed_delay"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_74
    const-string v0, "PREFC2_BOOSTPULSEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_BOOSTPULSE"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "boostpulse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_75
    const-string v0, "PREFC2_BOOSTPULSE_DURATIONBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_BOOSTPULSE_DURATION"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "boostpulse_duration"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_76
    const-string v0, "PREFC2_GO_HISPEED_LOADBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_GO_HISPEED_LOAD"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "go_hispeed_load"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_77
    const-string v0, "PREFC2_INPUT_BOOST_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_INPUT_BOOST_FREQ"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_78
    const-string v0, "PREFC2_MIN_SAMPLE_TIMEBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_MIN_SAMPLE_TIME"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "min_sample_time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_79
    const-string v0, "PREFC2_UP_THRESHOLD_ANY_CPU_FREQBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_UP_THRESHOLD_ANY_CPU_FREQ"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "up_threshold_any_cpu_freq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7a
    const-string v0, "PREFC2_DOWN_THRESHOLDBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_DOWN_THRESHOLD"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "down_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7b
    const-string v0, "PREFC2_FREQ_STEPBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_FREQ_STEP"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "freq_step"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7c
    const-string v0, "PREFC2_BOOSTBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "PREFC2_BOOST"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "boost"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7d
    const-string v0, "prefC2LowLoadDownThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2LowLoadDownThreshold"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "low_load_down_threshold"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7e
    const-string v0, "prefC2MaxFreqHysteresisBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2MaxFreqHysteresis"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "max_freq_hysteresis"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7f
    const-string v0, "prefC2InputBoostDurationBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefC2InputBoostDuration"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "input_boost_duration"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_80
    const-string v0, "PREF_SOUND_MIC_GAINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_81
    const-string v0, "PREF_SOUND_CAM_GAINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_82
    const-string v0, "PREF_SOUND_SPEAKER_GAINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_83
    const-string v0, "PREF_SOUND_HEADPHONE_GAINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_84
    const-string v0, "PREF_SOUND_HEADPHONE_PA_GAINBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_85
    const-string v0, "prefSoundLockedBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :sswitch_86
    const-string v0, "prefMicBoostBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefMicBoost"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/mic_boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_87
    const-string v0, "prefVolBoostBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefVolBoost"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_88
    const-string v0, "prefBoostMSBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostMS"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/boost_ms"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_89
    const-string v0, "prefBoostEnabledBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostEnable"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/cpuboost_enable"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8a
    const-string v0, "prefBoostInputBoostFreqBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostInputBoostFreq"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8b
    const-string v0, "prefBoostInputBoostMSBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostInputBoostMS"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/input_boost_ms"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8c
    const-string v0, "prefBoostLoadBasedSyncsBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostLoadBasedSyncs"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/load_based_syncs"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8d
    const-string v0, "prefBoostMigrationLoadThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostMigrationLoadThreshold"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/migration_load_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8e
    const-string v0, "prefBoostSyncThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBoostSyncThreshold"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8f
    const-string v0, "prefHotplugBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefHotplug"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mpdecision"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "HOTPLUG="

    const-string v2, "HOTPLUG=0"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_14
    const-string v0, "prefHotplug"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "custom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "HOTPLUG="

    const-string v2, "HOTPLUG=1"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_90
    const-string v0, "prefSleeperEnabledBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperEnabled"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_91
    const-string v0, "prefSleeperPlugAllBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperPlugAll"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/plug_all"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_92
    const-string v0, "prefSleeperUpThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperUpThreshold"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/up_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_93
    const-string v0, "prefSleeperUpCountMaxBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperUpCountMax"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/up_count_max"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_94
    const-string v0, "prefSleeperUpCountMaxBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperDownCountMax"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/down_count_max"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_95
    const-string v0, "prefSleeperMaxOnlineBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperMaxOnline"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_96
    const-string v0, "prefSleeperSuspendMaxOnlineBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSleeperSuspendMaxOnline"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/platform/msm_sleeper/suspend_max_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_97
    const-string v0, "prefEnabledBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefEnabled"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_98
    const-string v0, "prefCoresOnTouchBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCoresOnTouch"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/cores_on_touch"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_99
    const-string v0, "prefCpufreqUnplugLimitBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCpufreqUnplugLimit"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9a
    const-string v0, "prefFirstLevelBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefFirstLevel"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/first_level"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9b
    const-string v0, "prefHighLoadCounterBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefHighLoadCounter"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/high_load_counter"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9c
    const-string v0, "prefLoadThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefLoadThreshold"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9d
    const-string v0, "prefMaxLoadCounterBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefMaxLoadCounter"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/max_load_counter"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9e
    const-string v0, "prefMinTimeCpuOnlineBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefMinTimeCpuOnline"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/min_time_cpu_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9f
    const-string v0, "prefTimerBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefTimer"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/timer"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a0
    const-string v0, "prefSuspendFrequencyBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefSuspendFrequency"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/class/misc/mako_hotplug_control/suspend_frequency"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a1
    const-string v0, "prefBluPlugEnabledBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugEnabled"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/enabled"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a2
    const-string v0, "prefBluPlugPowersaverModeBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugPowersaverMode"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/powersaver_mode"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a3
    const-string v0, "prefBluPlugMinOnlineBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugMinOnline"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/min_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a4
    const-string v0, "prefBluPlug/max_onlineBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlug/max_online"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/max_online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a5
    const-string v0, "prefBluPlugMaxCoresScreenoffBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugMaxCoresScreenoff"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/max_cores_screenoff"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a6
    const-string v0, "prefBluPlugMaxFreqScreenoffBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugMaxFreqScreenoff"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/max_freq_screenoff"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a7
    const-string v0, "prefBluPlugUpThresholdBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugUpThreshold"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/up_threshold"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a8
    const-string v0, "prefBluPlugUpTimerCntBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugUpTimerCnt"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/up_timer_cnt"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a9
    const-string v0, "prefBluPlugDownTimerCntBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBluPlugDownTimerCnt"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/blu_plug/parameters/down_timer_cnt"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_aa
    const-string v0, "prefCPUQuietEnabledBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefCPUQuietEnabled"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCPUQuietGovernor"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpuquiet/current_governor"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_ab
    const-string v1, "prefSensorIndBoot"

    invoke-static {v1, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v1, "prefSensorInd"

    iget-object v2, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/wakeup/parameters/enable_si_ws"

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefSensorInd"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "N"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    const/4 v0, 0x0

    :cond_15
    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->a:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "SENSOR_IND="

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SENSOR_IND="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_ac
    const-string v0, "prefBFSRRIntervalBoot"

    invoke-static {v0, p2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const-string v0, "prefBFSRRInterval"

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a;->b:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/proc/sys/kernel/rr_interval"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x16b47 -> :sswitch_7f
        -0x16ae3 -> :sswitch_7e
        -0x16a7f -> :sswitch_7d
        -0x16a1b -> :sswitch_5e
        -0x169b7 -> :sswitch_61
        -0x16953 -> :sswitch_60
        -0x168ef -> :sswitch_5f
        -0x1688b -> :sswitch_7c
        -0x16827 -> :sswitch_7b
        -0x167c3 -> :sswitch_7a
        -0x166fb -> :sswitch_79
        -0x16697 -> :sswitch_78
        -0x16633 -> :sswitch_77
        -0x165cf -> :sswitch_76
        -0x1656b -> :sswitch_75
        -0x16507 -> :sswitch_74
        -0x164a3 -> :sswitch_73
        -0x1643f -> :sswitch_72
        -0x163db -> :sswitch_71
        -0x16313 -> :sswitch_70
        -0x162af -> :sswitch_6f
        -0x1624b -> :sswitch_6e
        -0x16183 -> :sswitch_6d
        -0x1611f -> :sswitch_6c
        -0x160bb -> :sswitch_6b
        -0x16057 -> :sswitch_6a
        -0x15ff3 -> :sswitch_69
        -0x1f3f -> :sswitch_68
        -0x1edb -> :sswitch_67
        -0x1e77 -> :sswitch_66
        -0x1e13 -> :sswitch_65
        -0x1daf -> :sswitch_64
        -0x1d4b -> :sswitch_63
        -0x1ce7 -> :sswitch_62
        -0x1c83 -> :sswitch_5d
        -0x1bbb -> :sswitch_5c
        -0x4b8 -> :sswitch_a9
        -0x4b7 -> :sswitch_a8
        -0x4b6 -> :sswitch_a7
        -0x4b5 -> :sswitch_a6
        -0x4b4 -> :sswitch_a5
        -0x4b3 -> :sswitch_a4
        -0x4b2 -> :sswitch_a3
        -0x4b1 -> :sswitch_a2
        -0x4b0 -> :sswitch_a1
        -0x455 -> :sswitch_a0
        -0x454 -> :sswitch_9f
        -0x453 -> :sswitch_9e
        -0x452 -> :sswitch_9d
        -0x451 -> :sswitch_9c
        -0x450 -> :sswitch_9b
        -0x44f -> :sswitch_9a
        -0x44e -> :sswitch_99
        -0x44d -> :sswitch_98
        -0x44c -> :sswitch_97
        -0x3a1 -> :sswitch_5b
        -0x3a0 -> :sswitch_5a
        -0x39f -> :sswitch_59
        -0x39e -> :sswitch_3a
        -0x39d -> :sswitch_3d
        -0x39c -> :sswitch_3c
        -0x39b -> :sswitch_3b
        -0x39a -> :sswitch_58
        -0x399 -> :sswitch_57
        -0x398 -> :sswitch_56
        -0x396 -> :sswitch_55
        -0x395 -> :sswitch_54
        -0x394 -> :sswitch_53
        -0x393 -> :sswitch_52
        -0x392 -> :sswitch_51
        -0x391 -> :sswitch_50
        -0x390 -> :sswitch_4f
        -0x38f -> :sswitch_4e
        -0x38e -> :sswitch_4d
        -0x38c -> :sswitch_4c
        -0x38b -> :sswitch_4b
        -0x38a -> :sswitch_4a
        -0x388 -> :sswitch_49
        -0x387 -> :sswitch_48
        -0x386 -> :sswitch_47
        -0x385 -> :sswitch_46
        -0x384 -> :sswitch_45
        -0x327 -> :sswitch_87
        -0x326 -> :sswitch_86
        -0x325 -> :sswitch_85
        -0x324 -> :sswitch_84
        -0x323 -> :sswitch_83
        -0x322 -> :sswitch_82
        -0x321 -> :sswitch_81
        -0x320 -> :sswitch_80
        -0x2f8 -> :sswitch_91
        -0x2f6 -> :sswitch_aa
        -0x2f5 -> :sswitch_8f
        -0x2f4 -> :sswitch_90
        -0x2f3 -> :sswitch_94
        -0x2f2 -> :sswitch_93
        -0x2f1 -> :sswitch_95
        -0x2f0 -> :sswitch_96
        -0x2ef -> :sswitch_92
        -0x2c2 -> :sswitch_8e
        -0x2c1 -> :sswitch_8d
        -0x2c0 -> :sswitch_8c
        -0x2bf -> :sswitch_8b
        -0x2be -> :sswitch_8a
        -0x2bd -> :sswitch_89
        -0x2bc -> :sswitch_88
        -0xf1 -> :sswitch_13
        -0xd3 -> :sswitch_37
        -0xd2 -> :sswitch_35
        -0x74 -> :sswitch_3
        -0x73 -> :sswitch_2
        -0x70 -> :sswitch_9
        -0x6f -> :sswitch_8
        -0x6e -> :sswitch_7
        -0x6d -> :sswitch_6
        -0x6c -> :sswitch_5
        -0x6b -> :sswitch_4
        -0x4f -> :sswitch_44
        -0x4e -> :sswitch_43
        -0x4d -> :sswitch_42
        -0x4c -> :sswitch_41
        -0x4b -> :sswitch_40
        -0x4a -> :sswitch_3f
        -0x49 -> :sswitch_3e
        -0x48 -> :sswitch_39
        -0x46 -> :sswitch_38
        -0x44 -> :sswitch_ac
        -0x42 -> :sswitch_ab
        -0x41 -> :sswitch_32
        -0x40 -> :sswitch_2a
        -0x3e -> :sswitch_29
        -0x3d -> :sswitch_31
        -0x3c -> :sswitch_30
        -0x3b -> :sswitch_2f
        -0x3a -> :sswitch_2e
        -0x39 -> :sswitch_2d
        -0x38 -> :sswitch_2c
        -0x37 -> :sswitch_2b
        -0x36 -> :sswitch_28
        -0x35 -> :sswitch_27
        -0x34 -> :sswitch_26
        -0x33 -> :sswitch_25
        -0x32 -> :sswitch_24
        -0x2f -> :sswitch_23
        -0x2e -> :sswitch_22
        -0x2d -> :sswitch_21
        -0x2c -> :sswitch_20
        -0x2b -> :sswitch_1f
        -0x2a -> :sswitch_1e
        -0x29 -> :sswitch_1d
        -0x28 -> :sswitch_1c
        -0x27 -> :sswitch_1b
        -0x26 -> :sswitch_33
        -0x25 -> :sswitch_1a
        -0x24 -> :sswitch_19
        -0x23 -> :sswitch_18
        -0x1e -> :sswitch_17
        -0x1d -> :sswitch_36
        -0x1c -> :sswitch_34
        -0x1b -> :sswitch_16
        -0x1a -> :sswitch_15
        -0x19 -> :sswitch_14
        -0x18 -> :sswitch_12
        -0x16 -> :sswitch_11
        -0x15 -> :sswitch_10
        -0x14 -> :sswitch_f
        -0x13 -> :sswitch_d
        -0x12 -> :sswitch_c
        -0xf -> :sswitch_e
        -0xd -> :sswitch_b
        -0xc -> :sswitch_a
        -0xb -> :sswitch_1
        -0xa -> :sswitch_0
    .end sparse-switch
.end method
