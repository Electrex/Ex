.class public Lflar2/exkernelmanager/utilities/d;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "prefSubVersion"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    const-string v0, "prefDeviceName"

    invoke-static {v0, p1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 1

    const-string v0, "prefSubVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 1

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const-string v0, "os.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "prefCurrentKernel"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "null"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    const-string v0, "/system/build.prop"

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    const-string v0, "unknown"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    const-string v0, "incompatible"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/Scanner;

    invoke-direct {v3, v2}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V

    :cond_1
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ro.product.device="

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_2
    if-nez v0, :cond_3

    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    const-string v0, "unknown"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "incompatible"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const-string v0, "incompatible"

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const-string v0, "unknown"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    const-string v0, "incompatible"

    goto :goto_0

    :cond_3
    const-string v2, "m7"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    :goto_2
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SENSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "Sense"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_4
    :goto_3
    const-string v0, "prefHTC"

    invoke-static {v0, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "HTC_One_m7"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v1, "_google"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "GPE"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    const-string v1, "cyanogenmod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    const-string v1, "m7wls"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "m7wlv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    const-string v2, "m8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11

    :goto_4
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SENSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v0, "Sense"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_b
    :goto_5
    const-string v0, "prefHTC"

    invoke-static {v0, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "HTC_One_m8"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    const-string v1, "_google"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v0, "GPE"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_d
    const-string v1, "cyanogenmod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_e
    const-string v1, "m8wls"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "m8wlv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_f
    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_10
    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_11
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "HTC One M9"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    :goto_6
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SENSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    const-string v0, "Sense"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_12
    :goto_7
    const-string v0, "prefHTC"

    invoke-static {v0, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "HTC_One_m9"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_13
    const-string v1, "cyanogenmod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_7

    :cond_14
    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_6

    :cond_15
    const-string v2, "shamu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v0, "AOSP"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    const-string v0, "Nexus6"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_16
    const-string v2, "hammerhead"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1b

    :goto_8
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v2, "cyanogenmod"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "beanstalk"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "aicp_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_17
    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_18
    const-string v0, "cm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_19
    const-string v0, "Nexus5"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1a
    const-string v0, "AOSP"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_8

    :cond_1b
    const-string v2, "flounder"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1c

    const-string v0, "AOSP"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    const-string v0, "Nexus9"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1c
    const-string v2, "flo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_20

    :goto_9
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v2, "cyanogenmod"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_1d
    const-string v0, "cm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_1e
    const-string v0, "Nexus7"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1f
    const-string v0, "AOSP"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_9

    :cond_20
    const-string v2, "deb"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_24

    :goto_a
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v2, "cyanogenmod"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_23

    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_21
    const-string v0, "cm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_22

    const-string v0, "CAF"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_22
    const-string v0, "Nexus7"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_23
    const-string v0, "AOSP"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_a

    :cond_24
    const-string v1, "ville"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    :goto_b
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SENSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    const-string v0, "Sense"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    :cond_25
    const-string v0, "prefHTC"

    invoke-static {v0, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "HTC_One_S"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_26
    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto :goto_b

    :cond_27
    const-string v0, "unknown"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->b(Ljava/lang/String;)V

    const-string v0, "incompatible"

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/d;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_28
    invoke-virtual {p0}, Lflar2/exkernelmanager/utilities/d;->b()V

    const-string v0, "compatible"

    goto/16 :goto_0
.end method

.method public b()V
    .locals 3

    const-string v0, "prefCheckVersion"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://elementalx.org/kernels/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCheckChangelog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://elementalx.org/kernels/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "changelog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefChecksum"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://elementalx.org/kernels/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lflar2/exkernelmanager/utilities/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "checksum"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
