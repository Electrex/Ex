.class public abstract Lflar2/exkernelmanager/utilities/a/a;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:Landroid/view/GestureDetector;

.field private final b:I

.field private c:F

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lflar2/exkernelmanager/utilities/a/a;->a:Landroid/view/GestureDetector;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/utilities/a/a;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/utilities/a/a;->b:I

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    goto :goto_0
.end method

.method public abstract a()V
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lflar2/exkernelmanager/utilities/a/a;->e:Z

    return-void
.end method

.method public abstract b()V
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/utilities/a/a;->c:F

    const/4 v0, 0x0

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lflar2/exkernelmanager/utilities/a/a;->e:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-boolean v3, p0, Lflar2/exkernelmanager/utilities/a/a;->d:Z

    const/4 v2, 0x0

    cmpl-float v2, p4, v2

    if-lez v2, :cond_3

    move v2, v0

    :goto_1
    if-eq v3, v2, :cond_2

    iget-boolean v2, p0, Lflar2/exkernelmanager/utilities/a/a;->d:Z

    if-nez v2, :cond_4

    :goto_2
    iput-boolean v0, p0, Lflar2/exkernelmanager/utilities/a/a;->d:Z

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/utilities/a/a;->c:F

    :cond_2
    iget v0, p0, Lflar2/exkernelmanager/utilities/a/a;->c:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    iget v2, p0, Lflar2/exkernelmanager/utilities/a/a;->b:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_5

    invoke-virtual {p0}, Lflar2/exkernelmanager/utilities/a/a;->a()V

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    iget v2, p0, Lflar2/exkernelmanager/utilities/a/a;->b:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/utilities/a/a;->b()V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/a;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x0

    return v0
.end method
