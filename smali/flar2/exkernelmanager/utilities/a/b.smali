.class public Lflar2/exkernelmanager/utilities/a/b;
.super Lflar2/exkernelmanager/utilities/a/a;

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/a/a;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    iput-object p2, p0, Lflar2/exkernelmanager/utilities/a/b;->b:Landroid/view/View;

    iput-object p3, p0, Lflar2/exkernelmanager/utilities/a/b;->c:Landroid/view/View;

    iput p4, p0, Lflar2/exkernelmanager/utilities/a/b;->d:I

    iput p5, p0, Lflar2/exkernelmanager/utilities/a/b;->e:I

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/utilities/a/b;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->c:Landroid/view/View;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lflar2/exkernelmanager/utilities/a/b;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/a/b;->a(Z)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/utilities/a/b;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lflar2/exkernelmanager/utilities/a/b;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->b:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget v0, p0, Lflar2/exkernelmanager/utilities/a/b;->d:I

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/a/b;->a(I)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lflar2/exkernelmanager/utilities/a/b;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lflar2/exkernelmanager/utilities/a/b;->e:I

    invoke-direct {p0, v0}, Lflar2/exkernelmanager/utilities/a/b;->a(I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lflar2/exkernelmanager/utilities/a/c;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/utilities/a/c;-><init>(Lflar2/exkernelmanager/utilities/a/b;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/utilities/a/b;->a(Z)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
