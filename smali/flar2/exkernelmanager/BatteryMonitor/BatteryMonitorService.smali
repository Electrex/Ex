.class public Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;
.super Landroid/app/Service;


# instance fields
.field private a:Landroid/content/BroadcastReceiver;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    new-instance v0, Lflar2/exkernelmanager/BatteryMonitor/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/BatteryMonitor/b;-><init>(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;Lflar2/exkernelmanager/BatteryMonitor/a;)V

    iput-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)Z
    .locals 1

    iget-boolean v0, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b:Z

    return v0
.end method

.method static synthetic a(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b:Z

    return p1
.end method

.method private b()I
    .locals 5

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v0, "level"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v0, "scale"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    return v0

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I
    .locals 1

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b()I

    move-result v0

    return v0
.end method

.method private c()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J
    .locals 2

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 10

    const/4 v2, 0x1

    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const/4 v0, 0x0

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v3}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v3, "status"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_5

    :cond_0
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b:Z

    const-string v0, "prefBMStartMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    const-string v0, "prefBMFullContinue"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefBMScrOffTime"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOffMarker"

    invoke-static {v0, v8, v9}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOffUsage"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMScrOffLevel"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMScrOnTime"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOnMarker"

    invoke-static {v0, v8, v9}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOnUsage"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMScrOnLevel"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMPlugMarker"

    invoke-static {v0, v8, v9}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMChargeTime"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMDeepSleepOffset"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMFullMarker"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMFullTimeCharge"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMDeepSleepFullOffset"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMFullTimeOn"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMFullTimeOff"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMFullUsageOn"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMFullUsageOff"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMCustomMarker"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMCustomTimeCharge"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMDeepSleepCustomOffset"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMCustomTimeOn"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMCustomTimeOff"

    invoke-static {v0, v6, v7}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMCustomUsageOn"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMCustomUsageOff"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    :cond_1
    const-string v0, "prefBMStartMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-string v2, "prefBMDeepSleepOffset"

    invoke-static {v2, v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    :cond_2
    iget-boolean v0, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b:Z

    if-nez v0, :cond_7

    const-string v0, "prefBMStartMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_3

    const-string v0, "prefBMStartMarker"

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    :cond_3
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "prefBMScrOnMarker"

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOnLevel"

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b()I

    move-result v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    :cond_4
    :goto_2
    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a()V

    return-void

    :cond_5
    move v0, v1

    goto/16 :goto_1

    :cond_6
    const-string v0, "prefBMScrOffMarker"

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOffLevel"

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b()I

    move-result v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    goto :goto_2

    :cond_7
    const-string v0, "prefBMStartMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_4

    const-string v0, "prefBMPlugMarker"

    invoke-direct {p0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    goto :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "prefBMStartMarker"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
