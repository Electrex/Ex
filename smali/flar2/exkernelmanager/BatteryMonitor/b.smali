.class Lflar2/exkernelmanager/BatteryMonitor/b;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;Lflar2/exkernelmanager/BatteryMonitor/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/BatteryMonitor/b;-><init>(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I

    move-result v0

    const-string v1, "prefBMScrOnMarker"

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v1, "prefBMScrOffTime"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v4

    const-string v1, "prefBMScrOffMarker"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    const-string v1, "prefBMScrOffTime"

    invoke-static {v1, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v1, "prefBMScrOnLevel"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v1, "prefBMScrOffUsage"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    const-string v2, "prefBMScrOffLevel"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    sub-int v0, v2, v0

    add-int/2addr v0, v1

    const-string v1, "prefBMScrOffUsage"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v0}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I

    move-result v0

    const-string v1, "prefBMScrOffMarker"

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v1, "prefBMScrOnTime"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v4

    const-string v1, "prefBMScrOnMarker"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    const-string v1, "prefBMScrOnTime"

    invoke-static {v1, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v1, "prefBMScrOffLevel"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v1, "prefBMScrOnUsage"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    const-string v2, "prefBMScrOnLevel"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v2

    sub-int v0, v2, v0

    add-int/2addr v0, v1

    const-string v1, "prefBMScrOnUsage"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;Z)Z

    const-string v0, "prefBMPlugMarker"

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOffMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    const-string v2, "prefBMScrOnMarker"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    const-string v0, "prefBMScrOffTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    const-string v4, "prefBMScrOffMarker"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    const-string v2, "prefBMScrOffTime"

    invoke-static {v2, v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOffUsage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    const-string v1, "prefBMScrOffLevel"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    const-string v1, "prefBMScrOffUsage"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_3
    const-string v0, "prefBMScrOnTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    const-string v4, "prefBMScrOnMarker"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    const-string v2, "prefBMScrOnTime"

    invoke-static {v2, v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOnUsage"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    const-string v1, "prefBMScrOnLevel"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    const-string v1, "prefBMScrOnUsage"

    invoke-static {v1, v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->a(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;Z)Z

    const-string v0, "prefBMChargeTime"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v2}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    const-string v4, "prefBMPlugMarker"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    const-string v2, "prefBMChargeTime"

    invoke-static {v2, v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMStartMarker"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->d(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const-string v0, "prefBMStartMarker"

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    :cond_5
    const-string v0, "prefBMScrOnMarker"

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOnLevel"

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I

    move-result v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    const-string v0, "prefBMScrOffMarker"

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->c(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;J)V

    const-string v0, "prefBMScrOffLevel"

    iget-object v1, p0, Lflar2/exkernelmanager/BatteryMonitor/b;->a:Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-static {v1}, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;->b(Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;)I

    move-result v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
