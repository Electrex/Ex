.class public Lflar2/exkernelmanager/AboutActivity;
.super Landroid/support/v7/app/ad;


# instance fields
.field n:Lcom/a/a/a/a/c;

.field o:Lflar2/exkernelmanager/utilities/h;

.field p:Ljava/lang/String;

.field q:Ljava/lang/String;

.field r:Ljava/lang/String;

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lflar2/exkernelmanager/AboutActivity;->s:Z

    const-string v0, "$2.00 CAD"

    iput-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->p:Ljava/lang/String;

    const-string v0, "$5.00 CAD"

    iput-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->q:Ljava/lang/String;

    const-string v0, "$10.00 CAD"

    iput-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->r:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/AboutActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/AboutActivity;->l()V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/AboutActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/AboutActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/AboutActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lflar2/exkernelmanager/AboutActivity;->s:Z

    return p1
.end method

.method static synthetic b(Lflar2/exkernelmanager/AboutActivity;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/AboutActivity;->k()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/AboutActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lflar2/exkernelmanager/AboutActivity;->s:Z

    return v0
.end method

.method private k()V
    .locals 5

    const/16 v4, 0xf

    invoke-virtual {p0}, Lflar2/exkernelmanager/AboutActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/support/v7/app/ac;

    invoke-direct {v2, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    const v0, 0x7f0e0013

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v0, 0x7f0c0156

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c0157

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c0159

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c015b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c015d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c015f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c0161

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c0163

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "#4270c3"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0c0165

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "#4270c3"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const v0, 0x7f0e00ea

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/c;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/c;-><init>(Lflar2/exkernelmanager/AboutActivity;)V

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v2}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private l()V
    .locals 4

    invoke-virtual {p0}, Lflar2/exkernelmanager/AboutActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/support/v7/app/ac;

    invoke-direct {v2, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/support/v7/app/ac;->a(Landroid/view/View;)Landroid/support/v7/app/ac;

    const v0, 0x7f0e0011

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    const v0, 0x7f0c00cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/AboutActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c00d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/AboutActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c00d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lflar2/exkernelmanager/AboutActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c00cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lflar2/exkernelmanager/d;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/d;-><init>(Lflar2/exkernelmanager/AboutActivity;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c00ce

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lflar2/exkernelmanager/e;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/e;-><init>(Lflar2/exkernelmanager/AboutActivity;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c00d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lflar2/exkernelmanager/f;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/f;-><init>(Lflar2/exkernelmanager/AboutActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->o:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/a/a/a/a/c;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ad;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/AboutActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/AboutActivity;->overridePendingTransition(II)V

    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->setContentView(I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/AboutActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/a;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/a;-><init>(Lflar2/exkernelmanager/AboutActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->o:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c0056

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/AboutActivity;->o:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lcom/a/a/a/a/c;

    const-string v1, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm5DGkWq4RC8dQJwEUZTOlWoCPaztDE759fAQmHLFLq8JTXWO4V+lEVdjGdMe8YtOIpEZHJ133SuEiSvCt9YQtJU06sbpjkzSUKFnvLYyv1dPV/HmEn7NOzlYGgD7QjcAuSE4bW5FFI8pa4/KYruDtcZA1UFKiNM2wEYz0b3TSg3cTr1BAivY6jLld9pP1lDRZw0RYJj378oVo2IYuBvVzI+IF0FlrAxylYKqz0c6AQol874y20y21X3J7zg5YoZm2ec+uU2sMntbGczorwLiHYvnL8lFAeTNWixw8KGeTh6YW8+q3PXw3nsx8jcLSvgfuxSp5djp+g9Bf9cx6pE0hQIDAQAB"

    new-instance v2, Lflar2/exkernelmanager/b;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/b;-><init>(Lflar2/exkernelmanager/AboutActivity;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/a/a/a/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/a/e;)V

    iput-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    invoke-virtual {v0}, Lcom/a/a/a/a/c;->c()V

    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/ad;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lflar2/exkernelmanager/AboutActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
