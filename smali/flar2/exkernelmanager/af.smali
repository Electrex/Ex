.class Lflar2/exkernelmanager/af;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/do;


# instance fields
.field final synthetic a:Landroid/support/v4/view/ViewPager;

.field final synthetic b:[I

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/widget/Button;

.field final synthetic e:Landroid/widget/Button;

.field final synthetic f:Lflar2/exkernelmanager/utilities/CircleIndicator;

.field final synthetic g:Lflar2/exkernelmanager/TutorialActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/TutorialActivity;Landroid/support/v4/view/ViewPager;[ILandroid/view/View;Landroid/widget/Button;Landroid/widget/Button;Lflar2/exkernelmanager/utilities/CircleIndicator;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/af;->g:Lflar2/exkernelmanager/TutorialActivity;

    iput-object p2, p0, Lflar2/exkernelmanager/af;->a:Landroid/support/v4/view/ViewPager;

    iput-object p3, p0, Lflar2/exkernelmanager/af;->b:[I

    iput-object p4, p0, Lflar2/exkernelmanager/af;->c:Landroid/view/View;

    iput-object p5, p0, Lflar2/exkernelmanager/af;->d:Landroid/widget/Button;

    iput-object p6, p0, Lflar2/exkernelmanager/af;->e:Landroid/widget/Button;

    iput-object p7, p0, Lflar2/exkernelmanager/af;->f:Lflar2/exkernelmanager/utilities/CircleIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/af;->d:Landroid/widget/Button;

    iget-object v1, p0, Lflar2/exkernelmanager/af;->g:Lflar2/exkernelmanager/TutorialActivity;

    const v2, 0x7f0e007d

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/TutorialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/af;->d:Landroid/widget/Button;

    new-instance v1, Lflar2/exkernelmanager/ag;

    invoke-direct {v1, p0, p1}, Lflar2/exkernelmanager/ag;-><init>(Lflar2/exkernelmanager/af;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/af;->d:Landroid/widget/Button;

    iget-object v1, p0, Lflar2/exkernelmanager/af;->g:Lflar2/exkernelmanager/TutorialActivity;

    const v2, 0x7f0e00d8

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/TutorialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(IFI)V
    .locals 10

    const/4 v9, 0x4

    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v7, 0x447a0000    # 1000.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v5, 0x40800000    # 4.0f

    if-ge p1, v9, :cond_1

    if-ge p1, v9, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/af;->a:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lflar2/exkernelmanager/af;->g:Lflar2/exkernelmanager/TutorialActivity;

    iget-object v0, v0, Lflar2/exkernelmanager/TutorialActivity;->n:Landroid/animation/ArgbEvaluator;

    iget-object v2, p0, Lflar2/exkernelmanager/af;->b:[I

    aget v2, v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/af;->b:[I

    add-int/lit8 v4, p1, 0x1

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, p2, v2, v3}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setBackgroundColor(I)V

    :goto_0
    int-to-float v0, p1

    add-float/2addr v0, p2

    div-float/2addr v0, v5

    const/high16 v1, 0x457a0000    # 4000.0f

    mul-float/2addr v0, v1

    const v1, 0x453b8000    # 3000.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lflar2/exkernelmanager/af;->c:Landroid/view/View;

    div-float v2, v0, v7

    sub-float v2, v5, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/af;->d:Landroid/widget/Button;

    div-float v2, v0, v7

    sub-float v2, v5, v2

    sub-float/2addr v2, v8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setAlpha(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/af;->e:Landroid/widget/Button;

    div-float v2, v0, v7

    sub-float v2, v5, v2

    sub-float/2addr v2, v8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setAlpha(F)V

    iget-object v1, p0, Lflar2/exkernelmanager/af;->f:Lflar2/exkernelmanager/utilities/CircleIndicator;

    div-float/2addr v0, v7

    sub-float v0, v5, v0

    sub-float/2addr v0, v8

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->setAlpha(F)V

    :goto_1
    if-ne p1, v9, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/af;->g:Lflar2/exkernelmanager/TutorialActivity;

    invoke-virtual {v0}, Lflar2/exkernelmanager/TutorialActivity;->finish()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/af;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lflar2/exkernelmanager/af;->b:[I

    iget-object v2, p0, Lflar2/exkernelmanager/af;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setBackgroundColor(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/af;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lflar2/exkernelmanager/af;->d:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setAlpha(F)V

    iget-object v0, p0, Lflar2/exkernelmanager/af;->e:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setAlpha(F)V

    iget-object v0, p0, Lflar2/exkernelmanager/af;->f:Lflar2/exkernelmanager/utilities/CircleIndicator;

    invoke-virtual {v0, v6}, Lflar2/exkernelmanager/utilities/CircleIndicator;->setAlpha(F)V

    goto :goto_1
.end method

.method public b(I)V
    .locals 0

    return-void
.end method
