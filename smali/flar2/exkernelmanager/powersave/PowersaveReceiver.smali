.class public Lflar2/exkernelmanager/powersave/PowersaveReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/StartActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/powersave/PowersaveReceiver;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "flar2.exkernelmanager.powersaver.DISABLE_POWERSAVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefDeviceName"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lflar2/exkernelmanager/powersave/PowersaveReceiver;->a()V

    :cond_0
    :goto_0
    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/powersave/PowersaveReceiver;->a:Landroid/content/SharedPreferences;

    const-string v2, "prefAutoPowersave"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const-string v1, "flar2.exkernelmanager.powersaver.ENABLE_POWERSAVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lflar2/exkernelmanager/powersave/PowersaveReceiver;->a()V

    :cond_3
    :goto_1
    return-void

    :cond_4
    new-instance v1, Lflar2/exkernelmanager/powersave/a;

    invoke-direct {v1, p1}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    goto :goto_0

    :cond_5
    new-instance v0, Lflar2/exkernelmanager/powersave/a;

    invoke-direct {v0, p1}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    goto :goto_1
.end method
