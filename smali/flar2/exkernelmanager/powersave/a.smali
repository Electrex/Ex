.class public Lflar2/exkernelmanager/powersave/a;
.super Ljava/lang/Object;


# instance fields
.field a:Landroid/content/SharedPreferences;

.field protected b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/powersave/a;->a:Landroid/content/SharedPreferences;

    return-void
.end method

.method private b()V
    .locals 5

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/powersave/PowersaveWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    const-class v4, Lflar2/exkernelmanager/powersave/PowersaveWidgetProvider;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "appWidgetIds"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "flar2.exkernelmanager.powersaver.DISABLE_POWERSAVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    const v3, 0x7f0e00f6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    const v3, 0x7f0e00f7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f020080

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    iget v0, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v1, Landroid/app/Notification;->flags:I

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v2, 0x64

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public a(Z)V
    .locals 14

    const v13, 0x7f0e00dd

    const v12, 0x7f0e00d9

    const/4 v11, 0x1

    const/4 v10, 0x0

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    new-instance v1, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v1}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    new-instance v2, Lflar2/exkernelmanager/performance/a;

    iget-object v3, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, Lflar2/exkernelmanager/performance/a;-><init>(Landroid/content/Context;)V

    sget-object v3, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v3

    sget-object v4, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v4

    sget-object v5, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    invoke-virtual {v0, v5}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v5

    const-string v6, "prefCPUMaxPrefPS"

    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "prefGPUMaxPrefPS"

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz p1, :cond_e

    const-string v8, "prefPerformance"

    invoke-virtual {p0, v8}, Lflar2/exkernelmanager/powersave/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v2, v10}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    :cond_0
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "prefCPUMaxPS"

    const-string v8, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v2, "prefGPUMaxPS"

    sget-object v8, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    aget-object v8, v8, v5

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "prefBacklightBootPS"

    sget-object v8, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    aget-object v8, v8, v4

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "prefDeviceName"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v8, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "prefDeviceName"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v8, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const-string v2, "prefDT2WPS"

    sget-object v8, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    aget-object v8, v8, v10

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "prefS2WPS"

    sget-object v8, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    aget-object v8, v8, v10

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "prefVibPS"

    const-string v8, "[^0-9]"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, v2}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "prefSleeperMaxOnlinePS"

    const-string v3, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v0, v3}, Lflar2/exkernelmanager/utilities/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v6, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v2, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "0"

    const-string v2, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "0"

    const-string v2, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "0"

    const-string v2, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "0"

    const-string v2, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    sget-object v1, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v7, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefDimmerPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1"

    sget-object v2, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v1, "prefDeviceName"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v2, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "prefDeviceName"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v2, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    const-string v1, "prefWakePS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "0"

    sget-object v2, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "0"

    sget-object v2, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const-string v1, "prefVibOptPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "0"

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "1200"

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    aget-object v2, v2, v11

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v1, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "2"

    const-string v2, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const-string v0, "prefPowersaver"

    invoke-virtual {p0, v0, v11}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v11}, Lflar2/exkernelmanager/powersave/a;->b(Z)V

    invoke-direct {p0}, Lflar2/exkernelmanager/powersave/a;->b()V

    :goto_2
    return-void

    :cond_a
    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "prefCPUMaxPS"

    const-string v8, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v2, "prefCPUMaxPS"

    const-string v8, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v6, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v6, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v6, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v6, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "1"

    const-string v3, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v6, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    const-string v2, "prefCPUMaxPS"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v2, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    const-string v2, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const-string v1, "prefGPUMaxPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefBacklightBootPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefDeviceName"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v2, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, "prefDeviceName"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    invoke-virtual {v2, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    :cond_10
    const-string v1, "prefDT2WPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefS2WPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    const-string v1, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    const-string v1, "prefSleeperMaxOnlinePS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    const-string v1, "prefVibPS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefPowersaver"

    invoke-virtual {p0, v0, v10}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v10}, Lflar2/exkernelmanager/powersave/a;->b(Z)V

    invoke-direct {p0}, Lflar2/exkernelmanager/powersave/a;->b()V

    goto/16 :goto_2

    :cond_13
    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    const-string v2, "prefCPUMaxPS"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v2, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_14
    const-string v2, "prefCPUMaxPS"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v2, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "1"

    const-string v7, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v6, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v2, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "1"

    const-string v7, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v6, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v2, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "1"

    const-string v7, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v6, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v2, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public b(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 2

    if-eqz p1, :cond_1

    const-string v0, "prefPowersaverNotify"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/powersave/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/powersave/a;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lflar2/exkernelmanager/powersave/a;->b:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method
