.class public Lflar2/exkernelmanager/UserSettingsActivity;
.super Landroid/support/v7/app/ad;


# instance fields
.field n:Lflar2/exkernelmanager/utilities/h;

.field private o:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/UserSettingsActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/h;->b()Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onBackPressed()V

    const/4 v0, 0x0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/UserSettingsActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04001d

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/UserSettingsActivity;->overridePendingTransition(II)V

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/UserSettingsActivity;->o:Landroid/content/Context;

    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity;->setContentView(I)V

    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/aj;

    invoke-direct {v0, p0, p0}, Lflar2/exkernelmanager/aj;-><init>(Lflar2/exkernelmanager/UserSettingsActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/UserSettingsActivity;->n:Lflar2/exkernelmanager/utilities/h;

    const v0, 0x7f0c00bb

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity;->n:Lflar2/exkernelmanager/utilities/h;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
