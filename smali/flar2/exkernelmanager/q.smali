.class Lflar2/exkernelmanager/q;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/MainActivity;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/MainActivity;Lflar2/exkernelmanager/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/q;-><init>(Lflar2/exkernelmanager/MainActivity;)V

    return-void
.end method

.method private a()V
    .locals 4

    const-string v0, "prefCPUNotify"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v1}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "tempUnit"

    const-string v2, "prefTempUnit"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v1, v0}, Lflar2/exkernelmanager/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v2}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/MainActivity;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method private b()V
    .locals 4

    const-string v0, "prefDisableBatteryMon"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefDisableBatteryMon"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v2}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/MainActivity;->stopService(Landroid/content/Intent;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "prefDisableBatteryMon"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v2}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "prefCheckForUpdates"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lflar2/exkernelmanager/DailyListener;

    invoke-direct {v0}, Lflar2/exkernelmanager/DailyListener;-><init>()V

    iget-object v1, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v1}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/commonsware/cwac/wakeful/a;->a(Lcom/commonsware/cwac/wakeful/b;Landroid/content/Context;Z)V

    :cond_0
    const-string v0, "prefPowersaverNotify"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lflar2/exkernelmanager/powersave/a;

    iget-object v1, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v1}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    const-string v1, "prefPowersaverNotify"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "prefPowersaver"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0, v3}, Lflar2/exkernelmanager/powersave/a;->b(Z)V

    :cond_1
    :goto_0
    const-string v0, "prefPerformanceNotify"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflar2/exkernelmanager/performance/a;

    iget-object v1, p0, Lflar2/exkernelmanager/q;->a:Lflar2/exkernelmanager/MainActivity;

    invoke-virtual {v1}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/performance/a;-><init>(Landroid/content/Context;)V

    const-string v1, "prefPerformanceNotify"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "prefPerformance"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0, v3}, Lflar2/exkernelmanager/performance/a;->b(Z)V

    :cond_2
    :goto_1
    const-string v0, "prefColorDashboard"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_3
    const-string v0, "prefDisableBatteryMon"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lflar2/exkernelmanager/q;->b()V

    :cond_4
    const-string v0, "prefCPUNotify"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lflar2/exkernelmanager/q;->a()V

    :cond_5
    const-string v0, "prefTempUnit"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "prefCPUNotify"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lflar2/exkernelmanager/q;->a()V

    :cond_6
    return-void

    :cond_7
    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/powersave/a;->b(Z)V

    goto :goto_0

    :cond_8
    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/performance/a;->b(Z)V

    goto :goto_1
.end method
