.class public Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;
.super Landroid/preference/PreferenceFragment;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    const-string v0, "prefTempUnit"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const-string v1, "prefTempUnit"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v2, 0x7f0e009f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v2, 0x7f0e0040

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->d()V

    return-void
.end method

.method private b()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v2, 0x7f0e0075

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v3, 0x7f0e0074

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/al;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/al;-><init>(Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/ak;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/ak;-><init>(Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;)V
    .locals 0

    invoke-direct {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->e()V

    return-void
.end method

.method private c()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v2, 0x7f0e0176

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v3, 0x7f0e0175

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/an;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/an;-><init>(Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e00ea

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/am;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/am;-><init>(Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private d()V
    .locals 4

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ElementalX"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".zip"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->stopService(Landroid/content/Intent;)Z

    new-instance v0, Lflar2/exkernelmanager/powersave/a;

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    new-instance v0, Lflar2/exkernelmanager/performance/a;

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/performance/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finishAffinity()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lflar2/exkernelmanager/MainApp;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v0, 0x7f070003

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->addPreferencesFromResource(I)V

    const-string v0, "prefSubVersion"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Sense"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "catPower"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string v1, "prefPowersaverNotify"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    const-string v1, "prefAutoPowersave"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "prefTempUnit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "prefTempUnit"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefTempUnit"

    const-string v2, "2"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v2, 0x7f0e009f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "prefTempUnit"

    const-string v2, "1"

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a:Landroid/content/Context;

    const v2, 0x7f0e0040

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "prefTutorial"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/TutorialActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "prefDeleteDownloads"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->b()V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "prefDeleteData"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->c()V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    invoke-direct {p0}, Lflar2/exkernelmanager/UserSettingsActivity$PrefsFragment;->a()V

    return-void
.end method
