.class public Lflar2/exkernelmanager/boot/BootService;
.super Landroid/app/Service;


# static fields
.field static final synthetic e:Z


# instance fields
.field a:Lflar2/exkernelmanager/utilities/m;

.field b:Landroid/content/SharedPreferences;

.field c:Lflar2/exkernelmanager/utilities/f;

.field d:Lflar2/exkernelmanager/utilities/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lflar2/exkernelmanager/boot/BootService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflar2/exkernelmanager/boot/BootService;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    return-void
.end method

.method private a(ILflar2/exkernelmanager/utilities/n;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chmod 666 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    const-string v3, "prefkcalPath"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    const-string v2, "prefkcalPath"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v3

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v3

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/n;->ordinal()I

    move-result v3

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lflar2/exkernelmanager/r;->q:[Ljava/lang/String;

    const-string v3, "prefkcalPath"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->b:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 10

    const v9, 0x7f0e00db

    const v8, 0x7f0e00dd

    const v7, 0x7f0e00d9

    const/4 v2, 0x0

    const-string v0, "ElementalX Kernel"

    const-string v1, "apply settings"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "prefCPUNotify"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/boot/BootService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "tempUnit"

    const-string v3, "prefTempUnit"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    const-string v0, "prefDisableBatteryMon"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "prefBMStartMarker"

    const-wide/16 v4, -0x1

    invoke-virtual {p0, v0, v4, v5}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;J)V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflar2/exkernelmanager/boot/BootService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    const-string v0, "prefCPUTimeSaveOffsets"

    invoke-virtual {p0, v0, v2}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;Z)V

    const-string v0, "prefCPUGovBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUGov"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUGov"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUGov"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUGov"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v0, "prefGPUMinBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00df

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b0

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMin"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/tegra_gpu/gpu_floor_rate"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/kernel/tegra_gpu/gpu_floor_state"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    const-string v0, "prefGPUGovBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUGov"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    const-string v4, "prefgpuGovPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v0, "prefSchedBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSched"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v0, "prefTCPCongBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefTCPCong"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v0, "prefSensorIndBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSensorInd"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/wakeup/parameters/enable_si_ws"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const-string v0, "prefBFSRRIntervalBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBFSRRInterval"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/proc/sys/kernel/rr_interval"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v0, "prefKCALModuleBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "prefKCALModule"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "su -c insmod /system/lib/modules/msm_kcal_ctrl.ko"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "insmod /system/lib/modules/msm_kcal_ctrl.ko"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    :cond_9
    const-string v0, "prefMaxScroffBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "666"

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMaxScroff"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const-string v0, "prefVibBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "chmod 666 "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    const-string v4, "prefvibPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefVib"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    const-string v4, "prefvibPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const-string v0, "prefCCProfileBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "prefCCProfile"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    new-instance v3, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/ElementalX/color_profiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "prefCCProfile"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_1
    sget-boolean v3, Lflar2/exkernelmanager/boot/BootService;->e:Z

    if-nez v3, :cond_b1

    if-nez v0, :cond_b1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_c
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    aget-object v3, v0, v2

    const-string v4, "/sys/module/dsi_panel/kgamma_bn"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v3, 0x1

    aget-object v3, v0, v3

    const-string v4, "/sys/module/dsi_panel/kgamma_bp"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v3, 0x2

    aget-object v3, v0, v3

    const-string v4, "/sys/module/dsi_panel/kgamma_gn"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v3, 0x3

    aget-object v3, v0, v3

    const-string v4, "/sys/module/dsi_panel/kgamma_gp"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v3, 0x4

    aget-object v3, v0, v3

    const-string v4, "/sys/module/dsi_panel/kgamma_rn"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v3, 0x5

    aget-object v3, v0, v3

    const-string v4, "/sys/module/dsi_panel/kgamma_rp"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const/4 v3, 0x6

    aget-object v0, v0, v3

    const-string v3, "/sys/module/dsi_panel/kgamma_w"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const-string v0, "prefVoltageBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "prefVoltagePath"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefVoltage"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    const-string v0, "prefCPUMaxBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "prefCPUMax"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b3

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    :goto_2
    const-string v0, "prefCPUMinBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "prefCPUMin"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    const-string v0, "prefCPUC2MaxBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "prefCPUC2Max"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu5/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu6/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu7/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    const-string v0, "prefCPUC2MinBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "prefCPUC2Min"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu4/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu5/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu5/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu6/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu6/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu7/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu7/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    const-string v0, "prefMPDecisionBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/platform/msm_sleeper"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "prefMPDecision"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Disabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b6

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefDeviceName"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b5

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    :goto_3
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    :goto_4
    const-string v0, "prefThermBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefTherm"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->r:[Ljava/lang/String;

    const-string v4, "prefThermPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    const-string v0, "prefBacklightMinBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBacklightMin"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sys/devices/system/cpu/cpufreq/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefGBoostBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefGBoost"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "gboost"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    const-string v1, "PREF_INPUT_MIN_FREQ2Boot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_INPUT_MIN_FREQ2"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    const-string v1, "PREF_GBOOST_MIN_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_GBOOST_MIN_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "gboost_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    const-string v1, "PREF_MAX_SCREEN_OFF_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_MAX_SCREEN_OFF_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max_screen_off_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    const-string v1, "prefInputMinFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefInputMinFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_event_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1b
    const-string v1, "prefActiveFloorFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefActiveFloorFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "active_floor_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    const-string v1, "prefInputTimeoutBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1d

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefInputTimeout"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_event_timeout"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    const-string v1, "prefUISamplingRateBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefUISamplingRate"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ui_sampling_rate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1e
    const-string v1, "prefTwoPhaseFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefTwoPhaseFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "two_phase_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1f
    const-string v1, "prefUpThresholdBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefUpThreshold"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_20
    const-string v1, "prefDownDifferentialBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_21

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefDownDifferential"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "down_differential"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_21
    const-string v1, "prefSyncFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefSyncFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sync_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_22
    const-string v1, "prefOptFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_23

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefGBoost"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "optimal_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_23
    const-string v1, "PREF_SAMPLING_RATE_MINBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_24

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_SAMPLING_RATE_MIN"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sampling_rate_min"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_24
    const-string v1, "PREF_FREQ_DOWN_STEP_BARRIARBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_25

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_FREQ_DOWN_STEP_BARRIAR"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_down_step_barriar"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_25
    const-string v1, "PREF_FREQ_DOWN_STEPBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_26

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_FREQ_DOWN_STEP"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_down_step"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_26
    const-string v1, "PREF_SAMPLING_RATEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_27

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_SAMPLING_RATE"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sampling_rate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_27
    const-string v1, "PREF_SAMPLING_DOWN_FACTORBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_28

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_SAMPLING_DOWN_FACTOR"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sampling_down_factor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_28
    const-string v1, "PREF_UP_THRESHOLD_ANY_CPU_LOADBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_29

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_UP_THRESHOLD_ANY_CPU_LOAD"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold_any_cpu_load"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_29
    const-string v1, "PREF_TARGET_LOADSBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2a

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_TARGET_LOADS"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "target_loads"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2a
    const-string v1, "PREF_TIMER_SLACKBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2b

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_TIMER_SLACK"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "timer_slack"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2b
    const-string v1, "PREF_HISPEED_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2c

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_HISPEED_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "hispeed_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2c
    const-string v1, "PREF_TIMER_RATEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2d

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_TIMER_RATE"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "timer_rate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2d
    const-string v1, "PREF_ABOVE_HISPEED_DELAYBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_ABOVE_HISPEED_DELAY"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "above_hispeed_delay"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2e
    const-string v1, "PREF_BOOSTPULSEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2f

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_BOOSTPULSE"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "boostpulse"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2f
    const-string v1, "PREF_BOOSTPULSE_DURATIONBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_30

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_BOOSTPULSE_DURATION"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "boostpulse_duration"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_30
    const-string v1, "PREF_GO_HISPEED_LOADBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_31

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_GO_HISPEED_LOAD"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "go_hispeed_load"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_31
    const-string v1, "PREF_INPUT_BOOST_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_32

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_INPUT_BOOST_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_boost_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_32
    const-string v1, "PREF_MIN_SAMPLE_TIMEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_33

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_MIN_SAMPLE_TIME"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "min_sample_time"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_33
    const-string v1, "PREF_UP_THRESHOLD_ANY_CPU_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_34

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_UP_THRESHOLD_ANY_CPU_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold_any_cpu_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_34
    const-string v1, "PREF_DOWN_THRESHOLDBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_35

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_DOWN_THRESHOLD"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "down_threshold"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_35
    const-string v1, "PREF_FREQ_STEPBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_36

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_FREQ_STEP"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_step"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_36
    const-string v1, "PREF_BOOSTBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_37

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREF_BOOST"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "boost"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_37
    const-string v1, "prefLowLoadDownThresholdBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_38

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefLowLoadDownThreshold"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "low_load_down_threshold"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_38
    const-string v1, "prefMaxFreqHysteresisBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_39

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefMaxFreqHysteresis"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max_freq_hysteresis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_39
    const-string v1, "prefInputBoostDurationBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3a

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefInputBoostDuration"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "input_boost_duration"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sys/devices/system/cpu/cpufreq/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu4/cpufreq/scaling_governor"

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefC2GBoostBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3b

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2GBoost"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "gboost"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3b
    const-string v1, "PREFC2_INPUT_MIN_FREQ2Boot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3c

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_INPUT_MIN_FREQ2"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3c
    const-string v1, "PREFC2_GBOOST_MIN_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3d

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_GBOOST_MIN_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "gboost_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3d
    const-string v1, "PREFC2_MAX_SCREEN_OFF_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3e

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_MAX_SCREEN_OFF_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max_screen_off_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3e
    const-string v1, "prefC2InputMinFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3f

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2InputMinFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_event_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3f
    const-string v1, "prefC2ActiveFloorFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_40

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2ActiveFloorFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "active_floor_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_40
    const-string v1, "prefC2InputTimeoutBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_41

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2InputTimeout"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_event_timeout"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_41
    const-string v1, "prefC2UISamplingRateBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_42

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2UISamplingRate"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ui_sampling_rate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_42
    const-string v1, "prefC2TwoPhaseFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_43

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2TwoPhaseFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "two_phase_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_43
    const-string v1, "prefC2UpThresholdBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_44

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2UpThreshold"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_44
    const-string v1, "prefC2DownDifferentialBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_45

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2DownDifferential"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "down_differential"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_45
    const-string v1, "prefC2SyncFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_46

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2SyncFreq"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sync_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_46
    const-string v1, "prefC2OptFreqBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_47

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2GBoost"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "optimal_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_47
    const-string v1, "PREFC2_SAMPLING_RATE_MINBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_48

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_SAMPLING_RATE_MIN"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sampling_rate_min"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_48
    const-string v1, "PREFC2_FREQ_DOWN_STEP_BARRIARBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_49

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_FREQ_DOWN_STEP_BARRIAR"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_down_step_barriar"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_49
    const-string v1, "PREFC2_FREQ_DOWN_STEPBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4a

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_FREQ_DOWN_STEP"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_down_step"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4a
    const-string v1, "PREFC2_SAMPLING_RATEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4b

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_SAMPLING_RATE"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sampling_rate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4b
    const-string v1, "PREFC2_SAMPLING_DOWN_FACTORBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4c

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_SAMPLING_DOWN_FACTOR"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sampling_down_factor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4c
    const-string v1, "PREFC2_UP_THRESHOLD_ANY_CPU_LOADBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4d

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_UP_THRESHOLD_ANY_CPU_LOAD"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold_any_cpu_load"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4d
    const-string v1, "PREFC2_TARGET_LOADSBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4e

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_TARGET_LOADS"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "target_loads"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4e
    const-string v1, "PREFC2_TIMER_SLACKBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4f

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_TIMER_SLACK"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "timer_slack"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4f
    const-string v1, "PREFC2_HISPEED_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_50

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_HISPEED_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "hispeed_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_50
    const-string v1, "PREFC2_TIMER_RATEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_51

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_TIMER_RATE"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "timer_rate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_51
    const-string v1, "PREFC2_ABOVE_HISPEED_DELAYBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_52

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_ABOVE_HISPEED_DELAY"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "above_hispeed_delay"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_52
    const-string v1, "PREFC2_BOOSTPULSEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_53

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_BOOSTPULSE"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "boostpulse"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_53
    const-string v1, "PREFC2_BOOSTPULSE_DURATIONBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_54

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_BOOSTPULSE_DURATION"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "boostpulse_duration"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_54
    const-string v1, "PREFC2_GO_HISPEED_LOADBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_55

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_GO_HISPEED_LOAD"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "go_hispeed_load"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_55
    const-string v1, "PREFC2_INPUT_BOOST_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_56

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_INPUT_BOOST_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "input_boost_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_56
    const-string v1, "PREFC2_MIN_SAMPLE_TIMEBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_57

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_MIN_SAMPLE_TIME"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "min_sample_time"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_57
    const-string v1, "PREFC2_UP_THRESHOLD_ANY_CPU_FREQBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_58

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_UP_THRESHOLD_ANY_CPU_FREQ"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "up_threshold_any_cpu_freq"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_58
    const-string v1, "PREFC2_DOWN_THRESHOLDBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_59

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_DOWN_THRESHOLD"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "down_threshold"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_59
    const-string v1, "PREFC2_FREQ_STEPBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5a

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_FREQ_STEP"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "freq_step"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5a
    const-string v1, "PREFC2_BOOSTBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5b

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "PREFC2_BOOST"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "boost"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5b
    const-string v1, "prefC2LowLoadDownThresholdBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5c

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2LowLoadDownThreshold"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "low_load_down_threshold"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5c
    const-string v1, "prefC2MaxFreqHysteresisBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5d

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2MaxFreqHysteresis"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max_freq_hysteresis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5d
    const-string v1, "prefC2InputBoostDurationBoot"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5e

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "prefC2InputBoostDuration"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "input_boost_duration"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5e
    const-string v0, "PREF_SOUND_MIC_GAINBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5f

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "PREF_SOUND_MIC_GAIN"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/sound_control_3/gpl_mic_gain"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5f
    const-string v0, "PREF_SOUND_CAM_GAINBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_60

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "PREF_SOUND_CAM_GAIN"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/sound_control_3/gpl_cam_mic_gain"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_60
    const-string v0, "PREF_SOUND_SPEAKER_GAINBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_61

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "PREF_SOUND_SPEAKER_GAIN"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/sound_control_3/gpl_speaker_gain"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_61
    const-string v0, "PREF_SOUND_HEADPHONE_GAINBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_62

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "PREF_SOUND_HEADPHONE_GAIN"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/sound_control_3/gpl_headphone_gain"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_62
    const-string v0, "PREF_SOUND_HEADPHONE_PA_GAINBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_63

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "PREF_SOUND_HEADPHONE_PA_GAIN"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/sound_control_3/gpl_headphone_pa_gain"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_63
    const-string v0, "prefSoundLockedBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_64

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSoundLocked"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_64
    const-string v0, "prefMicBoostBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMicBoost"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/virtual/misc/soundcontrol/mic_boost"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_65
    const-string v0, "prefVolBoostBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_66

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefVolBoost"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_66
    const-string v0, "prefBoostMSBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_67

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostMS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/boost_ms"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_67
    const-string v0, "prefBoostEnabledBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_68

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostEnable"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/cpuboost_enable"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_68
    const-string v0, "prefBoostInputBoostFreqBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_69

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostInputBoostFreq"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_69
    const-string v0, "prefBoostInputBoostMSBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6a

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostInputBoostMS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_ms"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6a
    const-string v0, "prefBoostLoadBasedSyncsBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6b

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostLoadBasedSyncs"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/load_based_syncs"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6b
    const-string v0, "prefBoostMigrationLoadThresholdBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6c

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostMigrationLoadThreshold"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/migration_load_threshold"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6c
    const-string v0, "prefBoostSyncThresholdBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBoostSyncThreshold"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6d
    const-string v0, "prefSleeperEnabledBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6e

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperEnabled"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6e
    const-string v0, "prefHotplugBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6f

    const-string v0, "prefHotplug"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mpdecision"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b8

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    const-string v3, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "start mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefHotplug"

    const-string v1, "mpdecision"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6f
    :goto_5
    const-string v0, "prefSleeperUpThresholdBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_70

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperUpThreshold"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/up_threshold"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_70
    const-string v0, "prefSleeperUpCountMaxBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_71

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperUpCountMax"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/up_count_max"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_71
    const-string v0, "prefSleeperDownCountMaxBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_72

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperDownCountMax"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/down_count_max"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_72
    const-string v0, "prefSleeperMaxOnlineBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_73

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperMaxOnline"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/max_online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_73
    const-string v0, "prefSleeperSuspendMaxOnlineBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_74

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperSuspendMaxOnline"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/suspend_max_online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_74
    const-string v0, "prefSleeperPlugAllBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_75

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSleeperPlugAll"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_sleeper/plug_all"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_75
    const-string v0, "prefCPUQuietEnabledBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_76

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUQuietEnabled"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUQuietGovernor"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/system/cpu/cpuquiet/current_governor"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_76
    const-string v0, "prefEnabledBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_77

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefEnabled"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_77
    const-string v0, "prefCoresOnTouchBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_78

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCoresOnTouch"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/cores_on_touch"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_78
    const-string v0, "prefCpufreqUnplugLimitBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_79

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCpufreqUnplugLimit"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_79
    const-string v0, "prefFirstLevelBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7a

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefFirstLevel"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/first_level"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7a
    const-string v0, "prefHighLoadCounterBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7b

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefHighLoadCounter"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/high_load_counter"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7b
    const-string v0, "prefLoadThresholdBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7c

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefLoadThreshold"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7c
    const-string v0, "prefMaxLoadCounterBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7d

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMaxLoadCounter"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/max_load_counter"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7d
    const-string v0, "prefMinTimeCpuOnlineBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7e

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMinTimeCpuOnline"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/min_time_cpu_online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7e
    const-string v0, "prefTimerBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7f

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefTimer"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/timer"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7f
    const-string v0, "prefSuspendFrequencyBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_80

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSuspendFrequency"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/misc/mako_hotplug_control/suspend_frequency"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_80
    const-string v0, "prefBluPlugEnabledBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_81

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugEnabled"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_81
    const-string v0, "prefBluPlugPowersaverModeBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_82

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugPowersaverMode"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/powersaver_mode"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_82
    const-string v0, "prefBluPlugMinOnlineBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_83

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugMinOnline"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/min_online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_83
    const-string v0, "prefBluPlug/max_onlineBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_84

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlug/max_online"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/max_online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_84
    const-string v0, "prefBluPlugMaxCoresScreenoffBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_85

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugMaxCoresScreenoff"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/max_cores_screenoff"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_85
    const-string v0, "prefBluPlugMaxFreqScreenoffBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_86

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugMaxFreqScreenoff"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/max_freq_screenoff"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_86
    const-string v0, "prefBluPlugUpThresholdBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_87

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugUpThreshold"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/up_threshold"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_87
    const-string v0, "prefBluPlugUpTimerCntBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_88

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugUpTimerCnt"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/up_timer_cnt"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_88
    const-string v0, "prefBluPlugDownTimerCntBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_89

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBluPlugDownTimerCnt"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/blu_plug/parameters/down_timer_cnt"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_89
    const-string v0, "prefGPUMaxBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8a

    const-string v0, "prefDeviceName"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00af

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c0

    :try_start_2
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "666"

    sget-object v3, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    const-string v4, "prefgpuMaxPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/e/a/a/a; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :goto_6
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMax"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    const-string v4, "prefgpuMaxPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_3
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "444"

    sget-object v3, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    const-string v4, "prefgpuMaxPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Lcom/e/a/a/a; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    :goto_7
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/kernel/tegra_gpu/gpu_cap_state"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8a

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/kernel/tegra_gpu/gpu_cap_state"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8a
    const-string v0, "prefBacklightBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8b

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBacklight"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->i:[Ljava/lang/String;

    const-string v4, "prefbacklightPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8b
    const-string v0, "prefCoolerColorsBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8c

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCoolerColors"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/mdss_dsi/parameters/color_preset"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8c
    const-string v0, "prefHTCColorBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8d

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefHTCColor"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8d
    const-string v0, "prefWGBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8e

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefWG"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/wake_gestures"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8e
    const-string v0, "prefS2SBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8f

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefS2S"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/sweep2sleep"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8f
    const-string v0, "prefS2WBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_90

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefS2W"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    const-string v4, "prefs2wPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_90
    const-string v0, "prefDT2WBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_91

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefDT2W"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    const-string v4, "prefdt2wPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_91
    const-string v0, "prefPDBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_92

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefPD"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->o:[Ljava/lang/String;

    const-string v4, "prefs2wPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_92
    const-string v0, "prefWGCamBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_93

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefWGCam"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/camera_gesture"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_93
    const-string v0, "prefWGVibBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_94

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefWGVib"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/vib_strength"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_94
    const-string v0, "prefOrientBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_95

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefOrient"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/orientation"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_95
    const-string v0, "prefShortSweepBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_96

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefShortSweep"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/shortsweep"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_96
    const-string v0, "prefWTOBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_97

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefWTO"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/wake_timeout"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_97
    const-string v0, "prefPwrKeySuspendBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_98

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefPwrKeySuspend"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/qpnp_power_on/parameters/pwrkey_suspend"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefPwrKeySuspend"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/pwrkey_suspend"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_98
    const-string v0, "prefWGLidBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_99

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefWGLid"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/lid_suspend"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_99
    const-string v0, "prefL2WBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9a

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefL2W"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/logo2wake"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9a
    const-string v0, "prefReadaheadBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9b

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefReadahead"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/block/mmcblk0/queue/read_ahead_kb"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9b
    const-string v0, "prefFsyncBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9c

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefFsync"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/sync/parameters/fsync_enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9c
    const-string v0, "prefExFATBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "prefExFAT"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9d

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "su -c insmod /system/lib/modules/exfat.ko"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->e(Ljava/lang/String;)Ljava/lang/String;

    :cond_9d
    const-string v0, "prefzRam0Boot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9e

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefzRam0"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " /dev/block/zram0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefzRam0"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " /dev/block/zram1"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefzRam0"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " /dev/block/zram2"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefzRam0"

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " /dev/block/zram3"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    :cond_9e
    const-string v0, "prefSwappinessBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9f

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefSwappiness"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/proc/sys/vm/swappiness"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9f
    const-string v0, "prefFCBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a0

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefFC"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a0
    const-string v0, "prefBLEBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a1

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBLE"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/i2c-0/0-006a/float_voltage"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a1
    const-string v0, "prefOTGCBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefOTGC"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/module/msm_otg/parameters/usbhost_charge_mode"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a2
    const-string v0, "prefLidBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a3

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefLid"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->l:[Ljava/lang/String;

    const-string v4, "preflidPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a3
    const-string v0, "prefL2MBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a4

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefL2M"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_touch/logo2menu"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a4
    const-string v0, "prefBLNBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a5

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefBLN"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/class/leds/button-backlight/blink_buttons"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a5
    const-string v0, "prefDT2SBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a6

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefDT2S"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/android_key/doubletap2sleep"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a6
    const-string v0, "prefKcalACCBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_ae

    const-string v0, "pref_KcalGreyscale"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a7

    const-string v0, "pref_KcalGreyscale"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a7

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "128"

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a7
    :try_start_4
    const-string v0, "prefRed"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a8

    const-string v0, "prefRed"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_a8
    const-string v0, "prefGreen"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a9

    const-string v0, "prefGreen"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_a9
    const-string v0, "prefBlue"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_aa

    const-string v0, "prefBlue"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-direct {p0, v0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(ILflar2/exkernelmanager/utilities/n;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_8

    :cond_aa
    :goto_8
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const-string v0, "prefKcalSat"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ab

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefKcalSat"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_sat"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_ab
    const-string v0, "prefKcalVal"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ac

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefKcalVal"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_val"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_ac
    const-string v0, "prefKcalCont"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ad

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefKcalCont"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_cont"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_ad
    const-string v0, "prefKcalHue"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ae

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefKcalHue"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/devices/platform/kcal_ctrl.0/kcal_hue"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_ae
    const-string v0, "prefPerformance"

    invoke-virtual {p0, v0, v2}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;Z)V

    const-string v0, "prefPowersaver"

    invoke-virtual {p0, v0, v2}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;Z)V

    const-string v0, "prefChargeLEDBoot"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_af

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefChargeLED"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/leds/charging/trigger"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_af
    return-void

    :cond_b0
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMin"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    const-string v4, "prefgpuMinPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto/16 :goto_1

    :cond_b1
    :goto_9
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_9

    :cond_b2
    const-string v0, "prefVoltSize"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v1

    move v0, v2

    :goto_a
    if-ge v0, v1, :cond_e

    iget-object v3, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "prefVolt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    const-string v6, "prefVoltagePath"

    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v3, v4, v5}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_b3
    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v3, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b4

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b4
    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "1"

    const-string v4, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v1, v3, v4}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b5
    const-string v0, "prefDeviceName"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_b6
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b7

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "start mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMPDecision_NW"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/power/pnpmgr/hotplug/mp_nw"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMPDecision_NS"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/power/pnpmgr/hotplug/mp_ns"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefMPDecision_TW"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "/sys/power/pnpmgr/hotplug/mp_tw"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_b7
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "start mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_4

    :cond_b8
    const-string v0, "prefHotplug"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "custom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bc

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b9

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v9}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bb

    :cond_b9
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_ba
    :goto_b
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefHotplug"

    const-string v1, "custom"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_bb
    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ba

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    :cond_bc
    const-string v0, "prefHotplug"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "disabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6f

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "0"

    const-string v3, "/sys/devices/platform/msm_sleeper/enabled"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    const-string v1, "stop mpdecision"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/f;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_bd

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v9}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bf

    :cond_bd
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "300000"

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_be
    :goto_c
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v3, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefHotplug"

    const-string v1, "disabled"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_bf
    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8}, Lflar2/exkernelmanager/boot/BootService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_be

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "384000"

    const-string v3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto/16 :goto_6

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/e/a/a/a;->printStackTrace()V

    goto/16 :goto_6

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto/16 :goto_7

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/e/a/a/a;->printStackTrace()V

    goto/16 :goto_7

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    :cond_c0
    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->a:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMax"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/boot/BootService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    const-string v4, "prefgpuMaxPath"

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/boot/BootService;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v0, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    :catch_8
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_8
.end method

.method public a(Ljava/lang/String;J)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->b:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->b:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->b:Landroid/content/SharedPreferences;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/boot/BootService;->c:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/f;->b()Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lflar2/exkernelmanager/boot/a;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/boot/a;-><init>(Lflar2/exkernelmanager/boot/BootService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x2

    return v0
.end method
