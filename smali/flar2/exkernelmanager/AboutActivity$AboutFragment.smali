.class public Lflar2/exkernelmanager/AboutActivity$AboutFragment;
.super Landroid/preference/PreferenceFragment;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->a:I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/AboutActivity;

    const/high16 v1, 0x7f070000

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->addPreferencesFromResource(I)V

    const-string v1, "donateAction"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/g;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/g;-><init>(Lflar2/exkernelmanager/AboutActivity$AboutFragment;Lflar2/exkernelmanager/AboutActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v1, "openSourceLicenses"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    new-instance v2, Lflar2/exkernelmanager/h;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/h;-><init>(Lflar2/exkernelmanager/AboutActivity$AboutFragment;Lflar2/exkernelmanager/AboutActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "prefAbout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->a:I

    invoke-virtual {p0}, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iget v1, p0, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :pswitch_0
    const-string v1, "one more..."

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    iput v2, p0, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->a:I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "http://www.metroperspectives.com/p432533056/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p570195139/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p381697527/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p185028595/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p1017027173/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p988347040/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p843179115/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p922041233/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p351495148/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p331432028/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p994483897/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p83337615/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p931516590/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "http://www.metroperspectives.com/p419884029/slideshow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/AboutActivity$AboutFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
