.class Lflar2/exkernelmanager/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lflar2/exkernelmanager/a/a;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/a/a;I)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/a/e;->b:Lflar2/exkernelmanager/a/a;

    iput p2, p0, Lflar2/exkernelmanager/a/e;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    sget-object v0, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    sget-object v1, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const-string v2, ""

    const-string v1, "prefVoltagePath"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lflar2/exkernelmanager/a/e;->a:I

    iget v3, p0, Lflar2/exkernelmanager/a/e;->a:I

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    sget-object v1, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    array-length v4, v0

    const/4 v1, 0x0

    move v6, v1

    move-object v1, v2

    move v2, v6

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v1, Lflar2/exkernelmanager/fragments/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v3

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_1
    iget-object v1, p0, Lflar2/exkernelmanager/a/e;->b:Lflar2/exkernelmanager/a/a;

    iget-object v1, v1, Lflar2/exkernelmanager/a/a;->a:Lflar2/exkernelmanager/a/g;

    invoke-interface {v1, v0}, Lflar2/exkernelmanager/a/g;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    sget-object v1, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    sget-object v2, Lflar2/exkernelmanager/fragments/gb;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/a/e;->a:I

    iget v3, p0, Lflar2/exkernelmanager/a/e;->a:I

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit16 v3, v3, 0x1388

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lflar2/exkernelmanager/a/e;->a:I

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lflar2/exkernelmanager/a/e;->a:I

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
