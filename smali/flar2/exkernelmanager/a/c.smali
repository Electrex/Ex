.class Lflar2/exkernelmanager/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/a/m;

.field final synthetic b:Lflar2/exkernelmanager/a/a;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/a/a;Lflar2/exkernelmanager/a/m;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    iput-object p2, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8

    const/4 v7, 0x1

    const/16 v5, -0x36

    const/high16 v6, 0x42c80000    # 100.0f

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    if-ne v1, v5, :cond_d

    const-string v1, "prefvibPath"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v7, :cond_c

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    add-int/lit16 v2, p2, 0x4b0

    invoke-static {v1, v2}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;I)I

    :goto_0
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x1c

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e00fc

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1}, Lflar2/exkernelmanager/a/a;->c(Lflar2/exkernelmanager/a/a;)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1}, Lflar2/exkernelmanager/a/a;->d(Lflar2/exkernelmanager/a/a;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v2}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_0
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0xd2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e00a9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1}, Lflar2/exkernelmanager/a/a;->c(Lflar2/exkernelmanager/a/a;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1}, Lflar2/exkernelmanager/a/a;->d(Lflar2/exkernelmanager/a/a;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v2}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_1
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x1d

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0032

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1}, Lflar2/exkernelmanager/a/a;->c(Lflar2/exkernelmanager/a/a;)I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1}, Lflar2/exkernelmanager/a/a;->d(Lflar2/exkernelmanager/a/a;)Lflar2/exkernelmanager/utilities/m;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v2}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v2

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_2
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    if-ne v1, v5, :cond_3

    const-string v1, "prefvibPath"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v7, :cond_e

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    iget-object v2, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v2}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v2

    div-int/lit8 v2, v2, 0x64

    mul-int/lit8 v2, v2, 0x64

    invoke-static {v1, v2}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;I)I

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x4541c000    # 3100.0f

    div-float/2addr v3, v4

    mul-float/2addr v3, v6

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_1
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x29

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42700000    # 60.0f

    div-float/2addr v3, v4

    mul-float/2addr v3, v6

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x320

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e00ab

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x321

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e003d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x322

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0138

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x323

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e00a2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x324

    if-ne v1, v2, :cond_9

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e00f4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    add-int/lit8 v3, v3, -0x6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x326

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mic boost: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    add-int/lit8 v3, v3, -0x14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    const/16 v1, -0x327

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v0, v0, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Volume boost: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v2}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v2

    add-int/lit8 v2, v2, -0x14

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    iget-object v0, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v0, p1}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;Landroid/widget/SeekBar;)V

    return-void

    :cond_c
    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1, p2}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;I)I

    goto/16 :goto_0

    :cond_d
    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v1, p2}, Lflar2/exkernelmanager/a/a;->a(Lflar2/exkernelmanager/a/a;I)I

    goto/16 :goto_0

    :cond_e
    iget-object v1, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v1, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v3}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/a/c;->b:Lflar2/exkernelmanager/a/a;

    invoke-static {v0, p1}, Lflar2/exkernelmanager/a/a;->b(Lflar2/exkernelmanager/a/a;Landroid/widget/SeekBar;)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/c;->a:Lflar2/exkernelmanager/a/m;

    iget-object v0, v0, Lflar2/exkernelmanager/a/m;->d:Landroid/widget/ImageButton;

    const v1, 0x7f020069

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    return-void
.end method
