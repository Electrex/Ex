.class public Lflar2/exkernelmanager/a/a;
.super Landroid/widget/ArrayAdapter;


# static fields
.field public static b:Z


# instance fields
.field a:Lflar2/exkernelmanager/a/g;

.field private c:Lflar2/exkernelmanager/utilities/a;

.field private d:Lflar2/exkernelmanager/utilities/m;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/view/LayoutInflater;

.field private j:Ljava/util/List;

.field private k:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v0, Lflar2/exkernelmanager/utilities/a;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/a;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/a/a;->c:Lflar2/exkernelmanager/utilities/a;

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iput v1, p0, Lflar2/exkernelmanager/a/a;->e:I

    const/16 v0, 0x64

    iput v0, p0, Lflar2/exkernelmanager/a/a;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lflar2/exkernelmanager/a/a;->g:I

    iput-object p2, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    iput-object p1, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const-string v0, "prefkcalPath"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflar2/exkernelmanager/a/a;->e:I

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/a/a;I)I
    .locals 0

    iput p1, p0, Lflar2/exkernelmanager/a/a;->h:I

    return p1
.end method

.method static synthetic a(Lflar2/exkernelmanager/a/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 11

    const v10, 0x7f02006b

    const v9, 0x7f020069

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v0, 0x7f0c0144

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v3

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    const v5, 0x7f040016

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iget-object v5, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    const v6, 0x7f040017

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    iget-object v6, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0, v9}, Lflar2/exkernelmanager/a/o;->c(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0, v7}, Lflar2/exkernelmanager/a/o;->a(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->c:Lflar2/exkernelmanager/utilities/a;

    invoke-virtual {v0, v3, v7}, Lflar2/exkernelmanager/utilities/a;->a(IZ)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setImageResource(I)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0, v10}, Lflar2/exkernelmanager/a/o;->c(I)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/a/o;->a(Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->c:Lflar2/exkernelmanager/utilities/a;

    invoke-virtual {v0, v3, v8}, Lflar2/exkernelmanager/utilities/a;->a(IZ)V

    goto :goto_0
.end method

.method private a(Landroid/widget/SeekBar;)V
    .locals 8

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    const-wide v4, 0xffffffffL

    iget v1, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v2, v1, -0x1e

    const/16 v1, 0x26

    const-string v3, "prefDeviceName"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    const v7, 0x7f0e00d9

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v1, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v1, v1, -0x6

    rsub-int/lit8 v1, v1, 0x26

    :cond_0
    const-string v3, "prefDeviceName"

    invoke-static {v3}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    const v7, 0x7f0e00dd

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget v1, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v1, v1, -0x6

    rsub-int/lit8 v1, v1, 0x16

    move v3, v1

    :goto_0
    if-gez v2, :cond_8

    add-int/lit16 v1, v2, 0x100

    :goto_1
    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "0"

    const-string v7, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v2, v6, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v2

    const/16 v6, -0x320

    if-ne v2, v6, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-long v6, v1

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v7, "/sys/kernel/sound_control_3/gpl_mic_gain"

    invoke-virtual {v6, v2, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "PREF_SOUND_MIC_GAIN"

    invoke-static {v6, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v2

    const/16 v6, -0x321

    if-ne v2, v6, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-long v6, v1

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v7, "/sys/kernel/sound_control_3/gpl_cam_mic_gain"

    invoke-virtual {v6, v2, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "PREF_SOUND_CAM_GAIN"

    invoke-static {v6, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v2

    const/16 v6, -0x322

    if-ne v2, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v6, v1, 0x2

    int-to-long v6, v6

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v7, "/sys/kernel/sound_control_3/gpl_speaker_gain"

    invoke-virtual {v6, v2, v7}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "PREF_SOUND_SPEAKER_GAIN"

    invoke-static {v6, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v2

    const/16 v6, -0x323

    if-ne v2, v6, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v1, v1, 0x2

    int-to-long v6, v1

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v6, "/sys/kernel/sound_control_3/gpl_headphone_gain"

    invoke-virtual {v2, v1, v6}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_HEADPHONE_GAIN"

    invoke-static {v2, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x324

    if-ne v1, v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-int/lit8 v2, v3, 0x2

    int-to-long v2, v2

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v3, "/sys/kernel/sound_control_3/gpl_headphone_pa_gain"

    invoke-virtual {v2, v1, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PREF_SOUND_HEADPHONE_PA_GAIN"

    invoke-static {v2, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x326

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v2, v2, -0x14

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/virtual/misc/soundcontrol/mic_boost"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "prefMicBoost"

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v2, v2, -0x14

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    const/16 v1, -0x327

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v1, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v1, v1, -0x14

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefVolBoost"

    iget v1, p0, Lflar2/exkernelmanager/a/a;->h:I

    add-int/lit8 v1, v1, -0x14

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void

    :cond_8
    move v1, v2

    goto/16 :goto_1

    :cond_9
    move v3, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lflar2/exkernelmanager/a/a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/a/a;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/a/a;Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/a/a;->a(Landroid/widget/SeekBar;)V

    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/a/a;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/a/a;->h:I

    return v0
.end method

.method private b(Landroid/widget/SeekBar;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0, v5}, Lflar2/exkernelmanager/a/o;->a(Z)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->c:Lflar2/exkernelmanager/utilities/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v2

    invoke-virtual {v1, v2, v5}, Lflar2/exkernelmanager/utilities/a;->a(IZ)V

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x1c

    if-ne v1, v2, :cond_1

    iget v1, p0, Lflar2/exkernelmanager/a/a;->e:I

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->a:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_0
    const-string v1, "prefRedBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefRed"

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0xd2

    if-ne v1, v2, :cond_3

    iget v1, p0, Lflar2/exkernelmanager/a/a;->e:I

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->b:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_2
    const-string v1, "prefGreenBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefGreen"

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x1d

    if-ne v1, v2, :cond_5

    iget v1, p0, Lflar2/exkernelmanager/a/a;->e:I

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    sget-object v3, Lflar2/exkernelmanager/utilities/n;->c:Lflar2/exkernelmanager/utilities/n;

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(ILflar2/exkernelmanager/utilities/n;)V

    :cond_4
    const-string v1, "prefBlueBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefBlue"

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x36

    if-ne v1, v2, :cond_6

    const-string v1, "prefVibBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefVib"

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lflar2/exkernelmanager/r;->k:[Ljava/lang/String;

    const-string v4, "prefvibPath"

    invoke-static {v4}, Lflar2/exkernelmanager/utilities/k;->c(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/a/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    const-wide/16 v2, 0x118

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_6
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x29

    if-ne v1, v2, :cond_7

    const-string v1, "prefWGVibBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefWGVib"

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/android_touch/vib_strength"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/a/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iget v2, p0, Lflar2/exkernelmanager/a/a;->h:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_7
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x320

    if-ne v1, v2, :cond_8

    const-string v1, "PREF_SOUND_MIC_GAINBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefSoundLocked"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "2"

    const-string v3, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x321

    if-ne v1, v2, :cond_9

    const-string v1, "PREF_SOUND_CAM_GAINBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefSoundLocked"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "2"

    const-string v3, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x322

    if-ne v1, v2, :cond_a

    const-string v1, "PREF_SOUND_SPEAKER_GAINBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefSoundLocked"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "2"

    const-string v3, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v1

    const/16 v2, -0x323

    if-ne v1, v2, :cond_b

    const-string v1, "PREF_SOUND_HEADPHONE_GAINBoot"

    invoke-static {v1, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v1, "prefSoundLocked"

    invoke-static {v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "2"

    const-string v3, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v1, v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->b()I

    move-result v0

    const/16 v1, -0x324

    if-ne v0, v1, :cond_c

    const-string v0, "PREF_SOUND_HEADPHONE_PA_GAINBoot"

    invoke-static {v0, v5}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    const-string v0, "prefSoundLocked"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "2"

    const-string v2, "/sys/kernel/sound_control_3/gpl_sound_control_locked"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    return-void
.end method

.method static synthetic b(Lflar2/exkernelmanager/a/a;Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/a/a;->b(Landroid/widget/SeekBar;)V

    return-void
.end method

.method static synthetic c(Lflar2/exkernelmanager/a/a;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/a/a;->e:I

    return v0
.end method

.method static synthetic d(Lflar2/exkernelmanager/a/a;)Lflar2/exkernelmanager/utilities/m;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->d:Lflar2/exkernelmanager/utilities/m;

    return-object v0
.end method


# virtual methods
.method public a(Lflar2/exkernelmanager/a/g;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/a/a;->a:Lflar2/exkernelmanager/a/g;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    const/4 v4, 0x7

    const/4 v3, 0x5

    const/4 v5, 0x1

    const v8, 0x7f0c0144

    const/4 v7, 0x0

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/a/o;

    const/16 v1, 0x9

    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f090007

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f090009

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    const v6, 0x7f040015

    invoke-static {v1, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    if-nez v1, :cond_8

    if-nez p2, :cond_7

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v3, 0x7f03003d

    invoke-virtual {v1, v3, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v3, Lflar2/exkernelmanager/a/j;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/a/j;-><init>(Lflar2/exkernelmanager/a/a;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    iput-object v1, v3, Lflar2/exkernelmanager/a/j;->a:Lflar2/exkernelmanager/a/o;

    const v1, 0x7f0c014d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lflar2/exkernelmanager/a/j;->b:Landroid/widget/TextView;

    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    if-lez p1, :cond_6

    move v1, p1

    :goto_1
    iget v4, p0, Lflar2/exkernelmanager/a/a;->f:I

    mul-int/2addr v1, v4

    int-to-long v4, v1

    invoke-virtual {v6, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    sget-boolean v1, Lflar2/exkernelmanager/a/a;->b:Z

    if-eqz v1, :cond_19

    if-ge p1, v2, :cond_19

    invoke-virtual {p2, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move-object v1, v3

    :goto_2
    iget-object v1, v1, Lflar2/exkernelmanager/a/j;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_3
    iput p1, p0, Lflar2/exkernelmanager/a/a;->g:I

    return-object p2

    :cond_1
    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f09000b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v4

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f09000a

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v1, 0xa

    move v2, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f09000b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v1, 0xe

    move v2, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f090008

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v1, 0xa

    move v2, v1

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lflar2/exkernelmanager/a/a;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f09000a

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v1, 0x14

    move v2, v1

    goto/16 :goto_0

    :cond_6
    move v1, v5

    goto :goto_1

    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/j;

    goto :goto_2

    :cond_8
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    if-ne v1, v4, :cond_9

    if-nez p2, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v1, 0x7f030030

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_3

    :cond_9
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    if-ne v1, v3, :cond_b

    if-nez p2, :cond_a

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v2, 0x7f03003e

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_a
    new-instance v2, Lflar2/exkernelmanager/a/k;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/a/k;-><init>(Lflar2/exkernelmanager/a/a;)V

    const v1, 0x7f0c014e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/k;->a:Landroid/widget/ImageView;

    iget-object v1, v2, Lflar2/exkernelmanager/a/k;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_b
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    const/4 v3, 0x6

    if-ne v1, v3, :cond_e

    if-nez p2, :cond_c

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v2, 0x7f03003c

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v2, Lflar2/exkernelmanager/a/i;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/a/i;-><init>(Lflar2/exkernelmanager/a/a;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    iput-object v1, v2, Lflar2/exkernelmanager/a/i;->a:Lflar2/exkernelmanager/a/o;

    iput p1, v2, Lflar2/exkernelmanager/a/i;->f:I

    const v1, 0x7f0c0149

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/i;->b:Landroid/widget/TextView;

    const v1, 0x7f0c014a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/i;->c:Landroid/widget/TextView;

    const v1, 0x7f0c014c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/i;->d:Landroid/widget/TextView;

    const v1, 0x7f0c014b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v2, Lflar2/exkernelmanager/a/i;->e:Landroid/widget/ProgressBar;

    iget-object v1, v2, Lflar2/exkernelmanager/a/i;->e:Landroid/widget/ProgressBar;

    iget-object v3, v2, Lflar2/exkernelmanager/a/i;->a:Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_4
    iget-object v2, v1, Lflar2/exkernelmanager/a/i;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/i;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/i;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v1, Lflar2/exkernelmanager/a/i;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->h()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/a/a;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v0, p0, Lflar2/exkernelmanager/a/a;->g:I

    if-le p1, v0, :cond_d

    const v0, 0x7f040021

    :goto_5
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_3

    :cond_c
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/i;

    goto :goto_4

    :cond_d
    const v0, 0x7f04000b

    goto :goto_5

    :cond_e
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_10

    if-nez p2, :cond_f

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v3, 0x7f03003b

    invoke-virtual {v1, v3, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    iget v1, p0, Lflar2/exkernelmanager/a/a;->f:I

    mul-int/2addr v1, p1

    int-to-long v4, v1

    invoke-virtual {v6, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    sget-boolean v1, Lflar2/exkernelmanager/a/a;->b:Z

    if-eqz v1, :cond_f

    if-ge p1, v2, :cond_f

    invoke-virtual {p2, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_f
    new-instance v2, Lflar2/exkernelmanager/a/h;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/a/h;-><init>(Lflar2/exkernelmanager/a/a;)V

    const v1, 0x7f0c0147

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/h;->a:Landroid/widget/TextView;

    iget-object v1, v2, Lflar2/exkernelmanager/a/h;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_10
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_12

    if-nez p2, :cond_11

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v3, 0x7f03003f

    invoke-virtual {v1, v3, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v3, Lflar2/exkernelmanager/a/m;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/a/m;-><init>(Lflar2/exkernelmanager/a/a;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    iput-object v1, v3, Lflar2/exkernelmanager/a/m;->a:Lflar2/exkernelmanager/a/o;

    const v1, 0x7f0c014f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    const v1, 0x7f0c0009

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, v3, Lflar2/exkernelmanager/a/m;->c:Landroid/widget/SeekBar;

    iget-object v1, v3, Lflar2/exkernelmanager/a/m;->c:Landroid/widget/SeekBar;

    iget-object v4, v3, Lflar2/exkernelmanager/a/m;->a:Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v3, Lflar2/exkernelmanager/a/m;->d:Landroid/widget/ImageButton;

    iget-object v1, v3, Lflar2/exkernelmanager/a/m;->d:Landroid/widget/ImageButton;

    new-instance v4, Lflar2/exkernelmanager/a/b;

    invoke-direct {v4, p0}, Lflar2/exkernelmanager/a/b;-><init>(Lflar2/exkernelmanager/a/a;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v3, Lflar2/exkernelmanager/a/m;->d:Landroid/widget/ImageButton;

    invoke-virtual {p2, v8, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget v1, p0, Lflar2/exkernelmanager/a/a;->f:I

    mul-int/2addr v1, p1

    int-to-long v4, v1

    invoke-virtual {v6, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    sget-boolean v1, Lflar2/exkernelmanager/a/a;->b:Z

    if-eqz v1, :cond_18

    if-ge p1, v2, :cond_18

    invoke-virtual {p2, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move-object v1, v3

    :goto_6
    iget-object v2, v1, Lflar2/exkernelmanager/a/m;->d:Landroid/widget/ImageButton;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/m;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/m;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->i()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/m;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/m;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->f()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, v1, Lflar2/exkernelmanager/a/m;->c:Landroid/widget/SeekBar;

    new-instance v2, Lflar2/exkernelmanager/a/c;

    invoke-direct {v2, p0, v1}, Lflar2/exkernelmanager/a/c;-><init>(Lflar2/exkernelmanager/a/a;Lflar2/exkernelmanager/a/m;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    goto/16 :goto_3

    :cond_11
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/m;

    goto :goto_6

    :cond_12
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_15

    if-nez p2, :cond_13

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v2, 0x7f030040

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v2, Lflar2/exkernelmanager/a/n;

    invoke-direct {v2, p0}, Lflar2/exkernelmanager/a/n;-><init>(Lflar2/exkernelmanager/a/a;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    iput-object v1, v2, Lflar2/exkernelmanager/a/n;->a:Lflar2/exkernelmanager/a/o;

    iput p1, v2, Lflar2/exkernelmanager/a/n;->f:I

    const v1, 0x7f0c0142

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/n;->b:Landroid/widget/TextView;

    const v1, 0x7f0c013b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lflar2/exkernelmanager/a/n;->d:Landroid/widget/ImageButton;

    iget-object v1, v2, Lflar2/exkernelmanager/a/n;->d:Landroid/widget/ImageButton;

    iget-object v3, v2, Lflar2/exkernelmanager/a/n;->a:Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    const v1, 0x7f0c0143

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lflar2/exkernelmanager/a/n;->c:Landroid/widget/TextView;

    const v1, 0x7f0c013d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lflar2/exkernelmanager/a/n;->e:Landroid/widget/ImageButton;

    iget-object v1, v2, Lflar2/exkernelmanager/a/n;->e:Landroid/widget/ImageButton;

    iget-object v3, v2, Lflar2/exkernelmanager/a/n;->a:Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_7
    iget-object v2, v1, Lflar2/exkernelmanager/a/n;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/n;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lflar2/exkernelmanager/a/n;->d:Landroid/widget/ImageButton;

    const v2, 0x7f020068

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, v1, Lflar2/exkernelmanager/a/n;->e:Landroid/widget/ImageButton;

    const v2, 0x7f02006a

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, v1, Lflar2/exkernelmanager/a/n;->d:Landroid/widget/ImageButton;

    new-instance v2, Lflar2/exkernelmanager/a/d;

    invoke-direct {v2, p0, p1}, Lflar2/exkernelmanager/a/d;-><init>(Lflar2/exkernelmanager/a/a;I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lflar2/exkernelmanager/a/n;->e:Landroid/widget/ImageButton;

    new-instance v1, Lflar2/exkernelmanager/a/e;

    invoke-direct {v1, p0, p1}, Lflar2/exkernelmanager/a/e;-><init>(Lflar2/exkernelmanager/a/a;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/a/a;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v0, p0, Lflar2/exkernelmanager/a/a;->g:I

    if-le p1, v0, :cond_14

    const v0, 0x7f040021

    :goto_8
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_3

    :cond_13
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/n;

    goto :goto_7

    :cond_14
    const v0, 0x7f04000b

    goto :goto_8

    :cond_15
    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    invoke-virtual {v1}, Lflar2/exkernelmanager/a/o;->a()I

    move-result v1

    if-ne v1, v5, :cond_0

    if-nez p2, :cond_16

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->i:Landroid/view/LayoutInflater;

    const v3, 0x7f03003a

    invoke-virtual {v1, v3, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v3, Lflar2/exkernelmanager/a/l;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/a/l;-><init>(Lflar2/exkernelmanager/a/a;)V

    iget-object v1, p0, Lflar2/exkernelmanager/a/a;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/o;

    iput-object v1, v3, Lflar2/exkernelmanager/a/l;->a:Lflar2/exkernelmanager/a/o;

    const v1, 0x7f0c0142

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lflar2/exkernelmanager/a/l;->b:Landroid/widget/TextView;

    const v1, 0x7f0c0143

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lflar2/exkernelmanager/a/l;->c:Landroid/widget/TextView;

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v3, Lflar2/exkernelmanager/a/l;->d:Landroid/widget/ImageButton;

    iget-object v1, v3, Lflar2/exkernelmanager/a/l;->d:Landroid/widget/ImageButton;

    new-instance v4, Lflar2/exkernelmanager/a/f;

    invoke-direct {v4, p0}, Lflar2/exkernelmanager/a/f;-><init>(Lflar2/exkernelmanager/a/a;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v3, Lflar2/exkernelmanager/a/l;->d:Landroid/widget/ImageButton;

    invoke-virtual {p2, v8, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget v1, p0, Lflar2/exkernelmanager/a/a;->f:I

    mul-int/2addr v1, p1

    int-to-long v4, v1

    invoke-virtual {v6, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    sget-boolean v1, Lflar2/exkernelmanager/a/a;->b:Z

    if-eqz v1, :cond_17

    if-ge p1, v2, :cond_17

    invoke-virtual {p2, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move-object v1, v3

    :goto_9
    iget-object v2, v1, Lflar2/exkernelmanager/a/l;->d:Landroid/widget/ImageButton;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/l;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflar2/exkernelmanager/a/l;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v1, Lflar2/exkernelmanager/a/l;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Lflar2/exkernelmanager/a/o;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_3

    :cond_16
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflar2/exkernelmanager/a/l;

    goto :goto_9

    :cond_17
    move-object v1, v3

    goto :goto_9

    :cond_18
    move-object v1, v3

    goto/16 :goto_6

    :cond_19
    move-object v1, v3

    goto/16 :goto_2

    :cond_1a
    move v2, v1

    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItemViewType(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/a/a;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
