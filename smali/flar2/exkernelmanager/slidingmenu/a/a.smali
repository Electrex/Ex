.class public Lflar2/exkernelmanager/slidingmenu/a/a;
.super Landroid/support/v7/widget/cc;


# static fields
.field public static a:Landroid/support/v7/widget/RecyclerView;

.field public static b:Ljava/util/ArrayList;


# instance fields
.field private c:Ljava/util/ArrayList;

.field private d:Lflar2/exkernelmanager/slidingmenu/a;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/cc;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->f:I

    iput-object p1, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/slidingmenu/a/a;)Lflar2/exkernelmanager/slidingmenu/a;
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->d:Lflar2/exkernelmanager/slidingmenu/a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/slidingmenu/b/a;->a()I

    move-result v0

    return v0
.end method

.method public synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cz;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lflar2/exkernelmanager/slidingmenu/a/a;->c(Landroid/view/ViewGroup;I)Lflar2/exkernelmanager/slidingmenu/a/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/cz;I)V
    .locals 0

    check-cast p1, Lflar2/exkernelmanager/slidingmenu/a/c;

    invoke-virtual {p0, p1, p2}, Lflar2/exkernelmanager/slidingmenu/a/a;->a(Lflar2/exkernelmanager/slidingmenu/a/c;I)V

    return-void
.end method

.method public a(Lflar2/exkernelmanager/slidingmenu/a/c;I)V
    .locals 3

    iget v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v1, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/slidingmenu/b/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->l:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/slidingmenu/b/a;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->a:Landroid/view/View;

    new-instance v1, Lflar2/exkernelmanager/slidingmenu/a/b;

    invoke-direct {v1, p0, p2}, Lflar2/exkernelmanager/slidingmenu/a/b;-><init>(Lflar2/exkernelmanager/slidingmenu/a/a;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->e:I

    if-ne v0, p2, :cond_4

    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->a:Landroid/view/View;

    iget-object v1, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    iget-object v1, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v1, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/slidingmenu/b/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->l:Landroid/widget/ImageView;

    iget-object v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-virtual {v0}, Lflar2/exkernelmanager/slidingmenu/b/a;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_4
    if-lez p2, :cond_2

    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->a:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_5
    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflar2/exkernelmanager/slidingmenu/a/c;->k:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method public c(Landroid/view/ViewGroup;I)Lflar2/exkernelmanager/slidingmenu/a/c;
    .locals 3

    const/4 v2, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002d

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v0, Lflar2/exkernelmanager/slidingmenu/a/c;

    invoke-direct {v0, v1, p2}, Lflar2/exkernelmanager/slidingmenu/a/c;-><init>(Landroid/view/View;I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v0, Lflar2/exkernelmanager/slidingmenu/a/c;

    invoke-direct {v0, v1, p2}, Lflar2/exkernelmanager/slidingmenu/a/c;-><init>(Landroid/view/View;I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v0, Lflar2/exkernelmanager/slidingmenu/a/c;

    invoke-direct {v0, v1, p2}, Lflar2/exkernelmanager/slidingmenu/a/c;-><init>(Landroid/view/View;I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002c

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v0, Lflar2/exkernelmanager/slidingmenu/a/c;

    invoke-direct {v0, v1, p2}, Lflar2/exkernelmanager/slidingmenu/a/c;-><init>(Landroid/view/View;I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)V
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->e:I

    iput p1, p0, Lflar2/exkernelmanager/slidingmenu/a/a;->e:I

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/slidingmenu/a/a;->c(I)V

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/slidingmenu/a/a;->c(I)V

    return-void
.end method
