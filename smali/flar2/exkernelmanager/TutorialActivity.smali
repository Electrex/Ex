.class public Lflar2/exkernelmanager/TutorialActivity;
.super Landroid/support/v4/app/o;


# static fields
.field private static final o:[I

.field private static final p:Ljava/lang/String;


# instance fields
.field n:Landroid/animation/ArgbEvaluator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lflar2/exkernelmanager/TutorialActivity;->o:[I

    const-class v0, Lflar2/exkernelmanager/TutorialActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/TutorialActivity;->p:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f02008b
        0x7f02008d
        0x7f02008c
        0x7f02008e
        0x7f02007b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/o;-><init>()V

    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/TutorialActivity;->n:Landroid/animation/ArgbEvaluator;

    return-void
.end method

.method static synthetic g()[I
    .locals 1

    sget-object v0, Lflar2/exkernelmanager/TutorialActivity;->o:[I

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Landroid/support/v4/app/o;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/TutorialActivity;->setContentView(I)V

    const v0, 0x7f0c00c0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0c00c1

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lflar2/exkernelmanager/utilities/CircleIndicator;

    const v0, 0x7f0c00c2

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    new-instance v0, Lflar2/exkernelmanager/ad;

    invoke-direct {v0, p0}, Lflar2/exkernelmanager/ad;-><init>(Lflar2/exkernelmanager/TutorialActivity;)V

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    new-instance v0, Lflar2/exkernelmanager/ae;

    invoke-direct {v0, p0, v2}, Lflar2/exkernelmanager/ae;-><init>(Lflar2/exkernelmanager/TutorialActivity;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lflar2/exkernelmanager/ai;

    invoke-virtual {p0}, Lflar2/exkernelmanager/TutorialActivity;->f()Landroid/support/v4/app/t;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/ai;-><init>(Lflar2/exkernelmanager/TutorialActivity;Landroid/support/v4/app/t;)V

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/bo;)V

    invoke-virtual {v7, v2}, Lflar2/exkernelmanager/utilities/CircleIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    const/4 v0, 0x5

    new-array v3, v0, [I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lflar2/exkernelmanager/TutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0a0043

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v3, v0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lflar2/exkernelmanager/TutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0a0045

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lflar2/exkernelmanager/TutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0a0044

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v3, v0

    const/4 v0, 0x3

    invoke-virtual {p0}, Lflar2/exkernelmanager/TutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0a0042

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v3, v0

    const/4 v0, 0x4

    invoke-virtual {p0}, Lflar2/exkernelmanager/TutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0a0041

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v3, v0

    new-instance v0, Lflar2/exkernelmanager/af;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lflar2/exkernelmanager/af;-><init>(Lflar2/exkernelmanager/TutorialActivity;Landroid/support/v4/view/ViewPager;[ILandroid/view/View;Landroid/widget/Button;Landroid/widget/Button;Lflar2/exkernelmanager/utilities/CircleIndicator;)V

    invoke-virtual {v7, v0}, Lflar2/exkernelmanager/utilities/CircleIndicator;->setOnPageChangeListener(Landroid/support/v4/view/do;)V

    return-void
.end method
