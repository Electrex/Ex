.class public Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;


# instance fields
.field a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 9

    const/4 v1, 0x0

    const v8, 0x7f0c0167

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;->a:Landroid/content/SharedPreferences;

    new-instance v0, Landroid/content/ComponentName;

    const-class v2, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;

    invoke-direct {v0, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget v4, v2, v0

    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030043

    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const-string v6, "prefPerformance"

    invoke-virtual {p0, v6}, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_0

    const v6, 0x7f02007e

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_1
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lflar2/exkernelmanager/performance/PerformanceReceiver;

    invoke-direct {v6, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "prefPerformance"

    invoke-virtual {p0, v7}, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "flar2.exkernelmanager.performance.DISABLE_PERFORMANCE"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_2
    const/high16 v7, 0x8000000

    invoke-static {p1, v1, v6, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {p2, v4, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const v6, 0x7f02007d

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    :cond_1
    const-string v7, "flar2.exkernelmanager.performance.ENABLE_PERFORMANCE"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_2
    return-void
.end method
