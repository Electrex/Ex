.class public Lflar2/exkernelmanager/performance/a;
.super Ljava/lang/Object;


# instance fields
.field a:Landroid/content/SharedPreferences;

.field protected b:Landroid/content/Context;

.field c:Lflar2/exkernelmanager/utilities/m;

.field d:Lflar2/exkernelmanager/utilities/e;

.field e:Lflar2/exkernelmanager/utilities/f;

.field f:I

.field g:I

.field h:I

.field i:I

.field j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    iput-object v0, p0, Lflar2/exkernelmanager/performance/a;->e:Lflar2/exkernelmanager/utilities/f;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/performance/a;->a:Landroid/content/SharedPreferences;

    const-string v0, "prefDeviceName"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    return-void
.end method

.method private b()V
    .locals 9

    const v8, 0x7f0e00dd

    const v7, 0x7f0e00af

    const v6, 0x7f0e00db

    const v5, 0x7f0e00df

    const/4 v4, 0x0

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "prefCPUMaxPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v0, "prefGPUMaxPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefGPUMinPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->f:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefGovPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/performance/a;->g:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefgboostPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->g:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "prefGPUGovPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->h:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prefCPUMaxPrefPF"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/performance/a;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "prefCPUMaxPrefPF"

    const-string v1, "2295000"

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    const-string v0, "prefCPUMaxPrefPF"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    sget-object v2, Lflar2/exkernelmanager/r;->g:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2, v4, v4}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "prefGPUMaxPrefPF"

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/performance/a;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const v3, 0x32c87d00

    if-ge v2, v3, :cond_c

    const-string v2, "prefGPUMaxPrefPF"

    const/16 v3, 0xe

    aget-object v1, v1, v3

    invoke-virtual {p0, v2, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_2
    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMaxPrefPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/kernel/tegra_gpu/gpu_cap_state"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "2"

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->f:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "prefCPUGovPF"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/performance/a;->g:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    sget-object v2, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->g:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "simple"

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->h:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void

    :cond_8
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "prefCPUMaxPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "prefCPUMaxPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "prefCPUMaxPrefPF"

    const-string v1, "2649600"

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    const-string v0, "prefCPUMaxPrefPF"

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    const-string v1, "prefGPUMaxPrefPF"

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v3, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v4, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_d
    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v3, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_e
    array-length v2, v1

    const/4 v3, 0x3

    if-le v2, v3, :cond_f

    :try_start_0
    const-string v2, "prefGPUMaxPrefPF"

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {p0, v2, v3}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v2

    const-string v2, "prefGPUMaxPrefPF"

    aget-object v1, v1, v4

    invoke-virtual {p0, v2, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_f
    const-string v2, "prefGPUMaxPrefPF"

    aget-object v1, v1, v4

    invoke-virtual {p0, v2, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_10
    const-string v2, "prefGPUMaxPrefPF"

    aget-object v1, v1, v4

    invoke-virtual {p0, v2, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_11
    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_12
    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_13
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMaxPrefPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method private c()V
    .locals 5

    const v4, 0x7f0e00db

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUMaxPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMaxPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/kernel/tegra_gpu/gpu_cap_state"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/kernel/tegra_gpu/gpu_cap_state"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUMinPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->f:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const v2, 0x7f0e00df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGovPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu1/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGovPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu2/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGovPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "1"

    const-string v2, "/sys/devices/system/cpu/cpu3/online"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGovPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    sget-object v1, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    iget v2, p0, Lflar2/exkernelmanager/performance/a;->g:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefgboostPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->g:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const v2, 0x7f0e00dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const v2, 0x7f0e00af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefGPUGovPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    iget v3, p0, Lflar2/exkernelmanager/performance/a;->h:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->d:Lflar2/exkernelmanager/utilities/e;

    const-string v1, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v1, "prefCPUMaxPF"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/cpu_tegra/parameters/cpu_user_cap"

    invoke-virtual {v0, v1, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "prefCPUMaxPF"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    const-string v2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v0, v2}, Lflar2/exkernelmanager/utilities/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private d()V
    .locals 5

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const-class v4, Lflar2/exkernelmanager/performance/PerformanceWidgetProvider;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "appWidgetIds"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "flar2.exkernelmanager.performance.DISABLE_PERFORMANCE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const v3, 0x7f0e00ed

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const v3, 0x7f0e00ee

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f02007c

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    iget v0, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v1, Landroid/app/Notification;->flags:I

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v2, 0x65

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public a(Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lflar2/exkernelmanager/powersave/a;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->f:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/performance/a;->f:I

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/performance/a;->g:I

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->h:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/performance/a;->h:I

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->c:Lflar2/exkernelmanager/utilities/m;

    sget-object v2, Lflar2/exkernelmanager/r;->e:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lflar2/exkernelmanager/performance/a;->i:I

    if-eqz p1, :cond_1

    const-string v1, "prefPowersaver"

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/performance/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    :cond_0
    invoke-direct {p0}, Lflar2/exkernelmanager/performance/a;->b()V

    const-string v0, "prefPerformance"

    invoke-virtual {p0, v0, v4}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v4}, Lflar2/exkernelmanager/performance/a;->b(Z)V

    :goto_0
    invoke-direct {p0}, Lflar2/exkernelmanager/performance/a;->d()V

    return-void

    :cond_1
    invoke-direct {p0}, Lflar2/exkernelmanager/performance/a;->c()V

    const-string v0, "prefPerformance"

    invoke-virtual {p0, v0, v3}, Lflar2/exkernelmanager/performance/a;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v3}, Lflar2/exkernelmanager/performance/a;->b(Z)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 2

    if-eqz p1, :cond_1

    const-string v0, "prefPerformanceNotify"

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/performance/a;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/performance/a;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lflar2/exkernelmanager/performance/a;->b:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/performance/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
