.class public Lflar2/exkernelmanager/MainActivity;
.super Landroid/support/v7/app/ad;


# static fields
.field public static o:Landroid/view/View;

.field public static p:Landroid/view/View;

.field private static final y:Landroid/os/Handler;


# instance fields
.field private A:Lflar2/exkernelmanager/powersave/a;

.field private B:Lflar2/exkernelmanager/performance/a;

.field private C:I

.field n:Landroid/support/v7/widget/cl;

.field q:Landroid/content/SharedPreferences;

.field r:Lflar2/exkernelmanager/q;

.field private s:Landroid/support/v4/widget/DrawerLayout;

.field private t:Landroid/support/v7/app/f;

.field private u:Landroid/support/v7/widget/Toolbar;

.field private v:Ljava/lang/CharSequence;

.field private w:Ljava/lang/CharSequence;

.field private x:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->y:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v7/app/ad;-><init>()V

    iput v0, p0, Lflar2/exkernelmanager/MainActivity;->x:I

    iput-boolean v0, p0, Lflar2/exkernelmanager/MainActivity;->z:Z

    return-void
.end method

.method static synthetic a(Lflar2/exkernelmanager/MainActivity;)I
    .locals 1

    iget v0, p0, Lflar2/exkernelmanager/MainActivity;->C:I

    return v0
.end method

.method private final a(Landroid/app/Fragment;I)V
    .locals 4

    if-nez p2, :cond_1

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget v1, Lflar2/exkernelmanager/MainApp;->a:I

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lflar2/exkernelmanager/MainApp;->a:I

    if-eq v0, p2, :cond_0

    iget v0, p0, Lflar2/exkernelmanager/MainActivity;->C:I

    if-ge p2, v0, :cond_2

    sput p2, Lflar2/exkernelmanager/MainApp;->a:I

    :cond_2
    sget-object v0, Lflar2/exkernelmanager/MainActivity;->y:Landroid/os/Handler;

    new-instance v1, Lflar2/exkernelmanager/p;

    invoke-direct {v1, p0, p1, p2}, Lflar2/exkernelmanager/p;-><init>(Lflar2/exkernelmanager/MainActivity;Landroid/app/Fragment;I)V

    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget v1, Lflar2/exkernelmanager/MainApp;->a:I

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    goto :goto_0
.end method

.method private k()V
    .locals 4

    new-instance v0, Landroid/support/v7/app/ac;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0034

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f020092

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(I)Landroid/support/v7/app/ac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->a(Z)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e0035

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;

    move-result-object v1

    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lflar2/exkernelmanager/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lflar2/exkernelmanager/m;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/m;-><init>(Lflar2/exkernelmanager/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    move-result-object v1

    const-string v2, "Install"

    new-instance v3, Lflar2/exkernelmanager/l;

    invoke-direct {v3, p0}, Lflar2/exkernelmanager/l;-><init>(Lflar2/exkernelmanager/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/ac;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;

    invoke-virtual {v0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-void
.end method

.method private l()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "prefPowersaver"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefPowersaver"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->A:Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "prefPowersaver"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->A:Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/powersave/a;->a(Z)V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "prefPerformance"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefPerformance"

    invoke-static {v0, v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->B:Lflar2/exkernelmanager/performance/a;

    invoke-virtual {v0, v2}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "prefPerformance"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->B:Lflar2/exkernelmanager/performance/a;

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/performance/a;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 8

    const-wide/16 v6, 0xe1

    const/4 v4, 0x0

    const/4 v1, 0x0

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Dashboard"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflar2/exkernelmanager/fragments/dy;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/dy;-><init>()V

    :goto_0
    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->b()V

    :cond_0
    iget-boolean v1, p0, Lflar2/exkernelmanager/MainActivity;->z:Z

    if-eqz v1, :cond_e

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const/high16 v2, 0x10b0000

    const v3, 0x10b0001

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0c00b1

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_1
    iget v0, p0, Lflar2/exkernelmanager/MainActivity;->C:I

    if-ge p1, v0, :cond_d

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    invoke-virtual {v0, p1}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    :goto_1
    iput-boolean v4, p0, Lflar2/exkernelmanager/MainActivity;->z:Z

    :goto_2
    iput v4, p0, Lflar2/exkernelmanager/MainActivity;->x:I

    return-void

    :cond_2
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "CPU"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lflar2/exkernelmanager/fragments/ac;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/ac;-><init>()V

    goto :goto_0

    :cond_3
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Graphics"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lflar2/exkernelmanager/fragments/cz;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/cz;-><init>()V

    goto :goto_0

    :cond_4
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Wake"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lflar2/exkernelmanager/fragments/gf;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/gf;-><init>()V

    goto :goto_0

    :cond_5
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Voltage"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lflar2/exkernelmanager/fragments/gb;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/gb;-><init>()V

    goto/16 :goto_0

    :cond_6
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Sound"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lflar2/exkernelmanager/fragments/ep;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/ep;-><init>()V

    goto/16 :goto_0

    :cond_7
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Miscellaneous"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lflar2/exkernelmanager/fragments/dk;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/dk;-><init>()V

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Update"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lflar2/exkernelmanager/fragments/ey;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/ey;-><init>()V

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Tools"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lflar2/exkernelmanager/fragments/es;

    invoke-direct {v0}, Lflar2/exkernelmanager/fragments/es;-><init>()V

    goto/16 :goto_0

    :cond_a
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Settings"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/UserSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->y:Landroid/os/Handler;

    new-instance v3, Lflar2/exkernelmanager/n;

    invoke-direct {v3, p0, v0}, Lflar2/exkernelmanager/n;-><init>(Lflar2/exkernelmanager/MainActivity;Landroid/content/Intent;)V

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-object v0, v1

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "About"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/AboutActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lflar2/exkernelmanager/MainActivity;->y:Landroid/os/Handler;

    new-instance v3, Lflar2/exkernelmanager/o;

    invoke-direct {v3, p0, v0}, Lflar2/exkernelmanager/o;-><init>(Lflar2/exkernelmanager/MainActivity;Landroid/content/Intent;)V

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_c
    move-object v0, v1

    goto/16 :goto_0

    :cond_d
    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget v1, Lflar2/exkernelmanager/MainApp;->a:I

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    goto/16 :goto_1

    :cond_e
    invoke-direct {p0, v0, p1}, Lflar2/exkernelmanager/MainActivity;->a(Landroid/app/Fragment;I)V

    goto/16 :goto_2
.end method

.method public onBackPressed()V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lflar2/exkernelmanager/MainActivity;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflar2/exkernelmanager/MainActivity;->x:I

    iget v0, p0, Lflar2/exkernelmanager/MainActivity;->x:I

    if-ne v0, v3, :cond_0

    sget v0, Lflar2/exkernelmanager/MainApp;->a:I

    if-ne v0, v3, :cond_3

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->finish()V

    :cond_0
    :goto_0
    iget v0, p0, Lflar2/exkernelmanager/MainActivity;->x:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->finish()V

    :cond_1
    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/high16 v1, 0x10b0000

    const v2, 0x10b0001

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0c00b1

    new-instance v2, Lflar2/exkernelmanager/fragments/dy;

    invoke-direct {v2}, Lflar2/exkernelmanager/fragments/dy;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    sput v3, Lflar2/exkernelmanager/MainApp;->a:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->t:Landroid/support/v7/app/f;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/f;->a(Landroid/content/res/Configuration;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    const/4 v11, 0x3

    const/4 v7, 0x0

    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x1

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030022

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->setContentView(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_0
    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    const/high16 v2, 0x4000000

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    :cond_1
    :goto_0
    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->u:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->u:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    const v0, 0x7f0c00b3

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->o:Landroid/view/View;

    const v0, 0x7f0c00b7

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lflar2/exkernelmanager/MainActivity;->p:Landroid/view/View;

    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    const-string v1, "/system/xbin/busybox"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lflar2/exkernelmanager/MainActivity;->k()V

    :cond_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->q:Landroid/content/SharedPreferences;

    new-instance v0, Lflar2/exkernelmanager/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflar2/exkernelmanager/q;-><init>(Lflar2/exkernelmanager/MainActivity;Lflar2/exkernelmanager/i;)V

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->r:Lflar2/exkernelmanager/q;

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->q:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->r:Lflar2/exkernelmanager/q;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    new-instance v0, Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/powersave/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->A:Lflar2/exkernelmanager/powersave/a;

    new-instance v0, Lflar2/exkernelmanager/performance/a;

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflar2/exkernelmanager/performance/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->B:Lflar2/exkernelmanager/performance/a;

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/f;->a()V

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->v:Ljava/lang/CharSequence;

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->w:Ljava/lang/CharSequence;

    const v0, 0x7f0c00b9

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    sput-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v8}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    new-instance v3, Lflar2/exkernelmanager/utilities/m;

    invoke-direct {v3}, Lflar2/exkernelmanager/utilities/m;-><init>()V

    new-instance v4, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v4}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    new-instance v5, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-direct {v5, v7}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(I)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v6, "Header"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lflar2/exkernelmanager/slidingmenu/b/a;

    aget-object v6, v0, v7

    invoke-virtual {v1, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    invoke-direct {v5, v8, v6, v7}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v6, "Dashboard"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lflar2/exkernelmanager/slidingmenu/b/a;

    aget-object v6, v0, v8

    invoke-virtual {v1, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    invoke-direct {v5, v8, v6, v7}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v6, "CPU"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lflar2/exkernelmanager/slidingmenu/b/a;

    aget-object v6, v0, v10

    invoke-virtual {v1, v10, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    invoke-direct {v5, v8, v6, v7}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v6, "Graphics"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    sget-object v6, Lflar2/exkernelmanager/r;->m:[Ljava/lang/String;

    invoke-virtual {v3, v6}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    sget-object v5, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    sget-object v6, Lflar2/exkernelmanager/r;->n:[Ljava/lang/String;

    invoke-virtual {v3, v6}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    new-instance v5, Lflar2/exkernelmanager/slidingmenu/b/a;

    aget-object v6, v0, v11

    invoke-virtual {v1, v11, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    invoke-direct {v5, v8, v6, v7}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v6, "Wake"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    sget-object v5, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    sget-object v6, Lflar2/exkernelmanager/r;->j:[Ljava/lang/String;

    invoke-virtual {v3, v6}, Lflar2/exkernelmanager/utilities/m;->a([Ljava/lang/String;)I

    move-result v3

    aget-object v3, v5, v3

    invoke-virtual {v4, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/4 v5, 0x4

    aget-object v5, v0, v5

    const/4 v6, 0x4

    invoke-virtual {v1, v6, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    invoke-direct {v3, v8, v5, v6}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v5, "Voltage"

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    const-string v3, "/sys/kernel/sound_control_3"

    invoke-virtual {v4, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "/sys/devices/virtual/misc/soundcontrol/volume_boost"

    invoke-virtual {v4, v3}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/4 v4, 0x5

    aget-object v4, v0, v4

    const/4 v5, 0x5

    invoke-virtual {v1, v5, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    invoke-direct {v3, v8, v4, v5}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v4, "Sound"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/4 v4, 0x6

    aget-object v4, v0, v4

    const/4 v5, 0x6

    invoke-virtual {v1, v5, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    invoke-direct {v3, v8, v4, v5}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v4, "Miscellaneous"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lflar2/exkernelmanager/utilities/d;

    invoke-direct {v3}, Lflar2/exkernelmanager/utilities/d;-><init>()V

    invoke-virtual {v3}, Lflar2/exkernelmanager/utilities/d;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "incompatible"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/4 v4, 0x7

    aget-object v4, v0, v4

    const/4 v5, 0x7

    invoke-virtual {v1, v5, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    invoke-direct {v3, v8, v4, v5}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v4, "Update"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_9

    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/16 v4, 0x8

    aget-object v4, v0, v4

    const/16 v5, 0x8

    invoke-virtual {v1, v5, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    invoke-direct {v3, v8, v4, v5}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v4, "Tools"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    invoke-direct {v3, v11}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v4, "Settings"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/16 v4, 0x9

    aget-object v4, v0, v4

    const/16 v5, 0x9

    invoke-virtual {v1, v5, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    invoke-direct {v3, v10, v4, v5}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v4, "Settings"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lflar2/exkernelmanager/MainActivity;->C:I

    new-instance v3, Lflar2/exkernelmanager/slidingmenu/b/a;

    const/16 v4, 0xa

    aget-object v0, v0, v4

    const/16 v4, 0xa

    invoke-virtual {v1, v4, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    invoke-direct {v3, v10, v0, v4}, Lflar2/exkernelmanager/slidingmenu/b/a;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v3, "About"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    invoke-direct {v0, v2}, Lflar2/exkernelmanager/slidingmenu/a/a;-><init>(Ljava/util/ArrayList;)V

    sget-object v1, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/cc;)V

    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->n:Landroid/support/v7/widget/cl;

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->n:Landroid/support/v7/widget/cl;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/cl;)V

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/cc;

    move-result-object v0

    check-cast v0, Lflar2/exkernelmanager/slidingmenu/a/a;

    sget v1, Lflar2/exkernelmanager/MainApp;->a:I

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/slidingmenu/a/a;->f(I)V

    const v0, 0x7f0c00b0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_a

    const-string v0, "prefHTC"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setStatusBarBackgroundColor(I)V

    :cond_a
    :goto_1
    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f020055

    const v2, 0x800003

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(II)V

    new-instance v0, Lflar2/exkernelmanager/i;

    iget-object v3, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    iget-object v4, p0, Lflar2/exkernelmanager/MainActivity;->u:Landroid/support/v7/widget/Toolbar;

    const v5, 0x7f0e0026

    const v6, 0x7f0e0026

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lflar2/exkernelmanager/i;-><init>(Lflar2/exkernelmanager/MainActivity;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    iput-object v0, p0, Lflar2/exkernelmanager/MainActivity;->t:Landroid/support/v7/app/f;

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->t:Landroid/support/v7/app/f;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/r;)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->t:Landroid/support/v7/app/f;

    invoke-virtual {v0}, Landroid/support/v7/app/f;->a()V

    :cond_b
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lflar2/exkernelmanager/j;

    invoke-direct {v1, p0}, Lflar2/exkernelmanager/j;-><init>(Lflar2/exkernelmanager/MainActivity;)V

    invoke-direct {v0, p0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    sget-object v1, Lflar2/exkernelmanager/slidingmenu/a/a;->a:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lflar2/exkernelmanager/k;

    invoke-direct {v2, p0, v0}, Lflar2/exkernelmanager/k;-><init>(Lflar2/exkernelmanager/MainActivity;Landroid/view/GestureDetector;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cn;)V

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "update"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "update"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "openUpdaterFragment"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "update"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    sget-object v0, Lflar2/exkernelmanager/slidingmenu/a/a;->b:Ljava/util/ArrayList;

    const-string v1, "Update"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->a(I)V

    :cond_c
    :goto_2
    const-string v0, "prefPowersaver"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->A:Lflar2/exkernelmanager/powersave/a;

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/powersave/a;->b(Z)V

    :cond_d
    const-string v0, "prefPerformance"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->B:Lflar2/exkernelmanager/performance/a;

    invoke-virtual {v0, v8}, Lflar2/exkernelmanager/performance/a;->b(Z)V

    :cond_e
    new-instance v0, Lflar2/exkernelmanager/utilities/e;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/e;-><init>()V

    const-string v1, "/system/xbin/busybox"

    invoke-virtual {v0, v1}, Lflar2/exkernelmanager/utilities/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    invoke-direct {p0}, Lflar2/exkernelmanager/MainActivity;->k()V

    :cond_f
    const-string v0, "prefCPUNotify"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_13

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "tempUnit"

    const-string v2, "prefTempUnit"

    invoke-static {v2}, Lflar2/exkernelmanager/utilities/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_3
    const-string v0, "prefDisableBatteryMon"

    invoke-static {v0}, Lflar2/exkernelmanager/utilities/k;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_14

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_4
    invoke-static {p0}, La/a/a/a;->a(Landroid/app/Activity;)La/a/a/a;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, La/a/a/a;->a(I)La/a/a/a;

    move-result-object v0

    sget-object v1, La/a/a/n;->b:La/a/a/n;

    invoke-virtual {v0, v1}, La/a/a/a;->a(La/a/a/n;)La/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, La/a/a/a;->a()V

    return-void

    :cond_10
    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setStatusBarBackgroundColor(I)V

    goto/16 :goto_1

    :cond_12
    if-nez p1, :cond_c

    iput-boolean v8, p0, Lflar2/exkernelmanager/MainActivity;->z:Z

    invoke-virtual {p0, v8}, Lflar2/exkernelmanager/MainActivity;->a(I)V

    goto/16 :goto_2

    :cond_13
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflar2/exkernelmanager/CPUTempMonitor/CPUTempService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->stopService(Landroid/content/Intent;)Z

    goto :goto_3

    :cond_14
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflar2/exkernelmanager/BatteryMonitor/BatteryMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflar2/exkernelmanager/MainActivity;->stopService(Landroid/content/Intent;)Z

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ad;->onDestroy()V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->q:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->r:Lflar2/exkernelmanager/q;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lflar2/exkernelmanager/MainActivity;->t:Landroid/support/v7/app/f;

    invoke-virtual {v2, p1}, Landroid/support/v7/app/f;->a(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/UserSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflar2/exkernelmanager/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflar2/exkernelmanager/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lflar2/exkernelmanager/MainActivity;->l()V

    move v0, v1

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lflar2/exkernelmanager/MainActivity;->m()V

    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c0173 -> :sswitch_0
        0x7f0c0174 -> :sswitch_1
        0x7f0c017f -> :sswitch_3
        0x7f0c0180 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/app/ad;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/MainActivity;->t:Landroid/support/v7/app/f;

    invoke-virtual {v0}, Landroid/support/v7/app/f;->a()V

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    iput-object p1, p0, Lflar2/exkernelmanager/MainActivity;->w:Ljava/lang/CharSequence;

    const-string v0, "Dashboard"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "Voltage"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ElementalX"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->w:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lflar2/exkernelmanager/MainActivity;->g()Landroid/support/v7/app/a;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/MainActivity;->w:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
