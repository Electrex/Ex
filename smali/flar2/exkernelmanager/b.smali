.class Lflar2/exkernelmanager/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/a/a/a/a/e;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/AboutActivity;


# direct methods
.method constructor <init>(Lflar2/exkernelmanager/AboutActivity;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(ILjava/lang/Throwable;)V
    .locals 3

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Billing Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/AboutActivity;->a(Lflar2/exkernelmanager/AboutActivity;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/a/a/a/a/i;)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    invoke-virtual {v0, p1}, Lcom/a/a/a/a/c;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    const-string v1, "Thanks for your support :)\n"

    invoke-static {v0, v1}, Lflar2/exkernelmanager/AboutActivity;->a(Lflar2/exkernelmanager/AboutActivity;Ljava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflar2/exkernelmanager/AboutActivity;->a(Lflar2/exkernelmanager/AboutActivity;Z)Z

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    const-string v1, "coffee"

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/c;->d(Ljava/lang/String;)Lcom/a/a/a/a/h;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lcom/a/a/a/a/h;->g:Ljava/lang/String;

    iput-object v0, v1, Lflar2/exkernelmanager/AboutActivity;->p:Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    const-string v1, "beer"

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/c;->d(Ljava/lang/String;)Lcom/a/a/a/a/h;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lcom/a/a/a/a/h;->g:Ljava/lang/String;

    iput-object v0, v1, Lflar2/exkernelmanager/AboutActivity;->q:Ljava/lang/String;

    iget-object v0, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lflar2/exkernelmanager/AboutActivity;->n:Lcom/a/a/a/a/c;

    const-string v1, "lunch"

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/c;->d(Ljava/lang/String;)Lcom/a/a/a/a/h;

    move-result-object v0

    iget-object v1, p0, Lflar2/exkernelmanager/b;->a:Lflar2/exkernelmanager/AboutActivity;

    iget-object v0, v0, Lcom/a/a/a/a/h;->g:Ljava/lang/String;

    iput-object v0, v1, Lflar2/exkernelmanager/AboutActivity;->r:Ljava/lang/String;

    return-void
.end method
