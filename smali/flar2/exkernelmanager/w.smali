.class Lflar2/exkernelmanager/w;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lflar2/exkernelmanager/u;


# direct methods
.method private constructor <init>(Lflar2/exkernelmanager/u;)V
    .locals 0

    iput-object p1, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflar2/exkernelmanager/u;Lflar2/exkernelmanager/v;)V
    .locals 0

    invoke-direct {p0, p1}, Lflar2/exkernelmanager/w;-><init>(Lflar2/exkernelmanager/u;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    new-instance v0, Lflar2/exkernelmanager/utilities/f;

    invoke-direct {v0}, Lflar2/exkernelmanager/utilities/f;-><init>()V

    invoke-virtual {v0}, Lflar2/exkernelmanager/utilities/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lflar2/exkernelmanager/StartActivity;->b:Z

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    invoke-interface {v0}, Lflar2/exkernelmanager/x;->c()V

    :cond_0
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lflar2/exkernelmanager/x;->a(I)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/w;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    invoke-interface {v0}, Lflar2/exkernelmanager/x;->b()V

    :cond_0
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/w;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflar2/exkernelmanager/w;->a:Lflar2/exkernelmanager/u;

    invoke-static {v0}, Lflar2/exkernelmanager/u;->a(Lflar2/exkernelmanager/u;)Lflar2/exkernelmanager/x;

    move-result-object v0

    invoke-interface {v0}, Lflar2/exkernelmanager/x;->a()V

    :cond_0
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lflar2/exkernelmanager/w;->a([Ljava/lang/Integer;)V

    return-void
.end method
