.class La/a/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:La/a/a/a;


# direct methods
.method constructor <init>(La/a/a/a;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, La/a/a/b;->b:La/a/a/a;

    iput-object p2, p0, La/a/a/b;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "market://details?id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, La/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, La/a/a/b;->b:La/a/a/a;

    invoke-static {v1}, La/a/a/a;->a(La/a/a/a;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, La/a/a/b;->b:La/a/a/a;

    iget-object v1, p0, La/a/a/b;->b:La/a/a/a;

    invoke-static {v1}, La/a/a/a;->b(La/a/a/a;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-static {v0, v1}, La/a/a/a;->a(La/a/a/a;Landroid/view/ViewGroup;)V

    iget-object v0, p0, La/a/a/b;->b:La/a/a/a;

    invoke-static {v0}, La/a/a/a;->c(La/a/a/a;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "clicked"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, La/a/a/b;->b:La/a/a/a;

    invoke-static {v0}, La/a/a/a;->d(La/a/a/a;)V

    iget-object v0, p0, La/a/a/b;->b:La/a/a/a;

    invoke-static {v0}, La/a/a/a;->e(La/a/a/a;)La/a/a/f;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/a/b;->b:La/a/a/a;

    invoke-static {v0}, La/a/a/a;->e(La/a/a/a;)La/a/a/f;

    move-result-object v0

    invoke-interface {v0}, La/a/a/f;->b()V

    :cond_0
    return-void
.end method
