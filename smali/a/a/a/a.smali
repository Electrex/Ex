.class public La/a/a/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:La/a/a/n;

.field private e:La/a/a/f;

.field private f:Landroid/content/SharedPreferences;

.field private g:Landroid/content/SharedPreferences$Editor;

.field private h:I

.field private i:J

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:La/a/a/g;

.field private m:J

.field private n:Z

.field private o:J

.field private p:J

.field private q:I

.field private r:Landroid/view/ViewGroup;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    iput v0, p0, La/a/a/a;->c:I

    sget-object v0, La/a/a/n;->b:La/a/a/n;

    iput-object v0, p0, La/a/a/a;->d:La/a/a/n;

    iput v1, p0, La/a/a/a;->h:I

    sget-object v0, La/a/a/g;->a:La/a/a/g;

    iput-object v0, p0, La/a/a/a;->l:La/a/a/g;

    iput-boolean v1, p0, La/a/a/a;->n:Z

    iput-object p1, p0, La/a/a/a;->a:Landroid/app/Activity;

    return-void
.end method

.method public static a(Landroid/app/Activity;)La/a/a/a;
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a;

    invoke-direct {v0, p0}, La/a/a/a;-><init>(Landroid/app/Activity;)V

    sget v1, La/a/a/m;->dra_rate_app:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, La/a/a/a;->b:Ljava/lang/String;

    const-string v1, "app_rate_prefs"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, v0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    iget-object v1, v0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, v0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, La/a/a/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(La/a/a/a;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(La/a/a/a;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1}, La/a/a/a;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 2

    iget-boolean v0, p0, La/a/a/a;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    sget v1, La/a/a/i;->fade_out_from_top:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    :goto_0
    new-instance v1, La/a/a/e;

    invoke-direct {v1, p0, p1}, La/a/a/e;-><init>(La/a/a/a;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    return-void

    :cond_0
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    sget v1, La/a/a/i;->fade_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "DicreetAppRate"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic b(La/a/a/a;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic b(La/a/a/a;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1}, La/a/a/a;->b(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private b(Landroid/view/ViewGroup;)V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz p1, :cond_0

    iget-boolean v0, p0, La/a/a/a;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    sget v1, La/a/a/i;->fade_in_from_top:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, La/a/a/a;->e:La/a/a/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, La/a/a/a;->e:La/a/a/f;

    invoke-interface {v0, p0, p1}, La/a/a/f;->a(La/a/a/a;Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    sget v1, La/a/a/i;->fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Z
    .locals 8

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v4, "last_count_update"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, La/a/a/a;->p:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    iget-boolean v1, p0, La/a/a/a;->j:Z

    if-eqz v1, :cond_0

    const-string v1, "Count not incremented due to minimum interval not reached"

    invoke-direct {p0, v1}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    const-string v2, "count"

    iget-object v3, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v4, "count"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    const-string v1, "last_count_update"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0}, La/a/a/a;->d()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(La/a/a/a;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    iget-object v0, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method private c()V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/16 v9, 0x10

    const/high16 v8, -0x1000000

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget v0, p0, La/a/a/a;->q:I

    if-eqz v0, :cond_7

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    :try_start_0
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, La/a/a/a;->q:I

    iget-object v2, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    sget v1, La/a/a/k;->dar_close:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    sget v1, La/a/a/k;->dar_rate_element:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    sget v2, La/a/a/k;->dar_container:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-boolean v2, p0, La/a/a/a;->n:Z

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v6, 0x30

    iput v6, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    iget-object v2, p0, La/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, La/a/a/a;->k:Ljava/lang/String;

    new-instance v6, La/a/a/b;

    invoke-direct {v6, p0, v2}, La/a/a/b;-><init>(La/a/a/a;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    if-eqz v3, :cond_2

    new-instance v2, La/a/a/c;

    invoke-direct {v2, p0}, La/a/a/c;-><init>(La/a/a/a;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget v2, p0, La/a/a/a;->q:I

    if-nez v2, :cond_3

    iget-object v2, p0, La/a/a/a;->l:La/a/a/g;

    sget-object v6, La/a/a/g;->b:La/a/a/g;

    if-ne v2, v6, :cond_c

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    iget-object v6, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, La/a/a/j;->ic_action_remove:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6, v8, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    move-object v2, v3

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    const v0, -0x77000001

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_b

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, La/a/a/j;->selectable_button_light:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_6

    if-eqz v1, :cond_6

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget-boolean v2, p0, La/a/a/a;->n:Z

    if-eqz v2, :cond_10

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x4000000

    invoke-static {v0, v2}, La/a/a/o;->a(II)Z

    move-result v2

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    const/16 v3, 0x200

    if-ne v0, v3, :cond_e

    const/4 v0, 0x1

    :goto_3
    if-nez v2, :cond_4

    if-eqz v0, :cond_6

    :cond_4
    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_5

    const-string v0, "Activity is translucent"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_f

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-static {v2}, La/a/a/o;->d(Landroid/app/Activity;)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    :goto_4
    iget v0, p0, La/a/a/a;->h:I

    if-lez v0, :cond_13

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, La/a/a/d;

    invoke-direct {v1, p0}, La/a/a/d;-><init>(La/a/a/a;)V

    iget v2, p0, La/a/a/a;->h:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_5
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, La/a/a/l;->app_rate:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    iput v5, p0, La/a/a/a;->q:I

    goto/16 :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, La/a/a/l;->app_rate:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    iput v5, p0, La/a/a/a;->q:I

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, La/a/a/l;->app_rate:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v6, 0xa

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v6, 0x50

    iput v6, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v6, 0xc

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, La/a/a/j;->selectable_button_light:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, La/a/a/j;->ic_action_remove:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    move-object v0, v3

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/high16 v0, -0x56000000

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_d

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, La/a/a/j;->selectable_button_dark:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_d
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, La/a/a/j;->selectable_button_dark:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_e
    move v0, v5

    goto/16 :goto_3

    :cond_f
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-static {v2}, La/a/a/o;->d(Landroid/app/Activity;)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    :cond_10
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x8000000

    invoke-static {v0, v2}, La/a/a/o;->a(II)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_11

    const-string v0, "Activity is translucent"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_11
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_12

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    :goto_6
    if-eqz v0, :cond_6

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_4

    :pswitch_0
    iget-object v2, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-static {v2}, La/a/a/o;->a(Landroid/app/Activity;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    :cond_12
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_14

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_6

    :pswitch_1
    iget-object v2, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-static {v2}, La/a/a/o;->b(Landroid/app/Activity;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    :cond_13
    iget-object v0, p0, La/a/a/a;->r:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, La/a/a/a;->b(Landroid/view/ViewGroup;)V

    goto/16 :goto_5

    :cond_14
    move-object v0, v4

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    iget-object v0, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method static synthetic d(La/a/a/a;)V
    .locals 0

    invoke-direct {p0}, La/a/a/a;->d()V

    return-void
.end method

.method static synthetic e(La/a/a/a;)La/a/a/f;
    .locals 1

    iget-object v0, p0, La/a/a/a;->e:La/a/a/f;

    return-object v0
.end method


# virtual methods
.method public a(I)La/a/a/a;
    .locals 0

    iput p1, p0, La/a/a/a;->c:I

    return-object p0
.end method

.method public a(La/a/a/n;)La/a/a/a;
    .locals 0

    iput-object p1, p0, La/a/a/a;->d:La/a/a/n;

    return-object p0
.end method

.method public a()V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-static {v0}, La/a/a/o;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "Play Store is not installed. Won\'t do anything"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Last crash: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v4, "last_crash"

    invoke-interface {v1, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds ago"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v3, "last_crash"

    invoke-interface {v2, v3, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-wide v2, p0, La/a/a/a;->m:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "A recent crash avoids anything to be done."

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "monitor_total"

    invoke-interface {v0, v1, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iget-wide v2, p0, La/a/a/a;->o:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "Monitor time not reached. Nothing will be done"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-static {v0}, La/a/a/o;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "Device is not online. AppRate try to show up next time."

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, La/a/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    iget-object v0, p0, La/a/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, La/a/a/a;->k:Ljava/lang/String;

    invoke-static {v0, v1}, La/a/a/o;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, La/a/a/a;->i:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_6

    iget-boolean v2, p0, La/a/a/a;->j:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Date not reached. Time elapsed since installation (in sec.): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long v0, v4, v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "elapsed_time"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    const-string v1, "elapsed_time"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_7

    const-string v0, "First time after the time is elapsed"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "count"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget v1, p0, La/a/a/a;->c:I

    if-le v0, v1, :cond_9

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_8

    const-string v0, "Initial count passed. Resetting to initialLaunchCount"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, La/a/a/a;->g:Landroid/content/SharedPreferences$Editor;

    const-string v1, "count"

    iget v2, p0, La/a/a/a;->c:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_9
    invoke-direct {p0}, La/a/a/a;->d()V

    :cond_a
    iget-object v0, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "clicked"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/a/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "count"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget v1, p0, La/a/a/a;->c:I

    if-ne v0, v1, :cond_c

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_b

    const-string v0, "initialLaunchCount reached"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_b
    invoke-direct {p0}, La/a/a/a;->c()V

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, La/a/a/a;->d:La/a/a/n;

    sget-object v2, La/a/a/n;->a:La/a/a/n;

    if-ne v1, v2, :cond_e

    iget v1, p0, La/a/a/a;->c:I

    rem-int v1, v0, v1

    if-nez v1, :cond_e

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_d

    const-string v0, "initialLaunchCount incremental reached"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_d
    invoke-direct {p0}, La/a/a/a;->c()V

    goto/16 :goto_0

    :cond_e
    iget-object v1, p0, La/a/a/a;->d:La/a/a/n;

    sget-object v2, La/a/a/n;->b:La/a/a/n;

    if-ne v1, v2, :cond_10

    iget v1, p0, La/a/a/a;->c:I

    rem-int v1, v0, v1

    if-nez v1, :cond_10

    iget v1, p0, La/a/a/a;->c:I

    div-int v1, v0, v1

    invoke-static {v1}, La/a/a/o;->a(I)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-boolean v0, p0, La/a/a/a;->j:Z

    if-eqz v0, :cond_f

    const-string v0, "initialLaunchCount exponential reached"

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    :cond_f
    invoke-direct {p0}, La/a/a/a;->c()V

    goto/16 :goto_0

    :cond_10
    iget-boolean v1, p0, La/a/a/a;->j:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Nothing to show. initialLaunchCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, La/a/a/a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - Current count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/a/a;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
