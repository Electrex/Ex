.class Lcom/c/a/z;
.super Landroid/view/View;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/c/a/z;->setSaveEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/c/a/z;->setWillNotDraw(Z)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/c/a/z;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onWindowSystemUiVisibilityChanged(I)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/View;->onWindowSystemUiVisibilityChanged(I)V

    invoke-virtual {p0}, Lcom/c/a/z;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Lcom/c/a/n;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/c/a/n;

    invoke-virtual {v0, p1}, Lcom/c/a/n;->c(I)V

    :cond_0
    return-void
.end method
