.class public Lcom/c/a/aa;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/os/Handler;

.field private static c:Lcom/c/a/n;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/c/a/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/c/a/aa;->a:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/c/a/aa;->b:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/c/a/n;
    .locals 1

    sget-object v0, Lcom/c/a/aa;->c:Lcom/c/a/n;

    return-object v0
.end method

.method public static a(Lcom/c/a/n;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/c/a/n;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p0, v0}, Lcom/c/a/aa;->a(Lcom/c/a/n;Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/c/a/aa;->a:Ljava/lang/String;

    const-string v2, "Couldn\'t get Activity from the Snackbar\'s Context. Try calling #show(Snackbar, Activity) instead"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Lcom/c/a/n;Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/c/a/aa;->b:Landroid/os/Handler;

    new-instance v1, Lcom/c/a/ab;

    invoke-direct {v1, p0, p1}, Lcom/c/a/ab;-><init>(Lcom/c/a/n;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/c/a/n;)Lcom/c/a/n;
    .locals 0

    sput-object p0, Lcom/c/a/aa;->c:Lcom/c/a/n;

    return-object p0
.end method
