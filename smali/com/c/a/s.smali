.class Lcom/c/a/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcom/c/a/n;


# direct methods
.method constructor <init>(Lcom/c/a/n;)V
    .locals 0

    iput-object p1, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-virtual {v0}, Lcom/c/a/n;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-static {v0}, Lcom/c/a/n;->l(Lcom/c/a/n;)Lcom/c/a/c/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-static {v0}, Lcom/c/a/n;->m(Lcom/c/a/n;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-static {v0}, Lcom/c/a/n;->l(Lcom/c/a/n;)Lcom/c/a/c/c;

    move-result-object v0

    iget-object v1, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-interface {v0, v1}, Lcom/c/a/c/c;->b(Lcom/c/a/n;)V

    :goto_0
    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-static {v0}, Lcom/c/a/n;->n(Lcom/c/a/n;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-static {v0}, Lcom/c/a/n;->l(Lcom/c/a/n;)Lcom/c/a/c/c;

    move-result-object v0

    iget-object v1, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-interface {v0, v1}, Lcom/c/a/c/c;->c(Lcom/c/a/n;)V

    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/c/a/n;->c(Lcom/c/a/n;Z)Z

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-static {v0}, Lcom/c/a/n;->l(Lcom/c/a/n;)Lcom/c/a/c/c;

    move-result-object v0

    iget-object v1, p0, Lcom/c/a/s;->a:Lcom/c/a/n;

    invoke-interface {v0, v1}, Lcom/c/a/c/c;->a(Lcom/c/a/n;)V

    goto :goto_0
.end method
