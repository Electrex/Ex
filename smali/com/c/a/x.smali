.class public final enum Lcom/c/a/x;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/c/a/x;

.field public static final enum b:Lcom/c/a/x;

.field public static final enum c:Lcom/c/a/x;

.field private static final synthetic e:[Lcom/c/a/x;


# instance fields
.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/c/a/x;

    const-string v1, "LENGTH_SHORT"

    const-wide/16 v2, 0x7d0

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/c/a/x;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/c/a/x;->a:Lcom/c/a/x;

    new-instance v0, Lcom/c/a/x;

    const-string v1, "LENGTH_LONG"

    const-wide/16 v2, 0xdac

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/c/a/x;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/c/a/x;->b:Lcom/c/a/x;

    new-instance v0, Lcom/c/a/x;

    const-string v1, "LENGTH_INDEFINITE"

    const-wide/16 v2, -0x1

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/c/a/x;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/c/a/x;->c:Lcom/c/a/x;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/c/a/x;

    sget-object v1, Lcom/c/a/x;->a:Lcom/c/a/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/c/a/x;->b:Lcom/c/a/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/c/a/x;->c:Lcom/c/a/x;

    aput-object v1, v0, v6

    sput-object v0, Lcom/c/a/x;->e:[Lcom/c/a/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, Lcom/c/a/x;->d:J

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/c/a/x;
    .locals 1

    const-class v0, Lcom/c/a/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/c/a/x;

    return-object v0
.end method

.method public static values()[Lcom/c/a/x;
    .locals 1

    sget-object v0, Lcom/c/a/x;->e:[Lcom/c/a/x;

    invoke-virtual {v0}, [Lcom/c/a/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/c/a/x;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/c/a/x;->d:J

    return-wide v0
.end method
