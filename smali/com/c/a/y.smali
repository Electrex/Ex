.class public final enum Lcom/c/a/y;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/c/a/y;

.field public static final enum b:Lcom/c/a/y;

.field private static final synthetic d:[Lcom/c/a/y;


# instance fields
.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/c/a/y;

    const-string v1, "TOP"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v3, v2}, Lcom/c/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/c/a/y;->a:Lcom/c/a/y;

    new-instance v0, Lcom/c/a/y;

    const-string v1, "BOTTOM"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v4, v2}, Lcom/c/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/c/a/y;->b:Lcom/c/a/y;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/c/a/y;

    sget-object v1, Lcom/c/a/y;->a:Lcom/c/a/y;

    aput-object v1, v0, v3

    sget-object v1, Lcom/c/a/y;->b:Lcom/c/a/y;

    aput-object v1, v0, v4

    sput-object v0, Lcom/c/a/y;->d:[Lcom/c/a/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/c/a/y;->c:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/c/a/y;
    .locals 1

    const-class v0, Lcom/c/a/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/c/a/y;

    return-object v0
.end method

.method public static values()[Lcom/c/a/y;
    .locals 1

    sget-object v0, Lcom/c/a/y;->d:[Lcom/c/a/y;

    invoke-virtual {v0}, [Lcom/c/a/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/c/a/y;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/c/a/y;->c:I

    return v0
.end method
