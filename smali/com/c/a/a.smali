.class Lcom/c/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/c/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/c/a/d;

    invoke-direct {v0}, Lcom/c/a/d;-><init>()V

    sput-object v0, Lcom/c/a/a;->a:Lcom/c/a/b;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/c/a/c;

    invoke-direct {v0}, Lcom/c/a/c;-><init>()V

    sput-object v0, Lcom/c/a/a;->a:Lcom/c/a/b;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/c/a/e;

    invoke-direct {v0}, Lcom/c/a/e;-><init>()V

    sput-object v0, Lcom/c/a/a;->a:Lcom/c/a/b;

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/Float;)I
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-static {v0, v1}, Lcom/c/a/a;->b(Landroid/view/Display;Landroid/graphics/Point;)V

    iget v0, v1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 1

    sget-object v0, Lcom/c/a/a;->a:Lcom/c/a/b;

    invoke-virtual {v0, p0, p1}, Lcom/c/a/b;->a(Landroid/view/Display;Landroid/graphics/Point;)V

    return-void
.end method

.method public static b(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 1

    sget-object v0, Lcom/c/a/a;->a:Lcom/c/a/b;

    invoke-virtual {v0, p0, p1}, Lcom/c/a/b;->b(Landroid/view/Display;Landroid/graphics/Point;)V

    return-void
.end method
