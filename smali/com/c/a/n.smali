.class public Lcom/c/a/n;
.super Lcom/c/a/b/a;


# instance fields
.field private A:Lcom/c/a/c/a;

.field private B:Lcom/c/a/c/b;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Lcom/c/a/c/c;

.field private G:Landroid/graphics/Typeface;

.field private H:Landroid/graphics/Typeface;

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Landroid/graphics/Rect;

.field private M:Landroid/graphics/Rect;

.field private N:Landroid/graphics/Point;

.field private O:Landroid/graphics/Point;

.field private P:Landroid/app/Activity;

.field private Q:Ljava/lang/Float;

.field private R:Z

.field private S:Ljava/lang/Runnable;

.field private T:Ljava/lang/Runnable;

.field private a:I

.field private b:I

.field private c:Lcom/c/a/a/a;

.field private d:Lcom/c/a/x;

.field private e:Ljava/lang/CharSequence;

.field private f:Landroid/widget/TextView;

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/lang/Integer;

.field private k:Lcom/c/a/y;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:J

.field private r:J

.field private s:J

.field private t:Ljava/lang/CharSequence;

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:J


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    const-wide/16 v4, -0x1

    const/16 v0, -0x2710

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/c/a/b/a;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/c/a/n;->a:I

    iput v0, p0, Lcom/c/a/n;->b:I

    sget-object v0, Lcom/c/a/a/a;->a:Lcom/c/a/a/a;

    iput-object v0, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    sget-object v0, Lcom/c/a/x;->b:Lcom/c/a/x;

    iput-object v0, p0, Lcom/c/a/n;->d:Lcom/c/a/x;

    iget v0, p0, Lcom/c/a/n;->a:I

    iput v0, p0, Lcom/c/a/n;->g:I

    iget v0, p0, Lcom/c/a/n;->a:I

    iput v0, p0, Lcom/c/a/n;->h:I

    sget-object v0, Lcom/c/a/y;->b:Lcom/c/a/y;

    iput-object v0, p0, Lcom/c/a/n;->k:Lcom/c/a/y;

    iget v0, p0, Lcom/c/a/n;->b:I

    iput v0, p0, Lcom/c/a/n;->l:I

    iput v1, p0, Lcom/c/a/n;->m:I

    iput v1, p0, Lcom/c/a/n;->n:I

    iput v1, p0, Lcom/c/a/n;->o:I

    iput v1, p0, Lcom/c/a/n;->p:I

    iput-wide v4, p0, Lcom/c/a/n;->s:J

    iget v0, p0, Lcom/c/a/n;->a:I

    iput v0, p0, Lcom/c/a/n;->u:I

    iput-boolean v2, p0, Lcom/c/a/n;->v:Z

    iput-boolean v2, p0, Lcom/c/a/n;->w:Z

    iput-boolean v1, p0, Lcom/c/a/n;->x:Z

    iput-boolean v1, p0, Lcom/c/a/n;->y:Z

    iput-wide v4, p0, Lcom/c/a/n;->z:J

    iput-boolean v2, p0, Lcom/c/a/n;->E:Z

    iput-boolean v1, p0, Lcom/c/a/n;->I:Z

    iput-boolean v2, p0, Lcom/c/a/n;->J:Z

    iput-boolean v1, p0, Lcom/c/a/n;->K:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/c/a/n;->L:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/c/a/n;->M:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/c/a/n;->N:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/c/a/n;->O:Landroid/graphics/Point;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/c/a/n;->Q:Ljava/lang/Float;

    new-instance v0, Lcom/c/a/o;

    invoke-direct {v0, p0}, Lcom/c/a/o;-><init>(Lcom/c/a/n;)V

    iput-object v0, p0, Lcom/c/a/n;->S:Ljava/lang/Runnable;

    new-instance v0, Lcom/c/a/p;

    invoke-direct {v0, p0}, Lcom/c/a/p;-><init>(Lcom/c/a/n;)V

    iput-object v0, p0, Lcom/c/a/n;->T:Ljava/lang/Runnable;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/c/a/z;

    invoke-virtual {p0}, Lcom/c/a/n;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/c/a/z;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/c/a/n;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private static a(IF)I
    .locals 2

    int-to-float v0, p0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lcom/c/a/y;)I
    .locals 1

    sget-object v0, Lcom/c/a/y;->a:Lcom/c/a/y;

    if-ne p0, v0, :cond_0

    sget v0, Lcom/c/a/g;->sb__top_in:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/c/a/g;->sb__bottom_in:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/c/a/n;J)J
    .locals 1

    iput-wide p1, p0, Lcom/c/a/n;->r:J

    return-wide p1
.end method

.method private a(Landroid/content/Context;Landroid/app/Activity;Landroid/view/ViewGroup;Z)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 8

    const/16 v7, 0x8

    const/4 v6, -0x2

    const/4 v5, 0x1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/c/a/m;->sb__template:I

    invoke-virtual {v0, v1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/c/a/b/a;

    invoke-virtual {v0, v5}, Lcom/c/a/b/a;->setOrientation(I)V

    invoke-virtual {p0}, Lcom/c/a/n;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v1, p0, Lcom/c/a/n;->g:I

    iget v2, p0, Lcom/c/a/n;->a:I

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/c/a/n;->g:I

    :goto_0
    iput v1, p0, Lcom/c/a/n;->g:I

    sget v1, Lcom/c/a/j;->sb__offset:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/c/a/n;->i:I

    iput-boolean p4, p0, Lcom/c/a/n;->R:Z

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    iget-boolean v1, p0, Lcom/c/a/n;->R:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    invoke-virtual {v1}, Lcom/c/a/a/a;->a()I

    move-result v1

    invoke-static {v1, v2}, Lcom/c/a/n;->a(IF)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->setMinimumHeight(I)V

    iget-object v1, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    invoke-virtual {v1}, Lcom/c/a/a/a;->b()I

    move-result v1

    invoke-static {v1, v2}, Lcom/c/a/n;->a(IF)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->setMaxHeight(I)V

    iget v1, p0, Lcom/c/a/n;->g:I

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->setBackgroundColor(I)V

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/c/a/n;->k:Lcom/c/a/y;

    invoke-static {p3, v1, v6, v2}, Lcom/c/a/n;->a(Landroid/view/ViewGroup;IILcom/c/a/y;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v1

    move-object v2, v1

    :goto_1
    iget v1, p0, Lcom/c/a/n;->l:I

    iget v4, p0, Lcom/c/a/n;->b:I

    if-eq v1, v4, :cond_0

    iget v1, p0, Lcom/c/a/n;->l:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/c/a/n;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v1, p0, Lcom/c/a/n;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    sget v1, Lcom/c/a/l;->sb__divider:I

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/c/a/n;->j:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_2
    sget v1, Lcom/c/a/l;->sb__text:I

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/c/a/n;->e:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/c/a/n;->G:Landroid/graphics/Typeface;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget v1, p0, Lcom/c/a/n;->h:I

    iget v4, p0, Lcom/c/a/n;->a:I

    if-eq v1, v4, :cond_1

    iget-object v1, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    iget v4, p0, Lcom/c/a/n;->h:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    iget-object v1, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    invoke-virtual {v4}, Lcom/c/a/a/a;->c()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    sget v1, Lcom/c/a/l;->sb__action:I

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/c/a/n;->t:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p0}, Lcom/c/a/n;->requestLayout()V

    iget-object v1, p0, Lcom/c/a/n;->t:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/c/a/n;->H:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget v1, p0, Lcom/c/a/n;->u:I

    iget v4, p0, Lcom/c/a/n;->a:I

    if-eq v1, v4, :cond_2

    iget v1, p0, Lcom/c/a/n;->u:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    new-instance v1, Lcom/c/a/q;

    invoke-direct {v1, p0}, Lcom/c/a/q;-><init>(Lcom/c/a/n;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    invoke-virtual {v1}, Lcom/c/a/a/a;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    :goto_3
    invoke-virtual {p0, v5}, Lcom/c/a/n;->setClickable(Z)V

    iget-boolean v0, p0, Lcom/c/a/n;->J:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/c/a/h;->sb__is_swipeable:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/c/a/c/d;

    const/4 v1, 0x0

    new-instance v3, Lcom/c/a/r;

    invoke-direct {v3, p0}, Lcom/c/a/r;-><init>(Lcom/c/a/n;)V

    invoke-direct {v0, p0, v1, v3}, Lcom/c/a/c/d;-><init>(Landroid/view/View;Ljava/lang/Object;Lcom/c/a/c/f;)V

    invoke-virtual {p0, v0}, Lcom/c/a/n;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_3
    return-object v2

    :cond_4
    sget v1, Lcom/c/a/i;->sb__background:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_0

    :cond_5
    sget-object v1, Lcom/c/a/a/a;->a:Lcom/c/a/a/a;

    iput-object v1, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    sget v1, Lcom/c/a/j;->sb__min_width:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->setMinimumWidth(I)V

    iget-object v1, p0, Lcom/c/a/n;->Q:Ljava/lang/Float;

    if-nez v1, :cond_6

    sget v1, Lcom/c/a/j;->sb__max_width:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_4
    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->setMaxWidth(I)V

    sget v1, Lcom/c/a/k;->sb__bg:I

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->setBackgroundResource(I)V

    invoke-virtual {v0}, Lcom/c/a/b/a;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    iget v4, p0, Lcom/c/a/n;->g:I

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v1, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    invoke-virtual {v1}, Lcom/c/a/a/a;->b()I

    move-result v1

    invoke-static {v1, v2}, Lcom/c/a/n;->a(IF)I

    move-result v1

    iget-object v2, p0, Lcom/c/a/n;->k:Lcom/c/a/y;

    invoke-static {p3, v6, v1, v2}, Lcom/c/a/n;->a(Landroid/view/ViewGroup;IILcom/c/a/y;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/c/a/n;->Q:Ljava/lang/Float;

    invoke-static {p2, v1}, Lcom/c/a/a;->a(Landroid/app/Activity;Ljava/lang/Float;)I

    move-result v1

    goto :goto_4

    :cond_7
    sget v1, Lcom/c/a/l;->sb__divider:I

    invoke-virtual {v0, v1}, Lcom/c/a/b/a;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_8
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method private static a(Landroid/view/ViewGroup;IILcom/c/a/y;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 3

    const/4 v2, -0x1

    instance-of v0, p0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p3}, Lcom/c/a/y;->a()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    sget-object v1, Lcom/c/a/y;->a:Lcom/c/a/y;

    if-ne p3, v1, :cond_1

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    :cond_1
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    :cond_2
    instance-of v0, p0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p3}, Lcom/c/a/y;->a()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Requires FrameLayout or RelativeLayout for the parent of Snackbar"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/c/a/n;)Lcom/c/a/c/a;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->A:Lcom/c/a/c/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/c/a/n;
    .locals 1

    new-instance v0, Lcom/c/a/n;

    invoke-direct {v0, p0}, Lcom/c/a/n;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(J)V
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->S:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p1, p2}, Lcom/c/a/n;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/graphics/Rect;)V
    .locals 8

    const/4 v7, 0x0

    iput v7, p2, Landroid/graphics/Rect;->bottom:I

    iput v7, p2, Landroid/graphics/Rect;->right:I

    iput v7, p2, Landroid/graphics/Rect;->top:I

    iput v7, p2, Landroid/graphics/Rect;->left:I

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/c/a/n;->c(Landroid/app/Activity;)Z

    move-result v2

    invoke-direct {p0, v0}, Lcom/c/a/n;->a(Landroid/view/ViewGroup;)Z

    move-result v3

    iget-object v4, p0, Lcom/c/a/n;->M:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/c/a/n;->O:Landroid/graphics/Point;

    iget-object v6, p0, Lcom/c/a/n;->N:Landroid/graphics/Point;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-static {v1, v5}, Lcom/c/a/a;->b(Landroid/view/Display;Landroid/graphics/Point;)V

    invoke-static {v1, v6}, Lcom/c/a/a;->a(Landroid/view/Display;Landroid/graphics/Point;)V

    iget v0, v6, Landroid/graphics/Point;->x:I

    iget v1, v5, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_3

    if-nez v2, :cond_2

    if-eqz v3, :cond_0

    :cond_2
    iget v0, v5, Landroid/graphics/Point;->x:I

    iget v1, v6, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    iget v1, v5, Landroid/graphics/Point;->x:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p2, Landroid/graphics/Rect;->right:I

    goto :goto_0

    :cond_3
    iget v0, v6, Landroid/graphics/Point;->y:I

    iget v1, v5, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    if-nez v2, :cond_4

    if-eqz v3, :cond_0

    :cond_4
    iget v0, v5, Landroid/graphics/Point;->y:I

    iget v1, v6, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    iget v1, v5, Landroid/graphics/Point;->y:I

    iget v2, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;Landroid/view/ViewGroup$MarginLayoutParams;Landroid/view/ViewGroup;)V
    .locals 3

    invoke-virtual {p3, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getElevation()F

    move-result v1

    invoke-virtual {p0}, Lcom/c/a/n;->getElevation()F

    move-result v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/c/a/n;->setElevation(F)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p3, p0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/c/a/n;->bringToFront()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_2

    invoke-virtual {p3}, Landroid/view/ViewGroup;->requestLayout()V

    invoke-virtual {p3}, Landroid/view/ViewGroup;->invalidate()V

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/c/a/n;->I:Z

    iput-object p1, p0, Lcom/c/a/n;->P:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/c/a/n;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/c/a/s;

    invoke-direct {v1, p0}, Lcom/c/a/s;-><init>(Lcom/c/a/n;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    iget-boolean v0, p0, Lcom/c/a/n;->v:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/c/a/n;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/c/a/n;->h()V

    :cond_3
    :goto_1
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/c/a/n;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/c/a/n;->k:Lcom/c/a/y;

    invoke-static {v1}, Lcom/c/a/n;->a(Lcom/c/a/y;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/c/a/t;

    invoke-direct {v1, p0}, Lcom/c/a/t;-><init>(Lcom/c/a/n;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lcom/c/a/n;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    const/16 v0, 0x8

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/aj;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v4/view/a/aj;->a(Landroid/view/View;)V

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/c/a/n;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/c/a/n;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWindowSystemUiVisibility()I

    move-result v1

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/c/a/n;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/c/a/n;->D:Z

    return p1
.end method

.method public static b(Lcom/c/a/y;)I
    .locals 1

    sget-object v0, Lcom/c/a/y;->a:Lcom/c/a/y;

    if-ne p0, v0, :cond_0

    sget v0, Lcom/c/a/g;->sb__top_out:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/c/a/g;->sb__bottom_out:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/c/a/n;J)J
    .locals 1

    iput-wide p1, p0, Lcom/c/a/n;->s:J

    return-wide p1
.end method

.method static synthetic b(Lcom/c/a/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/c/a/n;->c(Z)V

    return-void
.end method

.method static b(Landroid/content/Context;)Z
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/c/a/h;->sb__is_phone:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/c/a/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->K:Z

    return v0
.end method

.method static synthetic c(Lcom/c/a/n;J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/c/a/n;->a(J)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/c/a/n;->K:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/c/a/n;->K:Z

    iget-object v0, p0, Lcom/c/a/n;->F:Lcom/c/a/c/c;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/c/a/n;->I:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/c/a/n;->x:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/c/a/n;->F:Lcom/c/a/c/c;

    invoke-interface {v0, p0}, Lcom/c/a/c/c;->e(Lcom/c/a/n;)V

    :cond_1
    :goto_1
    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/c/a/n;->i()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/c/a/n;->F:Lcom/c/a/c/c;

    invoke-interface {v0, p0}, Lcom/c/a/c/c;->d(Lcom/c/a/n;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/c/a/n;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/c/a/n;->k:Lcom/c/a/y;

    invoke-static {v1}, Lcom/c/a/n;->b(Lcom/c/a/y;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/c/a/v;

    invoke-direct {v1, p0}, Lcom/c/a/v;-><init>(Lcom/c/a/n;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lcom/c/a/n;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private c(Landroid/app/Activity;)Z
    .locals 3

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x8000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/c/a/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->D:Z

    return v0
.end method

.method static synthetic c(Lcom/c/a/n;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/c/a/n;->y:Z

    return p1
.end method

.method static synthetic d(Lcom/c/a/n;J)J
    .locals 1

    iput-wide p1, p0, Lcom/c/a/n;->q:J

    return-wide p1
.end method

.method static synthetic d(Lcom/c/a/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->C:Z

    return v0
.end method

.method static synthetic e(Lcom/c/a/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->E:Z

    return v0
.end method

.method static synthetic f(Lcom/c/a/n;)Lcom/c/a/c/b;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->B:Lcom/c/a/c/b;

    return-object v0
.end method

.method private f()Z
    .locals 1

    invoke-direct {p0}, Lcom/c/a/n;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 4

    invoke-virtual {p0}, Lcom/c/a/n;->getDuration()J

    move-result-wide v0

    sget-object v2, Lcom/c/a/x;->c:Lcom/c/a/x;

    invoke-virtual {v2}, Lcom/c/a/x;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/c/a/n;)Z
    .locals 1

    invoke-direct {p0}, Lcom/c/a/n;->g()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/c/a/n;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->S:Ljava/lang/Runnable;

    return-object v0
.end method

.method private h()V
    .locals 4

    iget-object v0, p0, Lcom/c/a/n;->S:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/c/a/n;->getDuration()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/c/a/n;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic i(Lcom/c/a/n;)J
    .locals 2

    iget-wide v0, p0, Lcom/c/a/n;->s:J

    return-wide v0
.end method

.method private i()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/c/a/n;->clearAnimation()V

    invoke-virtual {p0}, Lcom/c/a/n;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/c/a/n;->F:Lcom/c/a/c/c;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/c/a/n;->I:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/c/a/n;->F:Lcom/c/a/c/c;

    invoke-interface {v0, p0}, Lcom/c/a/c/c;->f(Lcom/c/a/n;)V

    :cond_1
    iput-boolean v1, p0, Lcom/c/a/n;->I:Z

    iput-boolean v1, p0, Lcom/c/a/n;->x:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/c/a/n;->P:Landroid/app/Activity;

    return-void
.end method

.method static synthetic j(Lcom/c/a/n;)J
    .locals 2

    iget-wide v0, p0, Lcom/c/a/n;->r:J

    return-wide v0
.end method

.method static synthetic k(Lcom/c/a/n;)J
    .locals 2

    iget-wide v0, p0, Lcom/c/a/n;->q:J

    return-wide v0
.end method

.method static synthetic l(Lcom/c/a/n;)Lcom/c/a/c/c;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->F:Lcom/c/a/c/c;

    return-object v0
.end method

.method static synthetic m(Lcom/c/a/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->y:Z

    return v0
.end method

.method static synthetic n(Lcom/c/a/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->v:Z

    return v0
.end method

.method static synthetic o(Lcom/c/a/n;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/c/a/n;)Z
    .locals 1

    invoke-direct {p0}, Lcom/c/a/n;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic q(Lcom/c/a/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/c/a/n;->h()V

    return-void
.end method

.method static synthetic r(Lcom/c/a/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/c/a/n;->i()V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/c/a/n;
    .locals 0

    iput p1, p0, Lcom/c/a/n;->g:I

    return-object p0
.end method

.method public a(Lcom/c/a/c/a;)Lcom/c/a/n;
    .locals 0

    iput-object p1, p0, Lcom/c/a/n;->A:Lcom/c/a/c/a;

    return-object p0
.end method

.method public a(Lcom/c/a/x;)Lcom/c/a/n;
    .locals 0

    iput-object p1, p0, Lcom/c/a/n;->d:Lcom/c/a/x;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/c/a/n;
    .locals 2

    iput-object p1, p0, Lcom/c/a/n;->e:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/c/a/n;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/c/a/n;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/c/a/n;
    .locals 0

    iput-boolean p1, p0, Lcom/c/a/n;->v:Z

    return-object p0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/c/a/n;->x:Z

    invoke-virtual {p0}, Lcom/c/a/n;->b()V

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/c/a/n;->y:Z

    invoke-virtual {p0, p1}, Lcom/c/a/n;->b(Landroid/app/Activity;)V

    return-void
.end method

.method protected a(Landroid/app/Activity;Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 2

    iget-boolean v0, p0, Lcom/c/a/n;->R:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/c/a/n;->m:I

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, p0, Lcom/c/a/n;->p:I

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v0, p0, Lcom/c/a/n;->o:I

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, p0, Lcom/c/a/n;->n:I

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    :goto_0
    iget-object v0, p0, Lcom/c/a/n;->L:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/c/a/n;->a(Landroid/app/Activity;Landroid/graphics/Rect;)V

    iget v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v1, p0, Lcom/c/a/n;->L:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/c/a/n;->L:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    return-void

    :cond_0
    iget v0, p0, Lcom/c/a/n;->m:I

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, p0, Lcom/c/a/n;->p:I

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v0, p0, Lcom/c/a/n;->o:I

    iget v1, p0, Lcom/c/a/n;->i:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, p0, Lcom/c/a/n;->n:I

    iget v1, p0, Lcom/c/a/n;->i:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_0
.end method

.method public b(I)Lcom/c/a/n;
    .locals 0

    iput p1, p0, Lcom/c/a/n;->u:I

    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lcom/c/a/n;
    .locals 0

    iput-object p1, p0, Lcom/c/a/n;->t:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public b(Z)Lcom/c/a/n;
    .locals 0

    iput-boolean p1, p0, Lcom/c/a/n;->w:Z

    return-object p0
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->w:Z

    invoke-direct {p0, v0}, Lcom/c/a/n;->c(Z)V

    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    const v0, 0x1020002

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1}, Lcom/c/a/n;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-direct {p0, p1, p1, v0, v1}, Lcom/c/a/n;->a(Landroid/content/Context;Landroid/app/Activity;Landroid/view/ViewGroup;Z)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/c/a/n;->a(Landroid/app/Activity;Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-direct {p0, p1, v1, v0}, Lcom/c/a/n;->a(Landroid/app/Activity;Landroid/view/ViewGroup$MarginLayoutParams;Landroid/view/ViewGroup;)V

    return-void
.end method

.method protected c()V
    .locals 2

    iget-boolean v0, p0, Lcom/c/a/n;->K:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/c/a/n;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/c/a/n;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Lcom/c/a/n;->P:Landroid/app/Activity;

    invoke-virtual {p0, v1, v0}, Lcom/c/a/n;->a(Landroid/app/Activity;Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {p0, v0}, Lcom/c/a/n;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method c(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/c/a/n;->d(I)V

    return-void
.end method

.method protected d(I)V
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->T:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/c/a/n;->T:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/c/a/n;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->I:Z

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/c/a/n;->K:Z

    return v0
.end method

.method public getActionColor()I
    .locals 1

    iget v0, p0, Lcom/c/a/n;->u:I

    return v0
.end method

.method public getActionLabel()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->t:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    iget v0, p0, Lcom/c/a/n;->g:I

    return v0
.end method

.method public getDuration()J
    .locals 4

    iget-wide v0, p0, Lcom/c/a/n;->z:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/c/a/n;->d:Lcom/c/a/x;

    invoke-virtual {v0}, Lcom/c/a/x;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/c/a/n;->z:J

    goto :goto_0
.end method

.method public getLineColor()I
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getOffset()I
    .locals 1

    iget v0, p0, Lcom/c/a/n;->i:I

    return v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    iget v0, p0, Lcom/c/a/n;->h:I

    return v0
.end method

.method public getType()Lcom/c/a/a/a;
    .locals 1

    iget-object v0, p0, Lcom/c/a/n;->c:Lcom/c/a/a/a;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/c/a/b/a;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/c/a/n;->S:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/c/a/n;->S:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/c/a/n;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/c/a/n;->T:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/c/a/n;->T:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/c/a/n;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method
