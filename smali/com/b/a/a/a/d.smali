.class Lcom/b/a/a/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/b/a/a/a/b;


# direct methods
.method constructor <init>(Lcom/b/a/a/a/b;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    iput p2, p0, Lcom/b/a/a/a/d;->a:I

    iput-object p3, p0, Lcom/b/a/a/a/d;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/b/a/a/a/d;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const-string v0, "LicenseChecker"

    const-string v1, "Received response."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    iget-object v0, v0, Lcom/b/a/a/a/b;->a:Lcom/b/a/a/a/a;

    invoke-static {v0}, Lcom/b/a/a/a/a;->a(Lcom/b/a/a/a/a;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    invoke-static {v1}, Lcom/b/a/a/a/b;->a(Lcom/b/a/a/a/b;)Lcom/b/a/a/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    invoke-static {v0}, Lcom/b/a/a/a/b;->b(Lcom/b/a/a/a/b;)V

    iget-object v0, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    invoke-static {v0}, Lcom/b/a/a/a/b;->a(Lcom/b/a/a/a/b;)Lcom/b/a/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    iget-object v1, v1, Lcom/b/a/a/a/b;->a:Lcom/b/a/a/a/a;

    invoke-static {v1}, Lcom/b/a/a/a/a;->b(Lcom/b/a/a/a/a;)Ljava/security/PublicKey;

    move-result-object v1

    iget v2, p0, Lcom/b/a/a/a/d;->a:I

    iget-object v3, p0, Lcom/b/a/a/a/d;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/b/a/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/b/a/a/a/i;->a(Ljava/security/PublicKey;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    iget-object v0, v0, Lcom/b/a/a/a/b;->a:Lcom/b/a/a/a/a;

    iget-object v1, p0, Lcom/b/a/a/a/d;->d:Lcom/b/a/a/a/b;

    invoke-static {v1}, Lcom/b/a/a/a/b;->a(Lcom/b/a/a/a/b;)Lcom/b/a/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/b/a/a/a/a;->b(Lcom/b/a/a/a/a;Lcom/b/a/a/a/i;)V

    :cond_0
    return-void
.end method
