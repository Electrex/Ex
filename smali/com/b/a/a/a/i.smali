.class Lcom/b/a/a/a/i;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/b/a/a/a/t;

.field private final b:Lcom/b/a/a/a/e;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/b/a/a/a/q;


# direct methods
.method constructor <init>(Lcom/b/a/a/a/t;Lcom/b/a/a/a/q;Lcom/b/a/a/a/e;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/b/a/a/a/i;->a:Lcom/b/a/a/a/t;

    iput-object p2, p0, Lcom/b/a/a/a/i;->f:Lcom/b/a/a/a/q;

    iput-object p3, p0, Lcom/b/a/a/a/i;->b:Lcom/b/a/a/a/e;

    iput p4, p0, Lcom/b/a/a/a/i;->c:I

    iput-object p5, p0, Lcom/b/a/a/a/i;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/b/a/a/a/i;->e:Ljava/lang/String;

    return-void
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lcom/b/a/a/a/i;->b:Lcom/b/a/a/a/e;

    invoke-interface {v0, p1}, Lcom/b/a/a/a/e;->c(I)V

    return-void
.end method

.method private a(ILcom/b/a/a/a/m;)V
    .locals 3

    const/16 v0, 0x17

    iget-object v1, p0, Lcom/b/a/a/a/i;->a:Lcom/b/a/a/a/t;

    invoke-interface {v1, p1, p2}, Lcom/b/a/a/a/t;->a(ILcom/b/a/a/a/m;)V

    const/16 v1, 0x16

    iget-object v2, p0, Lcom/b/a/a/a/i;->a:Lcom/b/a/a/a/t;

    invoke-interface {v2}, Lcom/b/a/a/a/t;->c()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/b/a/a/a/i;->b:Lcom/b/a/a/a/e;

    invoke-interface {v0, p1}, Lcom/b/a/a/a/e;->a(I)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/b/a/a/a/i;->b:Lcom/b/a/a/a/e;

    invoke-interface {v0, p1}, Lcom/b/a/a/a/e;->b(I)V

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(ILcom/b/a/a/a/m;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/16 v2, 0x123

    if-eqz p1, :cond_0

    if-ne p1, v3, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/i;->f:Lcom/b/a/a/a/q;

    invoke-interface {v0, p3}, Lcom/b/a/a/a/q;->a(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/b/a/a/a/i;->a(ILcom/b/a/a/a/m;)V

    :goto_0
    return-void

    :cond_1
    if-ne p1, v1, :cond_2

    invoke-direct {p0, p2}, Lcom/b/a/a/a/i;->a(Lcom/b/a/a/a/m;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x101

    if-ne p1, v0, :cond_3

    const-string v0, "LicenseValidator"

    const-string v1, "Error contacting licensing server."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2, p2}, Lcom/b/a/a/a/i;->a(ILcom/b/a/a/a/m;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    const-string v0, "LicenseValidator"

    const-string v1, "An error has occurred on the licensing server."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2, p2}, Lcom/b/a/a/a/i;->a(ILcom/b/a/a/a/m;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x5

    if-ne p1, v0, :cond_5

    const-string v0, "LicenseValidator"

    const-string v1, "Licensing server is refusing to talk to this device, over quota."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2, p2}, Lcom/b/a/a/a/i;->a(ILcom/b/a/a/a/m;)V

    goto :goto_0

    :cond_5
    const/16 v0, 0x102

    if-ne p1, v0, :cond_6

    invoke-direct {p0, v1}, Lcom/b/a/a/a/i;->a(I)V

    goto :goto_0

    :cond_6
    const/16 v0, 0x103

    if-ne p1, v0, :cond_7

    invoke-direct {p0, v3}, Lcom/b/a/a/a/i;->a(I)V

    goto :goto_0

    :cond_7
    if-ne p1, v4, :cond_8

    invoke-direct {p0, v4}, Lcom/b/a/a/a/i;->a(I)V

    goto :goto_0

    :cond_8
    const-string v0, "LicenseValidator"

    const-string v1, "Unknown response code for license check."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0
.end method

.method private a(Lcom/b/a/a/a/m;)V
    .locals 1

    const/16 v0, 0x231

    invoke-direct {p0, v0, p1}, Lcom/b/a/a/a/i;->a(ILcom/b/a/a/a/m;)V

    return-void
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/b/a/a/a/i;->b:Lcom/b/a/a/a/e;

    const/16 v1, 0x231

    invoke-interface {v0, v1}, Lcom/b/a/a/a/e;->b(I)V

    return-void
.end method


# virtual methods
.method public a()Lcom/b/a/a/a/e;
    .locals 1

    iget-object v0, p0, Lcom/b/a/a/a/i;->b:Lcom/b/a/a/a/e;

    return-object v0
.end method

.method public a(Ljava/security/PublicKey;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    const/4 v1, 0x2

    if-ne p2, v1, :cond_6

    :cond_0
    :try_start_0
    const-string v0, "SHA1withRSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    invoke-static {p4}, Lcom/b/a/a/a/a/b;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "LicenseValidator"

    const-string v1, "Signature verification failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/b/a/a/a/a/a; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/b/a/a/a/i;->a(I)V

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    const-string v0, "LicenseValidator"

    const-string v1, "Could not Base64-xdecode signature."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {p3}, Lcom/b/a/a/a/m;->a(Ljava/lang/String;)Lcom/b/a/a/a/m;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v0

    iget v1, v0, Lcom/b/a/a/a/m;->a:I

    if-eq v1, p2, :cond_2

    const-string v0, "LicenseValidator"

    const-string v1, "Response codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0

    :catch_4
    move-exception v0

    const-string v0, "LicenseValidator"

    const-string v1, "Could not xparse response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0

    :cond_2
    iget v1, v0, Lcom/b/a/a/a/m;->b:I

    iget v2, p0, Lcom/b/a/a/a/i;->c:I

    if-eq v1, v2, :cond_3

    const-string v0, "LicenseValidator"

    const-string v1, "Nonce doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lcom/b/a/a/a/m;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/b/a/a/a/i;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v0, "LicenseValidator"

    const-string v1, "Package name doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0

    :cond_4
    iget-object v1, v0, Lcom/b/a/a/a/m;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/b/a/a/a/i;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v0, "LicenseValidator"

    const-string v1, "Version codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto :goto_0

    :cond_5
    iget-object v1, v0, Lcom/b/a/a/a/m;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, "LicenseValidator"

    const-string v1, "User identifier is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/b/a/a/a/i;->d()V

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    :cond_7
    invoke-direct {p0, p2, v0, v1}, Lcom/b/a/a/a/i;->a(ILcom/b/a/a/a/m;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/b/a/a/a/i;->c:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/b/a/a/a/i;->d:Ljava/lang/String;

    return-object v0
.end method
