.class public final Lcom/e/a/a;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field public static b:Ljava/util/List;

.field public static c:Z

.field public static d:I

.field private static e:Lcom/e/a/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/e/a/a;->e:Lcom/e/a/c/b;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/e/a/a;->a:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/e/a/a;->b:Ljava/util/List;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/e/a/a;->c:Z

    const/16 v0, 0x4e20

    sput v0, Lcom/e/a/a;->d:I

    return-void
.end method

.method public static a(Lcom/e/a/c/b;)V
    .locals 0

    sput-object p0, Lcom/e/a/a;->e:Lcom/e/a/c/b;

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-static {v1, p0, v0, v1}, Lcom/e/a/a;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    return-void
.end method

.method public static a(Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0, p1, p2}, Lcom/e/a/a;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/e/a/a;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 1

    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/e/a/a;->a:Z

    if-eqz v0, :cond_1

    if-nez p0, :cond_0

    const-string p0, "RootTools v3.5"

    :cond_0
    packed-switch p2, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    invoke-static {p0, p1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_2
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a()Z
    .locals 1

    invoke-static {}, Lcom/e/a/a;->b()Lcom/e/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/e/a/c/b;->b()Z

    move-result v0

    return v0
.end method

.method private static final b()Lcom/e/a/c/b;
    .locals 1

    sget-object v0, Lcom/e/a/a;->e:Lcom/e/a/c/b;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/e/a/c/b;->a()V

    sget-object v0, Lcom/e/a/a;->e:Lcom/e/a/c/b;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/e/a/a;->e:Lcom/e/a/c/b;

    goto :goto_0
.end method
