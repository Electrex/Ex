.class public abstract Lcom/e/a/b/a;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/e/a/b/d;

.field b:Landroid/os/Handler;

.field c:Z

.field d:[Ljava/lang/String;

.field e:Z

.field f:Landroid/content/Context;

.field g:Z

.field h:Z

.field i:Z

.field j:I

.field k:I

.field l:I


# direct methods
.method public varargs constructor <init>(IZ[Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/e/a/b/a;->a:Lcom/e/a/b/d;

    iput-object v2, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/e/a/b/a;->c:Z

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/e/a/b/a;->d:[Ljava/lang/String;

    iput-boolean v1, p0, Lcom/e/a/b/a;->e:Z

    iput-object v2, p0, Lcom/e/a/b/a;->f:Landroid/content/Context;

    iput-boolean v1, p0, Lcom/e/a/b/a;->g:Z

    iput-boolean v1, p0, Lcom/e/a/b/a;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/e/a/b/a;->i:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/e/a/b/a;->j:I

    iput v1, p0, Lcom/e/a/b/a;->k:I

    sget v0, Lcom/e/a/a;->d:I

    iput v0, p0, Lcom/e/a/b/a;->l:I

    iput-object p3, p0, Lcom/e/a/b/a;->d:[Ljava/lang/String;

    iput p1, p0, Lcom/e/a/b/a;->k:I

    invoke-direct {p0, p2}, Lcom/e/a/b/a;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/e/a/b/a;->i:Z

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "CommandHandler created"

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/e/a/b/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/e/a/b/c;-><init>(Lcom/e/a/b/a;Lcom/e/a/b/b;)V

    iput-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    :goto_0
    return-void

    :cond_0
    const-string v0, "CommandHandler not created"

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/e/a/b/a;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/e/a/b/a;->g:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method protected a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/e/a/b/a;->j:I

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract a(II)V
.end method

.method public abstract a(ILjava/lang/String;)V
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/e/a/b/f;->e()V

    const-string v0, "Terminating all shells."

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/e/a/b/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected b()V
    .locals 4

    iget-boolean v0, p0, Lcom/e/a/b/a;->h:Z

    if-nez v0, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/e/a/b/a;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "action"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Command "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/e/a/b/a;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " finished."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/e/a/b/a;->a()V

    monitor-exit p0

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/e/a/b/a;->k:I

    iget v1, p0, Lcom/e/a/b/a;->j:I

    invoke-virtual {p0, v0, v1}, Lcom/e/a/b/a;->a(II)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract b(ILjava/lang/String;)V
.end method

.method protected b(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/e/a/b/a;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "action"

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "text"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Command "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/e/a/b/a;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " did not finish because it was terminated. Termination reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/e/a/b/a;->a(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/e/a/b/a;->h:Z

    invoke-virtual {p0}, Lcom/e/a/b/a;->a()V

    monitor-exit p0

    return-void

    :cond_0
    iget v0, p0, Lcom/e/a/b/a;->k:I

    invoke-virtual {p0, v0, p1}, Lcom/e/a/b/a;->b(ILjava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Ljava/lang/String;
    .locals 6

    const/16 v5, 0xa

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v2, p0, Lcom/e/a/b/a;->e:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/e/a/b/a;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v3, p0, Lcom/e/a/b/a;->d:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dalvikvm -cp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/anbuild.dex"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " com.android.internal.util.WithFramework"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " com.stericson.RootTools.containers.RootClass "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/e/a/b/a;->d:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/e/a/b/a;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/e/a/b/a;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c(ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/e/a/b/a;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "action"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "text"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/e/a/b/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/e/a/b/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/e/a/b/a;->c:Z

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/e/a/b/a;->g:Z

    return v0
.end method

.method protected f()V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/e/a/b/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/e/a/b/d;-><init>(Lcom/e/a/b/a;Lcom/e/a/b/b;)V

    iput-object v0, p0, Lcom/e/a/b/a;->a:Lcom/e/a/b/d;

    iget-object v0, p0, Lcom/e/a/b/a;->a:Lcom/e/a/b/d;

    invoke-virtual {v0, v2}, Lcom/e/a/b/d;->setPriority(I)V

    iget-object v0, p0, Lcom/e/a/b/a;->a:Lcom/e/a/b/d;

    invoke-virtual {v0}, Lcom/e/a/b/d;->start()V

    iput-boolean v2, p0, Lcom/e/a/b/a;->c:Z

    return-void
.end method
