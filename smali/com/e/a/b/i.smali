.class Lcom/e/a/b/i;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/e/a/b/f;


# direct methods
.method constructor <init>(Lcom/e/a/b/f;)V
    .locals 0

    iput-object p1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v1, 0x0

    move-object v0, v2

    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->b(Lcom/e/a/b/f;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/e/a/b/f;->c:Z

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->k(Lcom/e/a/b/f;)Ljava/io/BufferedReader;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/e/a/b/f;->c:Z

    if-nez v4, :cond_3

    :cond_1
    :goto_1
    const-string v1, "Read all output"

    invoke-static {v1}, Lcom/e/a/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v1}, Lcom/e/a/b/f;->o(Lcom/e/a/b/f;)Ljava/lang/Process;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Process;->waitFor()I

    iget-object v1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v1}, Lcom/e/a/b/f;->o(Lcom/e/a/b/f;)Ljava/lang/Process;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->g(Lcom/e/a/b/f;)Ljava/io/OutputStreamWriter;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/e/a/b/f;->a(Lcom/e/a/b/f;Ljava/io/Writer;)V

    iget-object v1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->k(Lcom/e/a/b/f;)Ljava/io/BufferedReader;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/e/a/b/f;->a(Lcom/e/a/b/f;Ljava/io/Reader;)V

    const-string v1, "Shell destroyed"

    invoke-static {v1}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    :goto_3
    iget-object v1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v1}, Lcom/e/a/b/f;->e(Lcom/e/a/b/f;)I

    move-result v1

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->a(Lcom/e/a/b/f;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_8

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v0}, Lcom/e/a/b/f;->a(Lcom/e/a/b/f;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v1}, Lcom/e/a/b/f;->e(Lcom/e/a/b/f;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/e/a/b/a;

    :cond_2
    const-string v1, "Unexpected Termination."

    invoke-virtual {v0, v1}, Lcom/e/a/b/a;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v0}, Lcom/e/a/b/f;->m(Lcom/e/a/b/f;)I

    move-object v0, v2

    goto :goto_3

    :cond_3
    if-nez v0, :cond_9

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->e(Lcom/e/a/b/f;)I

    move-result v3

    iget-object v6, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v6}, Lcom/e/a/b/f;->a(Lcom/e/a/b/f;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v3, v6, :cond_4

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->b(Lcom/e/a/b/f;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v0}, Lcom/e/a/b/f;->a(Lcom/e/a/b/f;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v3}, Lcom/e/a/b/f;->e(Lcom/e/a/b/f;)I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/e/a/b/a;

    move-object v3, v0

    :goto_4
    const-string v0, "F*D^W@#FGF"

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_5

    iget v6, v3, Lcom/e/a/b/a;->k:I

    invoke-virtual {v3, v6, v4}, Lcom/e/a/b/a;->c(ILjava/lang/String;)V

    :cond_5
    if-lez v0, :cond_6

    iget v6, v3, Lcom/e/a/b/a;->k:I

    const/4 v7, 0x0

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/e/a/b/a;->c(ILjava/lang/String;)V

    :cond_6
    if-ltz v0, :cond_7

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    if-lt v0, v8, :cond_7

    const/4 v0, 0x1

    aget-object v0, v4, v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :try_start_3
    aget-object v0, v4, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result v0

    :goto_5
    const/4 v6, 0x2

    :try_start_4
    aget-object v4, v4, v6

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v4

    :goto_6
    :try_start_5
    iget-object v6, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v6}, Lcom/e/a/b/f;->l(Lcom/e/a/b/f;)I

    move-result v6

    if-ne v0, v6, :cond_7

    invoke-virtual {v3, v4}, Lcom/e/a/b/a;->a(I)V

    invoke-virtual {v3}, Lcom/e/a/b/a;->b()V

    iget-object v0, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v0}, Lcom/e/a/b/f;->m(Lcom/e/a/b/f;)I

    iget-object v0, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    invoke-static {v0}, Lcom/e/a/b/f;->n(Lcom/e/a/b/f;)I

    move-object v0, v2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_5

    :catch_1
    move-exception v4

    move v4, v5

    goto :goto_6

    :cond_7
    move-object v0, v3

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/e/a/b/i;->a:Lcom/e/a/b/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/e/a/b/f;->b(Lcom/e/a/b/f;I)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :goto_7
    return-void

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8, v0}, Lcom/e/a/a;->a(Ljava/lang/String;ILjava/lang/Exception;)V

    goto :goto_7

    :catch_3
    move-exception v1

    goto/16 :goto_2

    :cond_9
    move-object v3, v0

    goto :goto_4
.end method
