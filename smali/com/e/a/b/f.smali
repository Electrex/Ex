.class public Lcom/e/a/b/f;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/e/a/b/j;

.field private static d:Lcom/e/a/b/f;

.field private static e:Lcom/e/a/b/f;

.field private static f:Lcom/e/a/b/f;


# instance fields
.field public b:Z

.field public c:Z

.field private g:I

.field private h:Lcom/e/a/b/k;

.field private i:Lcom/e/a/b/j;

.field private j:Ljava/lang/String;

.field private final k:Ljava/lang/Process;

.field private final l:Ljava/io/BufferedReader;

.field private final m:Ljava/io/OutputStreamWriter;

.field private final n:Ljava/util/List;

.field private o:Z

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Z

.field private v:Ljava/lang/Runnable;

.field private w:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    sput-object v0, Lcom/e/a/b/f;->e:Lcom/e/a/b/f;

    sput-object v0, Lcom/e/a/b/f;->f:Lcom/e/a/b/f;

    sget-object v0, Lcom/e/a/b/j;->a:Lcom/e/a/b/j;

    sput-object v0, Lcom/e/a/b/f;->a:Lcom/e/a/b/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/e/a/b/k;Lcom/e/a/b/j;I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x61a8

    iput v0, p0, Lcom/e/a/b/f;->g:I

    iput-object v5, p0, Lcom/e/a/b/f;->h:Lcom/e/a/b/k;

    sget-object v0, Lcom/e/a/b/j;->a:Lcom/e/a/b/j;

    iput-object v0, p0, Lcom/e/a/b/f;->i:Lcom/e/a/b/j;

    const-string v0, ""

    iput-object v0, p0, Lcom/e/a/b/f;->j:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    iput-boolean v2, p0, Lcom/e/a/b/f;->o:Z

    iput-boolean v2, p0, Lcom/e/a/b/f;->b:Z

    iput-boolean v2, p0, Lcom/e/a/b/f;->c:Z

    const/16 v0, 0x1388

    iput v0, p0, Lcom/e/a/b/f;->p:I

    iput v2, p0, Lcom/e/a/b/f;->q:I

    iput v2, p0, Lcom/e/a/b/f;->r:I

    iput v2, p0, Lcom/e/a/b/f;->s:I

    iput v2, p0, Lcom/e/a/b/f;->t:I

    iput-boolean v2, p0, Lcom/e/a/b/f;->u:Z

    new-instance v0, Lcom/e/a/b/g;

    invoke-direct {v0, p0}, Lcom/e/a/b/g;-><init>(Lcom/e/a/b/f;)V

    iput-object v0, p0, Lcom/e/a/b/f;->v:Ljava/lang/Runnable;

    new-instance v0, Lcom/e/a/b/i;

    invoke-direct {v0, p0}, Lcom/e/a/b/i;-><init>(Lcom/e/a/b/f;)V

    iput-object v0, p0, Lcom/e/a/b/f;->w:Ljava/lang/Runnable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting shell: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/e/a/b/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Timeout: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/e/a/b/f;->h:Lcom/e/a/b/k;

    if-lez p4, :cond_0

    :goto_0
    iput p4, p0, Lcom/e/a/b/f;->g:I

    iput-object p3, p0, Lcom/e/a/b/f;->i:Lcom/e/a/b/j;

    iget-object v0, p0, Lcom/e/a/b/f;->i:Lcom/e/a/b/j;

    sget-object v1, Lcom/e/a/b/j;->a:Lcom/e/a/b/j;

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/ProcessBuilder;

    new-array v1, v4, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/ProcessBuilder;->redirectErrorStream(Z)Ljava/lang/ProcessBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v0

    iput-object v0, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    :goto_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v2, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lcom/e/a/b/f;->l:Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/OutputStreamWriter;

    iget-object v1, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    invoke-virtual {v1}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/e/a/b/f;->m:Ljava/io/OutputStreamWriter;

    new-instance v0, Lcom/e/a/b/l;

    invoke-direct {v0, p0, v5}, Lcom/e/a/b/l;-><init>(Lcom/e/a/b/f;Lcom/e/a/b/g;)V

    invoke-virtual {v0}, Lcom/e/a/b/l;->start()V

    :try_start_0
    iget v1, p0, Lcom/e/a/b/f;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/e/a/b/l;->join(J)V

    iget v1, v0, Lcom/e/a/b/l;->a:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, -0x38f

    if-ne v1, v2, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/e/a/b/f;->l:Ljava/io/BufferedReader;

    invoke-direct {p0, v1}, Lcom/e/a/b/f;->a(Ljava/io/Reader;)V

    iget-object v1, p0, Lcom/e/a/b/f;->m:Ljava/io/OutputStreamWriter;

    invoke-direct {p0, v1}, Lcom/e/a/b/f;->a(Ljava/io/Writer;)V

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    iget-object v2, p0, Lcom/e/a/b/f;->j:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    invoke-virtual {v0}, Lcom/e/a/b/l;->interrupt()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    :cond_0
    iget p4, p0, Lcom/e/a/b/f;->g:I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/ProcessBuilder;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--context "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/e/a/b/f;->i:Lcom/e/a/b/j;

    invoke-virtual {v3}, Lcom/e/a/b/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/ProcessBuilder;->redirectErrorStream(Z)Ljava/lang/ProcessBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v0

    iput-object v0, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    goto/16 :goto_1

    :cond_2
    :try_start_3
    iget v1, v0, Lcom/e/a/b/l;->a:I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v2, -0x2a

    if-ne v1, v2, :cond_3

    :try_start_4
    iget-object v1, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    :goto_3
    :try_start_5
    iget-object v1, p0, Lcom/e/a/b/f;->l:Ljava/io/BufferedReader;

    invoke-direct {p0, v1}, Lcom/e/a/b/f;->a(Ljava/io/Reader;)V

    iget-object v1, p0, Lcom/e/a/b/f;->m:Ljava/io/OutputStreamWriter;

    invoke-direct {p0, v1}, Lcom/e/a/b/f;->a(Ljava/io/Writer;)V

    new-instance v1, Lcom/e/a/a/a;

    const-string v2, "Root Access Denied"

    invoke-direct {v1, v2}, Lcom/e/a/a/a;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/e/a/b/f;->v:Ljava/lang/Runnable;

    const-string v3, "Shell Input"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/e/a/b/f;->w:Ljava/lang/Runnable;

    const-string v3, "Shell Output"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    return-void

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/e/a/b/f;I)I
    .locals 0

    iput p1, p0, Lcom/e/a/b/f;->r:I

    return p1
.end method

.method public static a(II)Lcom/e/a/b/f;
    .locals 1

    sget-object v0, Lcom/e/a/b/f;->a:Lcom/e/a/b/j;

    invoke-static {p0, v0, p1}, Lcom/e/a/b/f;->a(ILcom/e/a/b/j;I)Lcom/e/a/b/f;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILcom/e/a/b/j;I)Lcom/e/a/b/f;
    .locals 4

    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    if-nez v0, :cond_1

    const-string v0, "Starting Root Shell!"

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    const-string v3, "su"

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    if-nez v1, :cond_2

    :try_start_0
    new-instance v1, Lcom/e/a/b/f;

    sget-object v2, Lcom/e/a/b/k;->b:Lcom/e/a/b/k;

    invoke-direct {v1, v3, v2, p1, p0}, Lcom/e/a/b/f;-><init>(Ljava/lang/String;Lcom/e/a/b/k;Lcom/e/a/b/j;I)V

    sput-object v1, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v2, v1

    add-int/lit8 v1, v0, 0x1

    if-lt v0, p2, :cond_0

    const-string v0, "IOException, could not start shell"

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    throw v2

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    iget-object v0, v0, Lcom/e/a/b/f;->i:Lcom/e/a/b/j;

    if-eq v0, p1, :cond_3

    :try_start_1
    const-string v0, "Context is different than open shell, switching context..."

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    invoke-virtual {v0, p1}, Lcom/e/a/b/f;->a(Lcom/e/a/b/j;)Lcom/e/a/b/f;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    return-object v0

    :catch_1
    move-exception v0

    const-string v1, "Context could not be switched for existing root shell..."

    invoke-static {v1}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    throw v0

    :cond_3
    const-string v0, "Using Existing Root Shell!"

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/e/a/b/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/e/a/b/f;->j:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/e/a/b/f;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/e/a/b/f;Ljava/io/Reader;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/e/a/b/f;->a(Ljava/io/Reader;)V

    return-void
.end method

.method static synthetic a(Lcom/e/a/b/f;Ljava/io/Writer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/e/a/b/f;->a(Ljava/io/Writer;)V

    return-void
.end method

.method private a(Ljava/io/Reader;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/Reader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/io/Writer;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/Writer;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/e/a/b/f;I)I
    .locals 0

    iput p1, p0, Lcom/e/a/b/f;->q:I

    return p1
.end method

.method public static b()V
    .locals 1

    sget-object v0, Lcom/e/a/b/f;->f:Lcom/e/a/b/f;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/e/a/b/f;->f:Lcom/e/a/b/f;

    invoke-virtual {v0}, Lcom/e/a/b/f;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/e/a/b/f;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/e/a/b/f;->o:Z

    return v0
.end method

.method static synthetic c(Lcom/e/a/b/f;)I
    .locals 1

    iget v0, p0, Lcom/e/a/b/f;->r:I

    return v0
.end method

.method public static c()V
    .locals 1

    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    invoke-virtual {v0}, Lcom/e/a/b/f;->a()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/e/a/b/f;)I
    .locals 1

    iget v0, p0, Lcom/e/a/b/f;->p:I

    return v0
.end method

.method public static d()V
    .locals 1

    sget-object v0, Lcom/e/a/b/f;->e:Lcom/e/a/b/f;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/e/a/b/f;->e:Lcom/e/a/b/f;

    invoke-virtual {v0}, Lcom/e/a/b/f;->a()V

    goto :goto_0
.end method

.method public static d(Lcom/e/a/b/a;)V
    .locals 1

    invoke-static {}, Lcom/e/a/b/f;->g()Lcom/e/a/b/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/e/a/b/f;->a(Lcom/e/a/b/a;)Lcom/e/a/b/a;

    return-void
.end method

.method static synthetic e(Lcom/e/a/b/f;)I
    .locals 1

    iget v0, p0, Lcom/e/a/b/f;->q:I

    return v0
.end method

.method public static e()V
    .locals 0

    invoke-static {}, Lcom/e/a/b/f;->d()V

    invoke-static {}, Lcom/e/a/b/f;->c()V

    invoke-static {}, Lcom/e/a/b/f;->b()V

    return-void
.end method

.method static synthetic f(Lcom/e/a/b/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/e/a/b/f;->h()V

    return-void
.end method

.method public static g()Lcom/e/a/b/f;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/e/a/b/f;->a(II)Lcom/e/a/b/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/e/a/b/f;)Ljava/io/OutputStreamWriter;
    .locals 1

    iget-object v0, p0, Lcom/e/a/b/f;->m:Ljava/io/OutputStreamWriter;

    return-object v0
.end method

.method static synthetic h(Lcom/e/a/b/f;)I
    .locals 1

    iget v0, p0, Lcom/e/a/b/f;->s:I

    return v0
.end method

.method private h()V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/e/a/b/f;->u:Z

    iget v0, p0, Lcom/e/a/b/f;->p:I

    iget v2, p0, Lcom/e/a/b/f;->p:I

    div-int/lit8 v2, v2, 0x4

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cleaning up: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/e/a/b/f;->q:I

    iget-object v0, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/e/a/b/f;->r:I

    iput-boolean v1, p0, Lcom/e/a/b/f;->u:Z

    return-void
.end method

.method static synthetic i(Lcom/e/a/b/f;)I
    .locals 2

    iget v0, p0, Lcom/e/a/b/f;->r:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/e/a/b/f;->r:I

    return v0
.end method

.method static synthetic j(Lcom/e/a/b/f;)I
    .locals 2

    iget v0, p0, Lcom/e/a/b/f;->s:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/e/a/b/f;->s:I

    return v0
.end method

.method static synthetic k(Lcom/e/a/b/f;)Ljava/io/BufferedReader;
    .locals 1

    iget-object v0, p0, Lcom/e/a/b/f;->l:Ljava/io/BufferedReader;

    return-object v0
.end method

.method static synthetic l(Lcom/e/a/b/f;)I
    .locals 1

    iget v0, p0, Lcom/e/a/b/f;->t:I

    return v0
.end method

.method static synthetic m(Lcom/e/a/b/f;)I
    .locals 2

    iget v0, p0, Lcom/e/a/b/f;->q:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/e/a/b/f;->q:I

    return v0
.end method

.method static synthetic n(Lcom/e/a/b/f;)I
    .locals 2

    iget v0, p0, Lcom/e/a/b/f;->t:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/e/a/b/f;->t:I

    return v0
.end method

.method static synthetic o(Lcom/e/a/b/f;)Ljava/lang/Process;
    .locals 1

    iget-object v0, p0, Lcom/e/a/b/f;->k:Ljava/lang/Process;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/e/a/b/a;)Lcom/e/a/b/a;
    .locals 2

    iget-boolean v0, p0, Lcom/e/a/b/f;->o:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to add commands to a closed shell"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/e/a/b/f;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/e/a/b/f;->f()V

    return-object p1
.end method

.method public a(Lcom/e/a/b/j;)Lcom/e/a/b/f;
    .locals 2

    iget-object v0, p0, Lcom/e/a/b/f;->h:Lcom/e/a/b/k;

    sget-object v1, Lcom/e/a/b/k;->b:Lcom/e/a/b/k;

    if-ne v0, v1, :cond_0

    :try_start_0
    invoke-static {}, Lcom/e/a/b/f;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v0, p0, Lcom/e/a/b/f;->g:I

    const/4 v1, 0x3

    invoke-static {v0, p1, v1}, Lcom/e/a/b/f;->a(ILcom/e/a/b/j;I)Lcom/e/a/b/f;

    move-result-object p0

    :goto_1
    return-object p0

    :catch_0
    move-exception v0

    const-string v0, "Problem closing shell while trying to switch context..."

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "Can only switch context on a root shell!"

    invoke-static {v0}, Lcom/e/a/a;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    if-ne p0, v0, :cond_1

    sput-object v1, Lcom/e/a/b/f;->d:Lcom/e/a/b/f;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/e/a/b/f;->o:Z

    invoke-virtual {p0}, Lcom/e/a/b/f;->f()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    sget-object v0, Lcom/e/a/b/f;->e:Lcom/e/a/b/f;

    if-ne p0, v0, :cond_2

    sput-object v1, Lcom/e/a/b/f;->e:Lcom/e/a/b/f;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/e/a/b/f;->f:Lcom/e/a/b/f;

    if-ne p0, v0, :cond_0

    sput-object v1, Lcom/e/a/b/f;->f:Lcom/e/a/b/f;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lcom/e/a/b/a;)I
    .locals 1

    iget-object v0, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public c(Lcom/e/a/b/a;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Command is in position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/e/a/b/f;->b(Lcom/e/a/b/a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " currently executing command at position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/e/a/b/f;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and the number of commands is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/e/a/b/f;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f()V
    .locals 1

    new-instance v0, Lcom/e/a/b/h;

    invoke-direct {v0, p0}, Lcom/e/a/b/h;-><init>(Lcom/e/a/b/f;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
