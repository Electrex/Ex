.class Lcom/a/a/a/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/a/a/a/a/c;


# direct methods
.method constructor <init>(Lcom/a/a/a/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-static {p2}, Lcom/android/vending/a/b;->a(Landroid/os/IBinder;)Lcom/android/vending/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/a/c;->a(Lcom/a/a/a/a/c;Lcom/android/vending/a/a;)Lcom/android/vending/a/a;

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-static {v0}, Lcom/a/a/a/a/c;->a(Lcom/a/a/a/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-virtual {v0}, Lcom/a/a/a/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-virtual {v0}, Lcom/a/a/a/a/c;->f()V

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-static {v0}, Lcom/a/a/a/a/c;->b(Lcom/a/a/a/a/c;)Lcom/a/a/a/a/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-static {v0}, Lcom/a/a/a/a/c;->b(Lcom/a/a/a/a/c;)Lcom/a/a/a/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a/e;->a()V

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-static {v0}, Lcom/a/a/a/a/c;->b(Lcom/a/a/a/a/c;)Lcom/a/a/a/a/e;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    invoke-static {v0}, Lcom/a/a/a/a/c;->b(Lcom/a/a/a/a/c;)Lcom/a/a/a/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a/e;->b()V

    :cond_1
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Lcom/a/a/a/a/d;->a:Lcom/a/a/a/a/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/a/a/a/c;->a(Lcom/a/a/a/a/c;Lcom/android/vending/a/a;)Lcom/android/vending/a/a;

    return-void
.end method
