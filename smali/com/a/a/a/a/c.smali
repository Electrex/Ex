.class public Lcom/a/a/a/a/c;
.super Lcom/a/a/a/a/a;


# instance fields
.field private a:Lcom/android/vending/a/a;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/a/a/a/a/b;

.field private e:Lcom/a/a/a/a/b;

.field private f:Lcom/a/a/a/a/e;

.field private g:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/a/e;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/a/a/a/a/a;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/a/a/a/a/d;

    invoke-direct {v0, p0}, Lcom/a/a/a/a/d;-><init>(Lcom/a/a/a/a/c;)V

    iput-object v0, p0, Lcom/a/a/a/a/c;->g:Landroid/content/ServiceConnection;

    iput-object p2, p0, Lcom/a/a/a/a/c;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/c;->b:Ljava/lang/String;

    new-instance v0, Lcom/a/a/a/a/b;

    const-string v1, ".products.cache.v2_6"

    invoke-direct {v0, p1, v1}, Lcom/a/a/a/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    new-instance v0, Lcom/a/a/a/a/b;

    const-string v1, ".subscriptions.cache.v2_6"

    invoke-direct {v0, p1, v1}, Lcom/a/a/a/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/c;->e:Lcom/a/a/a/a/b;

    invoke-direct {p0}, Lcom/a/a/a/a/c;->g()V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/a/c;Lcom/android/vending/a/a;)Lcom/android/vending/a/a;
    .locals 0

    iput-object p1, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    return-object p1
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v7

    :goto_0
    return v0

    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/a/a/a/a/c;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/a/a/a/a/c;->b:Ljava/lang/String;

    move-object v3, p2

    move-object v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/android/vending/a/a;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "BUY_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const v2, 0x1f76a0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/16 v1, 0x67

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    goto :goto_0

    :cond_4
    const/4 v0, 0x7

    if-ne v1, v0, :cond_7

    :try_start_1
    invoke-virtual {p0, p2}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0, p2}, Lcom/a/a/a/a/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->e()Z

    :cond_5
    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_2

    invoke-virtual {p0, p2}, Lcom/a/a/a/a/c;->e(Ljava/lang/String;)Lcom/a/a/a/a/i;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-virtual {p0, p2}, Lcom/a/a/a/a/c;->f(Ljava/lang/String;)Lcom/a/a/a/a/i;

    move-result-object v0

    :cond_6
    iget-object v1, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    invoke-interface {v1, p2, v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;Lcom/a/a/a/a/i;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/16 v1, 0x65

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static synthetic a(Lcom/a/a/a/a/c;)Z
    .locals 1

    invoke-direct {p0}, Lcom/a/a/a/a/c;->h()Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Lcom/a/a/a/a/b;)Z
    .locals 9

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    const/4 v1, 0x3

    iget-object v4, p0, Lcom/a/a/a/a/c;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v0, v1, v4, p1, v5}, Lcom/android/vending/a/a;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Lcom/a/a/a/a/b;->d()V

    const-string v1, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v1, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    move v4, v2

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_2
    const-string v8, "productId"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7, v0, v1}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move-object v1, v3

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/16 v3, 0x64

    invoke-interface {v1, v3, v0}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V

    :cond_3
    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/a/c;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/a/c;->c:Ljava/lang/String;

    invoke-static {p1, v0, p2, p3}, Lcom/a/a/a/a/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/a/a/a/a/c;)Lcom/a/a/a/a/e;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    return-object v0
.end method

.method private b(Ljava/lang/String;Lcom/a/a/a/a/b;)Lcom/a/a/a/a/i;
    .locals 3

    invoke-virtual {p2, p1}, Lcom/a/a/a/a/b;->b(Ljava/lang/String;)Lcom/a/a/a/a/f;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/a/a/a/a/f;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    new-instance v0, Lcom/a/a/a/a/i;

    invoke-direct {v0, v1}, Lcom/a/a/a/a/i;-><init>(Lcom/a/a/a/a/f;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "iabv3"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load saved purchase details for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/a/h;
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    if-eqz v0, :cond_3

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "ITEM_ID_LIST"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/a/a/a/a/c;->b:Ljava/lang/String;

    invoke-interface {v0, v3, v4, p2, v2}, Lcom/android/vending/a/a;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "DETAILS_LIST"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "productId"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/a/a/a/a/h;

    invoke-direct {v0, v3}, Lcom/a/a/a/a/h;-><init>(Lorg/json/JSONObject;)V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V

    :cond_2
    const-string v0, "iabv3"

    const-string v3, "Failed to retrieve info for %s: error %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "iabv3"

    const-string v3, "Failed to call getSkuDetails %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private g()V
    .locals 4

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/a/a/a/a/c;->g:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".purchase.last.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method private h()Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".products.restored.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private i()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".purchase.last.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/a/c;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a()Landroid/content/Context;
    .locals 1

    invoke-super {p0}, Lcom/a/a/a/a/a;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)Z
    .locals 9

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    const v2, 0x1f76a0

    if-eq p1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-nez p3, :cond_1

    const-string v1, "iabv3"

    const-string v2, "handleActivityResult: data is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {p3, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "iabv3"

    const-string v4, "resultCode = %d, responseCode = %d"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/a/a/a/a/c;->i()Ljava/lang/String;

    move-result-object v3

    const/4 v0, -0x1

    if-ne p2, v0, :cond_7

    if-nez v2, :cond_7

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v5, "productId"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "developerPayload"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    const-string v6, "subs"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v5, v2, v4}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v6, :cond_4

    iget-object v0, p0, Lcom/a/a/a/a/c;->e:Lcom/a/a/a/a/b;

    :goto_1
    invoke-virtual {v0, v5, v2, v4}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    new-instance v3, Lcom/a/a/a/a/i;

    new-instance v6, Lcom/a/a/a/a/f;

    invoke-direct {v6, v2, v4}, Lcom/a/a/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v6}, Lcom/a/a/a/a/i;-><init>(Lcom/a/a/a/a/f;)V

    invoke-interface {v0, v5, v3}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;Lcom/a/a/a/a/i;)V

    :cond_3
    :goto_2
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    goto :goto_1

    :cond_5
    const-string v0, "iabv3"

    const-string v2, "Public key signature doesn\'t match!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/16 v2, 0x66

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v2, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/16 v2, 0x6e

    invoke-interface {v0, v2, v8}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V

    goto :goto_2

    :cond_6
    :try_start_1
    const-string v2, "iabv3"

    const-string v4, "Payload mismatch: %s != %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/16 v2, 0x66

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    invoke-interface {v0, v2, v8}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V

    goto :goto_2
.end method

.method public a(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 1

    const-string v0, "inapp"

    invoke-direct {p0, p1, p2, v0}, Lcom/a/a/a/a/c;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    invoke-virtual {v0, p1}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/c;->e:Lcom/a/a/a/a/b;

    invoke-virtual {v0, p1}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/a/a/a/a/c;->g:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/a/a/a/a/c;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/a/c;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    invoke-virtual {v0}, Lcom/a/a/a/a/b;->c()V

    invoke-super {p0}, Lcom/a/a/a/a/a;->c()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->d()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    invoke-direct {p0, p1, v2}, Lcom/a/a/a/a/c;->b(Ljava/lang/String;Lcom/a/a/a/a/b;)Lcom/a/a/a/a/i;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/a/a/a/a/i;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/a/a/a/a/c;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/a/a/a/a/i;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v2}, Lcom/android/vending/a/a;->b(ILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    invoke-virtual {v2, p1}, Lcom/a/a/a/a/b;->c(Ljava/lang/String;)V

    const-string v2, "iabv3"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Successfully consumed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " purchase."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/a/a/c;->f:Lcom/a/a/a/a/e;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/a/a/a/a/e;->a(ILjava/lang/Throwable;)V

    :cond_3
    const-string v1, "iabv3"

    const-string v3, "Failed to consume %s: error %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "iabv3"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Lcom/a/a/a/a/h;
    .locals 1

    const-string v0, "inapp"

    invoke-direct {p0, p1, v0}, Lcom/a/a/a/a/c;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/a/h;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/c;->a:Lcom/android/vending/a/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/a/a/a/a/i;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    invoke-direct {p0, p1, v0}, Lcom/a/a/a/a/c;->b(Ljava/lang/String;Lcom/a/a/a/a/b;)Lcom/a/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 2

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "inapp"

    iget-object v1, p0, Lcom/a/a/a/a/c;->d:Lcom/a/a/a/a/b;

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;Lcom/a/a/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "subs"

    iget-object v1, p0, Lcom/a/a/a/a/c;->e:Lcom/a/a/a/a/b;

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;Lcom/a/a/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)Lcom/a/a/a/a/i;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/c;->e:Lcom/a/a/a/a/b;

    invoke-direct {p0, p1, v0}, Lcom/a/a/a/a/c;->b(Ljava/lang/String;Lcom/a/a/a/a/b;)Lcom/a/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/a/a/a/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".products.restored.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/a/c;->a(Ljava/lang/String;Ljava/lang/Boolean;)Z

    return-void
.end method
