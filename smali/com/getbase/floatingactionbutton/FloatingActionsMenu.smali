.class public Lcom/getbase/floatingactionbutton/FloatingActionsMenu;
.super Landroid/view/ViewGroup;


# static fields
.field private static v:Landroid/view/animation/Interpolator;

.field private static w:Landroid/view/animation/Interpolator;

.field private static x:Landroid/view/animation/Interpolator;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:Landroid/animation/AnimatorSet;

.field private l:Landroid/animation/AnimatorSet;

.field private m:Lcom/getbase/floatingactionbutton/a;

.field private n:Lcom/getbase/floatingactionbutton/i;

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Lcom/getbase/floatingactionbutton/p;

.field private u:Lcom/getbase/floatingactionbutton/h;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v0}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    sput-object v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->v:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->w:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->x:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const-wide/16 v2, 0x12c

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->k:Landroid/animation/AnimatorSet;

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->l:Landroid/animation/AnimatorSet;

    invoke-direct {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const-wide/16 v2, 0x12c

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->k:Landroid/animation/AnimatorSet;

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->l:Landroid/animation/AnimatorSet;

    invoke-direct {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a:I

    return v0
.end method

.method static synthetic a(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;Lcom/getbase/floatingactionbutton/i;)Lcom/getbase/floatingactionbutton/i;
    .locals 0

    iput-object p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->n:Lcom/getbase/floatingactionbutton/i;

    return-object p1
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lcom/getbase/floatingactionbutton/e;

    invoke-direct {v0, p0, p1}, Lcom/getbase/floatingactionbutton/e;-><init>(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    sget v1, Lcom/getbase/floatingactionbutton/n;->fab_expand_menu_button:I

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/a;->setId(I)V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->d:I

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/a;->setSize(I)V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    new-instance v1, Lcom/getbase/floatingactionbutton/f;

    invoke-direct {v1, p0}, Lcom/getbase/floatingactionbutton/f;-><init>(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)V

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/a;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-super {p0}, Landroid/view/ViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/getbase/floatingactionbutton/l;->fab_actions_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/getbase/floatingactionbutton/l;->fab_shadow_radius:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/getbase/floatingactionbutton/l;->fab_shadow_offset:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/getbase/floatingactionbutton/l;->fab_labels_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->h:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/getbase/floatingactionbutton/l;->fab_shadow_offset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->i:I

    new-instance v0, Lcom/getbase/floatingactionbutton/p;

    invoke-direct {v0, p0}, Lcom/getbase/floatingactionbutton/p;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    sget-object v0, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu:[I

    invoke-virtual {p1, p2, v0, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_addButtonPlusIconColor:I

    const v2, 0x106000b

    invoke-direct {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_addButtonColorNormal:I

    const v2, 0x1060013

    invoke-direct {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->b:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_addButtonColorPressed:I

    const v2, 0x1060012

    invoke-direct {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->c:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_addButtonSize:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->d:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_addButtonStrokeVisible:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->e:Z

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_expandDirection:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_labelStyle:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->q:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu_fab_labelsPosition:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->r:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->q:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action labels in horizontal expand orientation is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a(Landroid/content/Context;)V

    return-void
.end method

.method private b(I)I
    .locals 1

    mul-int/lit8 v0, p1, 0xc

    div-int/lit8 v0, v0, 0xa

    return v0
.end method

.method static synthetic b(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->b:I

    return v0
.end method

.method static synthetic c(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->c:I

    return v0
.end method

.method static synthetic d()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->v:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic d(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->e:Z

    return v0
.end method

.method static synthetic e(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->k:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic e()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->x:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic f(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->l:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic f()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->w:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic g(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;)I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    return v0
.end method

.method private g()Z
    .locals 2

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 6

    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->q:I

    invoke-direct {v2, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->s:I

    if-ge v1, v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/getbase/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getTitle()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    if-eq v0, v4, :cond_0

    if-eqz v3, :cond_0

    sget v3, Lcom/getbase/floatingactionbutton/n;->fab_label:I

    invoke-virtual {v0, v3}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->q:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v3}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->addView(Landroid/view/View;)V

    sget v4, Lcom/getbase/floatingactionbutton/n;->fab_label:I

    invoke-virtual {v0, v4, v3}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/p;->a(Z)V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->k:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->u:Lcom/getbase/floatingactionbutton/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->u:Lcom/getbase/floatingactionbutton/h;

    invoke-interface {v0}, Lcom/getbase/floatingactionbutton/h;->b()V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->c()V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/p;->a(Z)V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->k:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->u:Lcom/getbase/floatingactionbutton/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->u:Lcom/getbase/floatingactionbutton/h;

    invoke-interface {v0}, Lcom/getbase/floatingactionbutton/h;->a()V

    :cond_0
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/getbase/floatingactionbutton/g;

    invoke-super {p0}, Landroid/view/ViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/getbase/floatingactionbutton/g;-><init>(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/getbase/floatingactionbutton/g;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/getbase/floatingactionbutton/g;-><init>(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/getbase/floatingactionbutton/g;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/getbase/floatingactionbutton/g;-><init>(Lcom/getbase/floatingactionbutton/FloatingActionsMenu;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->bringChildToFront(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->s:I

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->q:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->h()V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17

    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    return-void

    :pswitch_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    if-nez v1, :cond_3

    const/4 v1, 0x1

    move v11, v1

    :goto_0
    if-eqz p1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    invoke-virtual {v1}, Lcom/getbase/floatingactionbutton/p;->a()V

    :cond_1
    if-eqz v11, :cond_4

    sub-int v1, p5, p3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v2}, Lcom/getbase/floatingactionbutton/a;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    move v3, v1

    :goto_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->r:I

    if-nez v1, :cond_5

    sub-int v1, p4, p2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    move v4, v1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v1}, Lcom/getbase/floatingactionbutton/a;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v4, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v5}, Lcom/getbase/floatingactionbutton/a;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v6}, Lcom/getbase/floatingactionbutton/a;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v2, v1, v3, v5, v6}, Lcom/getbase/floatingactionbutton/a;->layout(IIII)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    div-int/lit8 v1, v1, 0x2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->h:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->r:I

    if-nez v2, :cond_6

    sub-int v9, v4, v1

    :goto_3
    if-eqz v11, :cond_7

    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    sub-int v1, v3, v1

    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->s:I

    add-int/lit8 v2, v2, -0x1

    move v10, v2

    :goto_5
    if-ltz v10, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    if-eq v12, v2, :cond_2

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-ne v2, v5, :cond_8

    :cond_2
    :goto_6
    add-int/lit8 v2, v10, -0x1

    move v10, v2

    goto :goto_5

    :cond_3
    const/4 v1, 0x0

    move v11, v1

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    move v3, v1

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    div-int/lit8 v1, v1, 0x2

    move v4, v1

    goto :goto_2

    :cond_6
    add-int v9, v4, v1

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v1}, Lcom/getbase/floatingactionbutton/a;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    add-int/2addr v1, v2

    goto :goto_4

    :cond_8
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v13, v4, v2

    if-eqz v11, :cond_b

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    move v5, v1

    :goto_7
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v13

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v5

    invoke-virtual {v12, v13, v5, v1, v2}, Landroid/view/View;->layout(IIII)V

    sub-int v1, v3, v5

    int-to-float v7, v1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v1, :cond_c

    move v1, v6

    :goto_8
    invoke-virtual {v12, v1}, Landroid/view/View;->setTranslationY(F)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v1, :cond_d

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_9
    invoke-virtual {v12, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/getbase/floatingactionbutton/g;

    invoke-static {v1}, Lcom/getbase/floatingactionbutton/g;->a(Lcom/getbase/floatingactionbutton/g;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v14, 0x0

    aput v6, v8, v14

    const/4 v14, 0x1

    aput v7, v8, v14

    invoke-virtual {v2, v8}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    invoke-static {v1}, Lcom/getbase/floatingactionbutton/g;->b(Lcom/getbase/floatingactionbutton/g;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v14, 0x0

    aput v7, v8, v14

    const/4 v14, 0x1

    aput v6, v8, v14

    invoke-virtual {v2, v8}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    invoke-virtual {v1, v12}, Lcom/getbase/floatingactionbutton/g;->a(Landroid/view/View;)V

    sget v1, Lcom/getbase/floatingactionbutton/n;->fab_label:I

    invoke-virtual {v12, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->r:I

    if-nez v2, :cond_e

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v9, v2

    :goto_a
    move-object/from16 v0, p0

    iget v8, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->r:I

    if-nez v8, :cond_f

    move v8, v2

    :goto_b
    move-object/from16 v0, p0

    iget v14, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->r:I

    if-nez v14, :cond_9

    move v2, v9

    :cond_9
    move-object/from16 v0, p0

    iget v14, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->i:I

    sub-int v14, v5, v14

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    sub-int v15, v15, v16

    div-int/lit8 v15, v15, 0x2

    add-int/2addr v14, v15

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v14

    invoke-virtual {v1, v8, v14, v2, v15}, Landroid/view/View;->layout(IIII)V

    new-instance v14, Landroid/graphics/Rect;

    invoke-static {v13, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    div-int/lit8 v15, v15, 0x2

    sub-int v15, v5, v15

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v16

    add-int v13, v13, v16

    invoke-static {v13, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    add-int v13, v13, v16

    invoke-direct {v14, v8, v15, v2, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    new-instance v8, Landroid/view/TouchDelegate;

    invoke-direct {v8, v14, v12}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v2, v8}, Lcom/getbase/floatingactionbutton/p;->a(Landroid/view/TouchDelegate;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v2, :cond_10

    move v2, v6

    :goto_c
    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v2, :cond_11

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_d
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/getbase/floatingactionbutton/g;

    invoke-static {v2}, Lcom/getbase/floatingactionbutton/g;->a(Lcom/getbase/floatingactionbutton/g;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const/4 v13, 0x2

    new-array v13, v13, [F

    const/4 v14, 0x0

    aput v6, v13, v14

    const/4 v14, 0x1

    aput v7, v13, v14

    invoke-virtual {v8, v13}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    invoke-static {v2}, Lcom/getbase/floatingactionbutton/g;->b(Lcom/getbase/floatingactionbutton/g;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const/4 v13, 0x2

    new-array v13, v13, [F

    const/4 v14, 0x0

    aput v7, v13, v14

    const/4 v7, 0x1

    aput v6, v13, v7

    invoke-virtual {v8, v13}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    invoke-virtual {v2, v1}, Lcom/getbase/floatingactionbutton/g;->a(Landroid/view/View;)V

    :cond_a
    if-eqz v11, :cond_12

    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    sub-int v1, v5, v1

    goto/16 :goto_6

    :cond_b
    move v5, v1

    goto/16 :goto_7

    :cond_c
    move v1, v7

    goto/16 :goto_8

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_9

    :cond_e
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v9

    goto/16 :goto_a

    :cond_f
    move v8, v9

    goto/16 :goto_b

    :cond_10
    move v2, v7

    goto :goto_c

    :cond_11
    const/4 v2, 0x0

    goto :goto_d

    :cond_12
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    add-int/2addr v1, v2

    goto/16 :goto_6

    :pswitch_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_14

    const/4 v1, 0x1

    move v7, v1

    :goto_e
    if-eqz v7, :cond_15

    sub-int v1, p4, p2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v2}, Lcom/getbase/floatingactionbutton/a;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    move v2, v1

    :goto_f
    sub-int v1, p5, p3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->p:I

    sub-int/2addr v1, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->p:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v4}, Lcom/getbase/floatingactionbutton/a;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int v8, v1, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v3}, Lcom/getbase/floatingactionbutton/a;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v4}, Lcom/getbase/floatingactionbutton/a;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v8

    invoke-virtual {v1, v2, v8, v3, v4}, Lcom/getbase/floatingactionbutton/a;->layout(IIII)V

    if-eqz v7, :cond_16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    sub-int v1, v2, v1

    :goto_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->s:I

    add-int/lit8 v3, v3, -0x1

    move v6, v3

    :goto_11
    if-ltz v6, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    if-eq v9, v3, :cond_13

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_17

    :cond_13
    :goto_12
    add-int/lit8 v3, v6, -0x1

    move v6, v3

    goto :goto_11

    :cond_14
    const/4 v1, 0x0

    move v7, v1

    goto :goto_e

    :cond_15
    const/4 v1, 0x0

    move v2, v1

    goto :goto_f

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v1}, Lcom/getbase/floatingactionbutton/a;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    add-int/2addr v1, v3

    goto :goto_10

    :cond_17
    if-eqz v7, :cond_18

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v1, v3

    move v3, v1

    :goto_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->m:Lcom/getbase/floatingactionbutton/a;

    invoke-virtual {v1}, Lcom/getbase/floatingactionbutton/a;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v8

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v9, v3, v1, v4, v5}, Landroid/view/View;->layout(IIII)V

    sub-int v1, v2, v3

    int-to-float v5, v1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v1, :cond_19

    move v1, v4

    :goto_14
    invoke-virtual {v9, v1}, Landroid/view/View;->setTranslationX(F)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v1, :cond_1a

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_15
    invoke-virtual {v9, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/getbase/floatingactionbutton/g;

    invoke-static {v1}, Lcom/getbase/floatingactionbutton/g;->a(Lcom/getbase/floatingactionbutton/g;)Landroid/animation/ObjectAnimator;

    move-result-object v10

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v4, v11, v12

    const/4 v12, 0x1

    aput v5, v11, v12

    invoke-virtual {v10, v11}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    invoke-static {v1}, Lcom/getbase/floatingactionbutton/g;->b(Lcom/getbase/floatingactionbutton/g;)Landroid/animation/ObjectAnimator;

    move-result-object v10

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v5, v11, v12

    const/4 v5, 0x1

    aput v4, v11, v5

    invoke-virtual {v10, v11}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    invoke-virtual {v1, v9}, Lcom/getbase/floatingactionbutton/g;->a(Landroid/view/View;)V

    if-eqz v7, :cond_1b

    move-object/from16 v0, p0

    iget v1, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    sub-int v1, v3, v1

    goto/16 :goto_12

    :cond_18
    move v3, v1

    goto :goto_13

    :cond_19
    move v1, v5

    goto :goto_14

    :cond_1a
    const/4 v1, 0x0

    goto :goto_15

    :cond_1b
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    add-int/2addr v1, v3

    goto/16 :goto_12

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->measureChildren(II)V

    iput v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    iput v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->p:I

    move v1, v2

    move v3, v2

    move v0, v2

    move v4, v2

    :goto_0
    iget v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->s:I

    if-ge v1, v5, :cond_2

    invoke-virtual {p0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v7, 0x8

    if-ne v5, v7, :cond_1

    move v5, v4

    move v4, v0

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    move v4, v5

    goto :goto_0

    :cond_1
    iget v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    packed-switch v5, :pswitch_data_0

    :goto_2
    move v5, v4

    move v4, v0

    :goto_3
    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g()Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/getbase/floatingactionbutton/n;->fab_label:I

    invoke-virtual {v6, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_1

    :pswitch_0
    iget v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v0, v5

    move v5, v4

    move v4, v0

    goto :goto_3

    :pswitch_1
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->p:I

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->p:I

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g()Z

    move-result v1

    if-nez v1, :cond_4

    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->o:I

    if-lez v3, :cond_3

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->h:I

    add-int/2addr v2, v3

    :cond_3
    add-int v4, v1, v2

    :goto_4
    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->f:I

    packed-switch v1, :pswitch_data_1

    :goto_5
    invoke-virtual {p0, v4, v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->setMeasuredDimension(II)V

    return-void

    :cond_4
    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->p:I

    goto :goto_4

    :pswitch_2
    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->b(I)I

    move-result v0

    goto :goto_5

    :pswitch_3
    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->g:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v1, v4

    invoke-direct {p0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->b(I)I

    move-result v4

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;

    iget-boolean v0, p1, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->t:Lcom/getbase/floatingactionbutton/p;

    iget-boolean v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    invoke-virtual {v0, v1}, Lcom/getbase/floatingactionbutton/p;->a(Z)V

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->n:Lcom/getbase/floatingactionbutton/i;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->n:Lcom/getbase/floatingactionbutton/i;

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x43070000    # 135.0f

    :goto_0
    invoke-virtual {v1, v0}, Lcom/getbase/floatingactionbutton/i;->setRotation(F)V

    :cond_0
    invoke-virtual {p1}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;

    invoke-direct {v1, v0}, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->j:Z

    iput-boolean v0, v1, Lcom/getbase/floatingactionbutton/FloatingActionsMenu$SavedState;->a:Z

    return-object v1
.end method

.method public setOnFloatingActionsMenuUpdateListener(Lcom/getbase/floatingactionbutton/h;)V
    .locals 0

    iput-object p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionsMenu;->u:Lcom/getbase/floatingactionbutton/h;

    return-void
.end method
