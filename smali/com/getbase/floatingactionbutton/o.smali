.class public final Lcom/getbase/floatingactionbutton/o;
.super Ljava/lang/Object;


# static fields
.field public static final AddFloatingActionButton:[I

.field public static final AddFloatingActionButton_fab_plusIconColor:I = 0x0

.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_fab_colorDisabled:I = 0x1

.field public static final FloatingActionButton_fab_colorNormal:I = 0x2

.field public static final FloatingActionButton_fab_colorPressed:I = 0x0

.field public static final FloatingActionButton_fab_icon:I = 0x3

.field public static final FloatingActionButton_fab_size:I = 0x4

.field public static final FloatingActionButton_fab_stroke_visible:I = 0x6

.field public static final FloatingActionButton_fab_title:I = 0x5

.field public static final FloatingActionsMenu:[I

.field public static final FloatingActionsMenu_fab_addButtonColorNormal:I = 0x1

.field public static final FloatingActionsMenu_fab_addButtonColorPressed:I = 0x0

.field public static final FloatingActionsMenu_fab_addButtonPlusIconColor:I = 0x3

.field public static final FloatingActionsMenu_fab_addButtonSize:I = 0x2

.field public static final FloatingActionsMenu_fab_addButtonStrokeVisible:I = 0x4

.field public static final FloatingActionsMenu_fab_expandDirection:I = 0x7

.field public static final FloatingActionsMenu_fab_labelStyle:I = 0x5

.field public static final FloatingActionsMenu_fab_labelsPosition:I = 0x6


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010020

    aput v2, v0, v1

    sput-object v0, Lcom/getbase/floatingactionbutton/o;->AddFloatingActionButton:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/getbase/floatingactionbutton/o;->FloatingActionsMenu:[I

    return-void

    :array_0
    .array-data 4
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
    .end array-data

    :array_1
    .array-data 4
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
    .end array-data
.end method
