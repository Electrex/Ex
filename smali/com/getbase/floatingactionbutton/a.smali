.class public Lcom/getbase/floatingactionbutton/a;
.super Lcom/getbase/floatingactionbutton/FloatingActionButton;


# instance fields
.field a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/getbase/floatingactionbutton/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v1, 0x0

    sget-object v0, Lcom/getbase/floatingactionbutton/o;->AddFloatingActionButton:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/getbase/floatingactionbutton/o;->AddFloatingActionButton_fab_plusIconColor:I

    const v2, 0x106000b

    invoke-virtual {p0, v2}, Lcom/getbase/floatingactionbutton/a;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/a;->a:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-super {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method getIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 6

    const/high16 v2, 0x40000000    # 2.0f

    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_icon_size:I

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/a;->b(I)F

    move-result v5

    div-float v3, v5, v2

    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_plus_icon_size:I

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/a;->b(I)F

    move-result v0

    sget v1, Lcom/getbase/floatingactionbutton/l;->fab_plus_icon_stroke:I

    invoke-virtual {p0, v1}, Lcom/getbase/floatingactionbutton/a;->b(I)F

    move-result v1

    div-float v4, v1, v2

    sub-float v0, v5, v0

    div-float v2, v0, v2

    new-instance v0, Lcom/getbase/floatingactionbutton/b;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/getbase/floatingactionbutton/b;-><init>(Lcom/getbase/floatingactionbutton/a;FFFF)V

    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget v2, p0, Lcom/getbase/floatingactionbutton/a;->a:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-object v1
.end method

.method public getPlusColor()I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/a;->a:I

    return v0
.end method

.method public setIcon(I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use FloatingActionButton if you want to use custom icon"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setPlusColor(I)V
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/a;->a:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/getbase/floatingactionbutton/a;->a:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/a;->a()V

    :cond_0
    return-void
.end method

.method public setPlusColorResId(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/getbase/floatingactionbutton/a;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/a;->setPlusColor(I)V

    return-void
.end method
