.class public Lcom/getbase/floatingactionbutton/FloatingActionButton;
.super Landroid/widget/ImageButton;


# instance fields
.field private a:I

.field b:I

.field c:I

.field d:I

.field e:Ljava/lang/String;

.field f:Z

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:F

.field private j:F

.field private k:F

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, p1, p2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(IF)Landroid/graphics/drawable/Drawable;
    .locals 6

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    invoke-direct {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->f(I)I

    move-result v0

    new-instance v3, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v4, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v4}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v3}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-direct {p0, v0, p2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c(IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aput-object v0, v4, v1

    const/16 v0, 0xff

    if-eq v2, v0, :cond_0

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->f:Z

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, p2, v2

    float-to-int v2, v2

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    return-object v0

    :cond_1
    new-instance v0, Lcom/getbase/floatingactionbutton/d;

    invoke-direct {v0, v2, v4}, Lcom/getbase/floatingactionbutton/d;-><init>(I[Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(F)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    new-array v1, v4, [I

    const v2, -0x101009e

    aput v2, v1, v3

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->d:I

    invoke-direct {p0, v2, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(IF)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c:I

    invoke-direct {p0, v2, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(IF)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v3, [I

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b:I

    invoke-direct {p0, v2, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(IF)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method private b(IF)I
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x3

    new-array v0, v0, [F

    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    aget v1, v0, v3

    mul-float/2addr v1, p2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    invoke-static {v1, v0}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v0

    return v0
.end method

.method private b(F)Landroid/graphics/drawable/Drawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    const v2, 0x3ca3d70a    # 0.02f

    invoke-direct {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    return-object v0
.end method

.method private b()V
    .locals 3

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->i:F

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->j:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->l:I

    return-void
.end method

.method private c(F)I
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method private c(I)I
    .locals 1

    const v0, 0x3f666666    # 0.9f

    invoke-direct {p0, p1, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(IF)I

    move-result v0

    return v0
.end method

.method private c(IF)Landroid/graphics/drawable/Drawable;
    .locals 8

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->f:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v7, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v7, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-direct {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c(I)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->e(I)I

    move-result v5

    invoke-direct {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->d(I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->e(I)I

    move-result v3

    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Lcom/getbase/floatingactionbutton/c;

    move-object v1, p0

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/getbase/floatingactionbutton/c;-><init>(Lcom/getbase/floatingactionbutton/FloatingActionButton;IIIII)V

    invoke-virtual {v7, v0}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    move-object v0, v7

    goto :goto_0
.end method

.method private c()V
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->h:I

    if-nez v0, :cond_0

    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_size_normal:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(I)F

    move-result v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->i:F

    return-void

    :cond_0
    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_size_mini:I

    goto :goto_0
.end method

.method private d(I)I
    .locals 1

    const v0, 0x3f8ccccd    # 1.1f

    invoke-direct {p0, p1, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(IF)I

    move-result v0

    return v0
.end method

.method private e(I)I
    .locals 4

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private f(I)I
    .locals 3

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method private setBackgroundCompat(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method a()V
    .locals 14

    const/4 v13, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x1

    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_stroke_width:I

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(I)F

    move-result v3

    const/high16 v0, 0x40000000    # 2.0f

    div-float v6, v3, v0

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x4

    new-array v4, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->h:I

    if-nez v2, :cond_0

    sget v2, Lcom/getbase/floatingactionbutton/m;->fab_bg_normal:I

    :goto_0
    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-direct {p0, v3}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-direct {p0, v3}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(F)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v4, v13

    invoke-direct {v0, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->i:F

    sget v3, Lcom/getbase/floatingactionbutton/l;->fab_icon_size:I

    invoke-virtual {p0, v3}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(I)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    div-int/lit8 v12, v2, 0x2

    iget v2, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->j:F

    float-to-int v2, v2

    iget v3, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->j:F

    iget v4, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->k:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->j:F

    iget v5, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->k:F

    add-float/2addr v4, v5

    float-to-int v5, v4

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    int-to-float v1, v2

    sub-float/2addr v1, v6

    float-to-int v8, v1

    int-to-float v1, v3

    sub-float/2addr v1, v6

    float-to-int v9, v1

    int-to-float v1, v2

    sub-float/2addr v1, v6

    float-to-int v10, v1

    int-to-float v1, v5

    sub-float/2addr v1, v6

    float-to-int v11, v1

    move-object v6, v0

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    add-int v6, v2, v12

    add-int/2addr v3, v12

    add-int v4, v2, v12

    add-int/2addr v5, v12

    move v1, v13

    move v2, v6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    invoke-direct {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setBackgroundCompat(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    sget v2, Lcom/getbase/floatingactionbutton/m;->fab_bg_mini:I

    goto :goto_0
.end method

.method a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton:[I

    invoke-virtual {p1, p2, v0, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_colorNormal:I

    const v2, 0x1060013

    invoke-virtual {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_colorPressed:I

    const v2, 0x1060012

    invoke-virtual {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_colorDisabled:I

    const/high16 v2, 0x1060000

    invoke-virtual {p0, v2}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->d:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_size:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->h:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_icon:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a:I

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->e:Ljava/lang/String;

    sget v1, Lcom/getbase/floatingactionbutton/o;->FloatingActionButton_fab_stroke_visible:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->f:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c()V

    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_shadow_radius:I

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(I)F

    move-result v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->j:F

    sget v0, Lcom/getbase/floatingactionbutton/l;->fab_shadow_offset:I

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b(I)F

    move-result v0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->k:F

    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b()V

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    return-void
.end method

.method b(I)F
    .locals 1

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0
.end method

.method public getColorDisabled()I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->d:I

    return v0
.end method

.method public getColorNormal()I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b:I

    return v0
.end method

.method public getColorPressed()I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c:I

    return v0
.end method

.method getIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->g:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a:I

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method

.method getLabelView()Landroid/widget/TextView;
    .locals 1

    sget v0, Lcom/getbase/floatingactionbutton/n;->fab_label:I

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->h:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/ImageButton;->onMeasure(II)V

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->l:I

    iget v1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->l:I

    invoke-virtual {p0, v0, v1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setMeasuredDimension(II)V

    return-void
.end method

.method public setColorDisabled(I)V
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->d:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->d:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_0
    return-void
.end method

.method public setColorDisabledResId(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setColorDisabled(I)V

    return-void
.end method

.method public setColorNormal(I)V
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_0
    return-void
.end method

.method public setColorNormalResId(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setColorNormal(I)V

    return-void
.end method

.method public setColorPressed(I)V
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c:I

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_0
    return-void
.end method

.method public setColorPressedResId(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->setColorPressed(I)V

    return-void
.end method

.method public setIcon(I)V
    .locals 1

    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_0
    return-void
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->g:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a:I

    iput-object p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_0
    return-void
.end method

.method public setSize(I)V
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use @FAB_SIZE constants only!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->h:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->h:I

    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->c()V

    invoke-direct {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->b()V

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_1
    return-void
.end method

.method public setStrokeVisible(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->f:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->f:Z

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->a()V

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/getbase/floatingactionbutton/FloatingActionButton;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getLabelView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/getbase/floatingactionbutton/FloatingActionButton;->getLabelView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method
