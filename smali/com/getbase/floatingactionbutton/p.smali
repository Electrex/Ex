.class public Lcom/getbase/floatingactionbutton/p;
.super Landroid/view/TouchDelegate;


# static fields
.field private static final a:Landroid/graphics/Rect;


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private c:Landroid/view/TouchDelegate;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/getbase/floatingactionbutton/p;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    sget-object v0, Lcom/getbase/floatingactionbutton/p;->a:Landroid/graphics/Rect;

    invoke-direct {p0, v0, p1}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/p;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/p;->c:Landroid/view/TouchDelegate;

    return-void
.end method

.method public a(Landroid/view/TouchDelegate;)V
    .locals 1

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/getbase/floatingactionbutton/p;->d:Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/getbase/floatingactionbutton/p;->d:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v4

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/TouchDelegate;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_0
    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/getbase/floatingactionbutton/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/getbase/floatingactionbutton/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/TouchDelegate;

    invoke-virtual {v0, p1}, Landroid/view/TouchDelegate;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-object v0, p0, Lcom/getbase/floatingactionbutton/p;->c:Landroid/view/TouchDelegate;

    move v2, v3

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move-object v0, v4

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/getbase/floatingactionbutton/p;->c:Landroid/view/TouchDelegate;

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/getbase/floatingactionbutton/p;->c:Landroid/view/TouchDelegate;

    iput-object v4, p0, Lcom/getbase/floatingactionbutton/p;->c:Landroid/view/TouchDelegate;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
