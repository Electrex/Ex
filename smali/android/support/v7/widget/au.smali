.class Landroid/support/v7/widget/au;
.super Landroid/support/v7/widget/ax;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/av;

.field final synthetic b:Landroid/support/v4/view/ea;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/support/v7/widget/am;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/am;Landroid/support/v7/widget/av;Landroid/support/v4/view/ea;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/widget/au;->d:Landroid/support/v7/widget/am;

    iput-object p2, p0, Landroid/support/v7/widget/au;->a:Landroid/support/v7/widget/av;

    iput-object p3, p0, Landroid/support/v7/widget/au;->b:Landroid/support/v4/view/ea;

    iput-object p4, p0, Landroid/support/v7/widget/au;->c:Landroid/view/View;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/ax;-><init>(Landroid/support/v7/widget/an;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/au;->d:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/au;->a:Landroid/support/v7/widget/av;

    iget-object v1, v1, Landroid/support/v7/widget/av;->b:Landroid/support/v7/widget/cz;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/am;->b(Landroid/support/v7/widget/cz;Z)V

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/au;->b:Landroid/support/v4/view/ea;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ea;->a(Landroid/support/v4/view/eq;)Landroid/support/v4/view/ea;

    iget-object v0, p0, Landroid/support/v7/widget/au;->c:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/bv;->c(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/widget/au;->c:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/bv;->a(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/widget/au;->c:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/bv;->b(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/widget/au;->d:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/au;->a:Landroid/support/v7/widget/av;

    iget-object v1, v1, Landroid/support/v7/widget/av;->b:Landroid/support/v7/widget/cz;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/am;->a(Landroid/support/v7/widget/cz;Z)V

    iget-object v0, p0, Landroid/support/v7/widget/au;->d:Landroid/support/v7/widget/am;

    invoke-static {v0}, Landroid/support/v7/widget/am;->h(Landroid/support/v7/widget/am;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/au;->a:Landroid/support/v7/widget/av;

    iget-object v1, v1, Landroid/support/v7/widget/av;->b:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/au;->d:Landroid/support/v7/widget/am;

    invoke-static {v0}, Landroid/support/v7/widget/am;->e(Landroid/support/v7/widget/am;)V

    return-void
.end method
