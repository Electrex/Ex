.class Landroid/support/v7/widget/as;
.super Landroid/support/v7/widget/ax;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/cz;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Landroid/support/v4/view/ea;

.field final synthetic e:Landroid/support/v7/widget/am;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/am;Landroid/support/v7/widget/cz;IILandroid/support/v4/view/ea;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/widget/as;->e:Landroid/support/v7/widget/am;

    iput-object p2, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/cz;

    iput p3, p0, Landroid/support/v7/widget/as;->b:I

    iput p4, p0, Landroid/support/v7/widget/as;->c:I

    iput-object p5, p0, Landroid/support/v7/widget/as;->d:Landroid/support/v4/view/ea;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/ax;-><init>(Landroid/support/v7/widget/an;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->h(Landroid/support/v7/widget/cz;)V

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->d:Landroid/support/v4/view/ea;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ea;->a(Landroid/support/v4/view/eq;)Landroid/support/v4/view/ea;

    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->e(Landroid/support/v7/widget/cz;)V

    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Landroid/support/v7/widget/am;

    invoke-static {v0}, Landroid/support/v7/widget/am;->g(Landroid/support/v7/widget/am;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Landroid/support/v7/widget/am;

    invoke-static {v0}, Landroid/support/v7/widget/am;->e(Landroid/support/v7/widget/am;)V

    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    if-eqz v0, :cond_0

    invoke-static {p1, v1}, Landroid/support/v4/view/bv;->a(Landroid/view/View;F)V

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/as;->c:I

    if-eqz v0, :cond_1

    invoke-static {p1, v1}, Landroid/support/v4/view/bv;->b(Landroid/view/View;F)V

    :cond_1
    return-void
.end method
