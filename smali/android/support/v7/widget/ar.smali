.class Landroid/support/v7/widget/ar;
.super Landroid/support/v7/widget/ax;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/cz;

.field final synthetic b:Landroid/support/v4/view/ea;

.field final synthetic c:Landroid/support/v7/widget/am;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/am;Landroid/support/v7/widget/cz;Landroid/support/v4/view/ea;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/widget/ar;->c:Landroid/support/v7/widget/am;

    iput-object p2, p0, Landroid/support/v7/widget/ar;->a:Landroid/support/v7/widget/cz;

    iput-object p3, p0, Landroid/support/v7/widget/ar;->b:Landroid/support/v4/view/ea;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/ax;-><init>(Landroid/support/v7/widget/an;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ar;->c:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/ar;->a:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->i(Landroid/support/v7/widget/cz;)V

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ar;->b:Landroid/support/v4/view/ea;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ea;->a(Landroid/support/v4/view/eq;)Landroid/support/v4/view/ea;

    iget-object v0, p0, Landroid/support/v7/widget/ar;->c:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/ar;->a:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->f(Landroid/support/v7/widget/cz;)V

    iget-object v0, p0, Landroid/support/v7/widget/ar;->c:Landroid/support/v7/widget/am;

    invoke-static {v0}, Landroid/support/v7/widget/am;->f(Landroid/support/v7/widget/am;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ar;->a:Landroid/support/v7/widget/cz;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/ar;->c:Landroid/support/v7/widget/am;

    invoke-static {v0}, Landroid/support/v7/widget/am;->e(Landroid/support/v7/widget/am;)V

    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/bv;->c(Landroid/view/View;F)V

    return-void
.end method
