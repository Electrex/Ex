.class public Landroid/support/v7/widget/w;
.super Landroid/widget/EditText;


# static fields
.field private static final a:[I


# instance fields
.field private b:Landroid/support/v7/internal/widget/be;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100d4

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/w;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Landroid/support/v7/a/b;->editTextStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/support/v7/internal/widget/bc;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v0, Landroid/support/v7/internal/widget/bf;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/w;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/w;->a:[I

    invoke-static {v0, p2, v1, p3, v3}, Landroid/support/v7/internal/widget/bh;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/internal/widget/bh;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/bh;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/bh;->c()Landroid/support/v7/internal/widget/bf;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/support/v7/internal/widget/bh;->f(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/bf;->c(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/w;->setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/bh;->b()V

    :cond_1
    return-void
.end method

.method private a()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/w;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    invoke-static {p0, v0}, Landroid/support/v7/internal/widget/bf;->a(Landroid/view/View;Landroid/support/v7/internal/widget/be;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    invoke-super {p0}, Landroid/widget/EditText;->drawableStateChanged()V

    invoke-direct {p0}, Landroid/support/v7/widget/w;->a()V

    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    iget-object v0, v0, Landroid/support/v7/internal/widget/be;->a:Landroid/content/res/ColorStateList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    iget-object v0, v0, Landroid/support/v7/internal/widget/be;->b:Landroid/graphics/PorterDuff$Mode;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/internal/widget/be;

    invoke-direct {v0}, Landroid/support/v7/internal/widget/be;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    iput-object p1, v0, Landroid/support/v7/internal/widget/be;->a:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/be;->d:Z

    invoke-direct {p0}, Landroid/support/v7/widget/w;->a()V

    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/internal/widget/be;

    invoke-direct {v0}, Landroid/support/v7/internal/widget/be;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    iput-object p1, v0, Landroid/support/v7/internal/widget/be;->b:Landroid/graphics/PorterDuff$Mode;

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/internal/widget/be;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/be;->c:Z

    invoke-direct {p0}, Landroid/support/v7/widget/w;->a()V

    return-void
.end method
