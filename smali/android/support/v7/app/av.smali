.class abstract Landroid/support/v7/app/av;
.super Landroid/graphics/drawable/Drawable;


# static fields
.field private static final b:F


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final c:F

.field private final d:F

.field private final e:F

.field private final f:F

.field private final g:F

.field private final h:Z

.field private final i:Landroid/graphics/Path;

.field private final j:I

.field private k:Z

.field private l:F

.field private m:F

.field private n:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide v0, 0x4046800000000000L    # 45.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Landroid/support/v7/app/av;->b:F

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 9

    const/4 v7, 0x1

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    iput-boolean v6, p0, Landroid/support/v7/app/av;->k:Z

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Landroid/support/v7/a/l;->DrawerArrowToggle:[I

    sget v3, Landroid/support/v7/a/b;->drawerArrowStyle:I

    sget v4, Landroid/support/v7/a/k;->Base_Widget_AppCompat_DrawerArrowToggle:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    sget v2, Landroid/support/v7/a/l;->DrawerArrowToggle_color:I

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_drawableSize:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/av;->j:I

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_barSize:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/app/av;->e:F

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_topBottomBarArrowSize:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/app/av;->d:F

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_thickness:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Landroid/support/v7/app/av;->c:F

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_gapBetweenBars:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/app/av;->g:F

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_spinBars:I

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/av;->h:Z

    sget v1, Landroid/support/v7/a/l;->DrawerArrowToggle_middleBarArrowSize:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Landroid/support/v7/app/av;->f:F

    iget v1, p0, Landroid/support/v7/app/av;->j:I

    int-to-float v1, v1

    iget v2, p0, Landroid/support/v7/app/av;->c:F

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Landroid/support/v7/app/av;->g:F

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x4

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/app/av;->n:F

    iget v1, p0, Landroid/support/v7/app/av;->n:F

    float-to-double v2, v1

    iget v1, p0, Landroid/support/v7/app/av;->c:F

    float-to-double v4, v1

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    iget v1, p0, Landroid/support/v7/app/av;->g:F

    float-to-double v6, v1

    add-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, p0, Landroid/support/v7/app/av;->n:F

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    iget v1, p0, Landroid/support/v7/app/av;->c:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v0, p0, Landroid/support/v7/app/av;->c:F

    div-float/2addr v0, v8

    float-to-double v0, v0

    sget v2, Landroid/support/v7/app/av;->b:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Landroid/support/v7/app/av;->m:F

    return-void
.end method

.method private static a(FFF)F
    .locals 1

    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method


# virtual methods
.method protected a(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/app/av;->k:Z

    return-void
.end method

.method abstract a()Z
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Landroid/support/v7/app/av;->l:F

    invoke-virtual {p0}, Landroid/support/v7/app/av;->invalidateSelf()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 12

    invoke-virtual {p0}, Landroid/support/v7/app/av;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v7/app/av;->a()Z

    move-result v3

    iget v0, p0, Landroid/support/v7/app/av;->e:F

    iget v1, p0, Landroid/support/v7/app/av;->d:F

    iget v4, p0, Landroid/support/v7/app/av;->l:F

    invoke-static {v0, v1, v4}, Landroid/support/v7/app/av;->a(FFF)F

    move-result v4

    iget v0, p0, Landroid/support/v7/app/av;->e:F

    iget v1, p0, Landroid/support/v7/app/av;->f:F

    iget v5, p0, Landroid/support/v7/app/av;->l:F

    invoke-static {v0, v1, v5}, Landroid/support/v7/app/av;->a(FFF)F

    move-result v5

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/app/av;->m:F

    iget v6, p0, Landroid/support/v7/app/av;->l:F

    invoke-static {v0, v1, v6}, Landroid/support/v7/app/av;->a(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v6, v0

    const/4 v0, 0x0

    sget v1, Landroid/support/v7/app/av;->b:F

    iget v7, p0, Landroid/support/v7/app/av;->l:F

    invoke-static {v0, v1, v7}, Landroid/support/v7/app/av;->a(FFF)F

    move-result v7

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-eqz v3, :cond_2

    const/high16 v0, 0x43340000    # 180.0f

    :goto_1
    iget v8, p0, Landroid/support/v7/app/av;->l:F

    invoke-static {v1, v0, v8}, Landroid/support/v7/app/av;->a(FFF)F

    move-result v1

    float-to-double v8, v4

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-float v0, v8

    float-to-double v8, v4

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-float v4, v8

    iget-object v7, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->rewind()V

    iget v7, p0, Landroid/support/v7/app/av;->g:F

    iget v8, p0, Landroid/support/v7/app/av;->c:F

    add-float/2addr v7, v8

    iget v8, p0, Landroid/support/v7/app/av;->m:F

    neg-float v8, v8

    iget v9, p0, Landroid/support/v7/app/av;->l:F

    invoke-static {v7, v8, v9}, Landroid/support/v7/app/av;->a(FFF)F

    move-result v7

    neg-float v8, v5

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget-object v9, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    add-float v10, v8, v6

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v9, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v6, v10

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    invoke-virtual {v9, v5, v6}, Landroid/graphics/Path;->rLineTo(FF)V

    iget-object v5, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    invoke-virtual {v5, v8, v7}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v5, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    invoke-virtual {v5, v0, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    iget-object v5, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    neg-float v6, v7

    invoke-virtual {v5, v8, v6}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v5, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    neg-float v4, v4

    invoke-virtual {v5, v0, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    iget-object v0, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Landroid/support/v7/app/av;->n:F

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-boolean v0, p0, Landroid/support/v7/app/av;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Landroid/support/v7/app/av;->k:Z

    xor-int/2addr v0, v3

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    :goto_2
    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    :cond_0
    :goto_3
    iget-object v0, p0, Landroid/support/v7/app/av;->i:Landroid/graphics/Path;

    iget-object v1, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_1
    const/high16 v0, -0x3ccc0000    # -180.0f

    move v1, v0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    if-eqz v3, :cond_0

    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    goto :goto_3
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Landroid/support/v7/app/av;->j:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Landroid/support/v7/app/av;->j:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isAutoMirrored()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/av;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method
