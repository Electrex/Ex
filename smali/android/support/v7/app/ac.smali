.class public Landroid/support/v7/app/ac;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/support/v7/app/t;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v7/app/ab;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/ac;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v7/app/t;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {p1, p2}, Landroid/support/v7/app/ab;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/support/v7/app/t;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput p2, p0, Landroid/support/v7/app/ac;->b:I

    return-void
.end method


# virtual methods
.method public a()Landroid/support/v7/app/ab;
    .locals 4

    new-instance v0, Landroid/support/v7/app/ab;

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->a:Landroid/content/Context;

    iget v2, p0, Landroid/support/v7/app/ac;->b:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/app/ab;-><init>(Landroid/content/Context;IZ)V

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    invoke-static {v0}, Landroid/support/v7/app/ab;->a(Landroid/support/v7/app/ab;)Landroid/support/v7/app/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/t;->a(Landroid/support/v7/app/r;)V

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-boolean v1, v1, Landroid/support/v7/app/t;->o:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ab;->setCancelable(Z)V

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-boolean v1, v1, Landroid/support/v7/app/t;->o:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ab;->setCanceledOnTouchOutside(Z)V

    :cond_0
    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->p:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ab;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->q:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ab;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->r:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->r:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ab;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    :cond_1
    return-object v0
.end method

.method public a(I)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput p1, v0, Landroid/support/v7/app/t;->c:I

    return-object p0
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/t;->i:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->j:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public a(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->q:Landroid/content/DialogInterface$OnDismissListener;

    return-object p0
.end method

.method public a(Landroid/view/View;)Landroid/support/v7/app/ac;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->w:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput v1, v0, Landroid/support/v7/app/t;->v:I

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-boolean v1, v0, Landroid/support/v7/app/t;->B:Z

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->f:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->i:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->j:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public a(Z)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-boolean p1, v0, Landroid/support/v7/app/t;->o:Z

    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->s:[Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->u:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/ac;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->s:[Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p3, v0, Landroid/support/v7/app/t;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->C:[Z

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/t;->D:Z

    return-object p0
.end method

.method public b()Landroid/support/v7/app/ab;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/app/ac;->a()Landroid/support/v7/app/ab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ab;->show()V

    return-object v0
.end method

.method public b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iget-object v1, v1, Landroid/support/v7/app/t;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/t;->k:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->l:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->h:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->k:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->l:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ac;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p1, v0, Landroid/support/v7/app/t;->m:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/app/ac;->a:Landroid/support/v7/app/t;

    iput-object p2, v0, Landroid/support/v7/app/t;->n:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method
