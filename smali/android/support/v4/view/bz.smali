.class Landroid/support/v4/view/bz;
.super Landroid/support/v4/view/by;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/view/by;-><init>()V

    return-void
.end method


# virtual methods
.method public a(III)I
    .locals 1

    invoke-static {p1, p2, p3}, Landroid/support/v4/view/cj;->a(III)I

    move-result v0

    return v0
.end method

.method a()J
    .locals 2

    invoke-static {}, Landroid/support/v4/view/cj;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/view/View;F)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->a(Landroid/view/View;F)V

    return-void
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 0

    invoke-static {p1, p2, p3}, Landroid/support/v4/view/cj;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/view/bz;->g(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Landroid/support/v4/view/bz;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->a(Landroid/view/View;Z)V

    return-void
.end method

.method public b(Landroid/view/View;F)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->b(Landroid/view/View;F)V

    return-void
.end method

.method public b(Landroid/view/View;Z)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->b(Landroid/view/View;Z)V

    return-void
.end method

.method public c(Landroid/view/View;F)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->c(Landroid/view/View;F)V

    return-void
.end method

.method public d(Landroid/view/View;F)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->d(Landroid/view/View;F)V

    return-void
.end method

.method public e(Landroid/view/View;F)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v4/view/cj;->e(Landroid/view/View;F)V

    return-void
.end method

.method public f(Landroid/view/View;)F
    .locals 1

    invoke-static {p1}, Landroid/support/v4/view/cj;->a(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public g(Landroid/view/View;)I
    .locals 1

    invoke-static {p1}, Landroid/support/v4/view/cj;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public k(Landroid/view/View;)I
    .locals 1

    invoke-static {p1}, Landroid/support/v4/view/cj;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public l(Landroid/view/View;)F
    .locals 1

    invoke-static {p1}, Landroid/support/v4/view/cj;->d(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public m(Landroid/view/View;)F
    .locals 1

    invoke-static {p1}, Landroid/support/v4/view/cj;->e(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public n(Landroid/view/View;)F
    .locals 1

    invoke-static {p1}, Landroid/support/v4/view/cj;->f(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public u(Landroid/view/View;)V
    .locals 0

    invoke-static {p1}, Landroid/support/v4/view/cj;->g(Landroid/view/View;)V

    return-void
.end method
