.class Landroid/support/v4/widget/j;
.super Landroid/database/DataSetObserver;


# instance fields
.field final synthetic a:Landroid/support/v4/widget/g;


# direct methods
.method private constructor <init>(Landroid/support/v4/widget/g;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/j;->a:Landroid/support/v4/widget/g;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/widget/g;Landroid/support/v4/widget/h;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/widget/j;-><init>(Landroid/support/v4/widget/g;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/j;->a:Landroid/support/v4/widget/g;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/widget/g;->a:Z

    iget-object v0, p0, Landroid/support/v4/widget/j;->a:Landroid/support/v4/widget/g;

    invoke-virtual {v0}, Landroid/support/v4/widget/g;->notifyDataSetChanged()V

    return-void
.end method

.method public onInvalidated()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/j;->a:Landroid/support/v4/widget/g;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/widget/g;->a:Z

    iget-object v0, p0, Landroid/support/v4/widget/j;->a:Landroid/support/v4/widget/g;

    invoke-virtual {v0}, Landroid/support/v4/widget/g;->notifyDataSetInvalidated()V

    return-void
.end method
